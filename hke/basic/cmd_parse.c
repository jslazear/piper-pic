#include <p30f5011.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "basic.h"

#define DELIM " ,\t\r\n"			// delimeters
#define SPACE	0
#define WORD	1
static char *next_tok = NULL;	// Pointer to next token
static int num_args;			// Number of arguments in our current command string

// Returns the number of arguments in a string
int tok_count_args( char *p ) {
	int count = 0;
	if (*p == '\0')
		return 0;
	char state;
	if (isspace(*p)) {
		state = SPACE;
		count = 0;
	} else {
		state = WORD;
		count = 1;
	}
	for (p = &(p[1]); *p; p++) {
		if (isspace(*p) && state == WORD)
			state = SPACE;
		else if (!isspace(*p) && state == SPACE) {
			state = WORD;
			count++;
		}
		if (*p == '\r' || *p == '\n' || *p == '\0')
			break;
	}
	return count;
}

/* Parses a list of channel arguments (numbers or strings). 
 *
 * num_ch_args   -- Number of arguments in the string that indicate channels
 *
 * RETURNS: a 16 bit integer where each bit represents a channel.
 *          0 indicates no channels were set, due to invalid channels in list
 */
unsigned short parse_channel_list( int num_ch_args ) {
	// Channel mask, set all to zero by default
	unsigned short ch_mask = 0;

	// Loop through arguments to read channel list
	int i;
	for (i = 0; i < num_ch_args; i++) {
		if (!tok_valid_uint( 2 ) ) {
			// If not a list of channels, could be a keyword
			char *str = tok_get_str();
			if ( !strcmp ( str, "all" ) )
				ch_mask = 0xFFFF;
			else if ( !strcmp ( str, "cal" ) )
				ch_mask |= 0xF000;
			else if ( !strcmp ( str, "therm" ) )
				ch_mask |= 0x0FFF;
			else
				return 0;
		} else {
			// Parse channel, add it to channel mask
			unsigned short ch = (unsigned short) tok_get_int16();
			if ( ch >= 16 )
				return 0;
			else
				ch_mask |= (1<<ch);	
		}
	}
	
	return ch_mask;
}

/*
 * Returns the number of tokens in the current command string
 */
int tok_get_num_args( void )
{
	return num_args;
}

/*
 * Subtract 1 from num_args
 * Call this if your command string starts with a board address
 */
void tok_decrement_num_args( void )
{
	num_args--;
}

// Parses a simple command string from the user
void cmd_parse( char *str )
{
	num_args = tok_count_args( str );
    next_tok = strtok( str, DELIM );
}


char *tok_get_str( void )
{
 char *result;
 result = next_tok;					// get the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}

unsigned char tok_get_uint8( void )
{
 short result;
 result = atoi( next_tok );			// convert the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return (unsigned char)result;
}

short tok_get_int16( void )
{
 short result;
 result = atoi( next_tok );			// convert the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}

long tok_get_int32( void )
{
 long result;
 result = atol( next_tok );			// convert the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}

float tok_get_float( void )
{
 float result;
 result = atof( next_tok );			// convert the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}


/*
 * Validate the next token as a number.
 * digits - the number of allowed digits (- and . don't count)
 * neg - 0 = no negative sign allowed, 1 = neg sign allowed as first char
 * dp  - 0 = no decimal point allowed, 1 = decimal point allowed anywhere
 */
char tok_valid_num( unsigned char digits, unsigned char neg, unsigned char dp )
{
 char c, *p = next_tok;
 unsigned char i;

 if( !next_tok )    return 0;       // no next token!

 for( i=0; i<digits; ++i ) {
    c = *p++;           // get the next character

    if( c < '0' || c > '9' )  {     // Oops, not a digit!
        if( c == 0 )            // too few digits is just fine
            return 1;

        if( i == 0 && c == '-' && neg ) {       // OK, it was a negative sign
            digits++;               // allow an extra digit, because it was neg
            continue;
        }

        if( c == '.' && dp )  {     // OK, it was a decimal point
            dp = 0;                 // only allow 1 decimal point per number
            digits++;               // allow an extra digit, because we found .
            continue;
        }

        return 0;               // not a valid number!
    }
 }

 c = *p;			// get the next character
 if( c != 0 )		// should be a NUL, since we used up all allowed digits
    return 0;       // there are too many digits in the number

 return 1;			// The number is valid!
}

/*
 * Validates the next token as an unsigned integer
 * Returns 1 if next token is a valid unsigned integer with fewer than "digits" digits
 */
char tok_valid_uint( char digits )
{
 return tok_valid_num( digits, 0, 0 );
}

/*
 * Is there a next token?
 */
char tok_available( void )
{
 return (next_tok != NULL);
}


/*
 * Converts an ascii hex character to its integer equivalant
 * 'E' -> 14
 * a return value of -1 indicates an invalid hex character
 */
int parse_address( char h )
{
 if( h >= '0' && h <= '9' )
	return h - '0';
 else if( h >= 'a' && h <= 'f' )
	return h - 'a' + 10;
 else if( h >= 'A' && h <= 'F' )
	return h - 'A' + 10;
 else
	return -1;
}
