#include <p30F5011.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>

#include "basic.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & XT_PLL8);          /* XT with 8xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF);                         /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN);           /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF);                    /* Code protect disabled */

/* Global variables */
int global = 17;

void init_pic()
{
	// Set up values for GPIO pins
	PORTCbits.RC1 = 1;		// Turn on LED1

	// Set the direction for GPIO
	TRISCbits.TRISC14 = 0;	// Set the RS-485 Enable pin as an output
	TRISCbits.TRISC1 = 0;	// Set the LED as an output

	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;	

	// Oscope debug pins, set them to outputs
	TRISDbits.TRISD4 = 0;
	TRISGbits.TRISG15 = 0;

	PIN_485_ENABLE = 1;		// Enable the RS-485 driver (so we can transmit over UART)

	// Short delay to wait for power supply voltages to settle
 	__delay32( (long)(0.01 * CLOCKFREQ) );

	// Setup the UART - 115.2 kbs
	unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
	unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
	OpenUART2( U2MODEvalue, U2STAvalue, BRG_SETTING );
	ConfigIntUART2( UART_RX_INT_DIS & UART_TX_INT_DIS );	// Disable TX and RX interrups

	// Setup timer 2 and set its period to 0.5 kHz
	// timer freq = 20MHz / period
	// period = 40000 gives 0.5kHz
	OpenTimer2( T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, 40000);

	// Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
	OpenOC2( OC_IDLE_CON & OC_TIMER2_SRC & OC_CONTINUE_PULSE, 200, 1 );

	// Finally, turn on interrupts!
	ConfigIntOC2( OC_INT_ON & OC_INT_PRIOR_2 );
}


int main( void )
{
	extern int dataReady;
	extern char buffer[];

	init_pic(); 	// Setup the PIC internal peripherals

	while(1) 
	{	
		char s[32];

		cmd_gets(s, sizeof(s));		// Get a command from the user
		process_cmd(s);				// Deal with the command
	}	
}