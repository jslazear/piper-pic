#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <spi.h>
#include <stdio.h>

#include "dspid.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & XT_PLL16);          /* XT with 8xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF);                         /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN);           /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF);                    /* Code protect disabled */

/* Global variables */
unsigned char CardAddress;


void init_pic()
{
	// Set up values for GPIO pins
	PORTCbits.RC1 = 1;		// Turn on the LED

	// Set the TMUX Address lines and Enable lines as outputs
	TRISBbits.TRISB11 = 0;
	TRISBbits.TRISB12 = 0;

	// Set up the thermometry mux to point to our default thermometer
	write_tmux( 0 );

	// Set the ADC mux as outputs
	TRISBbits.TRISB8  = 0;
	TRISBbits.TRISB9  = 0;
	TRISBbits.TRISB10 = 0;

	// Set the ADC mux to be at channel 0
	write_amux( 0 );

	// Set the direction for GPIO
	TRISCbits.TRISC14 = 0;	// Set the RS-485 Enable pin as an output
	TRISCbits.TRISC1 = 0;	// Set the LED as an output

	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;	

	// Load pins for the DACs
	TRISBbits.TRISB14 = 0;	// Gain DAC load pin
	TRISBbits.TRISB15 = 0;	// Excitation (amplitude) DAC load pin
	TRISGbits.TRISG2 = 0;	// Coil amplitude DAC
	TRISDbits.TRISD10 = 0;	// Analog output DAC

    PIN_ADAC_LD = 1;		// Turn off the CHIP_SELECT for amplitude dac
    PIN_GDAC_LD = 1;		// Turn off the CHIP_SELECT for gain dac
    PIN_CDAC_LD = 1;		// Turn off the CHIP_SELECT for coil dac
    PIN_AODAC_LD = 1;		// Turn off the CHIP_SELECT for analog output dac
	
	// Oscope debug pins, set them to outputs
	TRISDbits.TRISD4 = 0;
	TRISGbits.TRISG15 = 0;

	// PIC control over AC excitation chop state
	TRISDbits.TRISD8  = 0;	// PIC lead to control AC excitation chop state
	PIN_CHOP_CLK_PIC = 1;

	// ADC convert pin
	TRISDbits.TRISD1 = 0;	

	// Short delay to wait for power supply voltages to settle
 	__delay32( (long)(RESET_DELAY_SECONDS * CLOCKFREQ) );

	// Setup the SPI port2
	OpenSPI2( FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON & 
				MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
				PRI_PRESCAL_4_1,
				SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR );
	ConfigIntSPI2( SPI_INT_DIS );	// NO SPI interrupts

	// Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
	SPI2CONbits.CKE = 1;	// CKE=1 (see above)
	SPI2CONbits.CKP = 0;	// CKP=0 means SCLK is active high
	SPI2CONbits.SMP = 1;	// If the SMP bit is set, then the input sampling is done at the end of the bit output

	// ---SPI1--- CDAC
	// Setup the SPI port1 -- CDAC is on SPI1
	OpenSPI1( FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_OFF & 
				MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
				PRI_PRESCAL_4_1,
				SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR );
	ConfigIntSPI1( SPI_INT_DIS );	// NO SPI interrupts
	// Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
	// CKE=1, CKP=1 means that falling edges of SCLK occur in the middle of the data bit.
	// The AD5063 DAC which is our CDAC, samples the data on falling SCLK edges
	SPI1CONbits.CKE = 1;
	SPI1CONbits.CKP = 1;


	// Init all the DACS on the board to sensible initial values
	write_amp_dac( 6400 );		// 100 nA default excitation
	write_gain_dac( 8192 );		// Gain = 8 default gain
	write_cdac( 0 );            // Set to 0 volts output
	write_aout_dac( 0, 0 );     // Set analog outputs to 0 volts
	write_aout_dac( 1, 0 );
	write_aout_dac( 2, 0 );
	write_aout_dac( 3, 0 );

    // Setup the UART with our standard backplane settings
    config_uart2();

   	// Read in our card address and store in global variable
	CardAddress = get_addr();

	// Before we start the interrupt handler, wait for a rising edge on external frame clock
	// This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
	sync_to_frame_clock();

    // Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
    // Setup timer 2 and set its period (exact period depends on configuration, but it will be roughly a few KHZ)
    // turn on ~4 kHz interrupt handler
    config_timer2();
}


int main( void )
{
 // Setup the PIC internal peripherals
 init_pic();

 // Setup our data frame buffers
 init_frame_buf_backplane( 'D', CardAddress );	// Board type is 'D'SPID

 while(1) {
	char s[32];

	cmd_gets(s, sizeof(s));		// Get a command from the user
	process_cmd(s);				// Deal with the command
 }
}
