#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <stdlib.h>

#include "dspid.h"

static void Update_PID_sramp();

/*
 * Set which thermometer to read
 */
void write_tmux( unsigned short ch )
{
	if (ch >= 4)
		return;
	
	// Set address bits
	if( ch & 0b0001 )			//set LSB of address
		PIN_TMUX_A0 = 1;
	else
		PIN_TMUX_A0 = 0;
	
	if( ch & 0b0010 )			//set middle bit of address
		PIN_TMUX_A1 = 1;
	else
		PIN_TMUX_A1 = 0;
	
}

void write_amux( unsigned short ch )
{
	if (ch >= 8)
		return;
	
	// Set address bits
	if( ch & 0b0001 )			//set LSB of address
		PIN_ADC_MUX_A0 = 1;
	else
		PIN_ADC_MUX_A0 = 0;
	
	if( ch & 0b0010 )			//set middle bit of address
		PIN_ADC_MUX_A1 = 1;
	else
		PIN_ADC_MUX_A1 = 0;
	
	if( ch & 0b0100 )			//set MSB of address
		PIN_ADC_MUX_A2 = 1;
	else
		PIN_ADC_MUX_A2 = 0;
	
}

/*
 * Convert the most recent demod value into resistance in ohms.
 */
float calc_r_val( long data )
{
	// delta is the difference in adc value when chop is high and low (demodulated)
	// delta = demod value, averaged over 200 readings
	float delta = data / ((float)SUM_TICKS);
	unsigned int adac = read_amp_dac();
	unsigned int gdac = read_gain_dac();
	if (gdac == 0)
		return 0.0;
	float ratio = (float)adac / (float)gdac;

    // Fixed bridge resistance, R, is 201.1 k ohms
    // Preamp Gain = 100.0, 4.99 is gain of ADC scaling network
	float r = 201.1e3*delta/( 100.0*65536.0/4.99*ratio*4/R42_DIVIDE_FACTOR - delta );
	return r;
}

/*
 * Convert a resistance in ohms into a demod value.
 */
long calc_demod_from_r( long r )
{
	unsigned int adac = read_amp_dac();
	unsigned int gdac = read_gain_dac();
	if (gdac == 0 || r == 0)
		return 0;
	float ratio = (float)adac / (float)gdac;
	
	float delta = r * (100*65536.0*ratio*4 / (4.99*R42_DIVIDE_FACTOR))/(201.1*1000+r);
	return (long)(delta*200);
}


unsigned char get_addr(void)
{
	unsigned char addr = (PIN_ADDR3 << 3) | (PIN_ADDR2 << 2) | (PIN_ADDR1 << 1) | PIN_ADDR0;
	return addr;
}

void set_ina( unsigned short ina )
{
	unsigned short adac = INA_TO_ADAC(ina);
	write_amp_dac( adac );
}

/** 
 * Sets gain so that delta is at 50% of full amplitude, if it is <20% or >80%
 */
void auto_set_gain( void ) {
	unsigned long demod = read_demod();
	unsigned long delta = demod/200;
	unsigned short gain = read_gain_dac();
	// If delta is < 20% or > 80% of full amplitude, set gain such that delta is 50%
	if ( delta < 13107 ) {
		unsigned long dg = delta * gain;
		unsigned long g_new = dg/32768;
		write_gain_dac( (unsigned short) g_new );
	} else if ( delta > 52429 ) {
		unsigned long ratio = delta / 32768;
		unsigned short g_new = gain * ((short) ratio);
		write_gain_dac( g_new );
	}
}



// Updates counters and prints data when necessary
void adc_noise_update( int skip, long int num, char format )
{
	/*
	 short data = read_adc();
	 // Print out data in correct format
	 char str[10];	// Longest string is 65535\r\n\0 is 8 characters
	 switch (format) {
	 case 'd':					// Print number in Decimal, takes about 3 ms
	 sprintf(str, "%i\r\n", data); 
	 TTYPutString( str );
	 break;
	 case 'x':
	 TTYPrintHex16(data);	// Print number in Hex, takes about 450 us
	 break;
	 case 'b':					// Print number in binary, takes about 90 us
	 while( BusyUART2() );	// Wait till not busy
	 putcUART2(data);
	 while( BusyUART2() );	// Wait till not busy
	 putcUART2(data>>8);
	 break;
	 default:
	 break;
	 }
	 */
}

/** Prints out sample of ADC data
 *  skip    -> Number of samples to skip between reported data points
 *  num     -> Total number of samples to print
 *  format  -> Format of the data (decimal, hex, or binary)
 */
void adc_noise_test( int skip, long int num, char format )
{
	long data_counter = 0;
	
	while (data_counter < num) {
		
		// Check to see if we should print data
		if (get_sample_count() >= skip )  {
			zero_sample_count();
			data_counter++;
			
			// If new sample is ready, update counters and print if appropriate
			adc_noise_update(skip, num, format);
		}
	}
	zero_sample_count();
	data_counter = 0;
}

/*
 * Used by int. handler to signal OnIdle() to do tasks as 1 Hz 
 */
static volatile char flag_1hz_events = 0;
void Do_1Hz_Events()
{
	flag_1hz_events = 1;
}


/*
 * Called by cmd_gets() when it is just waiting for a user to hit a key.
 *
 * Use this for things that need to happen often, but are too much calculation
 * to do in the interrupt handler.
 */
void OnIdle( void )
{
 static unsigned adac_last_setting = 0;
 static unsigned gdac_last_setting = 0;

 unsigned adac = read_amp_dac();
 unsigned gdac = read_gain_dac();

 // If we've changed the ADAC or GDAC setting, recompute the PID setpoint
 // in demod units, based on the stored setpoint value in Ohms
 // I don't want to do this calculation in the int_handler() because it uses floats
 // and is probably very slow.
 if( adac != adac_last_setting || gdac != gdac_last_setting )  {
	adac_last_setting = adac;
	gdac_last_setting = gdac;

	pid_recompute_setpoint();
 }

 if( flag_1hz_events )  {
	flag_1hz_events = 0;

	// Update the PID sramp.  Done in OnIdle rather than int handler since it involves floats.
	// and may be very slow.  By doing it here, it just needs to finish in 1/8 second, 
	// to be done before next PID update.  But doesn't need to finish in 1/4000 second
	// which it would in the int_handler
	Update_PID_sramp();
 }
}

static volatile long sramp_target = 0;	// Target resistance in OHMS units for PID setpoint ramping
static volatile long sramp_delta = 0;	// Resistance step in OHMS for setpoint ramping (added to PID setpoint at 1Hz)
										// If sramp_delta == 0, then sramping is OFF

// Ramp the PID setpoint slowling for calibratint thermometers
void Enable_PID_sramp( long R_final, long delta_R )
{
	// Command only works if PID controlling already
	if( !is_pid_on() )
		return;

	sramp_target = R_final;
	sramp_delta  = delta_R;

	long pid_s = pid_get_s();

	// Set sign of sramp_delta by taking abs, then setting negative if needed
	sramp_delta = abs( sramp_delta );
	if( sramp_target < pid_s ) // ramping down
		sramp_delta = -sramp_delta;	 // force sign to be negative
}

void Disable_PID_sramp()
{
	sramp_delta = 0;
	sramp_target = 20000;
}

// Print PID Setpoint ramp status.  Pass in str buffer just
// to save allocating a buffer
void Print_PID_sramp_status( char *str )
{
	// Setpoint ramping
	if( sramp_delta != 0 )  {
		TTYPuts("Setpoint ramping ON");
		sprintf(str, "   target: %ld Ohms", sramp_target );
		TTYPuts(str);
		sprintf(str, "  delta: %ld Ohms.\r\n", sramp_delta );
		TTYPuts(str);
	}
}

// Call this function at 1 Hz to update the PID sramp
// Note: do not call from interrupt handler, call from OnIdle instead.
// this function does floating point math
static void Update_PID_sramp()
{
	if( !is_pid_on() || sramp_delta == 0 )
		return;

	long pid_s = pid_get_s();
	pid_s += sramp_delta;

	// Change the setpoint (note, this step involves floating point math)
	pid_set_s( pid_s );

	// See if we are done setpoint ramping
	if( sramp_delta > 0 && pid_s >= sramp_target )
		Disable_PID_sramp();
	if( sramp_delta < 0 && pid_s <= sramp_target )
		Disable_PID_sramp();	
}
