#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "dspid.h"
#include "version.h"        // Computer-generated version string (includes compile time and user name)


// Local function prototypes
static char do_cmd_led(void); // turn LED on or off
static char do_cmd_adac(void); // set thermometer excitation amplitude DAC
static char do_cmd_gdac(void); // set thermometer gain DAC
static char do_cmd_gain(void); // set thermometer gain
static char do_cmd_tmux(void); // set which thermometer to read
static char do_cmd_cdac(void); // sets the coil dac
static char do_cmd_cmv(void); // sets the coil voltage in millivolts
static char do_cmd_aodac(void); // sets the analog output dacs
static char do_cmd_aomv(void); // sets the analog output voltage in millivolts
static char do_cmd_aoramp(void); // ramps the analog output voltage in millivolts
static char do_cmd_adc(void); // prints the most recent adc value
static char do_cmd_demod(void); // read the demodulated thermometer signal
static char do_cmd_iramp(void); // ramps the coil current
static char do_cmd_ihold(void); // holds the coil current
static char do_cmd_iholdi(void); // sets the ihold I term coefficient
static char do_cmd_roffset(void); // sets the value of the stray resistance between coil and Vmon (0 for shiny)
static char do_cmd_sramp(void); // ramps the PID setpoint
static char do_cmd_pid(void); // Sets up PID control loop
static char do_cmd_chop(void); // set the thermometer excitation low/high/auto
static char do_cmd_r(void); // calculate resistance
static char do_cmd_addr(void); // read hex address
static char do_cmd_ina(void); // set current in nA
static char do_cmd_data(void); // turn data stream on or off
static char do_cmd_p(void); // prints dac status
static char do_cmd_echo(void); // changes echo on / echo off
static char do_cmd_version(void); // prints out our software version

#define CMP	!strcmp

/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */
void process_cmd(char *cmd_string)
{
    char *s;
    unsigned char r;

    // Parse command string and check address to see if it's for us
    s = bp_parse_cmd_string(cmd_string);
    if( s == NULL )     // Command is not for us
        return;

    if (CMP(s, "led")) r = do_cmd_led(); // set the LED
    else if (CMP(s, "adac")) r = do_cmd_adac(); // set the amplitude DAC
    else if (CMP(s, "gdac")) r = do_cmd_gdac(); // set the gain DAC
    else if (CMP(s, "aodac")) r = do_cmd_aodac(); // set the analog output DAC
    else if (CMP(s, "aomv")) r = do_cmd_aomv(); // set the voltage on that analog output DAC in millivolts
    else if (CMP(s, "aoramp")) r = do_cmd_aoramp(); // ramp the analog output DAC
    else if (CMP(s, "gain")) r = do_cmd_gain(); // set the gain
    else if (CMP(s, "tmux")) r = do_cmd_tmux(); // set which thermometer to read
    else if (CMP(s, "adc")) r = do_cmd_adc(); // print out most recent adc value
    else if (CMP(s, "iramp")) r = do_cmd_iramp(); // ramps the coil current
    else if (CMP(s, "ihold")) r = do_cmd_ihold(); // holds the coil current
    else if (CMP(s, "iholdi")) r = do_cmd_iholdi(); // sets the ihold i term value
    else if (CMP(s, "roffset"))r = do_cmd_roffset(); // sets the value of the stray resistance between coil and Vmon (0 for shiny)
    else if (CMP(s, "sramp")) r = do_cmd_sramp(); // ramps the PID setpoint
    else if (CMP(s, "pid")) r = do_cmd_pid(); // sets up PID control loop
    else if (CMP(s, "cdac")) r = do_cmd_cdac(); // sets the coil dac
    else if (CMP(s, "d")) r = do_cmd_demod(); // read the demodulated value
    else if (CMP(s, "cmv")) r = do_cmd_cmv(); // sets the coil voltage in millivolts
    else if (CMP(s, "chop")) r = do_cmd_chop(); // set chopper state
    else if (CMP(s, "r")) r = do_cmd_r(); // read resistance
    else if (CMP(s, "addr")) r = do_cmd_addr(); // read in hex address
    else if (CMP(s, "ina")) r = do_cmd_ina(); // set current in nA
    else if (CMP(s, "data")) r = do_cmd_data(); // turn data stream on or off
    else if (CMP(s, "p")) r = do_cmd_p(); // prints dac status
    else if (CMP(s, "echo")) r = do_cmd_echo(); // Sets echo on or off
    else if (CMP(s, "ver")) r = do_cmd_version(); // prints software version
    else r = STATUS_FAIL_CMD; // User typed unknown command

    print_status(r);
}

/** INA
 *	Sets current through the ADAC in nanoamps.
 *
 *	usage:
 *	ina 1000	-> Sets current to 1000 nA
 */
static char do_cmd_ina(void)
{
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_uint(4))
        return STATUS_FAIL_INVALID;
    unsigned short ina = (unsigned short) tok_get_int16();
    set_ina(ina);

    return STATUS_OK;
}

/** GAIN
 *	Set 2nd stage amplifier gain to 1, 2, 4, 8, ... 128.  Sets GDAC appropriately.
 *  The 1st stage gain is fixed at 100.
 *
 *	usage:
 *	gain 2	-> Sets gain to 2, GDAC to 32768
 */
static char do_cmd_gain(void)
{
    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of gain to set
    if (!tok_valid_uint(5))
        return STATUS_FAIL_INVALID;

    unsigned short u16 = (unsigned short) tok_get_int16();
    unsigned int gdac = 0;
    switch (u16) {
    case 1: gdac = 65535;
        break;
    case 2: gdac = 32768;
        break;
    case 4: gdac = 16384;
        break;
    case 8: gdac = 8192;
        break;
    case 16: gdac = 4096;
        break;
    case 32: gdac = 2048;
        break;
    case 64: gdac = 1024;
        break;
    case 128: gdac = 512;
        break;
    default: return STATUS_FAIL_INVALID;
    }

    write_gain_dac(gdac);

    return STATUS_OK;
}


/*----------------------------------------------------------------------
 * ADR Control Commands
 *----------------------------------------------------------------------*/

/** CMV
 *	Set the coil voltage in millivolts.
 *
 *  Millivolts (-2400, 2400)
 *
 *	usage:
 *	cmv 1024	-> Sets the CDAC so that the voltage is 1024 mV
 */
static char do_cmd_cmv(void)
{
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    //	if( !tok_valid_num(4, 1, 0) )
    //		return STATUS_FAIL_INVALID;
    int u16 = tok_get_int16();
    if (u16 > 2047 || u16 < -2048)
        return STATUS_FAIL_INVALID;

    int cdac = COIL_MV_TO_DAC(u16);

    set_mode_constant_vc(cdac);

    return STATUS_OK;
}

/** IRAMP
 *	Ramps the coil current.
 *
 *	usage:
 *	iramp 5000 100	-> Ramps at 0.1 V to 5 amps
 */
static char do_cmd_iramp(void)
{
    // Get current
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(4, 1, 0))
        return STATUS_FAIL_INVALID;
    short mamps = tok_get_int16();

    // Get voltage
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(3, 1, 0))
        return STATUS_FAIL_INVALID;
    short mvolts = tok_get_int16();

    // Tell int_handler to start ramping
    set_mode_iramp(I_SENSE_MA_TO_ADC(mamps), VMON_MV_TO_ADC(mvolts));

    return STATUS_OK;
}

/** IHOLD
 *	Holds the coil current.
 *
 *	usage:
 *      ihold 5000      -> Holds at 5 amps using default V_max (0.1 V).
 *	ihold 5000 200      -> Holds at 5 amps using at most 0.2 V
 */
static char do_cmd_ihold(void)
{
    short mamps_adc = 0, mvolts = 100;  // 100 is a decent default value

    // Get current
    if (tok_available())  {
        if (!tok_valid_num(4, 1, 0))
            return STATUS_FAIL_INVALID;
        
        short mamps = tok_get_int16();
        mamps_adc = I_SENSE_MA_TO_ADC(mamps);
        
        // Get voltage
        if (tok_available())  {
            if (!tok_valid_num(3, 1, 0))
                return STATUS_FAIL_INVALID;
            mvolts = tok_get_int16();
        }
    } else {
        // Get the current current (present amperes)
        mamps_adc = get_amux_reading( AMUX_COIL_I_SENSE );
    }
    // Tell int_handler to start ramping
    set_mode_ihold(mamps_adc, VMON_MV_TO_ADC(mvolts));

    return STATUS_OK;
}

/** IHOLDI
 *	Sets the PID I parameter for the IHold command.
 *
 *	usage:
 *      iholdi i
 *      where i=[0,32] (0 is no gain and 32 is max gain)
 *  example
 *      iholdi 24
 */
static char do_cmd_iholdi(void)
{
    // Get current
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(2, 1, 0))
        return STATUS_FAIL_INVALID;
    short i = tok_get_int16();

    ihold_set_ishift(32 - i);

    return STATUS_OK;
}

/** PID
 *  Configures the DSPID PID controller.
 *  
 *	USAGE:
 *   pid s RES - sets the setpoint resistance in ohms
 *   pid p P - sets the proportional gain to P [0-255]
 *   pid i I - sets the integral gain to I [0-255]
 *   pid d D - sets the derivative gain to D [0-255]
 *   pid p_shift PS - sets the proportional gain right-shift [0-255]
 *   pid i_shift IS - sets the integral gain right-shift [0-255]
 *   pid d_shift DS - sets the derivative gain right-shift [0-255]
 *   pid vmax v - sets the max coil voltage in mV [0-3000]
 *   pid imax v - sets the max coil voltage in mV [0-20000]
 *   pid on  - puts the board in PID control mode with the current P,I and S parameters
 *   pid off - turns off PID control, issues a CMV 0 command (ramps down coil)
 *   pid reset - resets the accumulator (sets the integral term to 0)
 *
 * TYPICAL USAGE SEQUENCE:
 *   pid s 9873  - sets pid setpoint to 9873 ohms
 *   pid p 32
 *   pid i 16
 *   pid on
 */
static char do_cmd_pid(void)
{
    char *arg;
    long num;
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();

    // Check if it is a simple string command
    if (CMP(arg, "on")) {
        pid_recompute_setpoint(); // recalc setpoint in hardware units based on current settings
        set_mode_pid();
        return STATUS_OK;
    }
    else if (CMP(arg, "off")) {
        // turn PID off, set cdac to 0 volts
        // TODO: Leave CMV what it was
        set_mode_constant_vc(0);
        return STATUS_OK;
    }
    else if (CMP(arg, "reset")) {
        pid_reset_integrator();
        return STATUS_OK;
    }

    // See if the next token is a number
    if (!tok_valid_uint(5))
        return STATUS_FAIL_INVALID;

    num = (long) tok_get_int32();

    if (CMP(arg, "vmax")) {
        if( num < 0 || num > 3000)
            return STATUS_FAIL_RANGE;
        short coil_vmax_counts = VMON_MV_TO_ADC(num);
        pid_set_vlim(coil_vmax_counts);
        return STATUS_OK;
    }
    else if (CMP(arg, "imax")) {
        if( num < 0 || num > 20000)
            return STATUS_FAIL_RANGE;
        short coil_imax_counts = I_SENSE_MA_TO_ADC(num);
        pid_set_imax(coil_imax_counts);
        return STATUS_OK;
    }
    else if(CMP(arg, "s") ) {
        pid_set_s(num);
    }
    else if(CMP(arg, "p") ) {
        if( num < 0 || num > 255)
            return STATUS_FAIL_RANGE;
        pid_set_p((unsigned char) num);
    }
    else if(CMP(arg, "i") ) {
        if( num < 0 || num > 255)
            return STATUS_FAIL_RANGE;
        pid_set_i((unsigned char) num);
    }
    else if(CMP(arg, "d") ) {
        if( num < 0 || num > 255)
            return STATUS_FAIL_RANGE;
        pid_set_d((unsigned char) num);
    }
    else if(CMP(arg, "p_shift") ) {
        if( num < 0 || num > 255)
            return STATUS_FAIL_RANGE;
        pid_set_p_shift((unsigned char) num);
    }
    else if(CMP(arg, "i_shift") ) {
        if( num < 0 || num > 255)
            return STATUS_FAIL_RANGE;
        pid_set_i_shift((unsigned char) num);
    }
    else if(CMP(arg, "d_shift") ) {
        if( num < 0 || num > 255)
            return STATUS_FAIL_RANGE;
        pid_set_d_shift((unsigned char) num);
    }
    else {
        return STATUS_FAIL_INVALID;
    }

    return STATUS_OK;
}

/** SRAMP
 *	Ramps the PID setpoint from its present value to the specified target value.
 *  The setpoint is incremented at 1 Hz with the specified delta value.
 *  This command is used to "sweep" the stage temperature for calibrating thermometers
 *  or making R of T measurements.  However, due to the non-linear cal curve of 
 *  thermometers, the "iramp" command may be a better choice.
 *
 *	usage:
 *	sramp 3000 10	-> Ramps setpoint to 3000 ohms in steps of 10 Ohms / second
 */
static char do_cmd_sramp(void)
{
    // Get destination in Ohms
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(7, 0, 0)) // Allow up to 7 digit number (9,999,999 is max)
        return STATUS_FAIL_INVALID;
    long R_final = tok_get_int32();

    // Get delta step size in Ohms
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(4, 0, 0))
        return STATUS_FAIL_INVALID;
    short delta_R = tok_get_int16();

    // Ensure that we are PID controlling already
    if (!is_pid_on()) {
        TTYPuts("You must be PID controlling already to use the sramp command.");
        return STATUS_FAIL_INVALID;
    }

    // Tell int_handler to start ramping PID setpoint
    Enable_PID_sramp(R_final, delta_R);

    return STATUS_OK;
}

/** ROFFSET
 *	Sets the value of the resistance between the coil and the Voltage monitor
 *  roughly 0 for Shiny, 16 mOhms for CRAAC
 *
 *	usage:
 *	roffset 16.5 -- sets resistance offset to 16.5 milli ohms
 */
static char do_cmd_roffset(void)
{
    // Get the offset resistance
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(8, 1, 1))
        return STATUS_FAIL_INVALID;
    float milliOhms = tok_get_float();

    // Convert from milliOhms to an int in units of 1/65536.0 of an ohm
    float fracOhms = milliOhms / 1000.0 * 65536.0;

    // Range check
    if (fracOhms > 32000 || fracOhms < -32000)
        return STATUS_FAIL_RANGE;

    // Convert to a signed int and print out
    int roffset = (int) fracOhms;
    char buf[64];
    sprintf(buf, "Roffset set to %d\r\n", roffset);
    TTYPuts(buf);

    // Tell int_handler
    set_roffset(roffset);

    return STATUS_OK;
}



/*----------------------------------------------------------------------
 * Low Level Commands
 *----------------------------------------------------------------------*/

/** ADC
 *	Report current ADC value.
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_adc(void)
{
    char str[16];
    unsigned short data;

    data = read_adc();

    sprintf(str, "%u\r\n", data);
    TTYPuts(str);
    return STATUS_OK;
}

/** ADAC
 *	Set thermometer excitation amplitude DAC.
 *  This is a low-level command for debugging and testing.
 *  In most cases, you should use the "ina" command to set the excitation in nano amps.
 *
 *	usage:
 *	adac 1024 -> Sets ADAC to 1024
 */
static char do_cmd_adac(void)
{

    // Get value of ADAC to set
    if (!tok_valid_uint(5))
        return STATUS_FAIL_INVALID;
    unsigned int u16 = (unsigned int) tok_get_int16();

    write_amp_dac(u16);

    return STATUS_OK;
}

/** GDAC
 *	Set amplifier gain DAC.
 *  This is a low-level command.  In most cases, you should use the "gain" command.
 *
 *	usage:
 *	gdac 1024	-> Sets the GDAC to 1024
 */
static char do_cmd_gdac(void)
{
    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of GDAC to set
    if (!tok_valid_uint(5))
        return STATUS_FAIL_INVALID;
    unsigned int u16 = (unsigned int) tok_get_int16();

    write_gain_dac(u16);

    return STATUS_OK;
}

/** LED
 *	Set LED light.
 *
 *	usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led(void)
{
    char *arg;

    // Check that the next token is a valid number
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    arg = tok_get_str();
    if (CMP(arg, "1")) {
        PIN_CHOP_CLK_PIC = 1;
        PIN_LED = 1;
    }
    else if (CMP(arg, "0")) {
        PIN_CHOP_CLK_PIC = 0;
        PIN_LED = 0;
    }
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** TMUX
 *	Set TMUX channel (default is 0).
 *  Channels 0 and 1 can be connected to external thermometers.
 *  Channels 2 and 3 are connected to internal calibration resistors.
 *  The input multiplexor for DSPID (unlike TRead) always stays fixed on the channel you
 *  specify with this command.
 *
 *	USAGE:
 *   tmux CH
 *   CH is channel number (0-3)
 *  EXAMPLE:
 *	 tmux 2		-> Sets tmux to channel 2
 */
static char do_cmd_tmux(void)
{
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    unsigned short u16 = (unsigned short) tok_get_int16();
    if (u16 > 3)
        return STATUS_FAIL_INVALID;
    write_tmux(u16);

    return STATUS_OK;
}

/** CDAC
 *	Sets the coil dac.
 *
 *	usage:
 *	cdac 1024	-> Sets the coil DAC to 1024
 */
static char do_cmd_cdac(void)
{
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int u16 = tok_get_int16();

    // set_mode_constant_vc( cdac ); ???
    write_cdac(u16);

    return STATUS_OK;
}

/** D
 *	Prints demod value.
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_demod(void)
{
    long data = read_demod();
    char str[16];
    sprintf(str, "%li\r\n", data);
    TTYPuts(str);
    return STATUS_OK;
}

/** CHOP
 *	Sets chopper state.
 *
 *	usage:
 *	chop low	-> Sets chop to low
 *	chop high	-> Sets chop to high
 *	chop auto	-> Sets chop to auto
 */
static char do_cmd_chop(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    arg = tok_get_str();
    if (CMP(arg, "low"))
        set_chop(0);
    else if (CMP(arg, "high"))
        set_chop(1);
    else if (CMP(arg, "auto"))
        set_chop(2);
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** R
 *	Calculates resistance value.
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_r(void)
{

    float r = calc_r_val(read_demod());
    char str[20]; // 0: 1007.431519\r\n\0 is 17 characters
    sprintf(str, "%f\r\n", r);
    TTYPuts(str);

    return STATUS_OK;
}

/*----------------------------------------------------------------------
 * Analog Output Commands
 *----------------------------------------------------------------------*/

/** AOMV
 *	Sets the analog output voltage in millivolts.
 *
 *	usage:
 *	aomv 0 100 -> Sets channel 0 of the analog output voltage to 100 millivolts
 */
static char do_cmd_aomv(void)
{
    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-3)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    unsigned char ch = (unsigned char) tok_get_uint8();
    if (ch > 3)
        return STATUS_FAIL_INVALID;

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get voltage to set, should be a 5 digit integer, possibly negative
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_RANGE;

    int mv = (int) tok_get_int16();
    // Must be between +/- 10240 mV.
    if (mv < -10240)
        mv = -10240;
    if (mv > 10240)
        mv = 10240;

    unsigned int dac = 0;

    // Convert from millivolts to DAC
    // mv = 5*DAC
    dac = AO_MV_TO_DAC(mv);
    write_aout_dac(ch, dac);
    return STATUS_OK;
}

/** AORAMP
 *	Ramps the analog output voltage in millivolts.
 *
 *  usage:
 *  aoramp <ch> <target_mv> <seconds>
 *
 *	example:
 *	aoramp 0 100 10	-> Ramps channel 0 of the analog output voltage to 100 millivolts over 10 seconds
 */
static char do_cmd_aoramp(void)
{
    char buf[80];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-3)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    unsigned char ch = (unsigned char) tok_get_uint8();
    if (ch > 3)
        return STATUS_FAIL_INVALID;

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get voltage to set, should be a 5 digit integer, possibly negative
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_RANGE;

    int mv = (int) tok_get_int16();
    // Must be between +/- 10240 mV.
    if (mv < -10240)
        mv = -10240;
    if (mv > 10240)
        mv = 10240;

    // Convert from millivolts to DAC (mv = 5*DAC)
    short target_dac = AO_MV_TO_DAC(mv);
    short current_dac = read_aout_dac(ch);

    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(6, 0, 0))
        return STATUS_FAIL_RANGE;

    int seconds = tok_get_int16();

    // Calculate the delta dac value to add at each demod_frame
    int sign;
    if (target_dac > current_dac)
        sign = 1;
    else
        sign = -1;
    float step = ((float)(target_dac - current_dac)) / ((float)(DEMODS_PER_FRAME * seconds));
    short delta = (short)(step + 0.5);  // round to the nearest integer
    if( delta == 0)
        delta = sign;

    sprintf(buf, "aoramp target_dac=%d  delta=%d\r\n", target_dac, delta);
    TTYPuts(buf);

    set_aoramp(ch, target_dac, delta);

    return STATUS_OK;
}

/** AODAC
 *	Set the analog output DAC.  This is a 4 channel, 12 bit 
 *
 *	USAGE:
 *   aodac CH DAC
 *   CH is channel number (0-3)
 *   DAC is dac setting (-2048 to 2047)
 *  EXAMPLE:
 *   aodac 0 1024 -> Sets channel 0 of the analog output dac to 1024
 */
static char do_cmd_aodac(void)
{
    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-3)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    unsigned char ch = (unsigned char) tok_get_uint8();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of AODAC to set
    if (!tok_valid_num(4, 1, 0))
        return STATUS_FAIL_INVALID;
    int u16 = tok_get_int16();

    write_aout_dac(ch, u16);

    return STATUS_OK;
}



/*----------------------------------------------------------------------
 * Standard Commands
 *----------------------------------------------------------------------*/

/** ADDR
 *	Prints hex address of the board.
 *
 *	usage:	
 *	No arguments
 */
static char do_cmd_addr(void)
{
    unsigned char addr = get_addr();
    char str[16];
    sprintf(str, "%x\r\n", addr);
    TTYPuts(str);
    return STATUS_OK;
}


/** ECHO
 *	Sets echo on or off.
 *
 *	usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if (CMP(arg, "on"))
        cmd_set_echo(1);
    else if (CMP(arg, "off"))
        cmd_set_echo(0);
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;

}

/** VER
 *	Spits out a version string for the current code.
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_version(void)
{
    TTYPuts("DSPID  ");

    bp_print_version_info(VERSION_STRING);

    return STATUS_OK;
}

/** DATA
 *	Turns on/off data stream.
 *
 *	usage:
 *	data on		-> Turns data stream on
 *	data off	-> Turns data stream off
 */
static char do_cmd_data(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if (CMP(arg, "on"))
        set_data_on(1); // turn data stream on
    else if (CMP(arg, "off"))
        set_data_on(0); // turn data stream off
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** P
 *	Prints a table of data: GDAC, ADAC, Demod, Delta, InA, r, ADC MUX Channels.
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_p(void)
{
    unsigned short gdac, adac;
    long demod;
    unsigned ina, delta;
    float r;
    int i;
    char str[100]; // max string len is about 51 chars, so 100 is very safe
    char *header = "\n   Gain    ADAC       Demod      Delta        I-nA     r\r\n";
    TTYPuts(header);

    gdac = read_gain_dac();
    unsigned char gain = 1;
    switch (gdac) {
    case 65535: gain = 1;
        break;
    case 32768: gain = 2;
        break;
    case 16384: gain = 4;
        break;
    case 8192: gain = 8;
        break;
    case 4096: gain = 16;
        break;
    case 2048: gain = 32;
        break;
    case 1024: gain = 64;
        break;
    case 512: gain = 128;
        break;
    default: gain = 0;
    }
    adac = read_amp_dac();
    demod = read_demod();
    r = calc_r_val(demod);

    ina = ADAC_TO_INA(adac);

    delta = (demod / 200);
    sprintf(str, "%6u%8u%12lu%12u%12u%10.3f\r\n", gain, adac, demod, delta, ina, r);
    TTYPuts(str);


    // Print out ADC values and voltages, and temp where appropriate
    TTYPuts("\r\n");
    TTYPuts("Device       ADC         mV          K\r\n");
    //	sprintf(str, "RUOX      %7d    %9d\r\n", get_amux_reading(0), ADC_TO_MV(get_amux_reading(0)));
    //	TTYPuts(str);

    short val;
    float kelvin;
    // Coil voltage monitor
    val = get_amux_reading(AMUX_COIL_V_MON);
    sprintf(str, "COIL_V    %7d    %9d\r\n", val, VMON_ADC_TO_MV(val));
    TTYPuts(str);

    // Coil current sense
    val = get_amux_reading(AMUX_COIL_I_SENSE);
    sprintf(str, "COIL_I    %7d    %9d\r\n", val, ADC_TO_MV(val));
    TTYPuts(str);

    // Analog input (spare)
    val = get_amux_reading(AMUX_ANALOG_IN);
    sprintf(str, "ANALOG_IN %7d    %9d\r\n", val, ADC_TO_MV(val));
    TTYPuts(str);

    // External temperature (OPA549 on hc_boost)
    val = get_amux_reading(AMUX_EXT_TEMP);
    kelvin = ADC_TO_KELVIN(val);
    sprintf(str, "EXT_TEMP  %7d    %9d    %f\r\n", val, ADC_TO_MV(val), kelvin);
    TTYPuts(str);

    // Ambiant temperature
    val = get_amux_reading(AMUX_BOARD_TEMP);
    kelvin = ADC_TO_KELVIN(val);
    sprintf(str, "BRD_TEMP  %7d    %9d    %f\r\n", val, ADC_TO_MV(val), kelvin);
    TTYPuts(str);

    // +15 supply rail for DSPID board
    // Multiply by 4 to get true supply voltage
    val = get_amux_reading(AMUX_V_SUPPLY);
    sprintf(str, "V_SUPPLY  %7d    %9d\r\n", val, ADC_TO_MV(val) << 2);
    TTYPuts(str);

    // GND supply rail for DSPID board
    val = get_amux_reading(AMUX_GND);
    sprintf(str, "GND       %7d    %9d\r\n", val, ADC_TO_MV(val));
    TTYPuts(str);

    TTYPuts("Analog Out: ");        
    for( i=0; i<4; ++i ) {
        val = read_aout_dac(i);
        sprintf(str, "ch %d = %d   ", i, AO_DAC_TO_MV(val));
        TTYPuts(str);        
    }
    TTYPuts("\r\n");

    TTYPuts("PID Parameters\r\n");
    sprintf(str, "   P: %3i\r\n", pid_get_p());
    TTYPuts(str);
    sprintf(str, "   I: %3i\r\n", pid_get_i());
    TTYPuts(str);
    sprintf(str, "   D: %3i\r\n", pid_get_d());
    TTYPuts(str);
    sprintf(str, "   P >>: %3i\r\n", pid_get_p_shift());
    TTYPuts(str);
    sprintf(str, "   I >>: %3i\r\n", pid_get_i_shift());
    TTYPuts(str);
    sprintf(str, "   D >>: %3i\r\n", pid_get_d_shift());
    TTYPuts(str);
    // print the R value of the PID setpoint
    sprintf(str, "   S: %ld Ohms\r\n", pid_get_s());
    TTYPuts(str);

    // PID V and I Limits
    val = pid_get_vlim();
    sprintf(str, "   VMAX: %d\r\n", VMON_ADC_TO_MV(val));
    TTYPuts(str);
    val = pid_get_imax();
    sprintf(str, "   IMAX: %d\r\n", I_SENSE_ADC_TO_MA(val));
    TTYPuts(str);

    // PID term values
    sprintf(str, "   PTERM: %ld   ITERM: %ld   DTERM:   %ld\r\n", pid_get_pterm(), pid_get_iterm(), pid_get_dterm());
    TTYPuts(str);

    // Print stuff for PID controller or IRAMP status
    TTYPuts("MODE\r\n");
    print_int_handler_status();

    return STATUS_OK;
}
