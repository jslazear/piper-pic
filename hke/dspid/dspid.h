#ifndef __DSPID_INCLUDED
#define __DSPID_INCLUDED

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

/*     ------------------------                        |------------
       |                      |                        |
       |                      |                        |
 CHOP  |                      |                        |
       |                      |                        |
       |                      |------------------------|

     +1     -------------------                             |------
            |                 |                             |
 Demod      |                 |                             |
      0 -----                 |-----                   |-----
                                   |                   |
     -1                            |-------------------|

       |----|-----------------|----|-------------------|----|
       0    A                 B    C                   D

        A = N_Gap
        B = N_Gap+N_Sum
        C = 2*N_Gap + N_Sum
        D = N_Period  (2*N_Gap + 2*N_Sum)
*/

// Constants used for timing/demodulation. Refer to timing diagram above.
#define DEMODS_PER_FRAME		8       // Number of thermometer readings in 1 frame

#ifdef PIPER
    #define GAP_TICKS			52											// Number of ticks between 0 and A on diagram
    #define SUM_TICKS			198											// Number of ticks between A and B
#endif

#ifdef BETTII
    #define GAP_TICKS			94											// Number of ticks between 0 and A on diagram
    #define SUM_TICKS			162											// Number of ticks between A and B
#endif

#define CHOP_TICKS		(SUM_TICKS + GAP_TICKS)	// Number of ticks between 0 and B
#define DEMOD_PERIOD		(2*CHOP_TICKS)		// Number of ticks between 0 and D

// Sanity checks on the timing constants, may catch some dumb errors
#if (DEMOD_PERIOD*DEMODS_PER_FRAME) != TICKS_PER_FRAME
    #error "Invalid timing constant definitions"
#endif
#if (GAP_TICKS < 16)    // Need enough gap ticks to measure the housekeeping data
    #error "GAP_TICKS must be >= 16 for reading in housekeeping channels, see int_handler.c update_spi_and_sum()"
#endif
#if (GAP_TICKS*T_HKE_TICK_NS < 3000000)    // Need enough gap ticks for the RuOx 910 Hz LP filter to settle (3 ms)
    #error "GAP_TICKS must allow at least 3 ms of settling time for LP filter"
#endif

//---------> Some useful macros
#define R42_DIVIDE_FACTOR		72.5	// Divide ratio of therm. excitation before ADAC	

// Useful conversion equations
#define INA_TO_ADAC(x) ((unsigned int)(x)<<7)	// JRH changed 6 to 7 on 20120629 to fix factor of 2 error in reported current
#define ADAC_TO_INA(x) ((unsigned int)(x)>>7)
//mv_out = 4096mV *( CDAC/65536 )
//CDAC = (mv_out/4096)*65536 = mv_out*16
#define COIL_MV_TO_DAC(x) ((short)(x)<<4)
#define COIL_DAC_TO_MV(x) ((short)(x)>>4)
/* Convert milli volts from board's voltage monitor input
 * to ADC units.  Boost board has gain of 1 on vmon.
 * DSPID board has gain of 5 on vmon input. 
 * ADC = (mV * 65536)*5/20480 = 16 * mV
 */
#define VMON_MV_TO_ADC(x) ((short)(x)<<4)
#define VMON_ADC_TO_MV(x) ((short)(x)>>4)
// V_adc = ADC * 20480 mV / 65536 = ADC * 10 / 2^5
#define ADC_TO_MV(x) (((short)(x)>>5)*10)
#define MV_TO_ADC(x) (((short)(x)/10)<<5)

// I_sense = V_sense*(1 mA/mV)
#define I_SENSE_ADC_TO_MA(x) ((short)(((long)x*10)>>5))
#define I_SENSE_MA_TO_ADC(x) ((short)(((long)x << 5)/10))

// Kelvin = ADC * 10 / (32 * 49.9) + 205.2
#define ADC_TO_KELVIN(x) (.006262525 * (x) + 205.2)

// Analog Output channels
#define AO_MV_TO_DAC(x) ((short)(x)/5)
#define AO_DAC_TO_MV(x) ((short)(x)*5)

// Definitions
#define CAL_CHANNEL_01          2
#define CAL_CHANNEL_02          3

#define AMUX_COIL_V_MON         1
#define AMUX_COIL_I_SENSE       2
#define AMUX_ANALOG_IN          3
#define AMUX_EXT_TEMP           4           // Temperature of associated Boost board
#define AMUX_BOARD_TEMP         5
#define AMUX_V_SUPPLY           6
#define AMUX_GND                7


//-----> OUTPUT PINS
#define PIN_LED             LATCbits.LATC1	// Front pannel LED

#define PIN_TMUX_A0         LATBbits.LATB11	// Thermometer MUX address bits
#define PIN_TMUX_A1         LATBbits.LATB12

#define PIN_ADC_MUX_A0      LATBbits.LATB10	// 8:1 MUX in front of ADC converter
#define PIN_ADC_MUX_A1      LATBbits.LATB9
#define PIN_ADC_MUX_A2      LATBbits.LATB8

#define PIN_CONVERT         LATDbits.LATD1	// ADC convert pin
#define PIN_485_ENABLE      LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

#define PIN_CHOP_CLK_PIC    LATDbits.LATD8	// Pin to chop therm. excitation

#define PIN_OSCOPE1         LATDbits.LATD4	// Debug pins for looking at with scope
#define PIN_OSCOPE2         LATGbits.LATG15

// DAC load/enable pins
#define PIN_GDAC_LD         LATBbits.LATB14	// Gain DAC
#define PIN_ADAC_LD         LATBbits.LATB15	// RuOx amplitude DAC
#define PIN_CDAC_LD         LATGbits.LATG2	// ADR Coil voltage DAC
#define PIN_AODAC_LD        LATDbits.LATD10	// Analog output DAC

//------> INPUT PINS
#define PIN_ADDR0           PORTFbits.RF1
#define PIN_ADDR1           PORTGbits.RG1
#define PIN_ADDR2           PORTGbits.RG0
#define PIN_ADDR3           PORTFbits.RF0
#define PIN_SYNC_CLK        PORTDbits.RD2
#define PIN_FRAME_CLK       PORTDbits.RD3



//---------> Function Prototypes
// DSPID.C
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// int_handler.c
void int_handler( void );	// interrupt handler routine
void update_interrupt_communication();
void print_adc( unsigned short num );	// debug function prints Num ADC readings

void write_amp_dac( unsigned int val );
void write_gain_dac( unsigned int val );

unsigned short read_gain_dac( void );
unsigned short read_amp_dac( void );

void write_aout_dac( unsigned char ch, short val );
short read_aout_dac( unsigned char ch );
void set_aoramp(char ch, short target, short delta );
void disable_aoramp(void);

void write_cdac( short val );
short read_cdac( void );

unsigned short read_adc( void );
long read_demod( void );
void set_data_on( unsigned char state );
unsigned char is_data_on( void );
void set_chop( int state );
unsigned short get_tmux( void );
void set_tmux( unsigned short ch );
short get_amux_reading( int ch );

void set_mode_iramp( short i_target, short v_target );
void set_mode_ihold( short i_target, short v_target );
void set_mode_constant_vc( int cdac );
unsigned int get_sample_count( void );
void zero_sample_count( void );
void set_mode_pid();
unsigned char is_pid_on( void );
void set_roffset( short roffset );

void print_int_handler_status( void );

// process_cmd.c
void process_cmd( char *str );

//do_cmd.c
void write_tmux( unsigned short ch );
void write_amux( unsigned short ch );
float calc_r_val( long data );
long calc_demod_from_r( long r );
unsigned char get_addr( void );
void set_ina( unsigned short ina );
void auto_set_gain( void );
void adc_noise_test(int skip, long int n, char f);
unsigned isense_ma_to_adc( short ma );
unsigned vmon_mv_to_adc( short mv );
short vmon_adc_to_mv( unsigned adc );
short isense_adc_to_ma( unsigned adc );
void OnIdle( void );	// Called when program has nothing to do
void Do_1Hz_Events( void );
void Enable_PID_sramp( long, long );
void Disable_PID_sramp( void );
void Print_PID_sramp_status( char * );

//pid.c
void update_velocity(long demod);
short compute_PID(long demod);
void pid_get_start_val( void );
void pid_set_debug( char s );
void pid_set_p( unsigned char p );
void pid_set_i( unsigned char i );
void pid_set_d( unsigned char d );
void pid_set_p_shift( unsigned char num_p_shift );
void pid_set_i_shift( unsigned char num_i_shift );
void pid_set_d_shift( unsigned char num_d_shift );
unsigned char pid_get_p(void);
unsigned char pid_get_i( void );
unsigned char pid_get_d( void );
int pid_get_p_shift(void);
int pid_get_i_shift(void);
int pid_get_d_shift(void);
long pid_get_pterm(void);
long pid_get_iterm(void);
long pid_get_dterm(void);
void pid_reset_integrator(void);
void pid_set_s( long sp );
long pid_get_s( void );
long pid_get_s_demod( void );
void pid_recompute_setpoint(void);
void pid_send_params_to_frame(void);
void pid_set_vlim( short vmax );
void pid_set_imax( short imax );
short pid_get_vlim( void );
short pid_get_imax( void );

// ihold.c
void init_ihold( short coil_dac, short coil_v_mon );
short compute_ihold(short coil_i_sense, short last_coil_dac, short coil_v_mon);
void ihold_set_ishift( unsigned char _ishift );
void ihold_set_s( short sp );  // Set the IHOLD setpoint in ADC units
short ihold_get_s( void );     // Get the IHOLD setpoint in ADC units
short ihold_get_ishift( void );
void ihold_reset_integrator(void);
void ihold_set_coil_v_limits(short v);
void ihold_send_params_to_frame(void);

#endif	// __DSPID_INCLUDED
