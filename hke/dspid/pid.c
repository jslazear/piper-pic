#include <p30F5011.h>
#include <string.h>
#include <uart.h>


#include "math_utilities.h"
#include <timer.h>
#include <stdio.h>

#include "dspid.h"
#include "../common/common.h"
#include "../common/frame_buf.h"

static char pid_debug = 1;			// Print debug messages while controlling

static unsigned char pid_p = 1;		// P term for PID
static unsigned char pid_i = 1;		// I term for PID
static unsigned char pid_d = 0;     // d term for PID
static long pid_setpoint_ohms = 5000;		// PID setpoint in ohms, as specified by the user
									// used to recompute pid_setpoint when user changes
									// ADAC or GDAC
static long pid_setpoint = 1487000;	// Setpoint for the PID controller, in demod units
static long error = 0;				// Most recent error (measurement - setpoint)
static long pterm = 0, iterm = 0;	// p*error, i*accum
static long dterm = 0;              // d*velocity
static long accumulator = 0;		// the integration accumulator

static short pid_coil_start_value;	// the coil dac setting when we start PID controlling
																// request a PID update cycle
static long pid_acc_max = 0;		// Max value to allow the accumulator to get to

// Max/Min values for voltage across coil and current through coil
// during PID control
static short coil_v_max = VMON_MV_TO_ADC(300);
static short coil_v_min = VMON_MV_TO_ADC(-300);
static short coil_i_max = I_SENSE_MA_TO_ADC(4100);
static short coil_i_min = I_SENSE_MA_TO_ADC(0);

static long last_demod = 0;         // previous value of demod
static long velocity = 0;           // Low-pass filtered derivative of demod

// This parameter sets the time constant for estimating the derivative term
static unsigned char err_shift = 2;
static unsigned char p_shift = 8;
static unsigned char i_shift = 15;
static unsigned char d_shift = 0;


#define LOG_EPSILON     3       // Epsilon is really 1 / 2^(this number)
//#define err_shift       2       // Error is right-shifted by this value
//#define p_shift         8       // p term is right-shifted by this value
//#define i_shift         15      // i term is right-shifted by this value
//// JL20140116 - D term is too small. Needs to be something like 1000x larger.
//// JL20140116 - Try with no shift (256x larger). May even need to left shift.
//// JL20140116 - #TODO Deadzone? More important if using left shift!
//#define d_shift         0       // d term is right-shifted by this value

void update_velocity(long demod)
{
    // compute the velocity
    // For LOG_EPSILON == 3
    //   velocity = (7*velocity + 1*(demod - last_demod)) / 8;
    velocity = (((1<<LOG_EPSILON)-1)*velocity + (demod - last_demod)) >> LOG_EPSILON;
    last_demod = demod;
}

/* Computes CDAC setting using supplied demod value
 */
short compute_PID(long demod)
{
	char add;
	long coil_dac;
	short last_coil_dac;

	PIN_OSCOPE1=1;

	// calculate the error, in hardware ADC units
	error = demod - pid_setpoint;		// Error is ~24 bits

//	error>>=err_shift;     // Divide down // (only works on positive numbers) not true  #DELME?
        error = fast_rshift_long(error, err_shift);  // Divide down // (only works on positive numbers) not true #FIXME?

        // Now deal with accumulator
	// Ensure |accumulator| < max (keeps iterm<2^32 from overflowing)
	add = 1;
	last_coil_dac = read_cdac();
	
	if( accumulator > pid_acc_max && error > 0 )
		add = 0;
	if( accumulator < - pid_acc_max && error < 0 )
		add = 0;
	
	// Anti-windup
	if( last_coil_dac >= 32000 && error > 0 ) {
		add = 0;
	}
	if( last_coil_dac <= -32000 && error < 0 ) {
		add = 0;
	}
	
	// Compute the P term of the PID
	pterm = pid_p * error;
//	pterm >>= p_shift;						// Divide down  #DELME?
	pterm = fast_rshift_long(pterm, p_shift);						// Divide down #FIXME?
	
	// Compute the I term of the PID
	iterm = pid_i * accumulator;
        iterm = fast_rshift_long(iterm, i_shift);  // #FIXME?
//	iterm >>= i_shift;   // #DELME?

        // Compute the D term of the PID
        dterm = pid_d * velocity;
//        dterm >>= d_shift;   // #DELME?
        dterm = fast_rshift_long(dterm, d_shift);  // #FIXME?
    
	// Figure out proposed coil_dac setting from P + I + D sum
	coil_dac = pid_coil_start_value + pterm + iterm + dterm;

	//---> Compute offset from previous CDAC setting, and clip to prevent large changes
	long delta = coil_dac - last_coil_dac;
	if( delta >=  1000 )	
		delta =  1000;
	if( delta <= -1000 )
	 	delta = -1000;

	//---> Change allowable coil voltage based on current limits
	int v_max = coil_v_max, v_min = coil_v_min;
	int coil_i_sense = get_amux_reading( AMUX_COIL_I_SENSE );
	if( coil_i_sense >= coil_i_max )  {
		v_max = 0;
		add = 0;		// anti-windup
	}
	if( coil_i_sense <= coil_i_min )  {
		v_min = 0;
		add = 0;		// anti-windup
	}

	//---> Impose coil voltage maximums to limit delta
	int coil_v_mon   = get_amux_reading( AMUX_COIL_V_MON );
	long new_coil_v = delta + coil_v_mon;	// any change in CDAC goes straight to coil voltage
	if( new_coil_v > v_max )  {
		delta = v_max - coil_v_mon;
		add = 0;		// anti-windup
	}
	if( new_coil_v < v_min )  {
		delta = v_min - coil_v_mon;
		add = 0;		// anti-windup
	}

	//---> Add delta to our previous dac setting to compute new setting
	coil_dac = last_coil_dac + delta;

	//---> Update the accumulator for next time, if no clipping is in effect
	if( add )
		accumulator += error;

	//---> Clip coil_dac to 16 bits
	// ***NOTE*** coil_dac is a LONG, we must truncate to make it fit
	// in a 16 bit value.
	if( coil_dac <= -32767 )
		return -32767;
	else if( coil_dac >= 32767 )
		return 32767;
	else
		return coil_dac;
	
	PIN_OSCOPE1 = 0;
}

/* 
 * Interface routines for the outside world.
 */
void pid_get_start_val( void )
{
	pid_coil_start_value = read_cdac();		// save the coil voltage dac setting when we start
}

void pid_set_debug( char s )
{
	pid_debug = s;
}

void pid_set_p( unsigned char p )
{
	pid_p = p;
}

void pid_set_i( unsigned char i )
{
	pid_i = i;
	pid_acc_max = ((2147483647L - 16777216L)/i);	// keeps accumulator from overflowing
}

void pid_set_d( unsigned char d )
{
	pid_d = d;
}

void pid_set_p_shift( unsigned char num_p_shift )
{
	p_shift = num_p_shift;
}

void pid_set_i_shift( unsigned char num_i_shift )
{
	i_shift = num_i_shift;
}

void pid_set_d_shift( unsigned char num_d_shift )
{
	d_shift = num_d_shift;
}

unsigned char pid_get_p(void)
{
	return pid_p;
}

unsigned char pid_get_i( void )
{
	return pid_i;
}

unsigned char pid_get_d( void )
{
	return pid_d;
}

int pid_get_p_shift(void)
{
	return p_shift;
}

int pid_get_i_shift( void )
{
	return i_shift;
}

int pid_get_d_shift( void )
{
	return d_shift;
}

long pid_get_pterm(void)
{
    return pterm;
}

long pid_get_iterm(void)
{
    return iterm;
}

long pid_get_dterm(void)
{
    return dterm;
}


void pid_reset_integrator()
{
	accumulator = 0;
}

// Set the PID setpoint in OHMS
void pid_set_s( long sp )
{
	pid_setpoint_ohms = sp;
	pid_recompute_setpoint();
}
// Get the PID setpoint in OHMS
long pid_get_s( void )
{
	return pid_setpoint_ohms;
}
// Get the PID setpoint in DEMOD units
long pid_get_s_demod( void )
{
	return pid_setpoint;
}

// Recompute the setpoint in DEMOD units given the
// current setting in OHMS
void pid_recompute_setpoint()
{
	pid_setpoint = calc_demod_from_r( pid_setpoint_ohms );
}

// Sets the maximum allowed coil voltage during PID control
void pid_set_vlim( short vmax )
{
	coil_v_max = vmax;
	coil_v_min = -vmax;
}
short pid_get_vlim( void )
{
	return coil_v_max;
}

// Sets the maximum allowed coil current during PID control
void pid_set_imax( short imax )
{
	coil_i_max = imax;
}
short pid_get_imax( void )
{
	return coil_i_max;
}

/*
 * Sends the PID terms to the frame buffer
 */
void pid_send_params_to_frame()
{
	fb_put32( pid_setpoint );
	fb_put32( error );
	fb_put32( accumulator );
	fb_put8( pid_p );
	fb_put8( pid_i );
}
