#!/bin/env python

"""
proerrors.py
jlazear
2013-10-18

Base error class for the PSquid project.
"""
version = 20131018
releasestatus = 'beta'


class PixieReadoutError(Exception):
    """
    PixieReadout base exception class.
    """
    pass
