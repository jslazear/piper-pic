#include <p30F5011.h>

#include "psquid.h"

/*
 * ch - DAC
 * 0  - S1 Bias (aka Row Select)
 * 1  - S2 Bias
 * 2  - S3 Bias (aka Series Array Bias)
 * 3  - Detector Bias
 * 4  - S1 Feedback
 * 5  - S2 Feedback
 * 6  - S3 Feedback (aka Series Array Feedback)
 * 7  - S3 Offset (aka Series Array Offset)
 * 8  - S3 Gain (aka Series Array Gain)
 */
void write_dac(unsigned val, int ch)
{
	unsigned short dummy;

    unsigned mask = 0x0100;

    // Lower the DAC chip select
    if( ch == 8 ) {
        PIN_S3_GAIN_DAC = 0;    // The gain dac is the only one not adjacent on RB
        PIN_OSCOPE3 = 1;  //DEBUG
    } else {
        mask <<= ch;
        LATB &= ~mask;
    }

	// Send the data
	SPI2STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
	SPI2BUF = val;                  // Write to the SPI port
	while( !SPI2STATbits.SPIRBF );	// Wait for data to be ready on SPI port
	dummy = SPI2BUF;
    
    // Raise the DAC chip select - DAC chip reads in values on rising edge of CS
    if( ch == 8 ) {
        PIN_S3_GAIN_DAC = 1;
        PIN_OSCOPE3 = 0;  //DEBUG
    } else {
        LATB |= mask;
    }
}

/*
 * Write to the DAC.
 *
 * Load up the specified DAC with the specified value. Note that the address
 * variable is for the memory address of the DAC's chip select line.
 *
 * Ex:
 * write_dac(12345, &PIN_DET_BIAS)
 */
/*
void write_dac(unsigned val, int *address)
{
	unsigned short dummy;

        *address = 0;                   // Lower chip select pin
//	PIN_CDAC_LD = 0;		// Chip select the Coil DAC

	// Only 10 ns required from falling edge of LD pin to first SCLK
	// No delay needed here.

	// Send control byte (all zeros puts DAC in normal mode)
	SPI2STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
	SPI2BUF = (unsigned char)0;		// Write to the SPI port
	while( !SPI2STATbits.SPIRBF );	// Wait for data to be ready on SPI port
	dummy = SPI2BUF;

	// Send MSB of data
	SPI2STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
	SPI2BUF = (unsigned char)(val>>8);	// Write to the SPI port
	while( !SPI2STATbits.SPIRBF );	// Wait for data to be ready on SPI port
	dummy = SPI2BUF;

	// Send LSB of data
	SPI2STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
	SPI2BUF = (unsigned char)val;	// Write to the SPI port
	while( !SPI2STATbits.SPIRBF );	// Wait for data to be ready on SPI port
	dummy = SPI2BUF;

        *address = 1;                   // Raise chip select pin
                                        // DAC chip reads in values on rising edge of CS
//        PIN_CDAC_LD = 1;		// Load the Coil DAC
}
*/