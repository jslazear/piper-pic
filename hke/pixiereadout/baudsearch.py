import time
import serial
import random
from matplotlib.pyplot import *
ion()

#port = '/dev/tty.usbserial-DAWVBF3H'
port = 'com3'
uses = []
baudrates = []
measbaudrates = []

def baudset(baudrate):
    br = int(baudrate)
    baudrates.append(br)
    try:
        ser.close()
    except:
        pass
    ser = serial.Serial(port=port, baudrate=br, timeout=0)
    time.sleep(.1)
    ser.write('\x00')
    us = float(raw_input("Enter period in microseconds: "))
    print "Effective baudrate: {0} MBaud".format(9.e6/us)
    return us, 9.e6/us

def baudsearch(start=1000000, stop=1500000, step=10000):
    try:
        uses = []
        baudrates = []
        measbaudrates = []
        steps = range(start, stop, step)
        print "num = ", len(steps)
        for br in steps:
            us, mbr = baudset(br)
            baudrates.append(br)
            uses.append(us)
            measbaudrates.append(mbr)
    except KeyboardInterrupt:
        pass

    plot(baudrates, measbaudrates)
    return baudrates, measbaudrates, uses

def randbaudsearch(start=1000000, stop=1500000, num=10):
    try:
        uses = []
        baudrates = []
        measbaudrates = []
        print "num = ", num
        for i in range(num):
            print "i = {0}".format(i)
            br = int(start + (stop - start)*random.random())
            print "baudrate = {br} MBaud".format(br=br/1.e6)
            us, mbr = baudset(br)
            baudrates.append(br)
            uses.append(us)
            measbaudrates.append(mbr)
    except KeyboardInterrupt:
        pass

    plot(baudrates, measbaudrates, 'o')
    xlabel('set baudrate')
    ylabel('measured baudrate')
    axhline(1.25e6, ls='--')
    return baudrates, measbaudrates, uses


#baudrates = range(int(1.e6), int(1.5e6), int(0.05e6))