#include <p30F5011.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>
#include <libpic30.h>

#include "pixiereadout.h"
#include "../bp_common/bp_common_base.h"
#include "../common/ptty.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL16); /* External clock with 16xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF); /* Code protect disabled */


void init_pic()
{
    // Set up values for GPIO pins
    TRISBbits.TRISB5 = 0; // PIC_LED

    // Set up the output pins for GPIO
    TRISBbits.TRISB8 = 0;   // CS_ADC3
    TRISBbits.TRISB9 = 0;   // CS_ADC2
    TRISBbits.TRISB10 = 0;  // CS_ADC1
    TRISBbits.TRISB11 = 0;  // CS_ADC0
    TRISBbits.TRISB12 = 0;  // BUCK_DAC_LD
    TRISBbits.TRISB13 = 0;  // POT_SYNC
    TRISDbits.TRISD8 = 0;   // GAIN_A0
    TRISDbits.TRISD9 = 0;   // GAIN_A1
    TRISDbits.TRISD11 = 0;  // MODE (low gain / high gain)

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD5 = 0;
    TRISDbits.TRISD6 = 0;
    TRISDbits.TRISD7 = 0;
    PIN_OSCOPE1 = 0;
    PIN_OSCOPE2 = 0;
    PIN_OSCOPE3 = 0;
    PIN_OSCOPE4 = 0;

    // Set us in DC coupled (no bucker, thank you) by default
    PIN_MODE = 1;

    // Short delay to wait for power supply voltages to settle
    __delay32((long) (0.01 * CLOCKFREQ));

    // Setup the SPI port2 (For DACs)
    OpenSPI2(FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON &
            MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
            PRI_PRESCAL_4_1,
            SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR);
    ConfigIntSPI2(SPI_INT_DIS); // NO SPI interrupts

    // Same settings as PSquid which uses same ADC and DAC
    SPI2CONbits.CKE = 1; // CKE=1 means SDO changes on RISING SCLK edges, because AD5754 DAC reads on the falling edge
    SPI2CONbits.CKP = 0; // CKP=0 means SCLK is active high
    SPI2CONbits.SMP = 1; // If the SMP bit is set, then the input sampling is done at the end of the bit output

    // Setup the UART for super speed operation!
    init_uart2(0);  // Note BRG=0 gives 1.25 MBAUD.   BRG=10 gives 115.2 kbs

    // Before we start the interrupt handler, wait for a rising edge on external frame clock
    // This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
    // Do this just before turning on the timer interrupt.
    sync_to_frame_clock();

    //!!!!!
    // TODO: Probably should syncronize TMR2 phase with TMR2 phase on PixieBias
    //!!!!!

    // Setup Timer2 - 8 kHz (TURBO=2)
    OpenTimer2(T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, PR_CYCLES_PER_TICK-1 );

    // Open the output compare module to generate 50 usec pulses
    // Convert needs to remain high for the duration of the time required to read out all 4 ADCs
    // See Figure 39 in AD7685.pdf
    OpenOC2(OC_IDLE_CON & OC_TIMER2_SRC & OC_CONTINUE_PULSE, PR_CONV_CYCLES, 0);
    ConfigIntOC2(OC_INT_OFF);   // OC2 hardware triggers ADC, no interrupt needed

    // Setup OC5 to generate interrupts a few microseconds after convert.  ADCs will be ready to read at this point.
    // From Family Reference 6.1.5, The priority levels start at ?1? as the lowest priority and level '7' as the highest
    OpenOC5(OC_IDLE_CON & OC_TIMER2_SRC & OC_CONTINUE_PULSE, PR_INTERRUPT_DELAY_CYCLES, 0);
    ConfigIntOC5(OC_INT_ON & OC_INT_PRIOR_2);

    // Setup timer2 - interrupts on overflow.  Higher priority than OC5 int.  This just increments our counter
    // We do this on a separate interrupt to ensure we inc count exactly once per 8kHz sample.
    ConfigIntTimer2(T2_INT_ON & T2_INT_PRIOR_4);
}

int main(void)
{
    init_pic(); // Setup the PIC internal peripherals
    cmd_set_echo(0);

    while (1) {
        char s[100]; // Note the CH command can have long argument list (up to 90 characters)

        cmd_gets(s, sizeof(s)); // Get a command from the user
        process_cmd(s); // Deal with the command
    }

}

void OnIdle(void)
{
    // pass
}