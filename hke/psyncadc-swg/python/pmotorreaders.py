#!/bin/env python

"""
pmotorreaders.py
jlazear
2014-09-19

File readers for pmotor data.

Example:

<example code here>
"""
version = 20140919
releasestatus = 'beta'

from types import StringTypes
from tempfile import _TemporaryFileWrapper
import numpy as np
import pandas as pd


class PMotorDataReader(object):
    """
    Reader for binary-encoded data from the psquid data command.

    See pyoscope.readers.ReaderInterface for info on readers.

    Note that this reader is stupidly inefficient...
    """
    def __init__(self, f, *args, **kwargs):
        # Load file
        if isinstance(f, file) or isinstance(f, _TemporaryFileWrapper):
            mode = f.mode
            if ('r' in mode) or ('+' in mode):
                self.f = f
            else:
                self.f = open(f.name, 'r')
        elif isinstance(f, StringTypes):
            self.f = open(f, 'r')
        else:
            raise TypeError('f must be a file handle or filename.')
        self.f.seek(0)
        self.filename = self.f.name

    @staticmethod
    def _split_columns(colstr, typecast=str):
        """
        "[rdac, adc]" or "rdac, adc" or "[rdac adc]" or "rdac adc"
        all go to -> ["rdac", "adc"]

        unless typecast is specified, in which case typecast() is called
        on every value in list
        """
        colstr = colstr.strip('[]')
        if ',' in colstr:
            splitchar = ','
        else:
            splitchar = ' '
        collist = colstr.split(splitchar)
        collist = [typecast(col.strip()) for col in collist]
        return collist

    def close(self):
        self.f.close()

    def init_data(self, *args, **kwargs):
        # self.f.seek(5)
        if self.f.closed:
            raise ValueError('I/O operation on closed file.')
        self.args = args
        self.kwargs = kwargs
    	# cols = ('amplitude', 'encoder')
        # ftxt = self.f.read()  # Read the ENTIRE file.

        # names = ('caret', 'amplitude', 'space', 'encoder', 'crnl')
        # formats = ('S1', '>i2', 'S1', '>i4', 'S2')
        # dtdict = {'names': names, 'formats': formats}
        # dt = np.dtype(dtdict)

        # ftxt2 = ftxt[:(len(ftxt)//10)*10]

        # data = np.fromstring(ftxt2, dt)



        names = ('caret', 'amplitude', 'space', 'encoder', 'crnl')
        formats = ('S1', '>i2', 'S1', '>i4', 'S2')
        dtdict = {'names': names, 'formats': formats}
        dt = np.dtype(dtdict)

        data = np.fromfile(self.f, dt)

	    # Get rid of control columns and '*PSQUID: data_off' response row
        data = data[[names[1], names[3]]][:-2]  #FIXME? #DELME

        # # Convert to floats
        # dt2 = np.dtype({'names': cols, 'formats': ('f4', 'f4')})
        # data = data.astype(dt2)

        # Convert to pandas dataframe
        data = pd.DataFrame(data)
        return data

    def update_data(self):
        args = self.args
        kwargs = self.kwargs
        return self.init_data(*args, **kwargs)

    def switch_file(self, f, *args, **kwargs):
        self.__init__(f)
        return self.init_data(*args, **kwargs)
