#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "../common/psync_common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"
#include "psyncadc-swg.h"
#include "sinetable.h"

// DAC Sweep parameters
struct freq_ramp_params {
    long inc;             // This is addeded to the sine wave delta (the delta delta)
                          // If set to 0, then no frequency ramp is in progress.
    long target_delta;    // The delta value we're trying to reach
};
static struct freq_ramp_params framp = {
    .inc = 0,
    .target_delta = 0,
};

/*
// Frequency ramp parameters
struct freq_ramp_params2 {
    long target;          // The delta value we're trying to reach

    long total_ticks;
    long fstep;
    long error;
    long deltaerr;
    long et;
};


	 int total_ticks := Total ticks
	 int deltaf := f1 - f0
	 int fstep := deltaf / total_ticks	// integer divide (will be 0 if steep==false)
	 int error = 0
	 int deltaerr := abs( deltaf - fstep*total_ticks )	// deltaf MOD total_ticks  (will be deltaf for steep==false)
	 int et = total_ticks / 2			// error threshold
	 int finc = (f0 < f1) ?1 :-1
	 int f := f0

	 // On each tick
	 f += fstep
	 error += deltaerr
	 if error >= et then
		 f += finc
		 error -= total_ticks
 */

/* SINE WAVE DIRECT DIGITAL SYNTHESIS
 * 
 * UPDATE_RATE = 4 kHz
 * TABLE_BITS = 8 bits (256 entries in table)
 * PAC_BITS = 32 bits (phase accumulator size)
 * DIVISOR  = 2^(PAC_BITS - TABLE_BITS)
 *          = 2^24 ~ 16.7 million
 * NAT_FREQ = UPDATE_RATE / 2^TABLE_BITS   (natural frequency is fastest sine freq without skipping any table entries)
 *          = 15.625 Hz
 * MIN_FREQ = NAT_FREQ / DIVISOR
 *          = UPDATE_RATE / 2^PAC_BITS      (note: independent of table-length, for fixed size pac register)
 *          = 0.931 uHz
 *
 * FREQ = NAT_FREQ * (DELTA / DIVISOR)
 *
 * DELTA = (FREQ / NAT_FREQ) * DIVISOR
 *
 * So, to generate 1 Hz sine wave in this example,
 * Delta = 1.0/15.625 * 16777216 = 1073742
 */

extern int amplitude;  // Defined in int_handler.c

// Sine wave parameters
static unsigned long pac = 0;       // phase accumulator
static long delta = DIVISOR;        // Set it to the natural frequency

static void update_sine(int ch, int index);

// Phase offsets for indexing into the sine table to generate three-phase
#define PHASE_OFFSET2   ((unsigned)(SINE_TABLE_LEN * 1.0 / 3 + 0.5))
#define PHASE_OFFSET3   ((unsigned)(SINE_TABLE_LEN * 2.0 / 3 + 0.5))

void update_sines()
{
    pac += delta;

    unsigned index1 = pac >> DIVISOR_BITS;
    unsigned index2 = (index1 + PHASE_OFFSET2) & SINE_TABLE_MASK;
    unsigned index3 = (index1 + PHASE_OFFSET3) & SINE_TABLE_MASK;

    update_sine(0, index1);
    update_sine(1, index2);
    update_sine(2, index3);
}

static void update_sine(int ch, int index)
{
    int tval = sine_table[index];

    // Scale the amplitude (not-needed if you have a separate attenuator DAC)
    long tmp = __builtin_mulss(tval, amplitude);  // Note mulss for signed*signed
    tval = tmp >> 16;

    DacWrite( DACREG_DAC, ch, tval );
}

void update_freq_ramp()
{
    if( framp.inc == 0) {
        PIN_LED1 = 0;
        return;
    }

    // Ramp the frequency a bit
    delta += framp.inc;

    // Check if we're done ramping
    if( framp.inc > 0 && delta >= framp.target_delta ) {
        delta = framp.target_delta; // Force to the target, since we likely overshot
        framp.inc = 0;      // Stop frequency ramping
    }
    if( framp.inc < 0 && delta <= framp.target_delta ) {
        delta = framp.target_delta; // Force to the target, since we likely overshot
        framp.inc = 0;      // Stop frequency ramping
    }
}


//------------------------------------------------------
// External interface functions
//------------------------------------------------------


// Set sine wave delta (frequency) in raw phase accumulator units
void set_sine_delta(long d)
{
    delta = d;
}

// Comptue the delta value for a given frequency
long compute_delta( float freq )
{
    // NAT_FREQ = UPDATE_RATE / 2^TABLE_BITS   (natural frequency is fastest sine freq without skipping any table entries)
    // DELTA = (FREQ / NAT_FREQ) * DIVISOR
    // For DAC_UPDATE_RATE=4,000 and SINE_TABLE_LEN=256, this gives: delta = freq * 1,073,742
    float delta = freq / ((float)DAC_UPDATE_RATE / SINE_TABLE_LEN) * DIVISOR;

    // Note, this cast will be safe (won't overflow long) as long as freq is < DAC_UPDATE_RATE
    return (long)delta;
}

void set_freq_ramp(long target_delta, int seconds)
{
    PIN_LED1 = 1;

    // Going from say 0 Hz to 10 Hz is a change in delta of 10 million!
    // seconds =  (Target_delta - Actual_delta) / (increment * update_rate)
    int dir = 1;
    long mag = target_delta - delta;
    if( mag < 0 ) {
        mag = -mag;
        dir = -1;
    }

    long inc = mag / (seconds * DAC_UPDATE_RATE);
    if( dir < 0 )
        inc = -inc;

    framp.target_delta = target_delta;
    framp.inc = inc;            // Set this last, as soon as this is non-zero, int_handler will start ramping!
}
