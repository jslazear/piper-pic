#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>

#include "psyncadc-swg.h"
#include "../common/ptty.h"


/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL16); /* External clock with 16xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF); /* Code protect disabled */

/*  Global variables */


void init_pic()
{
    // Set up values for GPIO pins

    // PAPPA power supply turn-on time may be very slow, so wait 500 ms to
    // make sure the power supply rails have initialized correctly.
    // Without this, DAC was not initializing correctly.
    __delay32((long) (0.5 * CLOCKFREQ)); // Wait 500 ms for DACs to power up

    // Outputs common to v1 and v2 hardware
    TRISBbits.TRISB11 = 0; // MUX Address lines
    TRISBbits.TRISB12 = 0; // MUX Address lines
    TRISBbits.TRISB13 = 0; // MUX Address lines
    TRISBbits.TRISB14 = 0; // MUX Address lines
    TRISBbits.TRISB15 = 0; // MUX Address lines
    TRISBbits.TRISB0 = 0; // Test Point 2

    TRISCbits.TRISC1 = 0; // Set LED as an output
    TRISBbits.TRISB5 = 0; // Set pic_ready_bar line to an output
    TRISFbits.TRISF0 = 0; // DAC SYNC
    TRISFbits.TRISF1 = 0; // DAC LOAD

    TRISDbits.TRISD4 = 0; // OSCOPE1
    TRISGbits.TRISG15 = 0; // OSCOPE2

    // Set the direction for GPIO

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD4 = 0;
    TRISGbits.TRISG15 = 0;
    PIN_OSCOPE1 = 0;
    PIN_OSCOPE2 = 0;
    PIN_TESTPOINT2 = 0;

    // Short delay to wait for power supply voltages to settle
    __delay32((long) (0.01 * CLOCKFREQ));

    // Setup the UART - 115.2 kbs
    unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
    unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
    OpenUART2(U2MODEvalue, U2STAvalue, BRG_SETTING);
    ConfigIntUART2(UART_RX_INT_DIS & UART_TX_INT_DIS); // Disable TX and RX interrups

    // Setup the SPI port1 (For ADC and DACs)
    OpenSPI1(FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON &
            MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
            PRI_PRESCAL_4_1,
            SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR);
    ConfigIntSPI1(SPI_INT_DIS); // NO SPI interrupts

    //------------------------------------------------------------------------------
    //The SPI module supports 4 different Serial Clock formats. The user can
    //select one of these 4 formats by configuring the Clock Polarity Select, or
    //CKP, and Clock Edge Select, or CKE, bits in the SPI Control Register.
    //See the dsPIC family reference for a description.
    //NOTE: the settings here are a bit off from our usual, the AD5724 DAC chip needed different settings
    //  -- it wants valid data on the falling edge of sclk.  Other DACs, like the DAC7715 (on the AO board) want valid
    // data on the rising SCLK edge
    //------------------------------------------------------------------------------
    // Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
    SPI1CONbits.CKE = 0; // CKE=1 means SDO changes on RISING SCLK edges, because AD5754 DAC reads on the falling edge
    SPI1CONbits.CKP = 0; // CKP=0 means SCLK is active high
    SPI1CONbits.SMP = 0; // If the SMP bit is set, then the input sampling is done at the end of the bit output

    // Power on and configure the AD5754 DAC chip
    // Galvanic isolator chip (ADUM1400) may intiailize with junk bits --
    // initialize twice to make sure it's correct
    DacInit();
    DacInit();

    // DacTest();

    INTCON1bits.NSTDIS = 0; // Allow interrupts to interrupt other interrupt handlers  (The default)

    // Setup timer 2 and set its period to 4 kHz
    OpenTimer2(T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, 5000 - 1);

    // Open the output compare module to generate 5 usec pulses, with interrupts on FALLING edge
    // these pulses will trigger the convert pin of the ADC (Not currently using ADC in this project, but trigger it anyway)
    OpenOC1(OC_IDLE_CON & OC_TIMER2_SRC & OC_CONTINUE_PULSE, CONVERT_PULSE_STOP_TICK, 0);

    // Finally, turn on OC interrupt -- this will start sending data to the DAC over the SPI port!
    ConfigIntOC1(OC_INT_ON & OC_INT_PRIOR_2); // Put this at a lower priority so encoder INT4 can interrupt it

    // INT4 is our encoder interrupt.  Needs high priority so it can interrupt the OC1 interrupt
    ConfigINT4(RISING_EDGE_INT & EXT_INT_ENABLE & EXT_INT_PRI_7);
}

int main(void)
{
    init_pic(); // Setup the PIC internal peripherals

    set_lead_angle(LEAD_ANGLE_OPTIMUM); // Set the default lead angle

    // Find home position on start-up
    set_amplitude(AMP_TO_DAC(1));  // Must energize coils to be able to find home
    find_home(1);
//    search(1);
    set_amplitude(AMP_TO_DAC(1));
    set_lead_angle(154);

    PIN_LED1 = 0;

    while (1) {
        char s[100]; // Note the CH command can have long argument list (up to 90 characters)

        cmd_gets(s, sizeof (s)); // Get a command from the user

        process_cmd(s); // Deal with the command
    }
}

void OnIdle(void)
{
}