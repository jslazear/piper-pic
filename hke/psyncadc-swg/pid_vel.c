#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "../common/psync_common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"
#include "../common/math_utilities.h"
#include "psyncadc-swg.h"

// ---------------------
// ----VARIABLE DEFS----
// ---------------------

struct pidparams {
    unsigned p; // P term for PID
    unsigned i; // I term for PID
    int setpoint; // PID setpoint (velocity units = counts/1024ticks)
    long accumulator; // PID integrator term accumulator
    unsigned char sign; // Sign of P term and I term
    unsigned char i_shift; // I term is divided by 2^i_shift
    unsigned char p_shift; // P term is divided by 2^p_shift
    int amp_init; // Initial amplitude
};

static struct pidparams pid = {
    .p = 0,
    .i = 500,
    .setpoint = 0,
    .accumulator = 0,
    .sign = 0,
    .i_shift = 10,
    .p_shift = 0,
    .amp_init = 0};

int compute_pid_vel(int velocity)
{
    static long error;
    static long pterm;
    static long iterm;
    static long sum;
    static int toret;
    //    static char buf[100];

    error = velocity - pid.setpoint;

    // NOTE: pid.sign is handled here for i coefficient so that accumulator is
    // always continuous.
    if (pid.sign)
        pid.accumulator += pid.i * error;
    else
        pid.accumulator -= pid.i * error;

    if (pid.p != 0) {
        pterm = pid.p * error;

        pterm = fast_rshift_long(pterm, pid.p_shift);
    } else {
        pterm = 0;
    }

    // Compiler handles this switch structure by indexing i_shift into a look-up BRA table
    // so there is no performance cost to having all of the cases
    iterm = fast_rshift_long(pid.accumulator, pid.i_shift);

    // Compute the full PID output
    // NOTE: Only p term of pid.sign is handled here, since i term handled above
    if (pid.sign)
        sum = (long)pid.amp_init + pterm + iterm;
    else
        sum = (long)pid.amp_init - pterm + iterm;

    // Coerce to signed 16-bit integer
    // PID algorithm should naturally fall into range, unless
    // specified set point is impossible to achieve, in which case
    // the controller will rail
    if (sum <= -32768L)
        toret = -32768;
    else if (sum >= 32767L)
        toret = 32767;
    else
        toret = (int)sum;

    return toret;
}



/* set_pid_vel_setpoint
 *
 * Adjusts the current PID loop to have a new setpoint
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * setpoint - value to which to rotate the wheel
 */
void set_pid_vel_setpoint(int setpoint)
{
    pid.setpoint = setpoint;
}

/* set_pid_vel_sign
 *
 * Valid arguments are 0 and 1.
 */
void set_pid_vel_sign(unsigned char sign)
{
    pid.sign = sign;
}

/* set_pid_vel_p
 *
 * Adjusts the current PID loop to have a new p constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_vel_p(unsigned p)
{
    pid.p = p;
}

/* set_pid_vel_p_shift
 *
 * Changes the fixed-point basis of the proportional term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */

void set_pid_vel_p_shift(unsigned p_shift)
{
    pid.p_shift = p_shift;
}


/* set_pid_vel_i
 *
 * Adjusts the current PID loop to have a new i constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */

void set_pid_vel_i(unsigned i)
{
    pid.i = i;
}

/* set_pid_vel_i_shift
 *
 * Changes the fixed-point basis of the integral term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop. Does attempt to compensate the accumulator for the new scaling
 * factor so that there is not too large a discontinuity. 
 */

void set_pid_vel_i_shift(unsigned i_shift)
{
    int diff = (int)pid.i_shift - (int)i_shift;
    if (diff >=0) {
        pid.accumulator = pid.accumulator >> diff;
    } else {
        pid.accumulator = pid.accumulator << -diff;
    }

    pid.i_shift = i_shift;
}

// Zeroes the accumulator.
void set_pid_vel_reset(void)
{
    pid.accumulator = 0;
}

void pid_vel_status(void)
{
    char str[100]; // max string len is about 51 chars, so 100 is very safe

    long error = (long)get_velocity() - (long)pid.setpoint;
    long iterm = fast_rshift_long(pid.accumulator, pid.i_shift);
    long pterm;
    if (pid.p != 0) {
        pterm = pid.p * error;

        pterm = fast_rshift_long(pterm, pid.p_shift);
    } else {
        pterm = 0;
    }


    sprintf(str, "PSG: ---VELOCITY PID PARAMETERS---\r\n");
    TTYPuts(str);
    sprintf(str, "PSG: s = %d, e = %ld\r\n", pid.setpoint, error);
    TTYPuts(str);
    sprintf(str, "PSG: accum = %ld, sign = %u\r\n", pid.accumulator, pid.sign);
    TTYPuts(str);
    sprintf(str, "PSG: p = %u, p_shift = %u, p_term = %ld\r\n", pid.p, pid.p_shift, pterm);
    TTYPuts(str);
    sprintf(str, "PSG: i = %u, i_shift = %u, i_term = %ld\r\n", pid.i, pid.i_shift, iterm);
    TTYPuts(str);

}