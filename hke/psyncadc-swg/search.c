// Search algorithm for minimizing a function.

#include <p30F5011.h>
#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>

#include "../common/common.h"
#include "../common/stty.h"
#include "psyncadc-swg.h"

static int f(int lead_angle);
static int iterate(int x1, int x2, int x3, int fx1, int fx2, int fx3, unsigned char verbose);

float phibar = 0.3819660112501051;  // = 2 - (1 + sqrt(5))/2

// The function to be minimized. In this case, setting the lead angle and
// measuring the resulting velocity.
static int f(int lead_angle)
{
    // Coerce lead_angle into correct range
    while (lead_angle < 0)
        lead_angle += 360;
    while (lead_angle >= 360)
        lead_angle -= 360;

    get_velocity();  // Discard old velocity measurement, if it exists.
    set_lead_angle(lead_angle);
    __delay32((long) (2.0 * CLOCKFREQ));   // Wait 2 seconds to settle.
    return -get_velocity_blocking();
}

int search(unsigned char verbose)
{
    int f0, f1, f2;

    set_mode(MODE_FREERUN);

    f0 = f(0);
    f1 = f(120);
    f2 = f(240);

    if ((f0 < f1) && (f0 < f2)) {
        return iterate(240, 360, 480, f2, f0, f1, verbose);
    } else if ((f1 < f0) && (f1 < f2)) {
        return iterate(0, 120, 240, f0, f1, f2, verbose);
    } else if ((f2 < f0) && (f2 < f1)) {
        return iterate(120, 240, 360, f1, f2, f0, verbose);
    } else {
        return 0;
    }
}

static int iterate(int x1, int x2, int x3, int fx1, int fx2, int fx3, unsigned char verbose)
{
    char str[100];
    unsigned char v = verbose;
    int x;
    static unsigned int i=0;  // Iteration counter

    int x32 = x3 - x2;
    int x21 = x2 - x1;

    int dx;

    if (v) {
        sprintf(str, "*PSG: ---LEAD ANGLE SEARCH (i=%d)---\r\n", i);
        TTYPuts(str);
        sprintf(str, "*PSG: x1 = %d, x2 = %d, x3 = %d\r\n", x1, x2, x3);
        TTYPuts(str);
        sprintf(str, "*PSG: f(x1) = %d, f(x2) = %d, f(x3) = %d\r\n", fx1, fx2, fx3);
        TTYPuts(str);

    }

    i++;

    unsigned char right_larger = (x32 > x21);  // Is the right side larger than the left?
    if (v) {
        if (right_larger)
            sprintf(str, "*PSG: RIGHT Larger ==> Subdividing RIGHT ==> x1 < x2 < x < x3\r\n");
        else
            sprintf(str, "*PSG: LEFT Larger ==> Subdividing LEFT ==> x1 < x < x2 < x3\r\n");
        TTYPuts(str);
    }


    // Subdivide the larger side
    if (right_larger)
        dx = (int)(phibar*(float)x32);
    else
        dx = (int)(-phibar*(float)x21);

    if (v) {
        sprintf(str, "*PSG: dx = %d\r\n", dx);
        TTYPuts(str);
    }

    // Terminate if no change.
    if (dx == 0) {
        if (v) {
            sprintf(str, "*PSG: No change... TERMINATING with max = %d.\r\n", x2);
            TTYPuts(str);
        }
        return x2;
    } else  // Otherwise continue with computing new test point
        x = x2 + dx;

    // NOTE: ordering of points:
    // IF right_larger == True  ==>  subdivided RIGHT side  ==>  x1 < x2 < x < x3
    // IF right_larger == False ==>  subdivided LEFT side   ==>  x1 < x < x2 < x3

    // Compute the new value at x
    int fx = f(x);
    if (v) {
        sprintf(str, "*PSG: x = %d, f(x) = %d.\r\n", x, fx);
        TTYPuts(str);
    }

    if (fx < fx2) {                                     // If value at new point is smaller than the interior point,
        if (right_larger)                               // and subdivided the right side,
            return iterate(x2, x, x3, fx2, fx, fx3, v);  // then use the rightmost 3 points
        else                                            // otherwise
            return iterate(x1, x, x2, fx1, fx, fx2, v);  // use the leftmost 3 points...
    }  else {                                           // Otherwise, if the new point is larger than the interior point
        if (right_larger)                               // and subdivided the right side,
            return iterate(x1, x2, x, fx1, fx2, fx, v);  // use the leftmost 3 points
        else                                            // otherwise
            return iterate(x, x2, x3, fx, fx2, fx3, v);  // use the rightmost 3 points
    }
}