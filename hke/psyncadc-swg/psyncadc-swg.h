// Define the version of the board this program is for
#define PSYNCADC1

//---------> Some useful stuff
#define CLOCKFREQ 20000000L		// 20.000 MHz instruction clock (synthesised from 5 MHz crystal, 16x PLL)

#define BRG_SETTING 2       // 416 kbs.  This is the fastest rate we can generate with a 20 MHz FCY that is
                            // compatible with the MAC using our FTDI chip USB to Fiber board.
                            // BRG = FCLK/(BaudRate*16) - 1

// Calculate the actual BAUD rate from the specified BRG setting
#define BAUD_RATE           (CLOCKFREQ/(BRG_SETTING+1) >> 4)


#define DAC_UPDATE_RATE   4000      // DAC update rate in Hz
#define FREQ_MAX          (DAC_UPDATE_RATE/4)       // Max frequency we can generate

#define ENCODER_PULSE_PER_REV     2500
#define ENCODER_MAX       (ENCODER_PULSE_PER_REV - 1)
#define LEAD_ANGLE_OPTIMUM      153   // Guessed optimum lead angle

#define NUM_ADC_CHANNELS	32

#define CONVERT_PULSE_STOP_TICK 100		// 100 gives a 5 us pulse on the ADC convert pin

#define MODE_FREERUN      0
#define MODE_PID_POSITION 1
#define MODE_PID_VELOCITY 2

#define PID_SIGN_POS
//-----> OUTPUT PINS
#define PIN_A0			LATBbits.LATB11	// ADC input mux
#define PIN_A1			LATBbits.LATB12	// ADC input mux
#define PIN_A2			LATBbits.LATB13	// ADC input mux
#define PIN_A3			LATBbits.LATB14	// ADC input mux
#define PIN_A4			LATBbits.LATB15	// ADC input mux
// #define PIN_TESTPOINT1		LATBbits.LATB1  // NOW ENCODER_A INPUT
#define PIN_TESTPOINT2		LATBbits.LATB0  // test point 2 output

//------> INPUT PINS
#define PIN_ENCODER_A		PORTDbits.RD11   // ENCODER_A INPUT
#define PIN_ENCODER_B		PORTBbits.RB1   // ENCODER_B INPUT (also external INT4 trigger)
#define PIN_ENCODER_HOME	PORTBbits.RB10  // ENCODER_ Index mark (once per revolution)

#define PIN_GATE		PORTDbits.RD8	// For READING Gate pin (normally triggers INT1)
#define PIN_CONVERT		PORTDbits.RD0	// For READING the ADC Convert pin (output is from OC1 hardware)

    // Outputs
#define PIN_LED1		LATCbits.LATC1	// Front pannel LED
// #define PIN_FRAME		LATBbits.LATB10 // Now ENCODER_INDEX
#define PIN_PIC_READY_BAR	LATBbits.LATB5	// Drive low to re-enable frame count capture circuit

#define PIN_DAC_SYNC	LATFbits.LATF1	// DAC Sync BAR pin
#define PIN_LOAD_DAC	LATFbits.LATF0	// LDAC BAR pin

    // Inputs
#define PIN_JP1_1		PORTDbits.RD3   // Unallocated Jumper inputs
#define PIN_JP1_2		PORTDbits.RD2
#define PIN_JP1_3		PORTDbits.RD1

// Diagnostic outputs
#define PIN_OSCOPE1		LATDbits.LATD4	// Debug pins for looking at with scope
#define PIN_OSCOPE2		LATGbits.LATG15

//  V_adc = ADC * 20480 mV / 65536 = ADC * 10 / 2^5
#define ADC_TO_MV(x) (((x>>4)*5)-10240)


// DAC Hardware
#define DACREG_DAC			0
#define DACREG_OUTPUT_RANGE		1
#define DACREG_POWER_CONTROL            2
#define DACREG_CONTROL			3

// DAC conversions
#define AMP_TO_DAC(x) (10000.0 * (x) / 3.00)
#define DAC_TO_AMP(x) (3.00 * (x) / 10000.0)


//---------> Function Prototypes
// psyncadc.c
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// process_cmd.c
void process_cmd( char *cmd );

// do_cmd.c
void write_mux( unsigned short ch );

// cmd_gets.c
void cmd_gets( char *str, int n );

// dac.c
void DacInit( void );
void DacTest( void );
void DacWrite( int reg, int ch, int data );

// int_handler.c
long measure_velocity(void);
char find_home(unsigned char verbose);
void set_lead_angle( int degrees );
void set_amplitude(int amp);
void set_mode(unsigned char new_mode);
void set_data_flag(char flag);
void set_abs_encoder_home(void);
unsigned char get_mode(void);
char get_data_flag(void);
unsigned char get_found_home(void);
int get_amplitude(void);
long get_abs_encoder(void);
int get_velocity(void);
int get_velocity_blocking(void);
int get_lead_angle(void);
int get_lead_angle_degrees(void);
int get_tick(void);  // #DELME
int get_tick2(void);  // #DELME

// sinegen.c
void update_sines( void );
void update_freq_ramp( void );

void set_sine_delta(long d);
long compute_delta( float freq );
void set_freq_ramp(long target_delta, int seconds);

// pid_pos.c
int compute_pid_pos(long abs_pos);
void set_pid_pos_setpoint(long setpoint);
void set_pid_pos_sign(unsigned char sign);
void set_pid_pos_p(unsigned p);
void set_pid_pos_p_shift(unsigned p_shift);
void set_pid_pos_i(unsigned i);
void set_pid_pos_i_shift(unsigned i_shift);
void set_pid_pos_d(unsigned d);
void set_pid_pos_d_shift(unsigned d_shift);
void set_pid_pos_reset(void);
void pid_pos_status(void);

// pid_vel.c
int compute_pid_vel(int velocity);
void set_pid_vel_setpoint(int setpoint);
void set_pid_vel_sign(unsigned char sign);
void set_pid_vel_p(unsigned p);
void set_pid_vel_p_shift(unsigned p_shift);
void set_pid_vel_i(unsigned i);
void set_pid_vel_i_shift(unsigned i_shift);
void set_pid_vel_reset(void);
void pid_vel_status(void);

// search.c
int search(unsigned char verbose);