#include <p30F5011.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>

#include "../common/common.h"
#include "../common/stty.h"
#include "../common/ee_io.h"

#define _EE_ROW_WORDS   (_EE_ROW/2)

static char rowBuf[_EE_ROW] __attribute__((aligned(2)));	// Regular RAM to hold one "row" of data while copying to EEPROM
static int index;      // Index to the next location in rowBuf to write to

static _prog_addressT EE_addr;

unsigned int crcCCITT;

//--------- CRC16 CHECKSUM ROUTINES----------

// The following routines compute a CRC16-CCITT (0x1D0F)
// The code is adapted from item 3 at http://fixituntilitsbroken.blogspot.com/2009/09/using-cyclical-redundancy-codes-crc-16.html
// Initial value: 0x1D0F
// Polynomial: 0x1021 (normal representation)
// Examples:
//   The CRC16 of  is 1D0F
//   The CRC16 of 123456789 is E5CC
//   The CRC16 of Hello World! is A5B9
// You can test more examples at: http://www.zorc.breitbandkatze.de/crc.html

static void initCRC( void )
{
    crcCCITT = 0x1D0F;
}

static void updateCRC( unsigned char x )
{
    unsigned crc_new = (unsigned char)(crcCCITT >> 8) | (crcCCITT << 8);
    crc_new ^= x;
    crc_new ^= (unsigned char)(crc_new & 0xff) >> 4;
    crc_new ^= crc_new << 12;
    crc_new ^= (crc_new & 0xff) << 5;
    crcCCITT = crc_new;
}

/* Uncomment if you want to test the CRC implementation
 * 
static void testCRCString( char *str )
{
    initCRC();

    char *p;
    for( p=str; *p; ++p )
        updateCRC(*p);

    TTYPuts("The CRC16 of ");
    TTYPuts(str);
    TTYPuts(" is ");
    TTYPrintHex16(crcCCITT);
    TTYPuts("\r\n");
}

void TestCRC(void)
{
    testCRCString("");              // Should be 1D0F
    testCRCString("123456789");     // Should be E5CC
    testCRCString("Hello World!");  // Should be A5B9
}
*/

//--------- WRITING ROUTINES----------

static int checkRowBufNeedsWriting()
{
    _prog_addressT ee = EE_addr;
    int i, word;

    for( i=0; i<_EE_ROW_WORDS; ++i)  {
        _memcpy_p2d16(&word, ee, 2);
        if( word != rowBuf[i] )
            return 1;           // We encountered a difference
        ee += 2;           // Advance to the next word
    }
    return 0;
}

static void flushRowBuf() {
    // Do not write to EEProm if we have nothing to write
    if( index == 0)
        return;

    // Check if we actually need to do the write (EEPROM has a finite life cycle, so only write if we need to)
    if( checkRowBufNeedsWriting() ) {
        // Erase the next ROW in Data EEPROM
        _erase_eedata(EE_addr, _EE_ROW);
        _wait_eedata();

        // Write the full rowBuf to EEPROM
        _write_eedata_row(EE_addr, (void*)rowBuf);
        _wait_eedata();
    }

    // Reset Variables
    index = 0;
    memset(rowBuf, '#', _EE_ROW); // Clear out our row buffer
    EE_addr += _EE_ROW;           // Advance to the next row in EEPROM
}

// Writes a single character to the rowBuf, flushing to EEPROM if necessary
static void rowBufPutc( char c ) {
    updateCRC(c);

    rowBuf[index++] = c;
    if( index >= _EE_ROW )
        flushRowBuf();          // Writes rowBuf to EEPROM and resets variables for writing to next row
}

//--------------------------------------------------------
// Public API
//-------------------------------------------------------

void eeOpenWrite(_prog_addressT ee_addr)
{
    index = 0;              // Init pointer to start of row buff
    EE_addr = ee_addr;          // Save the address of where in EEPROM we're writing
    memset(rowBuf, '#', _EE_ROW); // Clear out our row buffer
    initCRC();
}

void eeWriteByte( unsigned char b )
{
    rowBufPutc( b );
}

void eeWriteWord( unsigned w )
{
    // dsPic is little endian (see Family Reference 3.4.4)
    rowBufPutc( (char)(w) );
    rowBufPutc( (char)(w>>8) );
}

void eeWrite( char* ptr, int n)
{
    while( n --> 0 )
        rowBufPutc( *ptr++ );
}

void eeWriteChecksum( void )
{
    eeWriteWord(crcCCITT);
}

// Flushes any remaining data in the rowBuf out to the EE
void eeCloseWrite( void )
{
    flushRowBuf();
}

//--------- READING -----------

void eeOpenRead(_prog_addressT ee_addr)
{
    EE_addr = ee_addr;          // Save the address of where in EEPROM we're writing
    initCRC();             // Initializes the crcCCITT variable
}

unsigned char eeReadByte( void )
{
    unsigned char byte;
    _memcpy_p2d16(&byte, EE_addr, 1);
    
    EE_addr++;

    return byte;
}

unsigned eeReadWord( void )
{
    unsigned word;
    _memcpy_p2d16(&word, EE_addr, 2);
    EE_addr += 2;
    
    return word;
}

int eeVerifyChecksum( _prog_addressT ee_addr, int datalen )
{
    int i;

    initCRC();

    // Read in all the data bytes.
    for(i=0; i<datalen; ++i )  {
        unsigned char byte;
        _memcpy_p2d16(&byte, ee_addr, 1);
        ee_addr++;

        updateCRC(byte);
    }

    // Read in the stored checksum
    unsigned eeChecksum;
    _memcpy_p2d16(&eeChecksum, ee_addr, 2);

    if( eeChecksum == crcCCITT)
        return 1;               // Computed and stored checksums match.  All is good!
    return 0;
}

/**
 * Returns 1 if the specified block of EE memory is empty (all 0s)
 */
int eeCheckIsBlank( _prog_addressT ee_addr, int datalen )
{
    int i;

    // Read in all the data bytes.
    for(i=0; i<datalen; ++i )  {
        unsigned char byte;
        _memcpy_p2d16(&byte, ee_addr, 1);
        ee_addr++;

        if( byte )
            return 0;       // Not blank
    }

    return 1;               // All EE memory in block is blank
}


