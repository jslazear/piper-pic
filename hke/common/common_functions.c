/*
 * This file is for functions that are useful across all boards (not just the HKE backplane boards).
 */

#include "common.h"
#include "tty_print.h"


/*
 * Print a human readable status message depending on the status code.
 */
void print_status( char status )
{
	switch( status )  {
		case STATUS_OK:
			TTYPuts( "OK" );
			break;
		case STATUS_FAIL_CMD:
			TTYPuts( "FAIL: unknown command" );
			break;
		case STATUS_FAIL_NARGS:
			TTYPuts( "FAIL: incorrect number of arguments" );
			break;
		case STATUS_FAIL_INVALID:
			TTYPuts( "FAIL: invalid argument format" );
			break;
		case STATUS_FAIL_RANGE:
			TTYPuts( "FAIL: argument out of range" );
			break;
		default:
			TTYPuts( "FAIL: unknown error" );
			break;
	}

	// All responses must end with ! to signal parser this is end of response
	TTYPuts( "!\r\n" );
}
