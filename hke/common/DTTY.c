/*
These routines handle the TTY.  This is an old file.  It is not used in any current projects,
just included for reference.  Should be moved probably...
*/
#include <p18f2620.h>
#include "DTTY.h"

#ifdef DSPID
	#include "../dspid/dspid.h"		// Tells which pin is RS-485 enable
#endif
#ifdef ZSPID
	#include "../zspid/zspid.h"		// Tells which pin is RS-485 enable
#endif


#define SER_TXBUF_MASK (SER_TXBUF_SIZE-1)
#define SER_RXBUF_MASK (SER_RXBUF_SIZE-1)

//Transmit buffer - points to buffer of data to transmit
static volatile char *txBuf;
static volatile unsigned char txBuf_len;	// number of character to transmit

//Receive buffer - contains received data
static volatile char rxBuf[SER_RXBUF_SIZE];

// the buffers can only be 256 bytes max
static volatile unsigned char idxSerRxbufGet;	// get index
static volatile unsigned char idxSerRxbufPut;	// put index


void DTTYEnable( void ) 
{
    //Interrupts
	IPR1bits.RCIP = 0;		// use low priority interrupts
	IPR1bits.TXIP = 0;		// use low priority interrupts

	PIE1bits.RCIE = 1;   	// Enable USART Receive interrupt
	PIE1bits.TXIE = 0;   	// Disable USART Transmit interrupt - is only enabled once a TX'ing is started

 	PIN_485_ENABLE = 0;		// RELEASE the 485 bus 
}

void DTTYResetBuffers( void ) {
	txBuf = 0;				// Set to null pointer
	txBuf_len = 0;			// no more characters to transmit
	
	//Set receive buffer to empty
	idxSerRxbufGet = idxSerRxbufPut = 0;
}

void DTTYRxIsr( void ) {
    //Check for "overun error" (OERR bit 1) or "Framming Error" (FERR bit 2).
    if (RCSTA & 0x06) {
		//dbg junk = RCSTA;
        RCSTAbits.CREN = 0;
		RCSTAbits.CREN = 1;
    }

    //Add received byte to buffer if any char receivee
    while (PIE1bits.RCIE && PIR1bits.RCIF) {

	    rxBuf[idxSerRxbufPut] = RCREG;    // Get received byte. RCIF is cleared once there are no more bytes left
       	idxSerRxbufPut = (++idxSerRxbufPut) & SER_RXBUF_MASK;

		// if receive buffer full, overwrite oldest
		if( idxSerRxbufPut == idxSerRxbufGet )
			idxSerRxbufGet = (++idxSerRxbufGet) & SER_RXBUF_MASK;
    }
}

char DTTYGetC( void ) {
    char c;

	c = rxBuf[idxSerRxbufGet];
	idxSerRxbufGet = (++idxSerRxbufGet) & SER_RXBUF_MASK;

	return c;
}


unsigned char DTTYPeekC( void )
{
	return ( idxSerRxbufPut != idxSerRxbufGet );
}


void DTTYInit(unsigned long BaudRate, unsigned long ClockFreq )
{
	unsigned int br;
	char b;

	// compute 16 bit baud rate divisor
	br = (ClockFreq/BaudRate + 2)/4 - 1;
	
	TRISCbits.TRISC7 = 1;	// configure io pin for receiver
	TRISCbits.TRISC6 = 0;	// configure io pin for transmitter
	
	RCSTA   = 0b10010000;	// enable receiver (SPEN,CREN)
	TXSTA   = 0b00100100;	// enable transmitter (TXEN), BRGH set
	BAUDCON = 0b00001000;	// baud rate control reg set to async, 16 bit BRG
	SPBRG   = br & 0xff;	// baud rate value low 8 bits
	SPBRGH	= (br>>8) & 0xff;	// baud rate value high 8 bits
	
	b = RCREG;				// clear our receiver
	b = RCREG;
	
	// init the buffer pointers
	DTTYResetBuffers();
	
	//Interrupts
	PIE1bits.RCIE = 0;   //Disable USART Receive interrupt
	PIE1bits.TXIE = 0;   //Disable USART Transmit interrupt - is only enabled once a TXion is started
}


/*
 * Interrupt handler for the transmit section
 */
void DTTYTxIsr( void )
{
	// This check happens on all interrupts (in particular, every 1 ms clock tick)
	if( txBuf_len == 0 && TXSTAbits.TRMT == 1 ) // no more chars to transmit, and shift register empty
		PIN_485_ENABLE = 0;		// Release 485 bus, we're not transmitting anymore

	if( PIE1bits.TXIE && PIR1bits.TXIF ) {
	    //Transmit next byte contained in transmit buffer. If whole buffer has been transmitted, disable
	    //transmit interrupts.
	    if( txBuf_len > 0 )  {
	        TXREG = *txBuf++;	// Send the character and advance the pointer
			txBuf_len--;		// one fewer character to transmit
		}
	    else  {
	        PIE1bits.TXIE = 0;   //Disable USART Transmit interrupt when nothing more to TX
 			// NOTE - Last character still transmitting, DO NOT RELEASE the 485 bus 
		}
	}
}

/*
 * Send a buffer out the serial port.
 *
 * THE USE OF THIS ROUTINE IS SEVERELY RESTRICTED!!!!!
 *   *call only from INSIDE the main interrupt handler at a known safe time to transmit
 *   *must pass it pointer to a static buffer, since it does not copy the data
 */
void DTTYSendBuffer( volatile char * volatile buf, unsigned char n )
{
	// If we are currently transmitting, then this is very bad.  This function
	// should only be called from the interrupt handler at a known "safe" time
	if( PIE1bits.TXIE ) {
		return;			// Do nothing so as not to interfere with current transmission
	}

	// We are NOT currently TXing -- this is the usual

 	PIN_485_ENABLE = 1;			// GRAB the 485 bus
	Nop();						// Delay a bit to make sure we have the bus
	Nop();

	// Send the first byte (saves one interrupt)
    TXREG = *buf++;   // Send the first byte

	// Set the buffer for the interrupt handler
	txBuf = buf;		// Set the tx buffer to point to our data
	txBuf_len = n-1;	// Set the number of bytes to transmit

	// Enable TX interrupt (next instruction will branch to interrupt)
    PIE1bits.TXIE = 1;  // Enable the TX interrupt
}


/*
 * Send a character out to the port.
 *
 * SEVERE Restrictions on use.
 * Only call this for debugging, when interrupts are off, and there is only 1 card on backplane.
 */
void DTTY_Debug_Putc( char c )
{
    while( !PIR1bits.TXIF );	// Wait for port to be ready
	TXREG = c;					// send the character
}