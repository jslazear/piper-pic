#ifndef __TTY_PRINT_INCLUDED
#define __TTY_PRINT_INCLUDED

// Function prototypes for TTY output functions
// These are NOT defined in tty_print.c
// They are defined in stty.c or ptty.c, (pick one depending on the implementation you prefer)
// You must either include one of those files with project, or define them yourself in order to use the tty_print routines
void TTYPuts( char *s );
void TTYPutc( char c );

// Printing routines
void TTYPrintHex32( unsigned long d );
void TTYPrintHex16( unsigned int d );
void TTYPrintHex8( unsigned char d );

// Print to string routines, works like sprintf
// Returns # of chars written into str
// Appends '\0' (but does not include in count)
int sprintHex8(char *str, unsigned char d);
int sprintHex16(char *str, unsigned int d);
int sprintHex32(char *str, unsigned long d);


// Parsing, doesn't really belong here
int parse_hex_digit( char h );

#endif