#include <p30F5011.h>
#include <uart.h>
#include <stdio.h>

#include "stty.h"

/* RS-232 interface functions for standalone boards.
   i.e. boards not part of the card rack.
   These are basic functions that implement our standard TTY interface (TTYPuts, TTYPutc),
   so that you can use the more advanced printing functions defined in tty_print.c.
   These do not make use of any interrupt handlers.
*/


/*
 * Send a string over the UART
 */
void TTYPuts( char *s )
{
	while( *s )  {
		while( BusyUART2() );	// Wait till not busy
		putcUART2( *s );
		++s;
	}
	
}

/*
 * Send a character over the UART
 */
void TTYPutc( char c )
{
	while( BusyUART2() );	// Wait till not busy
	putcUART2( c );
}
