#ifndef __FRAME_BUF_INCLUDED
#define __FRAME_BUF_INCLUDED

#ifdef PMASTER
	/*
	Pos Len	Meaning
	0	1   '*'
	1	2	Address
	3	1	Card type ('M')
	4	2	Frame counter
	6	1	Unused
	7	1	Status
			HEX Character bits
			0 (LSB)	COM Mode (0 = RS232 or 1 = Fiber)
			1		UBC Frame Count present	(1 = present)
			2		CLK Source (0 = External or 1 = Internal)
			3 (MSB)	Unused

	8   8   UBC Frame Count
	16  8   PIC Frame Count
	24  1	'\r'
	25  1	'\n'
	26  1 	'\0'
	*/
	#define FRAME_BUF_LEN	(27)

#endif

#ifdef TREAD
	/*
	Pos Len	Meaning
	0	1   '*'
	1	2	Address
	3	1	Card type
	4	2	Frame counter	
	6	1	TMux		// Usually set to '@' for all.
	7	1	Status		// 0=standard mode, 1=high_res mode, 2=low_res mode
	
	8	8	Demod[0]
	16	4	ADAC[0]
	20	4	GDAC[0]
	
	24	8	Demod[1]
	32	4	ADAC[1]
	36	4	GDAC[1]
	
	40	8	Demod[2]
	48	4	ADAC[2]
	52	4	GDAC[2]
	 
	56	8	Demod[3]
	64	4	ADAC[3]
	68	4	GDAC[3]
	
	72	8	Demod[4]
	80	4	ADAC[4]
	84	4	GDAC[4]
	 
	88	8	Demod[5]
	96	4	ADAC[5]
	100	4	GDAC[5]
	
	104	8	Demod[6]
	112	4	ADAC[6]
	116	4	GDAC[6]
	
	120	8	Demod[7]
	128	4	ADAC[7]
	132	4	GDAC[7]
	
	136	8	Demod[8]
	144	4	ADAC[8]
	148	4	GDAC[8]
	
	152	8	Demod[9]
	160	4	ADAC[9]
	164	4	GDAC[9]
	
	168	8	Demod[10]
	176	4	ADAC[10]
	180	4	GDAC[10]
	
	184	8	Demod[11]
	192	4	ADAC[11]
	196	4	GDAC[11]
	
	200	8	Demod[12]
	208	4	ADAC[12]
	212	4	GDAC[12]
	
	216	8	Demod[13]
	224	4	ADAC[13]
	228	4	GDAC[13]
	
	232	8	Demod[14]
	240	4	ADAC[14]
	244	4	GDAC[14]
	
	248	8	Demod[15]
	256	4	ADAC[15]
	260	4	GDAC[15]

	264 2	N_SUM 	// How many samples per half reading
	 
	266 1	'\r'
	267 1	'\n'
	268 1 	'\0'
	*/
	#define FRAME_BUF_LEN	(269)
#endif

#ifdef DSPID
	/*
	Pos Len	Meaning
	0	1   '*'
	1	2	Address
	3	1	Card type
	4	2	Frame counter
	6	1	TMux
	7	1	Status
	8	8	Demod[0]
	16	4	Coil Current[0]
	20	4	Coil DAC[0]
	24	8	Demod[1]
	32	4	Coil Current[1]
	36	4	Coil DAC[1]
	40	8	Demod[2]
	48	4	Coil Current[2]
	52	4	Coil DAC[2]
	56	8	Demod[3]
	64	4	Coil Current[3]
	68	4	Coil DAC[3]
	72	8	Demod[4]
	80	4	Coil Current[4]
	84	4	Coil DAC[4]
	88	8	Demod[5]
	96	4	Coil Current[5]
	100	4	Coil DAC[5]
	104	8	Demod[6]
	112	4	Coil Current[6]
	116	4	Coil DAC[6]
	120	8	Demod[7]
   	128	4	Coil Current[7]
	132	4	Coil DAC[7]
   	136	4	ADAC	- Thermometer excitation amplitude dac
   	140	4	GDAC	- Thermometer readout gain dac
	144	4	VMon	- coil voltage monitor
	148 4	Analog IN
	152 4	External temp
	156 4	Board temp
	160 4	Vsupply
	164	4	Gnd
	168	4	AOUT[0]
	172	4	AOUT[1]
	176	4	AOUT[2]
	180	4	AOUT[3]
	184 8	PID setpoint (demod units)	
	192	8	PID error
	200	8	PID accumulator
	208	2	PID P coefficient
	210	2	PID I coefficient
        212     2       N_SUM  // How many samples per half-period
	214	1	'\r'
	215	1	'\n'
	216 1	'\0'	- not transmited, but makes frame a C string
*/
	#define FRAME_BUF_LEN  (217)
#endif

#ifdef ANALOG_OUT
	/*
	Pos Len	Meaning
	0	1   '*'
	1	2	Address
	3	1	Card type
	4	2	Frame counter
	6	1	Unused
	7	1	Status

	8	3	Analog Out[0]
	11	3	Analog Out[1]
	14	3	Analog Out[2]
	17	3	Analog Out[3]
	20	3	Analog Out[4]
	23	3	Analog Out[5]
	26	3	Analog Out[6]
	29	3	Analog Out[7]
	32	3	Analog Out[8]
	35	3	Analog Out[9]
	38	3	Analog Out[10]
	41	3	Analog Out[11]
	44	3	Analog Out[12]
	47	3	Analog Out[13]
	50	3	Analog Out[14]
	53	3	Analog Out[15]
	56	3	Analog Out[16]
	59	3	Analog Out[17]
	62	3	Analog Out[18]
	65	3	Analog Out[19]
	68	3	Analog Out[20]
	71	3	Analog Out[21]
	74	3	Analog Out[22]
	77	3	Analog Out[23]
	80	3	Analog Out[24]
	83	3	Analog Out[25]
	86	3	Analog Out[26]
	89	3	Analog Out[27]
	92	3	Analog Out[28]
	95	3	Analog Out[29]
	98	3	Analog Out[30]
	101	3	Analog Out[31]
	104	1	'\r'
	105	1	'\n'
	106	1	'\0'	- not transmited, but makes frame a C string
*/
	#define FRAME_BUF_LEN  (107)
#endif

#ifdef ANALOG_IN
	/*
	Pos Len	Meaning
	0	1   '*'
	1	2	Address
	3	1	Card type
	4	2	Frame counter
	6	1	Gain (0=gain of 1, 1=gain of 10, 2=gain of 100, T=AD590 temperature mode)
	7	1	Status
	8	2	Samples per channel per frame (defaults to 1)
	10	2	Num channels (1, 2, 4, 8, 16, 32, default to 32)

	12	4	Analog In[0]
	16	4	Analog In[1]
	20	4	Analog In[2]
	24	4	Analog In[3]
	28	4	Analog In[4]
	32	4	Analog In[5]
	36	4	Analog In[6]
	40	4	Analog In[7]
	44	4	Analog In[8]
	48	4	Analog In[9]
	52	4	Analog In[10]
	56	4	Analog In[11]
	60	4	Analog In[12]
	64	4	Analog In[13]
	68	4	Analog In[14]
	72	4	Analog In[15]
	76	4	Analog In[16]
	80	4	Analog In[17]
	84	4	Analog In[18]
	88	4	Analog In[19]
	92	4	Analog In[20]
	96	4	Analog In[21]
	100	4	Analog In[22]
	104	4	Analog In[23]
	108	4	Analog In[24]
	112	4	Analog In[25]
	116	4	Analog In[26]
	120	4	Analog In[27]
	124	4	Analog In[28]
	128	4	Analog In[29]
	132	4	Analog In[30]
	136	4	Analog In[31]
	140	1	'\r'
	141	1	'\n'
	142	1	'\0'	- not transmited, but makes frame a C string
*/
	#define FRAME_BUF_LEN  (143)
#endif

#ifdef PSYNCADC
/*
	Pos Len	 Meaning
	0	1    '*'
	1	8	 32 bit UBC SyncBox Frame count that triggered this conversion cycle
	9   1    - separator char
	10  128  32 x ADC samples, each sample is 2 bytes or 4 hex characters
	138	1	'\r'
	139	1	'\n'
	140	1	'\0'	- not transmited, but makes frame a C string
*/
	#define FRAME_BUF_LEN  (141)	// This is the MAX length, will often be shorter if
									// we're not reporting all 32 channels
									// We set it to the max here to allocate enough memory
	#define FRAME_BUF_LEN_FOR_NCH(nCh)	( ((nCh)<<2) + 13)	// 4 bytes per channel + 13 fixed bytes
#endif

#ifdef PIXIEBIAS
/*
    Pos Len Meaning
    0   1   '*'
    1   2   Address
    3   1   Card type ('B')
    4   2   Frame counter
    6   1   Source mask
            BINARY Bits
            0 (LSB) Channel 0 source (0 = bias, 1 = tickle)
            1       Channel 1 source
            2       Channel 2 source
            3 (MSB) Channel 3 source
    7   1   Status (Unused)
    8   8   EXT_CONVERT index
    16  4   EXT_CONVERT phase (in clock cycles, [0, 4799])
    20  4   Bias Amplitude DAC value
    24  4   Tickle Step Size (max is 4000)
    28  4   Tickle Offset
    32  4   Tickle Amplitude (power of 2 divisor)
    36  1   '\r'
    37  1   '\n'
    38  1   '\0'	- not transmited, but makes frame a C string
*/
    #define FRAME_BUF_LEN  (39)
#endif

#ifdef PMOTOR
	/*
	Pos Len	Meaning
	0   1   '*'
	1   2   Address
	3   1   Card type ('R')
	4   2   Frame counter
	6   2   Status
			HEX Character bits
			0 (LSB)	COM Mode (0 = RS232 or 1 = Fiber)
			1		UBC Frame Count present	(1 = present)
			2		CLK Source (0 = External or 1 = Internal)
			3 (MSB)	Unused

	8   8   Abs position
	16  4   Velocity in PIC units
        20  4   Amplitude (signed integer --> 2's complement format)
        24  1   Mode (FREERUN='F', PID_POSITION='P', PID_VELOCITY='V')
        25  3   Lead angle in degrees (in hex)
        28  1   PID_POSITION sign (+ = positive, - = minus)
        29  4   PID_POSITION p coefficient
        33  4   PID_POSITION i coefficient
        37  4   PID_POSITION d coefficient
        41  1   PID_POSITION p_shift coefficient
        42  1   PID_POSITION i_shift coefficient
        43  1   PID_POSITION d_shift coefficient
        44  1   PID_VELOCITY sign (+ = positive, - = minus)
        45  4   PID_VELOCITY p coefficient
        49  4   PID_VELOCITY i coefficient
        53  1   PID_VELOCITY p_shift coefficient
        54  1   PID_VELOCITY i_shift coefficient
	55  1	'\r'
	56  1	'\n'
	57  1 	'\0'
	*/
	#define FRAME_BUF_LEN   (58)

#endif

void init_frame_buf_min( int length );
void init_frame_buf_backplane( char BoardType, unsigned char cardAddress );
void swap_active_frame_buf( void );
void enqueue_last_frame_buf( void );

// Buffer writer routines
void fb_putc( unsigned char c );
void fb_puts(char *s);
void fb_put4( unsigned char c );
void fb_put8( unsigned char c );
void fb_put12( unsigned short d );
void fb_put16( unsigned short s );
void fb_put24( unsigned long d );
void fb_put32( unsigned long d );

#endif
