/* Common functionality for the PSync and PSyncADC boards */

#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>

// Include the appropriate .h file to give us the pin definitions
#ifdef PSYNC	// Note either PSYNC or PSYNCADC must be defined in the MPLAB project file
	#include "../psync/psync.h"
	#define PIN_TESTPOINT1 PIN_OSCOPE1
#endif
#ifdef PSYNCADC
	#include "../psyncadc/psyncadc.h"
#endif
#ifdef PMASTER
	#include "../pmaster/pmaster.h"

	#define PIN_TESTPOINT1 PIN_OSCOPE3
#endif

static int spiWait( char fail );
static void psyncFinalizeRead( void );

/* Tests for the presence of and correct connection of an external UBC Sync box
 * This function should not be interrupted
 *
 * USES: Timer1 - does not currently save/restore config of timer1 after its use
 *
 * RETURNS: 1 = successful frame up.  0 = no UBC sync box detected
 */
int test_psync()
{
	// Turn on timer1 to use as a stopwatch
	int timeoutTicks = (int)((CLOCKFREQ/256) *0.100);		// number of timer1 ticks (with /256 prescale) in 0.1 second
	ConfigIntTimer1(T1_INT_OFF);
	TMR1 = 0;
	OpenTimer1( T1_ON & T1_IDLE_CON & T1_GATE_OFF & T1_PS_1_256 & T1_SOURCE_INT, 65535U );

	PIN_TESTPOINT1 = 1;

	// Wait for a Sync pulse
	PIN_PIC_READY_BAR = 0;	// Set low to reset the S/R flip flop on clock gate
	Nop();
	PIN_PIC_READY_BAR = 1;

    int gate_now, gate_prev = PIN_GATE;

	while( 1 )		// Wait for pulse to start (gate will go high when pulse starts)
	{
        gate_now = PIN_GATE;

        // With no sync box connected, PIN_GATE can power up to either high or low, but cannot transition (no clock on D flip flop)
        // A transition on the gate indicates that something is connected to both frame and clock
        if( gate_prev==0 && gate_now==1 ) {
            CloseTimer1();			// We're done with timer 1
            PIN_TESTPOINT1 = 0;

            // Wait for Sync pulse to be over (they are 8 us long, this waits 15 us)
            // don't want to go into the rest of the code in the middle of a sync pulse
            __delay32( (long)(CLOCKFREQ * 15e-6) );

            return 1;               // We found a sync box!
        }

        // Check if we timed out, if so we did not find a sync box
		if( TMR1 > timeoutTicks )  {
			CloseTimer1();
    		PIN_TESTPOINT1 = 0;
			return 0;		// Failed to frame up
		}

        gate_prev = gate_now;
	}
}

/*
 * Init the PSync specific hardware.  i.e. the flip flop and SPI port used for receiving frame counts from the UBC Sync box.
 * This function should not be interrupted, so disable any interrupt sources
 * before calling.
 *
 * Must call "test_psync()" before calling this function, because this function will
 * hang if there is no sync box connected
 *
 */
void init_psync()
{
    //--- Skip the first sync pulse, in case we started up in the middle of a sync pulse
    
	// Wait for a Sync pulse
	PIN_PIC_READY_BAR = 0;	// Set low to reset the S/R flip flop on clock gate
	Nop();
	PIN_PIC_READY_BAR = 1;
	while( !PIN_GATE )		// Wait for pulse to start (gate will go high when pulse starts)
	{
		Nop();
	}

	// Wait for Sync pulse to be over (they are 8 us long, this waits 15 us)
	__delay32( (long)(CLOCKFREQ * 15e-6) );

    //--- Init psynch

	// Arm the flip flop
	PIN_FRAME = 0;			// Init frame testpoint pin low
	PIN_PIC_READY_BAR = 0;	// Set low to reset the S/R flip flop on clock gate
	Nop();	Nop();			// Delays to make sure S/R flip flop is really reset

	// Set high to arm the flip flop so we're ready for the next frame count signal from UBC Sync box
	PIN_PIC_READY_BAR = 1;

   	// Open the SPI2 port
	OpenSPI2( FRAME_ENABLE_OFF & ENABLE_SDO_PIN & SPI_MODE16_ON &
				MASTER_ENABLE_OFF & SLAVE_ENABLE_ON,
				SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR );
	ConfigIntSPI2( SPI_INT_DIS );	// NO SPI interrupts

	// Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
	SPI2CONbits.CKE = 0;	// Set to 0 for non-framed slave mode
	SPI2CONbits.CKP = 1;	// Sample on the rising edge of the clock.  Very important!
	SPI2CONbits.SMP = 0;	// If the SMP bit is set, then the input sampling is done at the end of the bit output
							// NOTE - SMP=0 is required for slave mode.  Since we're not using SDO on this port
							// it's value doesn't mater.  Only CKP matters.

	SPI2BUF = 0;			// Write to the SPI port
	SPI2STATbits.SPIEN = 1;	// Enable the SPI2 port

	// Do a dummy read and clear overflow bits to prepare SPI port (*very important*)
	volatile int __attribute__((unused))dummy_word = SPI2BUF;	// Read the port
	SPI2STATbits.SPIROV = 0;		// Clear SPI receive overflow bit

	// When a frame count data packet is arriving over fiber, INT0 pin will go high
	// We want to interrupt on the rising edge of this line
	// Give it priority 4, (middle / high) in case we want some higher or lower priority ints
	unsigned int config = RISING_EDGE_INT & EXT_INT_ENABLE & EXT_INT_PRI_4;
	#ifdef PSYNC
		ConfigINT0( config );
	#else
		ConfigINT1( config );		// PMaster, PsyncADC uses Int1 instead of Int0
	#endif
}

/*
 * Reads in a 40 bit frame count + status from the UBC Sync box.
 * This operation takes roughly 8 us.
 */
unsigned long psync_read_frame_count()
{
	// raw data
	unsigned int word1, word2, word3;
	unsigned long frame_count;

	// Read word 1 (timed with scope on 7/8/11, takes 2 us to get here.  *must* be here in < ~6 us, so we are very safe)
	SPI2BUF=0;						// Write to the SPI port (needed by hardware)
	if( spiWait( 'A' ) ) {
		psyncFinalizeRead();		// Reset psync hardware for next time
		return 0;
	}
	word1 = SPI2BUF;				// Read in data from ADC converter

	// Read word 2
	SPI2BUF = 0;					// Write to the SPI port (needed by hardware)
	if( spiWait( 'B' ) ) {
		psyncFinalizeRead();		// Reset psync hardware for next time
		return 0;
	}
	word2 = SPI2BUF;				// Read in data from ADC converter

	// Read word 3
	SPI2BUF = 0;					// Write to the SPI port (needed by hardware)
	if( spiWait( 'C' ) ) {
		psyncFinalizeRead();		// Reset psync hardware for next time
		return 0;
	}
	word3 = SPI2BUF;				// Read in data from ADC converter

	// Assemble frame count from word1,word2,word3
	// 1111-1111
	//          2222-2222 2222-2222
	//                             3333-3333
	// Done this way, takes 19 instructions
	frame_count = ((unsigned long)word1 << 24) | ((unsigned long)word2 << 8) | (word3 >> 8);

	// Alternate method, takes 18 cycles
	/*
	frame_count = word3 >> 8;
	*(((unsigned char*)&frame_count) + 1) = (unsigned char)word2;
	*(((unsigned char*)&frame_count) + 2) = (unsigned char)(word2>>8);
	*(((unsigned char*)&frame_count) + 3) = (unsigned char)(word1);
	*/

	// For debugging - prints "0008BE3C - 1:1000 2:08BE 3:3CFF = 0008BE3C"
	/*
	TTYPrintHex32(frame_count);
	TTYPuts(" - 1:");
	TTYPrintHex16(word1);
	TTYPuts(" 2:");
	TTYPrintHex16(word2);
	TTYPuts(" 3:");
	TTYPrintHex16(word3);
	//TTYPuts("\r\n");
	TTYPuts(" = ");
	*/
	psyncFinalizeRead();			// Reset psync hardware for next time

	return frame_count;
}

static void psyncFinalizeRead()
{
	PIN_PIC_READY_BAR = 0;	// Set low to reset the S/R flip flop on clock gate circuit (U5)	

	SPI2STATbits.SPIEN = 0;				// Disable the port

	SPI2STATbits.SPIROV = 0;			// Clear SPI receive overflow bit
	SPI2BUF = 0;						// Clear spi2buf
	volatile int __attribute__((unused))dummy_word  = SPI2BUF;	// Do a dummy read and clear overflow bits to prepare SPI port

	SPI2STATbits.SPIEN = 1;				// Re-enable the port

	PIN_PIC_READY_BAR = 1;  // Set high to arm the flip flop so we're ready for the next frame count signal from UBC Sync box
}

static int spiWait( char fail )
{
	// 16 bit read at 5 mbit/sec = 3.2 usec = 64 instructions
	// each loop takes ~ 6 instruction cycles
	int maxloops = 64;
	while( !SPI2STATbits.SPIRBF ) {
		if( --maxloops <= 0 )  {
			// TTYPutc lines are for debugging
			// TTYPutcDirect( SPI2STATbits.SPIROV + '0' );
			// TTYPutcDirect( fail );
			return 1;
		}
	}
	return 0;
}