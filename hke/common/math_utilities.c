#include "math_utilities.h"

// Compiler handles this switch structure by indexing shift into a look-up BRA table
// so there is no performance cost to having all of the cases
unsigned long fast_rshift_ulong(unsigned long value, unsigned char shift)
{
    switch (shift) {
        case 0: break;
        case 1: value >>= 1; break;
        case 2: value >>= 2; break;
        case 3: value >>= 3; break;
        case 4: value >>= 4; break;
        case 5: value >>= 5; break;
        case 6: value >>= 6; break;
        case 7: value >>= 7; break;
        case 8: value >>= 8; break;
        case 9: value >>= 9; break;
        case 10: value >>= 10; break;
        case 11: value >>= 11; break;
        case 12: value >>= 12; break;
        case 13: value >>= 13; break;
        case 14: value >>= 14; break;
        case 15: value >>= 15; break;
        case 16: value >>= 16; break;
        case 17: value >>= 17; break;
        case 18: value >>= 18; break;
        case 19: value >>= 19; break;
        case 20: value >>= 20; break;
        case 21: value >>= 21; break;
        case 22: value >>= 22; break;
        case 23: value >>= 23; break;
        case 24: value >>= 24; break;
        case 25: value >>= 25; break;
        case 26: value >>= 26; break;
        case 27: value >>= 27; break;
        case 28: value >>= 28; break;
        case 29: value >>= 29; break;
        case 30: value >>= 30; break;
        case 31: value >>= 31; break;
        default: value = 0; break;
    }

    return value;
}

// Compiler handles this switch structure by indexing shift into a look-up BRA table
// so there is no performance cost to having all of the cases
long fast_rshift_long(long value, unsigned char shift)
{
    switch (shift) {
        case 0: break;
        case 1: value >>= 1; break;
        case 2: value >>= 2; break;
        case 3: value >>= 3; break;
        case 4: value >>= 4; break;
        case 5: value >>= 5; break;
        case 6: value >>= 6; break;
        case 7: value >>= 7; break;
        case 8: value >>= 8; break;
        case 9: value >>= 9; break;
        case 10: value >>= 10; break;
        case 11: value >>= 11; break;
        case 12: value >>= 12; break;
        case 13: value >>= 13; break;
        case 14: value >>= 14; break;
        case 15: value >>= 15; break;
        case 16: value >>= 16; break;
        case 17: value >>= 17; break;
        case 18: value >>= 18; break;
        case 19: value >>= 19; break;
        case 20: value >>= 20; break;
        case 21: value >>= 21; break;
        case 22: value >>= 22; break;
        case 23: value >>= 23; break;
        case 24: value >>= 24; break;
        case 25: value >>= 25; break;
        case 26: value >>= 26; break;
        case 27: value >>= 27; break;
        case 28: value >>= 28; break;
        case 29: value >>= 29; break;
        case 30: value >>= 30; break;
        case 31: value >>= 31; break;
        default: value = 0; break;
    }

    return value;
}

// Compiler handles this switch structure by indexing shift into a look-up BRA table
// so there is no performance cost to having all of the cases
unsigned fast_rshift_uint(unsigned value, unsigned char shift)
{
    switch (shift) {
        case 0: break;
        case 1: value >>= 1; break;
        case 2: value >>= 2; break;
        case 3: value >>= 3; break;
        case 4: value >>= 4; break;
        case 5: value >>= 5; break;
        case 6: value >>= 6; break;
        case 7: value >>= 7; break;
        case 8: value >>= 8; break;
        case 9: value >>= 9; break;
        case 10: value >>= 10; break;
        case 11: value >>= 11; break;
        case 12: value >>= 12; break;
        case 13: value >>= 13; break;
        case 14: value >>= 14; break;
        case 15: value >>= 15; break;
        default: value = 0; break;
    }

    return value;
}

// Compiler handles this switch structure by indexing shift into a look-up BRA table
// so there is no performance cost to having all of the cases
int fast_rshift_int(int value, unsigned char shift)
{
    switch (shift) {
        case 0: break;
        case 1: value >>= 1; break;
        case 2: value >>= 2; break;
        case 3: value >>= 3; break;
        case 4: value >>= 4; break;
        case 5: value >>= 5; break;
        case 6: value >>= 6; break;
        case 7: value >>= 7; break;
        case 8: value >>= 8; break;
        case 9: value >>= 9; break;
        case 10: value >>= 10; break;
        case 11: value >>= 11; break;
        case 12: value >>= 12; break;
        case 13: value >>= 13; break;
        case 14: value >>= 14; break;
        case 15: value >>= 15; break;
        default: value = 0; break;
    }

    return value;
}
