#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "common.h"
#include "tty_print.h"

#define DELIM " ,\t\r\n"			// delimeters
#define SPACE	0
#define WORD	1
static char *next_tok = NULL;	// Pointer to next token
static int num_args;			// Number of arguments in our current command string

// Returns the number of arguments in a string
int tok_count_args( char *p ) {
	int count = 0;
	if (*p == '\0')
		return 0;
	char state;

    // Trim off any leading spaces (must be spaces, not any delimeter)
	if (isspace(*p)) {
		state = SPACE;
		count = 0;
	} else {
		state = WORD;
		count = 1;
	}

    // Loop over the remaining words
	for (p = &(p[1]); *p; p++) {
        int isdelim = (strchr(DELIM, *p) != NULL);  // Will be 1 if *p is a delimeter

		if (isdelim && state == WORD)
			state = SPACE;
		else if (!isdelim && state == SPACE) {
			state = WORD;
			count++;
		}

        // Terminate the loop if we hit one of the line terminators
		if (*p == '\r' || *p == '\n' || *p == '\0')
			break;
	}
	return count;
}

/**
 * Subtracts n from the command argment count.
 * This is used to remove tokens from the count that shouldn't count.
 */
void tok_decrement_num_args( int n)
{
    num_args -= n;
}

/* Parses a list of channel arguments (numbers or strings). 
 *
 * num_ch_args   -- Number of arguments in the string that indicate channels
 *
 * RETURNS: a 16 bit integer where each bit represents a channel.
 *          0 indicates no channels were set, due to invalid channels in list
 */
unsigned short parse_channel_list( int num_ch_args ) {
	// Channel mask, set all to zero by default
	unsigned short ch_mask = 0;

	// Loop through arguments to read channel list
	int i;
	for (i = 0; i < num_ch_args; i++) {
		if (!tok_valid_uint( 2 ) ) {
			// If not a list of channels, could be a keyword
			char *str = tok_get_str();
			if ( !strcmp ( str, "all" ) )
				ch_mask = 0xFFFF;
			else if ( !strcmp ( str, "cal" ) )
				ch_mask |= 0xF000;
			else if ( !strcmp ( str, "therm" ) )
				ch_mask |= 0x0FFF;
			else
				return 0;
		} else {
			// Parse channel, add it to channel mask
			unsigned short ch = (unsigned short) tok_get_int16();
			if ( ch >= 16 )
				return 0;
			else
				ch_mask |= (1<<ch);	
		}
	}
	
	return ch_mask;
}

/*
 * Returns the number of tokens in the current command string
 */
int tok_get_num_args( void )
{
	return num_args;
}


// Parses a simple command string from the user
void cmd_parse( char *str )
{
	num_args = tok_count_args( str );
    next_tok = strtok( str, DELIM );
}


char *tok_get_str( void )
{
 char *result;
 result = next_tok;					// get the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}

unsigned char tok_get_uint8( void )
{
 short result;
 result = atoi( next_tok );			// convert the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return (unsigned char)result;
}

short tok_get_int16( void )
{
 short result;
 result = atoi( next_tok );			// convert the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}

long tok_get_int32( void )
{
 long result;
 result = atol( next_tok );			// convert the next token 
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}

float tok_get_float( void )
{
 float result;
 result = atof( next_tok );			// convert the next token
 next_tok = strtok( NULL, DELIM );	// get a new next token
 return result;
}


// Gets a board address from the token stream
// Accepts 1 or 2 hex digit address.  i.e. "A", "a", "0", "00", "1F"...
// Returns a number larger than 8 bits if the address is invalid
// TODO: this should be moved into bp_common folder (since backplane specific).
// Returns: 1 if the next token matches our card address
int tok_is_our_address(int card_address)
{
    int n = strlen(next_tok);

    unsigned char msn = 0, lsn = 0;

    if (n == 1) { // Single character address (the usual)
        lsn = parse_hex_digit(next_tok[0]);
    }
    else if (n == 2) { // 2 character address
        msn = parse_hex_digit(next_tok[0]);
        lsn = parse_hex_digit(next_tok[1]);
    }
    else if (n == 3) {  // 3 characters
        if (!strcmp(next_tok, "all"))
            return 1;   // All matchs all board addresses
        return 0;       // Invalid address does not match our address
    }
    else
        return 0;       // Invalid address does not match our address

    // Verify that both digits are valid (parse_hex_digit returns -1 for invalid)
    if (lsn <= 0xF && msn <= 0xF) {
        unsigned char addr = (msn << 4) + lsn; // Compute the address
        next_tok = strtok(NULL, DELIM); // get a new next token

        // Subtract one from the total token count
        // HACK!!! this is silly and ugly
        // Should refactor all "process_commands" so that we call arg count after processing board address
        num_args--;

        return addr == card_address;
    }
    else
        return 0; // Invalid address does not match our address
}



/*
 * Validate the next token as a number.
 * digits - the number of allowed digits (- and . don't count)
 * neg - 0 = no negative sign allowed, 1 = neg sign allowed as first char
 * dp  - 0 = no decimal point allowed, 1 = decimal point allowed anywhere
 */
char tok_valid_num( unsigned char digits, unsigned char neg, unsigned char dp )
{
 char c, *p = next_tok;
 unsigned char i;

 if( !next_tok )    return 0;       // no next token!

 for( i=0; i<digits; ++i ) {
    c = *p++;           // get the next character

    if( c < '0' || c > '9' )  {     // Oops, not a digit!
        if( c == 0 )            // too few digits is just fine
            return 1;

        if( i == 0 && c == '-' && neg ) {       // OK, it was a negative sign
            digits++;               // allow an extra digit, because it was neg
            continue;
        }

        if( c == '.' && dp )  {     // OK, it was a decimal point
            dp = 0;                 // only allow 1 decimal point per number
            digits++;               // allow an extra digit, because we found .
            continue;
        }

        return 0;               // not a valid number!
    }
 }

 c = *p;			// get the next character
 if( c != 0 )		// should be a NUL, since we used up all allowed digits
    return 0;       // there are too many digits in the number

 return 1;			// The number is valid!
}

/*
 * Validates the next token as an unsigned integer
 * Returns 1 if next token is a valid unsigned integer with fewer than "digits" digits
 */
char tok_valid_uint( char digits )
{
 return tok_valid_num( digits, 0, 0 );
}

/*
 * Is there a next token?
 */
char tok_available( void )
{
 return (next_tok != NULL);
}
