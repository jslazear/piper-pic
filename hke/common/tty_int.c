/* This is a TTY library using the TX interrupt of the PIC.  It works very well, but I didn't
   realize this model can't really support the frame buffer stuff needed for data packets.
   So I'm going back to the 4 kHz timer model for the PSyncADC board's TTY needs */

#include <p30F5011.h>
#include <uart.h>
#include <stdio.h>

#include "common.h"
#include "ptty.h"

char *txDataBufPtr = NULL;			// Pointer to data packet buffer to transmit

#define RX_QUEUE_LEN	128

#define PIN_LED				LATCbits.LATC1	// Front pannel LED


// Transmit queue
#define TX_QUEUE_LEN	512
static char tx_queue[ TX_QUEUE_LEN ];
static volatile int tx_head=0;		// Pos in queue buffer to read from (int_handler does reading)
static int tx_tail=0;				// Insertion point for new characters in queue buffer
 

void init_tty_ints()
{
	SetPriorityIntU1TX(1);			// Set TX interrupt to lowest possible priority
	U2STAbits.UTXISEL = 1;			// UTXISEL = 1 generates ints only when 4 byte hardware TX buffer is empty
	IEC1bits.U2TXIE = 1;			// Enable UART transmit interrups

	ConfigIntUART2( UART_RX_INT_DIS & UART_TX_INT_EN );	// Disable TX and RX interrups
}

/*
 * Returns how many characters are queued up to transmit at the next opportunity.
 */
int txQueueBytes()
{
 _INTERRUPTS_OFF;
 int n = tx_tail - tx_head;
 _INTERRUPTS_ON;

 if( n >= 0 )
	return n;
 else
	return n + TX_QUEUE_LEN;
}

/*
 * Put a new character in the *software* serial port transmit queue.
 * --->Call only from main-line code (*not* from int_handler)
 */
void txQueuePutc( char c )
{
	// If the queue is full, we block, waiting on int_handler to clear it out
 	while( txQueueBytes() >= TX_QUEUE_LEN-1 )  {
		;	// Spin our wheels waiting for int_handler to clear out queue
		PIN_LED = 1;
		Nop();  Nop();  Nop(); Nop();  Nop();
		PIN_LED = 0;
		Nop();  Nop();  Nop(); Nop();  Nop();
 	}

	// Queue not full, so enqueue our new character
    _INTERRUPTS_OFF;
	tx_queue[ tx_tail ] = c;
 	tx_tail++;
 	if( tx_tail >= TX_QUEUE_LEN )
		tx_tail = 0;
    _INTERRUPTS_ON;
}

/*
 * Get a character from the software TX QUEUE.
 * --->Call only from inside the interrupt handler.
 */
static char txQueueGetc()
{
	char c;

	// Check if the queue is empty
 	if( tx_head == tx_tail )
		return 0;

	// Get the character and update the head index
	c = tx_queue[ tx_head ];
	tx_head++;
	if( tx_head >= TX_QUEUE_LEN )
		tx_head = 0;
	return c;	
}

/*
 * Interrupt handler -- Gets called by the hardware when the 4 byte hardware TX queue is empty.
 * Checks if we have any characters in our software queues to transmit, and loads them in hardware queue to send
 */
void __attribute__((__interrupt__, __auto_psv__)) _U2TXInterrupt( void ) 
{
	IFS1bits.U2TXIF = 0;		// Clear the interrupt flag
	
	// Check if there is any Data Packet data to send
	if( txDataBufPtr != NULL ) {
		// U2STAbits.UTXBF == 1 implies hardware TX buffer is full
		while( !U2STAbits.UTXBF ) {			// Loop while the 4 byte hardware TX FIFO has space in it
			if( *txDataBufPtr )				// If we have another character to transmit
				U2TXREG = *txDataBufPtr++;  // Transfer data byte to TX FIFO
			else {							// Else, we are at end of string
				txDataBufPtr = NULL;		// Set the pointer to NULL
				return;
			}
		}
	}

	// Otherwise, see if there's any regular data (from txQueue)
	else {
		while( !U2STAbits.UTXBF ) {			// Loop while the 4 byte hardware TX FIFO has space in it
			if( txQueueBytes() > 0 )		// If we have another character to transmit
				U2TXREG = txQueueGetc();  	// Transfer data byte to hardware TX FIFO
			else							// No more characters in software TX queue
				return;
		}
	}
}


void TTYSendFrameBuf( char *buf )
{
	// Check if we are currently transmitting
	if( txDataBufPtr != NULL )
		return;

	// Save a pointer to the buffer to transmit
	txDataBufPtr = buf;
}

/*
 * Send a string over the UART
 */
void TTYPutsDirect( char *s )
{
	while( *s )  {
		while( BusyUART2() );	// Wait till not busy
		putcUART2( *s++ );
	}
	
}

/*
 * Send a character over the UART
 */
void TTYPutcDirect( char c )
{
	while( BusyUART2() );	// Wait till not busy
	putcUART2( c );
}

/*
 * Queue up a character to be transmitted during our talk slot.
 */
void TTYPutc( char c )
{
	// @@#@# FIX THIS @@#$
    _INTERRUPTS_OFF;
	if( !U2STAbits.UTXBF ) {	// If there's room in the hardware buffer
		U2TXREG = c;
   		_INTERRUPTS_ON;
		return;
	}

	_INTERRUPTS_ON;
	txQueuePutc( c );
}

/*
 * Queue up a string to be transmitted during our talk slot.
 */
void TTYPuts( char *s )
{
	while( *s )
		TTYPutc( *s++ );
}

