#ifndef __MATH_UTILITIES_INCLUDED
#define __MATH_UTILITIES_INCLUDED

// FUNCTION PROTOTYPES

// math_utilities.c
unsigned long fast_rshift_ulong(unsigned long value, unsigned char shift);
long fast_rshift_long(long value, unsigned char shift);
unsigned fast_rshift_uint(unsigned value, unsigned char shift);
int fast_rshift_int(int value, unsigned char shift);

#endif