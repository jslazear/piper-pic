#ifndef __COMMON_INCLUDED
#define __COMMON_INCLUDED

// Macros to turn interrupts on/off
#define _INTERRUPTS_OFF		asm volatile ("disi #0x3FFF")
#define _INTERRUPTS_ON		DISICNT = 0

// Allowable command return status values
#define	STATUS_OK		    0		// Everything is OK, not a failure
#define STATUS_FAIL_CMD	    1		// Unknown command
#define STATUS_FAIL_NARGS	2		// incorrect number of arguments
#define STATUS_FAIL_INVALID 3		// Invalid type of argument (was expecting a number, got "bob")
#define STATUS_FAIL_RANGE   4		// Argument is out of range
#define STATUS_FAIL_COULD_NOT_COMPLETE 5	// Board could not complete the requested command

// FUNCTION PROTOTYPES

// cmd_gets.c
void cmd_gets( char *str, int n );
void cmd_set_echo( char e );
char cmd_get_echo( void );
void OnIdle( void );            // Must be implemented for each project

// cmd_parse.c
void cmd_parse( char *str );
unsigned short parse_channel_list( int num_ch_args );
char tok_available( void );
char *tok_get_str( void );
unsigned char tok_get_uint8( void );
short tok_get_int16( void );
long tok_get_int32( void );
float tok_get_float( void );
char tok_valid_num( unsigned char digits, unsigned char neg, unsigned char dp );
char tok_valid_uint( char digits );
unsigned tok_get_address( void );
int tok_count_args( char *str );
void tok_decrement_num_args( int n );
int tok_get_num_args( void );

// common_functions.c
void print_status( char status );	// print out response messages

#endif