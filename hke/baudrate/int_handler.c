#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "baudrate.h"
#include "../common/ptty.h"
//#include "../common/psync_common.h"
//#include "../common/frame_buf.h"
#include "../common/tty_print.h"

static void update_sweep_prod(void);
static void update_sweep_sum(void);
static void update_data(void);
static void sweep_write(unsigned code, unsigned long dac_accum, unsigned long adc_accum);
static void data_write(unsigned long dac_accum, unsigned long adc_accum);
static void push_wb(void);
static void wb_clear(void);
static void update_serial_port(void);
static unsigned compute_pid(unsigned data);

// NOTES
//int_handler
//
//    Read the ADC
//    adc_sum += adc
//    update_serial_port() // write till hardware buffer full, do this while waiting for ADC read to finish
//    if( locked_ch >= 0 )
//        error = adc - set_point
//        dac[locked_ch] = locked_p * error + locked_i * accumulator
//        spi write to locked DAC channel
//        lock_dac_sum += dac[locked_ch]
//    if( sweep >= 0 )
//        sw_counts += sweep_delta
//        dac[sweep_dac] = sweep_counts
//        dac_dirty[sweep_dac] = 1;
//        check stop condition
//        Print some hex data (see below)
//        spi write to sweep DAC channel
//    else if( data )
//        if( n_sum == block_len )
//            print data ( adc_sum >> block_len_ln2, lock_dac_sum >> block_len_ln2 )
//            n_sum = 0, adc_sum = 0, lock_dac_sum = 0
//    ++dac_update_ch;  if(dac_update_ch > NUM_DACS) dac_update_ch = 0;  // for the DAC command
//    if( dac_dirty[ dac_update_ch ] )
//        spi write to dac_update_ch, set dirty = 0;

//#ifdef INTS_DEFINED //DEBUG

// ---------------------
// ----VARIABLE DEFS----
// ---------------------
static unsigned int tick = 0; // Incremented each time we get a timer2 interrupt

//struct pidparams {
//    int ch; // Specifies the locked register; -1 means none locked
//    unsigned p; // P term for PID
//    unsigned i; // I term for PID
//    unsigned int setpoint; // PID setpoint (demod units)
//    long accumulator; // PID integrator term accumulator
//    unsigned char i_shift; // I term is divided by 2^i_shift
//    unsigned char p_shift; // P term is divided by 2^p_shift
//    unsigned int dac_init; // Initial dac value
//};
//
//struct sweepparams {
//    // User-specified sweep parameters
//    int ch; // Channel to sweep; -1 means not in sweep mode
//    unsigned char read_ch; // Additional dac channel to report while sweeping
//    unsigned int low; // Start dac code for sweep
//    unsigned int high; // End dac code for sweep
//    unsigned int delta; // Step size for sweep
//    unsigned int dwell; // How many samples to collect at each sweep location
//    unsigned int ntimer; // Sample once every ntimer OC2 interrupts
//    unsigned int nsweeps; // Number of sweeps to perform
//
//    // Internal utility variables
//    unsigned int code; // Current dac code
//    unsigned int dwell_counter;
//    unsigned int ntimer_counter;
//    unsigned int nsweeps_counter;
//    unsigned char reverse; // Whether the sweep is ascending or descending
//    unsigned long dac_accum; // Read DAC accumulator
//    unsigned long adc_accum; // ADC accumulator
//};
//
//struct dataparams {
//    // User-specified data parameters
//    int ch; // Channel to sweep; -1 means not in sweep mode
//    unsigned int dwell; // How many samples to collect at each sweep location
//
//    // Internal utility variables
//    unsigned int dwell_counter;
//    unsigned long dac_accum; // Read DAC accumulator
//    unsigned long adc_accum; // ADC accumulator
//};
//
//struct writebuffer {
//    unsigned long values[3];  // Values to be written
//    char types[3];            // Mode (e.g. uint16, uint32) of each value
//    int index;               // Index in value array
//};
//
//static struct pidparams pid = {.ch = -1,
//                               .p = 1,
//                               .i = 1,
//                               .setpoint = 0,
//                               .accumulator = 0,
//                               .i_shift = 15,
//                               .p_shift = 8,
//                               .dac_init = 0};
//
//static struct sweepparams sweep = {.ch = -1,
//                                   .read_ch = 0,
//                                   .low = 0,
//                                   .high = 0,
//                                   .delta = 1,
//                                   .dwell = 1,
//                                   .ntimer = 0,
//                                   .nsweeps = 1,
//                                   .code = 0,
//                                   .dwell_counter = 0,
//                                   .ntimer_counter = 0,
//                                   .nsweeps_counter = 0,
//                                   .reverse = 0,
//                                   .dac_accum = 0,
//                                   .adc_accum = 0};
//
//static struct dataparams data = {.ch = -1,
//                                 .dwell = 1,
//                                 .dwell_counter = 0,
//                                 .dac_accum = 0,
//                                 .adc_accum = 0};
//
//static struct writebuffer wb = {.values = {0, 0, 0},
//                                .types = {WBTYPE_STOP, WBTYPE_STOP, WBTYPE_STOP},
//                                .index = 0};
//
//static unsigned int adc_value = 0x0000;
//static unsigned int new_dac = 0; // New DAC setting after PID loop/scheduled iteration
//
//static unsigned dac_values[9] = {0}; // Array of DAC values
//static int dac_dirty[9] = {1, 1, 1, 1, 1, 1, 1, 1, 1}; // Array of DAC update statuses
//static unsigned dac_dirty_values[9] = {0, 0, 0, 0, 0, 0, 0, 0, 16384}; // Array of to-be-updated DAC values
//
//static unsigned int dac_update_ch = 0; // The DAC channel that is scheduled to be updated


// ---------------------
// -----INT HANDLER-----
// ---------------------
void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void)
{
    IFS0bits.OC2IF = 0; // reset Output Compare 1 interrupt flag
    PIN_OSCOPE2 = 1;

    if (tick >= 0xff) {
        tick = 0;
    }

    TTYPutc(tick);
    TTYPutc(tick);
    TTYPutc(tick);
    TTYPutc(tick);

    TTYUpdateTXNoFrameFast();

//    //-----> ADC Read
//    // Do the actual SPI write/read
//    SPI2STATbits.SPIROV = 0; // Clear SPI receive overflow bit
//    SPI2BUF = adc_value; // Write to the SPI port
//
//    update_serial_port();  // Trickle out data to serial port while ADC is being read
//
//    while (!SPI2STATbits.SPIRBF) {
//    }; // Wait for data to be ready on SPI port
//    adc_value = SPI2BUF; // Read in data from ADC converter
//
//
//    // Update PID-controlled DAC
//    if (pid.ch >= 0) {
//        // Compute, store, and set new DAC value for locked channel
//        new_dac = compute_pid(adc_value);
//        dac_values[pid.ch] = new_dac;
//        write_dac(new_dac, pid.ch);
//        PIN_LED_LOCK = 1;
//    } else {
//        PIN_LED_LOCK = 0;
//    }
//
//    // Update sweep, if sweeping
//    if (sweep.ch >= 0) {
//        update_sweep_sum();
//        PIN_LED_SWEEP = 1;
//        PIN_LED_MISC = 0;
//    } else if (data.ch >= 0) {
//        update_data();
//        PIN_LED_SWEEP = 0;
//        PIN_LED_MISC = 1;
//    } else {
//        PIN_LED_SWEEP = 0;
//        PIN_LED_MISC = 0;
//    }
//
//    // Update scheduled DAC
//    if (dac_dirty[dac_update_ch]) {
//        new_dac = dac_dirty_values[dac_update_ch];
//        dac_values[dac_update_ch] = new_dac;
//
//        write_dac(new_dac, dac_update_ch);
//
//        dac_dirty[dac_update_ch] = 0;
//    }
//
//    // Iterate DAC schedule
//    dac_update_ch++;
//    if (dac_update_ch >= 9)
//        dac_update_ch = 0;

    tick++;

    PIN_OSCOPE2 = 0;
}

///* update_serial_port (private)
// *
// * Writes buffered data to the serial port.
// *
// * PSquid uses a double-buffering system. Data to be reported by is added
// * to the length 3 long array data buffer, wb.values. When it is convenient,
// * this function is called to convert to a string one of the values in the
// * data buffer and push it to the serial buffer.
// *
// * This function iterates through the data buffer in order. The conversion
// * routine is specified by the types value. See WBTYPE_* macros for details.
// */
//static void update_serial_port(void)
//{
//    // Check current index's type, if WBTYPE_NONE, do nothing!
//    if (wb.types[wb.index] != WBTYPE_STOP) {
//
//        // If not at the beginning of the buffer, add a space
//        if ((wb.index > 0) && (wb.types[wb.index] != WBTYPE_NONE)) {
//            TTYPutc(' ');
//        }
//
//        // Figure out what type the value is, write appropriate type to serial port
//        switch (wb.types[wb.index]) {
//        case WBTYPE_UINT16:
//            TTYPrintHex16((unsigned)wb.values[wb.index]);
//            break;
//        case WBTYPE_UINT32:
//            TTYPrintHex32(wb.values[wb.index]);
//            break;
//        case WBTYPE_NONE:  // TODO: None and DoNothing is confusing, make only one!
//            break;
//        }
//
//        // Clear buffer at index
//        wb.types[wb.index] = WBTYPE_STOP;
//        wb.values[wb.index] = 0;
//
//        // If at end of buffer, write \r\n
//        if (wb.index >= 2) {
//            TTYPutc('\r');
//            TTYPutc('\n');
//            wb.index = 0;
//        } else {  // Otherwise step forward in buffer
//            wb.index += 1;
//        }
//    }
//}
//
///* push_wb (private)
// *
// * Writes buffered data to the serial port until the write buffer is empty.
// *
// * PSquid uses a double-buffering system. Data to be reported by is added
// * to the length 3 long array data buffer, wb.values. When it is convenient,
// * this function is called to convert to a string one of the values in the
// * data buffer and push it to the serial buffer.
// *
// * This function iterates through the data buffer in order. The conversion
// * routine is specified by the types value. See WBTYPE_* macros for details.
// */
//static void push_wb(void)
//{
//    // Check current index's type, if WBTYPE_NONE, do nothing!
//    while (wb.types[wb.index] != WBTYPE_STOP) {
//
//        // If not at the beginning of the buffer, add a space
//        if ((wb.index > 0) && (wb.types[wb.index] != WBTYPE_NONE)) {
//            TTYPutc(' ');
//        }
//
//        // Figure out what type the value is, write appropriate type to serial port
//        switch (wb.types[wb.index]) {
//        case WBTYPE_UINT16:
//            TTYPrintHex16((unsigned)wb.values[wb.index]);
//            break;
//        case WBTYPE_UINT32:
//            TTYPrintHex32(wb.values[wb.index]);
//            break;
//        case WBTYPE_NONE:  // TODO: None and DoNothing is confusing, make only one!
//            break;
//        }
//
//        // Clear buffer at index
//        wb.types[wb.index] = WBTYPE_STOP;
//        wb.values[wb.index] = 0;
//
//        // If at end of buffer, write \r\n
//        if (wb.index >= 2) {
//            TTYPutc('\r');
//            TTYPutc('\n');
//            wb.index = 0;
//        } else {  // Otherwise step forward in buffer
//            wb.index += 1;
//        }
//    }
//}
//
///* update_sweep_prod (private)
// *
// * Updates the sweep, if enabled, in the OC2 interrupt handler.
// * Step period = ntimer*dwell in units of OC2 interrupt counts
// * See update_sweep_sum for version where (step period) = ntimer + dwell.
// *
// * 1) Write DAC
// * 2) Report data every ntimer*dwell
// * 3) Check stop condition
// * 4) If (not stop), increment code
// */
//static void update_sweep_prod(void)
//{
//    if (sweep.ntimer_counter == 0) {
//        // --(1) Write DAC only on first cycle in step--
//        dac_values[sweep.ch] = sweep.code;
//        write_dac(sweep.code, sweep.ch);
//    }
//
//    if (sweep.ntimer_counter + 1 >= sweep.ntimer) {
//        sweep.ntimer_counter = 0;
//
//        sweep.dac_accum += dac_values[sweep.read_ch];
//        sweep.adc_accum += adc_value;
//
//        if (sweep.dwell_counter + 1 >= sweep.dwell) {
//            // --(2) Report data every ntimer*dwell--
//            sweep_write(sweep.code, sweep.dac_accum, sweep.adc_accum);
//            sweep.dwell_counter = 0;
//            sweep.dac_accum = 0;
//            sweep.adc_accum = 0;
//
//            if (!sweep.reverse) {
//                // --(3) Check stop condition--
//                // Must not add delta to code before check -- will overflow and
//                // wrongly continue if (end + delta) >= 2^16!
//                if (sweep.high - sweep.code < sweep.delta) {
//                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
//                        sweep.nsweeps_counter = 0;
//                        sweep.ch = -1;
//
////                        PIN_OSCOPE3 = 1; //DEBUG
////                        PIN_OSCOPE3 = 0; //DEBUG
//                    } else { // --(4) If (not stop), increment code--
//                        sweep.nsweeps_counter += 1;
//                        sweep.code = sweep.low;
//                    }
//                } else {
//                    sweep.code += sweep.delta;
//                }
//            } else {
//                // --(3) Check stop condition--
//                // Must not add delta to code before check -- will overflow and
//                // wrongly continue if (end + delta) >= 2^16!
//                if (sweep.code - sweep.low < sweep.delta) {
//                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
//                        sweep.nsweeps_counter = 0;
//                        sweep.ch = -1;
//
////                        PIN_OSCOPE3 = 1; //DEBUG
////                        PIN_OSCOPE3 = 0; //DEBUG
//                    } else { // --(4) If (not stop), increment code--
//                        sweep.nsweeps_counter += 1;
//                        sweep.code = sweep.high;
//                    }
//                } else {
//                    sweep.code -= sweep.delta;
//                }
//            }
//        } else {
//            sweep.dwell_counter += 1;
//        }
//    } else {
//        sweep.ntimer_counter += 1;
//    }
//}
//
///* update_sweep_sum (private)
// *
// * Updates the sweep, if enabled, in the OC2 interrupt handler.
// * Step period = ntimer + dwell in units of OC2 interrupt counts
// * See update_sweep_prod for version where (step period) = ntimer*dwell.
// *
// * 1) Write DAC
// * 2) Report data every ntimer*dwell
// * 3) Check stop condition
// * 4) If (not stop), increment code
// */
//static void update_sweep_sum(void)
//{
//    if (sweep.ntimer_counter == 0) {
//        // --(1) Write DAC only on first cycle in step--
//        dac_values[sweep.ch] = sweep.code;
//        write_dac(sweep.code, sweep.ch);
//    }
//
//    if (sweep.ntimer_counter >= sweep.ntimer) {
//        sweep.dac_accum += dac_values[sweep.read_ch];
//        sweep.adc_accum += adc_value;
//
//
//        if (sweep.dwell_counter + 1 >= sweep.dwell) {
//            // --(2) Report data every ntimer*dwell--
//            sweep_write(sweep.code, sweep.dac_accum, sweep.adc_accum);
//            sweep.ntimer_counter = 0;
//            sweep.dwell_counter = 0;
//            sweep.dac_accum = 0;
//            sweep.adc_accum = 0;
//
//            if (!sweep.reverse) {
//                // --(3) Check stop condition--
//                // Must not add delta to code before check -- will overflow and
//                // wrongly continue if (end + delta) >= 2^16!
//                if (sweep.high - sweep.code < sweep.delta) {
//                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
//                        sweep.nsweeps_counter = 0;
//                        sweep.ch = -1;
//
//                        // Send termination string at end of sweep
//                        push_wb();  // Ensure write buffer has been cleared
//                        TTYPuts("*PSQUID: sweep_off\r\nOK!\r\n");
//                    } else { // --(4) If (not stop), increment code--
//                        sweep.nsweeps_counter += 1;
//                        sweep.code = sweep.low;
//                    }
//                } else {
//                    sweep.code += sweep.delta;
//                }
//            } else {
//                // --(3) Check stop condition--
//                // Must not add delta to code before check -- will overflow and
//                // wrongly continue if (end + delta) >= 2^16!
//                if (sweep.code - sweep.low < sweep.delta) {
//                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
//                        sweep.nsweeps_counter = 0;
//                        sweep.ch = -1;
//
//                        // Send termination string at end of sweep
//                        push_wb();  // Ensure write buffer has been cleared
//                        TTYPuts("*PSQUID: sweep_off\r\nOK!\r\n");
//                    } else { // --(4) If (not stop), increment code--
//                        sweep.nsweeps_counter += 1;
//                        sweep.code = sweep.high;
//                    }
//                } else {
//                    sweep.code -= sweep.delta;
//                }
//            }
//        } else {
//            sweep.dwell_counter += 1;
//        }
//    } else {
//        sweep.ntimer_counter += 1;
//    }
//}
//
///* sweep_write (private)
// *
// * Writes sweep data to the data buffer for trickling out the serial port.
// */
//static void sweep_write(unsigned code, unsigned long dac_accum, unsigned long adc_accum)
//{
//    wb.values[0] = (unsigned long)code;
//    wb.values[1] = dac_accum;
//    wb.values[2] = adc_accum;
//
//    wb.types[0] = WBTYPE_UINT16;
//    wb.types[1] = WBTYPE_UINT32;
//    wb.types[2] = WBTYPE_UINT32;
//
//    wb.index = 0;
//}
//
///* update_data (private)
// *
// * Updates the data reporting, if enabled, in the OC2 interrupt handler.
// *
// * 2) Accumulate data every cycle
// * 1) Report data every dwell cycles
// */
//static void update_data(void)
//{
//    // --(1) Accumulate data every cycle--
//    data.dac_accum += dac_values[data.ch];
//    data.adc_accum += adc_value;
//
//    if (data.dwell_counter + 1 >= data.dwell) {
//        // --(2) Report data every dwell cycles--
//        data_write(data.dac_accum, data.adc_accum);
//        data.dwell_counter = 0;
//        data.dac_accum = 0;
//        data.adc_accum = 0;
//    } else {
//        data.dwell_counter += 1;
//    }
//}
//
///* data_write (private)
// *
// * Writes data data to the data buffer for trickling out the serial port.
// *
// * Only uses last 2 elements of data buffer array.
// */
//static void data_write(unsigned long dac_accum, unsigned long adc_accum)
//{
//    wb.values[0] = dac_accum;
//    wb.values[1] = adc_accum;
//
//    wb.types[0] = WBTYPE_UINT32;
//    wb.types[1] = WBTYPE_UINT32;
//    wb.types[2] = WBTYPE_NONE;
//
//    wb.index = 0;
//}
//
///* wb_clear (private)
// *
// * Clears the write buffer
// */
//static void wb_clear(void)
//{
//    wb.values[0] = 0;
//    wb.values[1] = 0;
//    wb.values[2] = 0;
//
//    wb.types[0] = WBTYPE_STOP;
//    wb.types[1] = WBTYPE_STOP;
//    wb.types[2] = WBTYPE_STOP;
//
//    wb.index = 0;
//}
//
//static unsigned compute_pid(unsigned data)
//{
//    static long error;
//    static long pterm;
//    static long sum;
//    static unsigned toret;
//
//    // error = int17; may want to think about throwing away 1 bit to get down to int16
//    // Will need to look at disassembly code to figure out if worthwhile
//    error = (long)data - (long)pid.setpoint;
//
//    // pid_i*error should be a 16x16 = 32 multiply; check disassembly
//    // default assembly code is not at all optimized...
//    pid.accumulator += pid.i*error;
//
//
//    // pid_p*error should be a 16x16=32 multiply; check disassembly
//    // default assembly code is not at all optimized...
//    pterm = (pid.p * error) >> pid.p_shift;
//
//    // Coerce to unsigned 16-bit integer
//    // PID algorithm should naturally fall into range, unless
//    // specified set point is impossible to achieve, in which case
//    // the controller will rail
//    sum = pid.dac_init + pterm + (pid.accumulator >> pid.i_shift);
//    if (sum <= 0)
//        toret = 0;
//    else if (sum >= 65535)
//        toret = 65535;
//    else
//        toret = (unsigned)sum;
//
////    sprintf(buf, "e: %08lx - p: %08lx - i: %08lx - r: %08lx\r\n", error, pterm, pid.accumulator, toret);  //DEBUG
////    TTYPuts(buf);  //DEBUG
//
////    if (!(tick & 0x00f)) {  //DEBUG
////    TTYPuts("e: ");  //DEBUG
////    TTYPrintHex32(error);  //DEBUG
////    TTYPuts(" - p: ");  //DEBUG
////    TTYPrintHex32(pterm);  //DEBUG
////    TTYPuts(" - i: ");  //DEBUG
////    TTYPrintHex32(pid.accumulator);  //DEBUG
////    TTYPuts(" - r: ");  //DEBUG
////    TTYPrintHex16(toret);  //DEBUG
////    TTYPuts("\r\n");  //DEBUG
////    }
//
//    return toret;
//}
//
//
//
//// ---------------------
//// ----SET FUNCTIONS----
//// ---------------------
//
///* set_dac
// *
// * Signals for a DAC to be set to a specified code.
// *
// * Signals the OC2 interrupt handler that the DAC specified by ch
// * should be changed to the code specified by val. Does not actually
// * update the DAC. The OC2 interrupt will update the DAC according
// * to its pre-defined schedule.
// *
// * Does not report back anything over the serial port.
// *
// * val - Code to which the specified DAC should be set to
// * ch  - Specifies the DAC to be changed (see table below)
// *
// * ch - DAC
// * 0  - S1 Bias (aka Row Select)
// * 1  - S2 Bias
// * 2  - S3 Bias (aka Series Array Bias)
// * 3  - Detector Bias
// * 4  - S1 Feedback
// * 5  - S2 Feedback
// * 6  - S3 Feedback (aka Series Array Feedback)
// * 7  - S3 Offset (aka Series Array Offset)
// * 8  - S3 Gain (aka Series Array Gain)
// */
//void set_dac(unsigned val, int ch)
//{
//    dac_dirty[ch] = 1;
//    dac_dirty_values[ch] = val;
//}
//
///* set_pid
// *
// * Signals for a DAC to be PID-controlled to a specified setpoint.
// *
// * Signals the OC2 interrupt handler that the DAC specified by ch
// * should be PID controlled to the specified setpoint. All parameters
// * are in DAC units, if relevant.
// *
// * ch       - DAC channel to be swept
// * p        - proportional parameter of PID loop
// * i        - integral parameter of PID loop
// * setpoint - value to which specified DAC should be controlled
// * i_shift  - log_2(divisor) on integral contribution to sum
// * p_shift  - log_2(divisor) on proportional contribution to sum
// */
//void set_pid(int ch, unsigned p, unsigned i, unsigned setpoint,
//             unsigned char i_shift, unsigned char p_shift)
//{
//    pid.p = p;
//    pid.i = i;
//    pid.setpoint = setpoint;
//    pid.i_shift = i_shift;
//    pid.p_shift = p_shift;
//
//    pid.dac_init = dac_values[ch];
//    pid.accumulator = 0;
//
//    pid.ch = ch;  // Set this last to prevent PID loop from starting prematurely
//}
//
///* set_pid_ch
// *
// * Adjusts the current PID loop to have a new channel
// *
// * Does not change any other PID parameters. Does not re-initialize the PID
// * loop.
// *
// * Note that this command may be used to enable/disable the PID loop without
// * changing the PID parameters, since ch = -1 indicates the the PID loop is
// * off.
// *
// * ch - DAC channel which should be actuated
// */
//void set_pid_ch(int ch)
//{
//    pid.ch = ch;
//}
//
///* set_pid_setpoint
// *
// * Adjusts the current PID loop to have a new setpoint
// *
// * Does not change any other PID parameters. Does not re-initialize the PID
// * loop.
// *
// * setpoint - value to which specified DAC should be controlled
// */
//void set_pid_setpoint(unsigned setpoint)
//{
//    pid.setpoint = setpoint;
//}
//
///* set_pid_p
// *
// * Adjusts the current PID loop to have a new p constant
// *
// * Does not change any other PID parameters. Does not re-initialize the PID
// * loop.
// *
// * p - value to which specified DAC should be controlled
// */
//
//void set_pid_p(unsigned p)
//{
//    pid.p = p;
//}
//
///* set_pid_p_shift
// *
// * Changes the fixed-point basis of the proportional term of the current PID loop
// *
// * Does not change any other PID parameters. Does not re-initialize the PID
// * loop.
// *
// * p_shift - value to which specified DAC should be controlled
// */
//
//void set_pid_p_shift(unsigned p_shift)
//{
//    pid.p_shift = p_shift;
//}
//
//
///* set_pid_i
// *
// * Adjusts the current PID loop to have a new i constant
// *
// * Does not change any other PID parameters. Does not re-initialize the PID
// * loop.
// *
// * i - value to which specified DAC should be controlled
// */
//
//void set_pid_i(unsigned i)
//{
//    pid.i = i;
//}
//
///* set_pid_i_shift
// *
// * Changes the fixed-point basis of the integral term of the current PID loop
// *
// * Does not change any other PID parameters. Does not re-initialize the PID
// * loop. Does attempt to compensate the accumulator for the new scaling
// * factor so that there is not too large a discontinuity.
// *
// * i_shift - value to which specified DAC should be controlled
// */
//
//void set_pid_i_shift(unsigned i_shift)
//{
//    int diff = (int)pid.i_shift - (int)i_shift;
//    if (diff >=0) {
//        pid.accumulator = pid.accumulator >> diff;
//    } else {
//        pid.accumulator = pid.accumulator << -diff;
//    }
//
//    pid.i_shift = i_shift;
//}
//
///* set_sweep
// *
// * Initialize a sweep and signal for it to begin.
// *
// * Begins a sweep of DAC identified by ch. Steps the DAC code through
// * the range [low, high] using the step size delta. Sits at each step
// * location for dwell ADC samplings. Samples the ADC every ntimer
// * OC2 interrupts. Performs nsweeps of these sweeps, in series.
// *
// * Reports back over the serial port the triplets
// * (dac_sweep, dac_read, adc_value) every time an ADC value is recorded,
// * i.e. every ntimer OC2 cycles.
// *
// * Note that the sweep may be ascending or descending in DAC code. This
// * property depends solely on the sign of delta, not on the relative
// * values of low and high. While delta is a long, it will be coerced into
// * the range [-UINT16_MAX, UINT16_MAX], i.e. [-65535, 65535].
// *
// * The values of low and high are sorted in the function, so may have
// * low > high without consequence. Note that low > high will not reverse
// * the orientation of the sweep.
// *
// * ch      - DAC channel to be swept
// * read_ch - Extra DAC channel to be read and reported
// * low     - Lower of the DAC values in sweep range
// * high    - Higher of the DAC values in sweep range
// * delta   - Step size of sweep
// * dwell   - Number of times to sample at each step
// * ntimer  - ADC is sampled once every ntimer OC2 interrupts
// * nsweeps - Number of times to perform the sweep
// */
//void set_sweep(int ch, unsigned char read_ch, unsigned int low,
//        unsigned int high, long delta, unsigned int dwell,
//        unsigned int ntimer, unsigned int nsweeps)
//{
//    // Disable data reporting if active
//    if (data.ch >= 0) {
//        data.ch = -1;
//    }
//
//    if (high < low) {
//        unsigned temp = low;
//        low = high;
//        high = temp;
//    }
//
//    sweep.read_ch = read_ch;
//    sweep.low = low;
//    sweep.high = high;
//    sweep.dwell = dwell;
//    sweep.ntimer = ntimer;
//    sweep.nsweeps = nsweeps;
//
//    sweep.dwell_counter = 0;
//    sweep.nsweeps_counter = 0;
//    sweep.ntimer_counter = 0;
//    sweep.dac_accum = 0;
//    sweep.adc_accum = 0;
//
//    if (delta == 0)
//        delta = 1;
//
//    if (delta > 0) {
//        sweep.reverse = 0;
//        sweep.code = sweep.low;
//    } else if (delta < 0) {
//        delta = -delta;
//        sweep.reverse = 1;
//        sweep.code = sweep.high;
//    }
//
//    if (delta > 65535)
//        delta = 65535;
//    sweep.delta = (int) delta;
//
//    wb_clear();  // Clear write buffer to ensure no leftover data
//    sweep.ch = ch;  // Set last to prevent sweep from starting prematurely
//}
//
///* set_data
// *
// * Initializes and signals that data reporting should begin
// *
// * Begins reporting data in (dac, adc) pairs.
// *
// * The ch specifies which DAC should be reported back. This is normally
// * the locked DAC, since otherwise there is nothing changing the DAC value.
// *
// * The data is accumulated dwell times before being reported.
// *
// * ch      - DAC channel to be reported
// * dwell   - Number of times to accumulate before reporting
// */
//void set_data(int ch, unsigned int dwell)
//{
//    // Disable sweeping if active
//    if (sweep.ch >= 0) {
//        sweep.ch = -1;
//    }
//
//    data.dwell = dwell;
//    data.dwell_counter = 0;
//
//    data.dac_accum = 0;
//    data.adc_accum = 0;
//
//    wb_clear();  // Clear write buffer to ensure no leftover data
//    data.ch = ch;  // Set last to prevent data from starting prematurely
//}
//
//// ---------------------
//// ----GET FUNCTIONS----
//// ---------------------
//unsigned int get_adc_value(void)
//{
//    return adc_value;
//}
//
//unsigned int get_tick(void)
//{
//    return tick;
//}
//
//int get_pid_ch(void)
//{
//    return pid.ch;
//}
//
//unsigned int get_pid_s(void)
//{
//    return pid.setpoint;
//}
//
//unsigned int get_pid_p(void)
//{
//    return pid.p;
//}
//
//unsigned int get_pid_i(void)
//{
//    return pid.i;
//}
//
//unsigned char get_pid_p_shift(void)
//{
//    return pid.p_shift;
//}
//
//unsigned char get_pid_i_shift(void)
//{
//    return pid.i_shift;
//}
//
//unsigned char get_silent_flag(void)
//{
//    if (sweep.ch >= 0 || data.ch >= 0) {
//        return 1;
//    } else {
//        return 0;
//    }
//}
//
////#endif //DEBUG