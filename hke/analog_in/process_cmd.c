#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "analog_in.h"
#include "version.h"        // Computer-generated version string (includes compile time and user name)

// Local function prototypes
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_data( void );	// turn data stream on or off
static char do_cmd_p( void );		// prints adc readings for all channels
static char do_cmd_echo( void );	// changes echo on / echo off
static char do_cmd_version(void);	// prints out our software version
static char do_cmd_addr( void );	// prints out card address
static char do_cmd_test( void );	// test command

#define CMP	!strcmp

/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */
void process_cmd( char *cmd_string )
{
	char *s;
	unsigned char r;
	
    // Parse command string and check address to see if it's for us
    s = bp_parse_cmd_string(cmd_string);
    if( s == NULL )     // Command is not for us
        return;
	
	if( CMP( s, "led" ))		r = do_cmd_led();		// set the LED
	else if( CMP( s, "addr" ))	r = do_cmd_addr();		// read in hex address
	else if( CMP( s, "data" ))	r = do_cmd_data();		// turn data stream on or off
	else if( CMP( s, "p" ))		r = do_cmd_p();			// prints adc voltages
	else if( CMP( s, "t" ))		r = do_cmd_test();		// prints test pattern
	else if( CMP( s, "echo" ))	r = do_cmd_echo();		// Sets echo on or off
	else if( CMP( s, "ver" ))	r = do_cmd_version();	// prints software version
	else 						r = STATUS_FAIL_CMD;	// User typed unknown command
	
	print_status( r );
}


/*----------------------------------------------------------------------
 * Standard Commands
 *----------------------------------------------------------------------*/

/** ADDR
 *	Prints hex address of the board.
 *
 *	usage:	
 *	No arguments
 */
static char do_cmd_addr( void )
{
	unsigned char addr = get_addr();
	char str[16];
	sprintf( str, "%x\r\n", addr );
	TTYPuts( str );
	return STATUS_OK;
}

/** DATA
 *	Turns on/off data stream.
 *
 *	usage:
 *	data on		-> Turns data stream on
 *	data off	-> Turns data stream off
 */
static char do_cmd_data( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		set_data_on(1); // turn data stream on
	else if( CMP( arg, "off" ) )
		set_data_on(0); // turn data stream off
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/** P
 *	Prints a table of ADC data
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_p( void )
{
	int i;
	char str[100];	// max string len is about 51 chars, so 100 is very safe
	char header[100];

	if (PIN_BOARD_MODE == 1)
		sprintf( header, "\nCH    ADC  T [K]      CH    ADC  T [K]\r\n" );
	else
		sprintf( header, "\nCH    ADC     MV      CH    ADC     MV\r\n" );

	TTYPuts(header);
	
	for( i=0; i<16; ++i )  {
		unsigned int adc1, adc2;
		int mv1, mv2;
		float temp1, temp2;

		adc1 = read_adc(i);
		adc2 = read_adc(i + 16);

		if (PIN_BOARD_MODE == 1)
		{
			temp1 = ADC_TO_TEMP( adc1 );
			temp2 = ADC_TO_TEMP( adc2 );

			sprintf( str, "%2d %6u %6.1f      %2d %6u %6.1f\r\n",  i, adc1, temp1, i+16, adc2, temp2 );
		}
		else
		{
			mv1 = ADC_TO_MV( adc1 );
			mv2 = ADC_TO_MV( adc2 );

			sprintf( str, "%2d %6u %6d      %2d %6u %6d\r\n",  i, adc1, mv1, i+16, adc2, mv2 );
		}

		TTYPuts(str);
	}

	return STATUS_OK;
}


/** ECHO
 *	Sets echo on or off.
 *
 *	usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		cmd_set_echo( 1 );
	else if( CMP( arg, "off" ) )
		cmd_set_echo( 0 );
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;

}

/** VER
 *	Spits out a version string for the current code.
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_version( void )
{
	if (PIN_BOARD_MODE == 1)
		TTYPuts("ANALOG IN (AD590)  ");
	if (PIN_BOARD_MODE == 0)
		TTYPuts("ANALOG IN (STANDARD)  ");

    bp_print_version_info(VERSION_STRING);
    
	return STATUS_OK;
}

/** LED
 *	Set LED light.
 *
 *	usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led( void )
{
	char *arg;
	
	// Check that the next token is a valid number
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	arg = tok_get_str();
	if( CMP( arg, "1" ) )
		PIN_LED = 1;
	else if( CMP( arg, "0" ) )
		PIN_LED = 0;
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/*----------------------------------------------------------------------
 * Diagnostic / Test Commands
 *----------------------------------------------------------------------*/

/** T
 *	Prints a table of test data
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_test( void )
{
	int i;
	char str[100];	// max string len is about 51 chars, so 100 is very safe
	
	for( i=0; i<31; ++i )  {
		short adc1, adc2;

		adc1 = read_adc(i);
		adc2 = read_adc(i+1);

		sprintf( str, "%6d\r\n", adc1 /*- adc2*/ );
		TTYPuts(str);
	}

	return STATUS_OK;
}
