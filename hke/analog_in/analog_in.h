#ifndef __ANALOG_IN_INCLUDED
#define __ANALOG_IN_INCLUDED

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

/*             ------------------------     |---------------------
               |                      |     |
 ADC           |                      |     |
 READOUT       |                      |     |
               |                      |     |
          -----|                      |-----|


          |----|----------------------|---------------------------|
          0    A                      B
*/

// Constants used for timing/demodulation. Refer to timing diagram above.
															// Has to be 125 for a 4 kHz sample clock.  4000/32 channels = 125
#ifdef PIPER
    #define N_SAMPLE_TICKS          64  // "Live" time where we accumulate (should be power of 2) (B-A)
    #define N_SAMPLE_TICKS_LOG2     6   // Log_2 of N_Sample ticks (used for bit shifting)
    #define N_PERIOD_TICKS          125 // Number of ticks between 0 and B, total period per channel
#endif

#ifdef BETTII
    #define N_SAMPLE_TICKS          64
    #define N_SAMPLE_TICKS_LOG2     6
    #define N_PERIOD_TICKS          128
#endif

#define N_DEAD_TICKS    (N_PERIOD_TICKS-N_SAMPLE_TICKS)	// "Dead" time where we don't accumulate (wait for filter to settle) (0-A)
#define N_CHANNELS                  32

#define READINGS_PER_FRAME          32			// Number of ADC readings stored in a data frame

// Sanity checks on the timing constants, may catch some dumb errors
#if (N_PERIOD_TICKS*N_CHANNELS) != TICKS_PER_FRAME
    #error "Analog-In Invalid timing constant definitions"
#endif

















//---------> Some useful macros

// AD590 mode, V_adc [mV] = ( 4096 mV / 65536 ) * ADC
//#define ADC_TO_MV(x) (x>>4)
		
// Standard mode, V_adc = ADC * 20480 mV / 65536 = ADC * 10 / 2^5
#define ADC_TO_MV(x) (((x>>4)*5)-10240)

//#define MV_TO_ADC(x) (((short)(x)/10)<<5)

#define ADC_TO_TEMP(x) (((x>>5)+2048)/10.0)		// AD590 mode, T[K] = ( ADC / 320 ) + 204.8K

// Constants

// Definitions

//-----> OUTPUT PINS
#define PIN_LED			LATCbits.LATC1	// Front pannel LED

#define PIN_ADC_MUX_A0		LATBbits.LATB10	// 32:1 MUX in front of ADC converter
#define PIN_ADC_MUX_A1		LATBbits.LATB9
#define PIN_ADC_MUX_A2		LATBbits.LATB8
#define PIN_ADC_MUX_A3		LATBbits.LATB11
#define PIN_ADC_MUX_A4		LATBbits.LATB12

#define PIN_CONVERT			LATDbits.LATD1	// ADC convert pin
#define PIN_485_ENABLE		LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

#define PIN_OSCOPE1		LATDbits.LATD4	// Debug pins for looking at with scope
#define PIN_OSCOPE2		LATGbits.LATG15

//------> INPUT PINS
#define PIN_ADDR0		PORTFbits.RF1
#define PIN_ADDR1		PORTGbits.RG1
#define PIN_ADDR2		PORTGbits.RG0
#define PIN_ADDR3		PORTFbits.RF0
#define PIN_SYNC_CLK		PORTDbits.RD2
#define PIN_FRAME_CLK		PORTDbits.RD3

#define PIN_BOARD_MODE		PORTGbits.RG13



//---------> Function Prototypes
// analog_in.C
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// int_handler.c
void int_handler( void );	// interrupt handler routine
short read_adc( int ch );
void set_data_on( unsigned char state );


// process_cmd.c
void process_cmd( char *str );

//do_cmd.c
void write_amux( unsigned short ch );
unsigned char get_addr( void );
void OnIdle( void );	// Called when program has nothing to do

#endif	// __ANALOG_IN_INCLUDED
