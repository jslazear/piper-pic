#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>

#include "analog_in.h"

// Local functions for the interrupt handler
static void update_spi_and_sum();
static void update_frame(void);
static void go_to_next_channel(void);

// Local variables for the interrupt handler
static unsigned int tick_ch = 0; // Increments on each interrupt, resets at end of a reading (32 Hz)
static long sum = 0; // The accumulator for reading out an ADC (accumulates N_SAMPLE_TICKS readings)
static unsigned char frame_counter = 0; // Which frame are we on?,

// Variables used to communicate with the outside world
static volatile unsigned char data_on = 1; // default: data stream is on
static volatile unsigned char channel = 0; // start at channel 0

static volatile unsigned int adc_readings[32]; // Most recent reading for each channel (used for "p" command)
static volatile char readings_in_frame = 0; // How many readings are stored in the current frame

/*
 * Interrupt handler, called at 4 kHz, on the *falling* edge of the convert pulse
 */
void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void)
{
    PIN_OSCOPE2 = 1;

    IFS0bits.OC2IF = 0; // reset Output Compare 2 interrupt flag

    update_spi_and_sum(); // Reads in the ADC, writes to the DACs, sums up ADC readings into a demod value
    update_frame();         // writes a new reading (if ready) to our data frame
    go_to_next_channel();   // sets mux to next channel, *must* call after update_frame()

    ++tick_ch;
    if( tick_ch >= N_PERIOD_TICKS ) {
        tick_ch = 0;
    }
    
    // Check if a new frame has started by reading in PIN_FRAME_CLK
    char isNewFrame = check_frame_clock();

    // If we have just started a new frame, then reset all our state variables (happens once per second)
    if (isNewFrame) {
        tick_ch = 0;
        sum = 0;
        channel = 0;
        readings_in_frame = 0;
    }

    // Update address counters and transmit buffer in our time slot (does not look at tick_ch)
    update_communication(isNewFrame);

    PIN_OSCOPE2 = 0;
}


//---------------------------------------
// Read / Write to the SPI port
// Add the newest ADC reading to the global "sum" variable
// Called once per tick
//---------------------------------------

static void update_spi_and_sum()
{
    unsigned int data = 0x0000;

    // Read in the ADC
    SPI2STATbits.SPIROV = 0; // Clear SPI receive overflow bit
    SPI2BUF = data; // Write to the SPI port
    while (!SPI2STATbits.SPIRBF); // Wait for data to be ready on SPI port
    data = SPI2BUF; // Read in data from ADC converter

    // Do the accumulation (we average a bunch of readings for lower noise)
    if (tick_ch >= N_DEAD_TICKS && tick_ch < N_PERIOD_TICKS) {
        //unsigned int adc_value_signed = (short)(data - 32767U);
        //sum += adc_value_signed;
        sum += data;
    }
}

static void update_frame(void)
{
    // Check if we've completed a reading for this channel
    if (tick_ch == N_PERIOD_TICKS - 1) {
        // Convert to 16-bit ADC value (do the average) and reset sum
        unsigned int adc = (unsigned int) (sum >> N_SAMPLE_TICKS_LOG2);
        adc_readings[ channel ] = adc;      // Save reading in an array (for "p" command)
        sum = 0;

        // Write the reading out to our data frame
        fb_put16((unsigned) adc);

        // Check if we just did the last demod for this data frame
        readings_in_frame++;
        if (readings_in_frame >= READINGS_PER_FRAME) {

            // This buffer is full, so swap to new one and queue up the old one for sending
            swap_active_frame_buf(); // Swap which frame buffer we're writing to
            if (data_on)
                enqueue_last_frame_buf();

            // Write out data for beginning of next frame
            // Write out the frame counter in new frame buffer
            frame_counter++; // Update frame_counter
            fb_put8(frame_counter); // Save the frame counter as 1st entry in new frame buffer

            // Write out frame header stuff
            fb_put4(0); // Gain (0=gain of 1, 1=gain of 10, 2=gain of 100, T=AD590 temperature mode)
            fb_put4(0); // Status (unused)
            fb_put8(1); // Samples per channel per frame -- UPDATE THIS!!!
            fb_put8(N_CHANNELS); // Number of channels

            // Reset the number of readings in this frame
            readings_in_frame = 0;
        }
    }
}

// Sets the mux to the next channel - should be last function called
static void go_to_next_channel(void)
{
    if (tick_ch == N_PERIOD_TICKS - 1) {
        channel++;              // Increment first, since channel is initialized to 0, and this function gets called
                                // at the end of the reading, and we want to set the mux to channel 1
        if (channel >= N_CHANNELS)  {
            channel = 0;
        }

        write_amux(channel);    // Set the hardware mux to the *next* channel to read
    }

    // Blink LED at 1 Hz to know we're alive (25% duty cycle)
    if (channel < 8)
        PIN_LED = 1;
    else
        PIN_LED = 0;
}


//----> PUBLIC FUNCTIONS

// Returns an ADC reading (mostly for "p" command)

short read_adc(int ch)
{
    if (ch >= 0 && ch < N_CHANNELS)
        return adc_readings[ch];
    return 0;
}

void set_data_on(unsigned char state)
{
    data_on = state;
}

unsigned char is_data_on(void)
{
    return data_on;
}
