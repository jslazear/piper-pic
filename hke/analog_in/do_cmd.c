#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "analog_in.h"


/*
 * Set which ADC input channel to read. (0 - 31)
 */
void write_amux( unsigned short ch )
{
	// range check
	if (ch >= 32)
		return;

	// Map input channels to appropriate mux setting
	// This is to descramble the scrambling introduced by board wiring
	unsigned short mux;	
	if( ch < 8 )		//  0-7  ---> 7-0
		mux = 7 - ch;
	else if( ch < 16 )	//  8-15 ---> 15-8
		mux = 23 - ch;
	else if( ch < 24 )	// 16-23 ---> 23-16
		mux = 39 - ch;
	else				// 24-31 ---> 31-24
		mux = 55 - ch;


	// Set address bits
	if( mux & 0b00001 )			//set LSB of address (bit 0)
		PIN_ADC_MUX_A0 = 1;
	else
		PIN_ADC_MUX_A0 = 0;
	
	if( mux & 0b00010 )			//set bit 1
		PIN_ADC_MUX_A1 = 1;
	else
		PIN_ADC_MUX_A1 = 0;

	if( mux & 0b00100 )			//set bit 2
		PIN_ADC_MUX_A2 = 1;
	else
		PIN_ADC_MUX_A2 = 0;

	if( mux & 0b01000 )			//set bit 3
		PIN_ADC_MUX_A3 = 1;
	else
		PIN_ADC_MUX_A3 = 0;
	
	if( mux & 0b10000 )			//set MSB of address (bit 4)
		PIN_ADC_MUX_A4 = 1;
	else
		PIN_ADC_MUX_A4 = 0;
}


// Returns the board's hex address
unsigned char get_addr(void)
{
	unsigned char addr = (PIN_ADDR3 << 3) | (PIN_ADDR2 << 2) | (PIN_ADDR1 << 1) | PIN_ADDR0;
	return addr;
}


/*
 * Called by cmd_gets() when it is just waiting for a user to hit a key.
 *
 * Use this for things that need to happen often, but are too much calculation
 * to do in the interrupt handler.
 */
void OnIdle( void )
{
}