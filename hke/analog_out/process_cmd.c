#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "analog_out.h"
#include "version.h"        // Computer-generated version string (includes compile time and user name)

// Local function prototypes
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_dac( void );		// sets the analog output dacs
static char do_cmd_all( void );		// sets all the analog output dacs
static char do_cmd_mv( void );		// sets the analog output voltage in millivolts
static char do_cmd_p( void );		// prints dac status
static char do_cmd_addr( void );	// prints out cards hex address
static char do_cmd_data( void );	// turn data stream on or off
static char do_cmd_echo( void );	// changes echo on / echo off
static char do_cmd_version(void);	// prints out our software version

#define CMP	!strcmp

/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */
void process_cmd( char *cmd_string )
{
	char *s;
	unsigned char r;
	
    // Parse command string and check address to see if it's for us
    s = bp_parse_cmd_string(cmd_string);
    if( s == NULL )     // Command is not for us
        return;
	
	if( CMP( s, "led" ))		r = do_cmd_led();		// set the LED
	else if( CMP( s, "dac" ))	r = do_cmd_dac();		// set the analog output DAC
	else if( CMP( s, "all" ))	r = do_cmd_all();		// set all the DACs to the same value
	else if( CMP( s, "mv" ))	r = do_cmd_mv();		// set the voltage on that analog output DAC in millivolts
	else if( CMP( s, "addr" ))	r = do_cmd_addr();		// prints card's hex address
	else if( CMP( s, "data" ))	r = do_cmd_data();		// turn data stream on or off
	else if( CMP( s, "p" ))		r = do_cmd_p();			// prints dac status
	else if( CMP( s, "echo" ))	r = do_cmd_echo();		// Sets echo on or off
	else if( CMP( s, "ver" ))	r = do_cmd_version();	// prints software version
	else 						r = STATUS_FAIL_CMD;	// User typed unknown command
	
	print_status( r );
}

/*----------------------------------------------------------------------
 * Analog Output Commands
 *----------------------------------------------------------------------*/

/** MV
 *	Sets the analog output voltage in millivolts.
 *
 *	usage:
 *	mv 0 100	-> Sets channel 0 of the analog output voltage to 100 millivolts
 */
static char do_cmd_mv( void )
{
	// Ensure that token is available
	if (!tok_available() )
		return STATUS_FAIL_NARGS;
	
	// Get the channel (0-31)
	if( !tok_valid_uint( 2 ) )
		return STATUS_FAIL_INVALID;
	unsigned char ch = (unsigned char)tok_get_uint8();
	if (ch > 31)
		return STATUS_FAIL_INVALID;
	
	// Ensure that token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	// Get voltage to set, should be a 5 digit integer, possibly negative
	if( !tok_valid_num( 5, 1, 0 ) )
		return STATUS_FAIL_RANGE;
	
	int mv = (int)tok_get_int16();
	// Must be between +/- 10240 mV.
	if (mv < -10240)
		mv = -10240;
	if (mv > 10240)
		mv = 10240;
	
	// Convert from millivolts to DAC
	short dac = MV_TO_DAC(mv);
	write_aout_dac( ch, dac );
	return STATUS_OK;	
}	


/** DAC
 *	Set the analog output DAC.
 *        This is a low-level command.
 * USAGE:
 *	dac 0 1024	-> Sets channel 0 of the analog output dac to 1024
 * 	Channel is 0-3
 *  DAC is 0 to 4095
 */
static char do_cmd_dac( void )	
{
	// Ensure that token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	// Get the channel (0-31)
	if( !tok_valid_uint( 2 ) )	// Allow up to 2 digit numbers
		return STATUS_FAIL_INVALID;
	unsigned char ch = (unsigned char)tok_get_uint8();	
	if( ch >= 32 )
		return STATUS_FAIL_RANGE;

	// Ensure that token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	// Get value of AODAC to set
	if( !tok_valid_num( 4, 0, 0 ) )	// Validate for 4 digit numbers, minus not allowed, no decimal point
		return STATUS_FAIL_INVALID;
	short i16 = tok_get_int16();
	
	write_aout_dac( ch, i16 );		// Tell the int_handler to set the DAC
	
	return STATUS_OK;
}


/** ALL
 *	Set ALL the analog output DAC.
 *
 *	usage:
 *	all 1024	-> Sets all channels of analog output dac to 1024
 *  DAC is -2048 to 2047
 */
static char do_cmd_all( void )	
{
	// Ensure that token is available (DAC setting)
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	// Get value of DAC to set
	if( !tok_valid_num( 4, 1, 0 ) )	// Validate for 4 digit numbers, minus allowed, no decimal point
		return STATUS_FAIL_INVALID;
	short i16 = tok_get_int16();
	
	// Loop over all channels, and set their value
	char ch;
	for( ch=0; ch<32; ++ch )
		write_aout_dac( ch, i16 );		// Tell the int_handler to set the DAC
	
	return STATUS_OK;
}


/*----------------------------------------------------------------------
 * Standard Commands
 *----------------------------------------------------------------------*/

/** ADDR
 *	Prints hex address of the board.
 *
 *	usage:	
 *	No arguments
 */
static char do_cmd_addr( void )
{
	unsigned char addr = get_addr();
	char str[16];
	sprintf( str, "%x\r\n", addr );
	TTYPuts( str );
	return STATUS_OK;
}


/** DATA
 *	Turns on/off data stream.
 *
 *	usage:
 *	data on		-> Turns data stream on
 *	data off	-> Turns data stream off
 */
static char do_cmd_data( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		set_data_on(1); // turn data stream on
	else if( CMP( arg, "off" ) )
		set_data_on(0); // turn data stream off
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/** P
 *	Prints a table of the current DAC settings.
 *        NOT IMPLEMENTED!!!!
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_p( void )
{
	int i;
	char str[100];	// max string len is about 51 chars, so 100 is very safe

	sprintf( str, "\nCH    DAC     MV      CH    DAC     MV\r\n" );
	TTYPuts(str);
	
	// Loop over all channels, printing 2 channels per row
	for( i=0; i<16; ++i )  {
		unsigned int dac1, dac2;
		int mv1, mv2;

		dac1 = read_aout_dac(i);
		dac2 = read_aout_dac(i + 16);

		mv1 = DAC_TO_MV( dac1 );
		mv2 = DAC_TO_MV( dac2 );

		sprintf( str, "%2d %6u %6d      %2d %6u %6d\r\n",  i, dac1, mv1, i+16, dac2, mv2 );

		TTYPuts(str);
	}

	return STATUS_OK;
}

/** ECHO
 *	Sets echo on or off.
 *
 *	usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		cmd_set_echo( 1 );
	else if( CMP( arg, "off" ) )
		cmd_set_echo( 0 );
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;

}

/** VER
 *	Spits out a version string for the current code.
 *
 *	usage:
 *	No arguments
 */
static char do_cmd_version( void )
{
 TTYPuts("ANALOG_OUT  ");

 // Print the standard version string and config name
 bp_print_version_info(VERSION_STRING);
 
 return STATUS_OK;
}

/** LED
 *	Set LED light.
 *
 *	usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led( void )
{
	char *arg;
	
	// Check that the next token is a valid number
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	arg = tok_get_str();
	if( CMP( arg, "1" ) )  {
		PIN_LED = 1;
	}
	else if( CMP( arg, "0" ) )  {
		PIN_LED = 0;
	}
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}
