#include <p30F5011.h>
#include <dsp.h>
#include <spi.h>
#include <stdio.h>
#include <libpic30.h>

#include "analog_out.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & XT_PLL16);         /* XT with 16xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF);                         /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN);           /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF);                    /* Code protect disabled */

/* Global variables */
unsigned char CardAddress;

void init_pic()
{
	int i;

	// Set up values for GPIO pins
//	PIN_LED = 1;

	// Set the diagnostic outputs to 0
	TRISBbits.TRISB0 = 0;
	TRISBbits.TRISB1 = 0;
	TRISBbits.TRISB2 = 0;
	TRISBbits.TRISB3 = 0;
	TRISBbits.TRISB4 = 0;
    PIN_RB0 = 0;
    PIN_RB1 = 0;
    PIN_RB2 = 0;
    PIN_RB3 = 0;
    PIN_RB4 = 0;

	// Set the DAC LOADREG pins as outputs
	TRISBbits.TRISB8 = 0;
	TRISBbits.TRISB9 = 0;
	TRISBbits.TRISB10 = 0;
	TRISBbits.TRISB11 = 0;
	TRISBbits.TRISB12 = 0;
	TRISBbits.TRISB13 = 0;
	TRISBbits.TRISB14 = 0;
	TRISBbits.TRISB15 = 0;

	// Set the direction for GPIO
	TRISCbits.TRISC14 = 0;	// Set the RS-485 Enable pin as an output
	TRISCbits.TRISC1 = 0;	// Set the LED as an output

	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;	

	// Oscope debug pins, set them to outputs
	TRISDbits.TRISD4 = 0;
	TRISGbits.TRISG15 = 0;

	// Init all the DACS on the board to sensible initial values
	for( i=0; i<32; ++i )
		write_aout_dac( i, 1000 );	// Set analog outputs to 0 volts


	// Short delay to wait for power supply voltages to settle
 	__delay32( (long)(RESET_DELAY_SECONDS * CLOCKFREQ) );

	// Setup the SPI port2
	OpenSPI2( FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON & 
				MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
				PRI_PRESCAL_4_1,
				SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR );
	ConfigIntSPI2( SPI_INT_DIS );	// NO SPI interrupts

	// The SPI module supports 4 different Serial Clock formats.
	// Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
	SPI2CONbits.CKE = 1;	// CKE=1 (see above)
	SPI2CONbits.CKP = 0;	// CKP=0 means SCLK is active high
	SPI2CONbits.SMP = 1;	// If the SMP bit is set, then the input sampling is done at the end of the bit output

    // Setup the UART with our standard backplane settings
    config_uart2();

	// Read in our card address and store in global variable
	CardAddress = get_addr();

    // Before we start the interrupt handler, wait for a rising edge on external frame clock
    // This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
    // Do this just before turning on the timer interrupt.
    sync_to_frame_clock();

    // Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
    // Setup timer 2 and set its period (exact period depends on configuration, but it will be roughly a few KHZ)
    // turn on ~4 kHz interrupt handler.  Do this last.
    config_timer2();
}


int main( void )
{
 // Setup the PIC internal peripherals
 init_pic();

 // Setup our data frame buffers
 init_frame_buf_backplane( 'O', CardAddress );	// Board type is ANALOG_'O'UT

 while(1) {
	char s[80];

	cmd_gets(s, sizeof(s));		// Get a command from the user
	process_cmd(s);				// Deal with the command
 }

}
