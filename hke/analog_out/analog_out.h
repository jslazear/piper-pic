#ifndef __ANALOG_OUT_INCLUDED
#define __ANALOG_OUT_INCLUDED

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

// Useful conversion equations

// convert 12 bit DAC value to MV
#define DAC_TO_MV(dac) ((dac-2048)*5)
#define MV_TO_DAC(mv) ((mv / 5) + 2048)

// Constants

//-----> OUTPUT PINS
#define PIN_LED			LATCbits.LATC1	// Front pannel LED

#define PIN_AO_DAC_LD0		LATBbits.LATB15	// DAC load pins
#define PIN_AO_DAC_LD1		LATBbits.LATB14
#define PIN_AO_DAC_LD2		LATBbits.LATB13
#define PIN_AO_DAC_LD3		LATBbits.LATB12
#define PIN_AO_DAC_LD4		LATBbits.LATB11
#define PIN_AO_DAC_LD5		LATBbits.LATB10
#define PIN_AO_DAC_LD6		LATBbits.LATB9
#define PIN_AO_DAC_LD7		LATBbits.LATB8

// Debug pins for looking at with scope
#define PIN_RB0         	LATBbits.LATB0
#define PIN_RB1         	LATBbits.LATB1
#define PIN_RB2         	LATBbits.LATB2
#define PIN_RB3         	LATBbits.LATB3
#define PIN_RB4         	LATBbits.LATB4

#define PIN_OSCOPE1		LATDbits.LATD4	// Debug pins for looking at with scope
#define PIN_OSCOPE2		LATGbits.LATG15

#define PIN_485_ENABLE		LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

//------> INPUT PINS
#define PIN_ADDR0			PORTFbits.RF1
#define PIN_ADDR1			PORTGbits.RG1
#define PIN_ADDR2			PORTGbits.RG0
#define PIN_ADDR3			PORTFbits.RF0
#define PIN_SYNC_CLK		PORTDbits.RD2
#define PIN_FRAME_CLK		PORTDbits.RD3


//---------> Function Prototypes
// ANALOG_OUT.C
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board

// int_handler.c
void int_handler( void );	// interrupt handler routine
void write_aout_dac( unsigned char ch, short val );
short read_aout_dac( unsigned char ch );
void set_data_on( unsigned char state );

// process_cmd.c
void process_cmd( char *str );

//do_cmd.c
unsigned char get_addr(void);
void OnIdle( void );	// Called when program has nothing to do

#endif	// __ANALOG_OUT_INCLUDED
