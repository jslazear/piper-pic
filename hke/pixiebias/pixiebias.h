#ifndef __PIXIEBIAS_INCLUDED
#define __PIXIEBIAS_INCLUDED

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

//-----> MODES
#define MODE_DC                 0                // DC excitation signal mode
#define MODE_AC                 1                // AC excitation signal mode

//-----> SOURCES
#define SOURCE_TICKLE           0                // Tickle excitation source
#define SOURCE_BIAS             1                // Bias excitation source

//-----> DAC Channels
#define DAC_BIAS_GEN            0                // Bias generator DAC identifier
#define DAC_BIAS_AMP            1                // Bias amplitude DAC identifier
#define DAC_TICKLE_GEN          2                // Tickle generator DAC identifier
#define DAC_TICKLE_AMP          3                // Tickle amplitude DAC identifier
#define DAC_HEATER              4                // Heater DAC identifier (for JFET heater)

//-----> TICKLE Parameters
#define TICKLE_TABLE_LEN        2000             // Numbers of elements in tickle table

//-----> OUTPUT PINS
#define PIN_LED                 LATBbits.LATB5	 // Front pannel LED

#define PIN_BIAS_GEN_LD         LATBbits.LATB8	 // Bias generator DAC
#define PIN_BIAS_AMP_LD         LATBbits.LATB9   // Bias amplitude scaling DAC
#define PIN_TICKLE_GEN_LD       LATBbits.LATB10  // Tickle generator DAC
#define PIN_TICKLE_AMP_LD       LATBbits.LATB11  // Tickle amplitude scaling DAC

#define PIN_HEATER_LD           LATBbits.LATB12  // Heater DAC

#define PIN_TRIGGER_READOUT     LATDbits.LATD0   // Initiates data collection on the external readout card

#define PIN_485_ENABLE          LATCbits.LATC14  // Enable the RS-485 driver (so we can transmit over UART)

#define PIN_OSCOPE1             LATDbits.LATD7   // Debug pins for looking at with scope
#define PIN_OSCOPE2             LATDbits.LATD6
#define PIN_OSCOPE3             LATDbits.LATD5
#define PIN_OSCOPE4             LATDbits.LATD4

#define PIN_A0                  LATDbits.LATD11  // Bias/tickle switch for Ch 0
#define PIN_A1                  LATDbits.LATD10  // Bias/tickle switch for Ch 1
#define PIN_A2                  LATDbits.LATD9   // Bias/tickle switch for Ch 2
#define PIN_A3                  LATDbits.LATD8   // Bias/tickle switch for Ch 3

//------> INPUT PINS
#define PIN_ADDR0               PORTFbits.RF1
#define PIN_ADDR1               PORTGbits.RG1
#define PIN_ADDR2               PORTGbits.RG0
#define PIN_ADDR3               PORTFbits.RF0

#define PIN_FRAME_CLK		PORTDbits.RD2
#define PIN_SYNC_CLK            PORTDbits.RD3

//-----> Constants
#define MAX_SWEEP_LEN       200     // size of the bias sweep table (max length of a bias sweep)


//---------> Function Prototypes
// pixiebias.c
void init_pic( void );		// Init the PIC ports and hardware

// int_handler.c
void set_data_frame(unsigned char state);
void set_dac(unsigned val, int ch);
void set_bias_mode(unsigned char mode);
void set_source(unsigned char source, int ch);
void set_tickle(int midpoint, unsigned delta, unsigned amp);
void set_tickle_mode(unsigned char mode);
void set_tickle_midpoint(int midpoint);
void set_tickle_delta(unsigned delta);
void set_tickle_amp(unsigned amp);
void enable_sweep( int on );

unsigned get_tickle_delta(void);
unsigned get_tickle_bottom(void);
int get_tickle_midpoint(void);
unsigned char get_tickle_mode(void);
unsigned get_tickle_amp();
unsigned char get_source_mask(void);

// process_cmd.c
void process_cmd( char *str ) ;

// dacs.c
void write_dac(unsigned val, int ch);

// do_cmd.c
unsigned char get_addr(void);
void OnIdle( void );

#endif
