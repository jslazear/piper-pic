#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "pixiebias.h"
#include "version.h"        // Computer-generated version string (includes compile time and user name)

// Local function prototypes
static char do_cmd_dac(void);           // Set a DAC
static char do_cmd_bamp(void);          // Set the bias excitation amplitude
static char do_cmd_bmode(void);         // Set the bias excitation mode (DC/AC)
static char do_cmd_source(void);        // Set the source (bias/tickle) for a channel
static char do_cmd_config_tickle(void); // Configure the tickle signal
static char do_cmd_tch(void);           // Set the tickle channel list (channels to tickle)
static char do_cmd_tfreq(void);         // Set the tickle frequency
static char do_cmd_tmode(void);         // Set the tickle excitation mode (DC/AC)
static char do_cmd_tamp_raw(void);      // Set the tickle excitation amplitude, with no range checking.
static char do_cmd_tamp(void);          // Set the tickle excitation amplitude
static char do_cmd_tmid_raw(void);      // Set the tickle midpoint in DAC units, with no range checking.
static char do_cmd_tmid(void);          // Set the tickle midpoint in DAC units
static char do_cmd_sweep(void);         // Run a bias amplitude sweep
static char do_cmd_td(void);            // Trigger the readout board to take data
static char do_cmd_led( void );         // turn LED on or off
static char do_cmd_addr( void );        // read hex address
static char do_cmd_data( void );        // turn data stream on or off
static char do_cmd_p( void );           // prints dac status
static char do_cmd_echo( void );        // changes the echo mode of the board
static char do_cmd_version( void );     // prints version of the code

#define CMP !strcmp

/*
 * Process a user command contained in the command buffer.
 */
void process_cmd( char *cmd_string )
{
    char *s;
    unsigned char r;

    // Parse command string and check address to see if it's for us
    s = bp_parse_cmd_string(cmd_string);
    if( s == NULL )     // Command is not for us
        return;

    if( CMP( s, "led" ))               r = do_cmd_led();           // set the LED
    else if( CMP( s, "dac" ))          r = do_cmd_dac();           // Set a DAC
    else if( CMP( s, "bamp" ))         r = do_cmd_bamp();          // Sets the bias amplitude
    else if( CMP( s, "bmode" ))        r = do_cmd_bmode();         // Set AC/DC mode for bias signal
    else if( CMP( s, "tmode" ))        r = do_cmd_tmode();         // Set AC/DC mode for tickle signal
    else if( CMP( s, "source" ))       r = do_cmd_source();        // Set excitation source (tickle/bias)
    else if( CMP( s, "config_tickle")) r = do_cmd_config_tickle(); // Config tickle signal
    else if( CMP( s, "tch"))           r = do_cmd_tch();           // Set tickle channel list (channels to tickle)
    else if( CMP( s, "tfreq"))         r = do_cmd_tfreq();         // Set tickle frequency in Hz
    else if( CMP( s, "tamp_raw"))      r = do_cmd_tamp_raw();      // Set tickle amplitude in DAC units
    else if( CMP( s, "tamp"))          r = do_cmd_tamp();          // Set tickle amplitude in DAC units
    else if( CMP( s, "tmid_raw"))      r = do_cmd_tmid_raw();      // Set tickle midpoint in DAC units
    else if( CMP( s, "tmid"))          r = do_cmd_tmid();          // Set tickle midpoint in DAC units
    else if( CMP( s, "addr" ))         r = do_cmd_addr();          // read in hex address
    else if( CMP( s, "data" ))         r = do_cmd_data();          // turn data stream on or off
    else if( CMP( s, "sweep"))         r = do_cmd_sweep();         // runs a bias amplitude sweep
    else if( CMP( s, "td"))            r = do_cmd_td();            // trigger readout board to take data
    else if( CMP( s, "p" ))            r = do_cmd_p();             // prints dac status
    else if( CMP( s, "echo" ))         r = do_cmd_echo();          // Sets echo on or off
    else if( CMP( s, "ver"))           r = do_cmd_version();       // prints software version
    else                               r = STATUS_FAIL_CMD;        // User typed unknown command

    // Print out the status, usually OK!
    print_status( r );
}

/*-----------------------------------------------------------
 * Excitation and Gain Commands
 *-------------------------------------------------------------*/


/*-----------------------------------------------------------
 * Low Level Excitation and Gain Commands
 *-------------------------------------------------------------*/

/* DAC
 *      Set a DAC
 *
 *      Arguments:
 *      <address=integer dac identifier>
 *      <DAC code>
 *
 *      Set the output of the specified DAC (all DACs are included) to the
 *      specified code.
 */
static char do_cmd_dac(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-4)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    int ch = tok_get_int16();
    if (ch >= 6) {
        return STATUS_FAIL_RANGE;
    }

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    int val = tok_get_int16();

    set_dac(val, ch);
    sprintf(str, "*PXBIAS: dac\r\n");
    TTYPuts(str);

    return STATUS_OK;
}


/** BAMP
 *      Set the bias amplitude in DAC units.
 *
 *      Arguments:
 *      <amp=amplitude>
 *
 *      amp is allowed to be between 0 and 65535, where 0 is no amplitude, and
 *      65535 is full scale.
 */
static char do_cmd_bamp(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned amp = tok_get_int16();

    set_dac(amp, DAC_BIAS_AMP);

    sprintf( str, "*PXBIAS: bamp -- Set to %u amplitude (code)\r\n", amp);
    TTYPuts(str);

    return STATUS_OK;
}


/** BMODE
 *  Sets AC/DC mode for bias excitation signal.
 *
 *  Usage:
 *  bmode ac     -> Sets to AC excitation mode
 *  bmode dc     -> Sets to DC excitation mode
 */
static char do_cmd_bmode(void)
{
    char *arg;

    // Check that the next token is available
    if( !tok_available() )
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if( CMP( arg, "ac" ) )
        set_bias_mode(MODE_AC); // turn data stream on
    else if( CMP( arg, "dc" ) )
        set_bias_mode(MODE_DC); // turn data stream off
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}


/** TMODE
 *  Sets AC/DC mode for tickle excitation signal.
 *
 *  Usage:
 *  tmode ac     -> Sets to AC excitation mode
 *  tmode dc     -> Sets to DC excitation mode
 */
static char do_cmd_tmode(void)
{
    char *arg;

    // Check that the next token is available
    if( !tok_available() )
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if( CMP( arg, "ac" ) )
        set_tickle_mode(MODE_AC); // turn data stream on
    else if( CMP( arg, "dc" ) )
        set_tickle_mode(MODE_DC); // turn data stream off
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}


/** TCH
 *      Set the channel(s) to tickle.
 *      Channels not set here will be biased.
 *    Arguments:
 *      <channel list (channel = 0,1,2, or 3)>
 *    Examples:
 *      tch 1      (tickles only channel 1; 0,2,3 are biased)
 *      tch 0 1 3  (tickles channels 0,1,3; 2 is biased )
 *      tch all    (tickles all channels, none biased)
 *      tch none   (no channels tickled, all biased)
 */
static char do_cmd_tch(void)
{
    char str[100];
  	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 1);

    int i;
	for (i = 0; i < 4; i++) {
        unsigned char source = ((ch_mask >> i) & 0x1) ?SOURCE_TICKLE :SOURCE_BIAS;
        set_source(source, i);

        sprintf( str, "*PXBIAS: CH %d %s\r\n", i, (source==SOURCE_TICKLE) ?"TICKLE" :"BIAS" );
        TTYPuts(str);
    }
    return STATUS_OK;
}

/** SOURCE
 *      Set the source for a channel
 *
 *      Arguments:
 *      <address=integer dac identifier>
 *      <source=['ac', 'dc']>
 *
 *      Set the specified channel's excitation source.
 */
static char do_cmd_source(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-3)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    int ch = tok_get_int16();
    if (ch >= 4) {
        return STATUS_FAIL_RANGE;
    }

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    char *arg = tok_get_str();
    unsigned char source;

    if( CMP( arg, "bias" ) )
        source = SOURCE_BIAS;
    else if( CMP( arg, "tickle" ) )
        source = SOURCE_TICKLE;
    else
        return STATUS_FAIL_INVALID;

    set_source(source, ch);

    sprintf( str, "*PXBIAS: source\r\n");
    TTYPuts(str);

    return STATUS_OK;
}


/** CONFIG_TICKLE
 *      Configure the tickle signal
 *
 *      Arguments:
 *      <midpoint=integer dac value of midpoint of sine wave, signed>
 *      <delta=step size through lookup table (f = f_0*delta = delta [Hz])
 *      <amp=peak-to-peak amplitude of excitation in DAC units)
 *
 *      Configures the tickle source. Midpoint is a signed integer that ranges
 *      from -32768 to 32767, with 0 corresponding to 0 volts midpoint. Delta
 *      allows you to select the frequency of the tickle excitation. Note that
 *      the fundamental frequency f_0 is 1 Hz, so the sine wave frequency is
 *      simply delta in Hz. Amp sets the peak-to-peak excitation amplitude of
 *      the sine wave, in DAC units. 0 corresponds to no amplitude and 65535
 *      corresponds to the full scale.
 */
static char do_cmd_config_tickle(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    int midpoint = tok_get_int16();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(4, 0, 0))
        return STATUS_FAIL_INVALID;
    int delta = tok_get_int16();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    int amp = tok_get_int16();


    set_tickle(midpoint, delta, amp);

    sprintf( str, "*PXBIAS: config_tickle\r\n");
    TTYPuts(str);

    return STATUS_OK;
}


/** TFREQ
 *      Set the tickle frequency in Hertz (1 Hz)
 *
 *      Arguments:
 *      <f=f Hz>
 *
 *      f is allowed to be between 0 and 2000 Hz in steps of 1 Hz.
 *
 */
static char do_cmd_tfreq(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(4, 0, 0))
        return STATUS_FAIL_INVALID;
    int f = tok_get_int16();

    if (f > 2000) {
        return  STATUS_FAIL_RANGE;
    }

    set_tickle_delta(f);

    sprintf( str, "*PXBIAS: tfreq -- Set to %d Hz\r\n", f);
    TTYPuts(str);

    return STATUS_OK;
}

/** TAMP_RAW
 *      Set the tickle amplitude in DAC units.
 *
 *      Arguments:
 *      <amp=amplitude>
 *
 *      amp is allowed to be between 0 and 65535, where 0 is no amplitude, and
 *      65535 is full scale. This version of TAMP will happily allow you to set
 *      parameters that will cause the DAC value to overflow and therefore the
 *      sine wave to wrap. It does not perform and range checking.
 *
 */
static char do_cmd_tamp_raw(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned amp = tok_get_int16();

    set_tickle_amp(amp);

    sprintf( str, "*PXBIAS: tamp -- Set to %u amplitude (code)\r\n", amp);
    TTYPuts(str);

    return STATUS_OK;
}

/** TAMP
 *      Set the tickle amplitude in DAC units.
 *
 *      Arguments:
 *      <amp=amplitude>
 *
 *      amp is allowed to be between 0 and 65535, where 0 is no amplitude, and
 *      65535 is full scale. This version of TAMP performs range checking and
 *      will not allow you to set a value of amp that will cause the DAC value
 *      to overflow.
 *
 */
static char do_cmd_tamp(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned amp = tok_get_int16();

    // Check bounds; table value 0 should result in a DAC value that is smaller
    // than the DAC value from table value 65535.
    int mid = get_tickle_midpoint();
    unsigned high = amp;
    high += (unsigned)(mid + 32768) - 1 - (amp >> 1);
    unsigned low = 0;
    low += (unsigned)(mid + 32768) - 1 - (amp >> 1);

    sprintf( str, "*PXBIAS: Results in range %u to %u\r\n", low, high);
    TTYPuts(str);
    if (high < low) {
        return STATUS_FAIL_RANGE;
    }

    set_tickle_amp(amp);

    sprintf( str, "*PXBIAS: tamp -- Set to %u amplitude (code)\r\n", amp);
    TTYPuts(str);

    return STATUS_OK;
}

/** TMID_RAW
 *      Set the tickle midpoint in DAC units, centered at 0.
 *
 *      Arguments:
 *      <mid=midpoint in DAC units>
 *
 *      mid is allowed to be between -32768 and 32767, where 0 corresponds to
 *      the center of the range. This version of the TMID does no range
 *      checking, so it will allow you to input values that will cause the
 *      DAC value to overflow and therefore cause the sine wave to wrap.
 *
 */
static char do_cmd_tmid_raw(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int mid = tok_get_int16();

    set_tickle_midpoint(mid);

    sprintf( str, "*PXBIAS: tmid -- Set to %d midpoint (code)\r\n", mid);
    TTYPuts(str);

    return STATUS_OK;
}

/** TMID
 *      Set the tickle midpoint in DAC units, centered at 0.
 *
 *      Arguments:
 *      <mid=midpoint in DAC units>
 *
 *      mid is allowed to be between -32768 and 32767, where 0 corresponds to
 *      the center of the range. This version of TMID does perform range
 *      checking and will not allow you to change the midpoint to a value that
 *      will cause overflow.
 *
 */
static char do_cmd_tmid(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int mid = tok_get_int16();

    // Check bounds; table value 0 should result in a DAC value that is smaller
    // than the DAC value from table value 65535.
    unsigned amp = get_tickle_amp();
    unsigned high = amp;
    high += (unsigned)(mid + 32768) - 1 - (amp >> 1);
    unsigned low = 0;
    low += (unsigned)(mid + 32768) - 1 - (amp >> 1);

    sprintf( str, "*PXBIAS: Results in range %u to %u\r\n", low, high);
    TTYPuts(str);
    if (high < low) {
        return STATUS_FAIL_RANGE;
    }

    set_tickle_midpoint(mid);

    sprintf( str, "*PXBIAS: tmid -- Set midpoint to %d (code)\r\n", mid);
    TTYPuts(str);

    return STATUS_OK;
}


/** SWEEP
 *  Sweeps the bias amplide DAC using the sweep parameters
 *  defined by a previous sweep_config command, or the default parameters.
 *
 *  Usage:
 *  sweep [on]  -> Runs a bias amplitude sweep
 *  sweep off   -> Stops a sweep in progress
 */
static char do_cmd_sweep( void )
{
    int state = 1;  // Sweep with no arguments "enables" the sweep

    // Check if the next token is available
    if( tok_available() )  {
        char *arg = tok_get_str();
        if( CMP( arg, "on" ) )
            state = 1;
        else if( CMP( arg, "off" ) )
            state = 0;
        else
            return STATUS_FAIL_INVALID;
    }
    enable_sweep(state);

    return STATUS_OK;
}

/** TD
 *
 *  Usage:
 *  td      -> Toggle EXT_TRIGGER pin (toggles Readout Board data collection)
 *  td on   -> Sets the EXT_TRIGGER pin high (starts Readout Board data collection)
 *  td off  -> Sets the EXT_TRIGGER pin low (stops Readout Board data collection)
 */
static char do_cmd_td( void )
{
    int trig = PIN_TRIGGER_READOUT;

    // Check if the next token is available
    if( tok_available() )  {
        char *arg = tok_get_str();
        if( CMP( arg, "on" ) )
            trig = 1;
        else if( CMP( arg, "off" ) )
            trig = 0;
        else
            return STATUS_FAIL_INVALID;
    }
    else {
        // No argument -- toggle the state
        trig = !trig;
    }

    // Set the external pin
    PIN_TRIGGER_READOUT = trig;

    if( trig )
        TTYPuts("EXT_TRIGGER set HIGH\r\n");
    else
        TTYPuts("EXT_TRIGGER set LOW\r\n");

    return STATUS_OK;
}

/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/

/** DATA
 *  Turns on/off data stream.
 *
 *  Usage:
 *  data on     -> Turns data stream on
 *  data off    -> Turns data stream off
 */
static char do_cmd_data( void )
{
    char *arg;

    // Check that the next token is available
    if( !tok_available() )
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if( CMP( arg, "on" ) )
        set_data_frame(1); // turn data stream on
    else if( CMP( arg, "off" ) )
        set_data_frame(0); // turn data stream off
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}


/** P
 *  Prints a table of data for specified channels:
 *  GDAC, ADAC, Demod, Delta, InA, r.
 *
 *  Usage:
 *  p           -> Prints all channels
 *  p 10            -> Prints data for channel 10
 *  p 10 11 12  -> Prints data for channel 10, 11, 12
 *  p cal       -> Prints calibration resistor data
 */
static char do_cmd_p( void )
{
    char str[100];  // max string len is about 52 chars, so 100 is very safe

    PIN_OSCOPE4 = 1;  //#DELME

    sprintf( str, "*PXBIAS: p\r\n");
    TTYPuts(str);

    PIN_OSCOPE4 = 0;  //#DELME
    return STATUS_OK;
}


/** ADDR
 *  Prints hex address of the board.
 *
 *  Usage:
 *  No arguments
 */
static char do_cmd_addr( void )
{
    unsigned char addr = get_addr();
    char str[16];
    sprintf( str, "%x\r\n", addr );
    TTYPuts( str );
    return STATUS_OK;
}


/** ECHO
 *  Sets echo on or off.
 *
 *  Usage:
 *  echo on     -> sets echo on
 *  echo off    -> sets echo off
 */
static char do_cmd_echo( void )
{
    char *arg;

    // Check that the next token is available
    if( !tok_available() )
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if( CMP( arg, "on" ) )
        cmd_set_echo( 1 );
    else if( CMP( arg, "off" ) )
        cmd_set_echo( 0 );
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;

}


/** VER
 *  Spits out a version string for the current code.
 *
 *  Usage:
 *  No arguments
 */
static char do_cmd_version( void )
{
 // Print the board name and mode
 TTYPuts("*PXBIAS:  \n");

 // Print the standard version string and config name
 bp_print_version_info(VERSION_STRING);

 return STATUS_OK;
}

/** LED
 *  Set the green front panel LED light.
 *
 *  Usage:
 *  led on  -> Sets LED on
 *  led off -> Sets LED off
 */
static char do_cmd_led( void )
{
    char *arg;

    // Check that the next token is a valid number
    if( !tok_available() )
        return STATUS_FAIL_NARGS;

    arg = tok_get_str();
    if( CMP( arg, "on" ) )  {
        PIN_LED = 1;
    }
    else if( CMP( arg, "off" ) )  {
        PIN_LED = 0;
    }
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/*-----------------------------------------------------------
 * Test / Diagnostic Commands
 *-------------------------------------------------------------*/

// None?
