#include <p30F5011.h>
#include <timer.h>
#include <dsp.h>
#include <spi.h>
#include <stdio.h>
#include <libpic30.h>
#include <outcompare.h>

#include "pixiebias.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & XT_PLL16); /* XT with 8xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF); /* Code protect disabled */


/* Global variables */
unsigned char CardAddress;

/*
 * Set all the GPIO leads on the pic to be either inputs or outputs
 * as needed.
 *
 * Set up the internal pic hardware, like the timers to generate square wave
 * for the convert input of the ADC.
 *
 * Set up the PIC SPI and UART (RS-232) port
 */
void init_pic()
{
    // Set up values for GPIO pins
    PORTCbits.RC1 = 1; // Turn on the LED

    // Set the bias/tickle switch pins as outputs
    TRISDbits.TRISD8 = 0;
    TRISDbits.TRISD9 = 0;
    TRISDbits.TRISD10 = 0;
    TRISDbits.TRISD11 = 0;
    PIN_A0 = SOURCE_BIAS;            // Default to BIAS
    PIN_A1 = SOURCE_BIAS;            // Default to BIAS
    PIN_A2 = SOURCE_BIAS;            // Default to BIAS
    PIN_A3 = SOURCE_BIAS;            // Default to BIAS

    TRISDbits.TRISD0 = 0;            // Triggers the external ADC board

    // Set the direction for GPIO
    TRISCbits.TRISC14 = 0; // Set the RS-485 Enable pin as an output
    TRISBbits.TRISB5 = 0;  // Set the LED as an output

    // IS THIS NECESSARY? NO ADCS. #DELME
//    // Set all ADC lines as digital IO (Family Ref 11.3)
//    ADPCFG = 0xFFFF;

    // Load pins for the DACs
    TRISBbits.TRISB8 = 0;  // Bias Generator DAC load pin
    TRISBbits.TRISB9 = 0;  // Bias amplitude scaling DAC load pin
    TRISBbits.TRISB10 = 0; // Tickle Generator DAC load pin
    TRISBbits.TRISB11 = 0; // Tickle amplitude scaling DAC load pin
    TRISBbits.TRISB12 = 0; // Heater DAC load pin
    PIN_BIAS_GEN_LD = 1;   // Turn off CHIP_SELECT for all DACs
    PIN_BIAS_AMP_LD = 1;
    PIN_TICKLE_GEN_LD = 1;
    PIN_TICKLE_AMP_LD = 1;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD5 = 0;
    TRISDbits.TRISD6 = 0;
    TRISDbits.TRISD7 = 0;

    // Turn off LED
    PIN_LED = 0;


    // Short delay to wait for power supply voltages to settle
    __delay32((long) (RESET_DELAY_SECONDS * CLOCKFREQ));

    // Setup the SPI port
    OpenSPI2(FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON &
             MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
             PRI_PRESCAL_4_1,
             SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR);
    ConfigIntSPI2(SPI_INT_DIS); // NO SPI interrupts

    // Set the same as TRead -- using same DAC chip as TRead (LTC1595)
    SPI2CONbits.CKE = 1; // CKE=1 (see above)
    SPI2CONbits.CKP = 0; // CKP=0 means SCLK is active high
    SPI2CONbits.SMP = 1; // If the SMP bit is set, then the input sampling is done at the end of the bit output

    // Setup the UART with our standard backplane settings
    config_uart2();

    // Read in our card address and store in global variable
    CardAddress = get_addr();

    // Before we start the interrupt handler, wait for a rising edge on external frame clock
    // This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
    // Do this just before turning on the timer interrupt.
    sync_to_frame_clock();

    // Setup timer 2 and set its period to 16 kHz
    // This triggers our main interrupt handler that sets the DACs
    unsigned period2 = CYCLES_PER_TICK/4 - 1;  // Note, PIC timer counts from 0 to period, inclusive, so need to sub 1 to get desired sampling / tick rate
                                              // and divide by 4 to get 16 kHz
    OpenTimer2(T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, period2);

    // Set timer phases
    TMR2 = 3*period2/4;
    
    // Enable Timer 2 interrupts
    ConfigIntTimer2(T2_INT_ON);
}

int main(void)
{
    // Setup the PIC internal peripherals
    init_pic();

    // Setup our data frame buffers
    init_frame_buf_backplane('B', CardAddress);

    // Main loop, wait for command from user, do it, repeat forever...
    while (1) {
        char s[32];

        cmd_gets(s, sizeof (s)); // Get a command from the user
        process_cmd(s); // Deal with the command
    }

}
