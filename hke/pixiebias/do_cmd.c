#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "pixiebias.h"


/* 
 * Gets the hex address of the card.
 */
unsigned char get_addr(void)
{
	unsigned char addr = (PIN_ADDR3 << 3) | (PIN_ADDR2 << 2) | (PIN_ADDR1 << 1) | PIN_ADDR0;
	return addr;
}

/*
 * Called by cmd_gets() when it is just waiting for a user to hit a key.
 *
 * Use this for things that need to happen often, but are too much calculation
 * to do in the interrupt handler.
 */
void OnIdle( void )
{
}
