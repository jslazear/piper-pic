#ifndef __PMASTER_INCLUDED
#define __PMASTER_INCLUDED

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"


//---------> Some useful macros
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)- 1 + 0.5)))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Add 0.5 to convert to int from float
// Constants


// Definitions

//-----> OUTPUT PINS
#define PIN_LED1		LATBbits.LATB3	// GREEN LED - 1 Hz frame clock
#define PIN_LED2		LATBbits.LATB2	// YELLOW LED - Clk src (OFF = external clk, ON = internal crystal)
#define PIN_LED3		LATBbits.LATB1	// GREEN LED - UNALLOCATED (COM MODE?)
#define PIN_LED4		LATBbits.LATB0	// RED LED - ON = UBC SyncBox not detected OFF = sync box detected

#define PIN_LED			LATBbits.LATB3	// For compatibility with common code (i.e. sync_to_frame_clock)
#define PIN_485_ENABLE		LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

#define PIN_SYNC_CLK_BAR	LATDbits.LATD3	// Drives the backplane sync clk line (not currently used by any boards)
#define PIN_FRAME_CLK		LATDbits.LATD2	// Backplane 1 Hz frame clock (generated on this board)

#define PIN_FRAME_CLEAR		LATDbits.LATD9	// Set to clear the hardware counters that divide 4 kHz clock down to 1 Hz frame clock (1 = clear)
#define PIN_BP_RESET_BAR	LATDbits.LATD10	// 0 = reset all cards on backplane except PMaster, 1 = normal state

#define PIN_PIC_READY_BAR	LATBbits.LATB5	// Drive low to re-enable frame count capture circuit

//------> DIAGNOSTIC OUTPUTS
#define PIN_FRAME		LATBbits.LATB10 // FRAME test point output
#define PIN_OSCOPE1		LATDbits.LATD7	// High when in INT1 (ubc frame) interrupt handler
#define PIN_OSCOPE2		LATDbits.LATD6  // High when in OC2 interrupt handler
#define PIN_OSCOPE3		LATDbits.LATD5  // Used as testpoint by psync_common code
#define PIN_OSCOPE4		LATDbits.LATD4  // Unused - use this one for random debugging

//------> INPUT PINS
#define PIN_CLK_MODE		PORTGbits.RG2	// 0 = external clock (UBC), 1 = internal clock
#define PIN_GATE		PORTDbits.RD8	// For READING psync Gate pin (normally triggers INT1)
#define PIN_COM_MODE		PORTBbits.RB15	// 0 = RS-232 (A,C row), 1 = FIBER (front panel)

/* For PMaster V2, moved Mode Jumper (JP3) connections on PIC.  These are currently unused
 *	Signal	V1	V2
 *	MD0	RG0	RG13
 *	MD1	RG1	RG12
 *	MD2	RF1	RG14
 *	MD3	RF0	RG0
 */
#define PIN_JP3_1_MODE		PORTGbits.RG13	// Set's board mode (undefined)
#define PIN_JP3_2_MODE		PORTGbits.RG12	// Set's board mode (undefined)
#define PIN_JP3_3_MODE		PORTGbits.RG14	// Set's board mode (undefined)
#define PIN_JP3_4_MODE		PORTGbits.RG0	// Set's board mode (undefined)

//---------> Function Prototypes
// PMASTER.C
void init_pic( void );		// Init the PIC ports and hardware

// int_handler.c
/** This phases up the UBC sync box */
void phase_up_to_ubc( void );
void set_data_frame( unsigned char state );
unsigned char data_frame_on( void );

// process_cmd.c
void process_cmd( char *str ) ;

//do_cmd.c
void OnIdle( void );

#endif	// __PMASTER_INCLUDED
