#include <p30F5011.h>
#include <dsp.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <libpic30.h>

#include "../common/psync_common.h"
#include "pmaster.h"

// Device configuration register macros for building the hex file
_FOSC(CSW_FSCM_OFF & XT_PLL16); // XT with 16xPLL oscillator, Failsafe clock off
_FWDT(WDT_OFF & WDTPSA_512 & WDTPSB_5); // Watchdog timer enabled, set to period of 5120 ms
                                        // Watchdog period = 2 ms * prescale_A * prescale_B

_FBORPOR(PBOR_OFF & MCLR_EN);   // Brown-out reset disabled, MCLR reset enabled
_FGS(CODE_PROT_OFF);            // Code protect disabled


/* Global variables */
unsigned char CardAddress = MASTER_ADDRESS; // For this board only, we hard-code the address since
                                            // he doesn't have a hex address switch
unsigned char SyncBoxPresent = 0; // init_pic() sets to 1 if sync box is present

/*
 * Set all the GPIO leads on the pic to be either inputs or outputs
 * as needed.
 *
 * Set up the internal pic hardware, like the timers to generate frame clock
 *
 * Set up the PIC SPI and UART (RS-232) port
 */
void init_pic()
{
    // Init LEDs
    // LEDs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;

    PIN_LED1 = 0;
    PIN_LED2 = 0;
    PIN_LED3 = 0;
    PIN_LED4 = 0;

    // Enable the watchdog timer
    RCONbits.SWDTEN = 1; // Turn on the watchdog (period is configured at the top of pmaster.c

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD5 = 0;
    TRISDbits.TRISD6 = 0;
    TRISDbits.TRISD7 = 0;

    // Frame clock - is an output on this board, unlike any of the others
    TRISDbits.TRISD2 = 0;
    PIN_FRAME_CLK = 1;  // Very important!  Must init to high, so that first rising edge generated in Int1 handler
                        // is guaranteed to be real.  Also, check_frame_clock() has prev initialized to 1.
    
    // Sync clock
    TRISDbits.TRISD3 = 0;
    PIN_SYNC_CLK_BAR = 1;

    // Frame Counters Clear (resets hardware divider that makes 1 Hz frame clock)
//    TRISDbits.TRISD9 = 0;
//    PIN_FRAME_CLEAR = 1; // 0 means counters will count, 1 means clear

    // Backplane reset
    TRISDbits.TRISD10 = 0;
    PIN_BP_RESET_BAR = 1; // 1 means do not reset other cards.  0 resets all cards

    // PSYNC pins
    TRISBbits.TRISB5 = 0;
    PIN_PIC_READY_BAR = 1; // Sets to not ready

    // RS-485 Enable pin
    TRISCbits.TRISC14 = 0; // Set the RS-485 Enable pin as an output
    PIN_485_ENABLE = 0;

    // Short delay to wait for power supply voltages to settle
    __delay32((long) (RESET_DELAY_SECONDS * CLOCKFREQ));

    // Setup the UART with our standard backplane settings
    config_uart2();

    // If internal clock mode, light yellow warning LED
    if (PIN_CLK_MODE)
        PIN_LED2 = 1;

    // Check for presence of UBC sync box
    SyncBoxPresent = test_psync();
    PIN_LED4 = !SyncBoxPresent; // If UBC sync box is not present, light red error LED

    ClrWdt();           	// Reset the watchdog timer

    // Init the psync hardware (SPI2...) if UBC sync box is present
    if (SyncBoxPresent)
        init_psync(); // Turns on INT1 handler which generates frame_clk

    // Set the card address and store in global variable (no hex switch on the master card!)
    CardAddress = MASTER_ADDRESS;

    // If the sync box is present, INT1 is generating our frame clock (turned on in init_psync).
    //    We need to wait here for a rising edge of frame_clk, which will occur when ubc_frame_count is a multiple of e.g. 512 frames.
    //    We must wait for the rising edge of frame_clk *before* OC2 interrupt is enabled when sync box is present.
    // If no sync box, OC2 generates frame clock, so we are guaranteed to be synchronized with it.
    if( SyncBoxPresent )  {
        phase_up_to_ubc();      // This causes us to wait until the external ubc frame count is a multiple of UBC_FRAMES_PER_HKE_FRAME
        
        sync_to_frame_clock();  // This causes us to wait until a rising edge of the hke frame clock, so that when
                                // we start up the OC2 timer interrupt, we will be starting at the start of a frame
    }
    
    PIN_OSCOPE4 = 1;    // TRIGGER THE SCOPE on power on / reboot

    // Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
    // Setup timer 2 and set its period (exact period depends on configuration, but it will be roughly a few KHZ)
    // turn on ~4 kHz interrupt handler
    config_timer2();
}


int main(void)
{
    // Setup the PIC internal peripherals
    init_pic();

    // Setup our data frame buffers
    init_frame_buf_backplane('M', CardAddress);

    // Main loop, wait for command from user, do it, repeat forever...
    while (1) {
        char s[32];

        cmd_gets(s, sizeof (s)); // Get a command from the user (also clears watchdog timer)
        process_cmd(s); // Deal with the command
    }
}
