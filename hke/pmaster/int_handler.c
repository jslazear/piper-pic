#include <p30F5011.h>
#include <stdio.h>

#include "../common/psync_common.h"
#include "pmaster.h"

#define TICKS_PER_HALF_FRAME (TICKS_PER_FRAME/2)
#define UBC_HALF_FRAME_MASK (UBC_FRAME_COUNT_MASK>>1)

extern unsigned char SyncBoxPresent; // init_pic() sets to 1 if sync box is present

// Local functions for the interrupt handler
static void pulse_oscope3(int n);
static void update_frame(); // writes new data (if we have any) to our data frame
static unsigned char get_status_byte(void);

// Local variables for the interrupt handler
static unsigned long ubc_frame = (unsigned long)-1; // Frame count as read from UBC sync box
                                    // ubc frame rate is ~200 Hz depending on sync box settings
                                    // By initing to -1, we can easily tell if no sync box is present.
                                    // Also prevents spurious rising edge on frame clock

static unsigned long frame_counter = 0; // Internal frame count, increments on every hke frame
                                        // frame rate depends on constants in bp_common.h
                                        // but is approximately 1 Hz

// Only PMaster keeps a 32 bit frame count, the other boards only have an 8 bit counter
static unsigned int tick = 0; // Increments on each interrupt, resets at end of each frame (4000)

static int ubc_tick = 0;
static char new_frame = 0;

// Variables used to communicate with the outside world
static volatile unsigned char data_on = 1; // default: data stream is on

/*
 * Int1 handler.  UBC Sync box triggers INT1 when a new frame count arrives.
 *
 * Note: it is essential that this routine does the minimum possible.  He needs to
 * read in the first word of the 40 bit frame count from the SPI port before the 2nd and 3rd arrive.
 * SO DO AS LITTLE AS POSSIBLE IN THIS ROUTINE!!!
 *
 */
void __attribute__((__interrupt__, __shadow__, __auto_psv__)) _INT1Interrupt(void)
{
    static int blink_tick = 0;

    IFS1bits.INT1IF = 0; // reset INT1 interrupt flag -- needed or int handler will not work right!!!

    PIN_OSCOPE1 = 1;

    // Turn off the RED led.  ON=no sync box, OFF=sync box
    PIN_LED4 = 0;

    // If the sync box is present, we generate the HKE Frame Clk here, so it is locked with the UBC frame clock.
    // To eliminate jitter on FRAME_CLK, we want to do this at the top of the interrupt handler,
    // before reading in the ubc frame count which takes a variable amount of time.
    // Note on first time in this function, ubc_frame == -1, so we won't generate a rising edge

    if( ubc_tick >= UBC_FRAMES_PER_HKE_FRAME ) {
        PIN_FRAME_CLK = 1;
        ubc_tick = 0;
    }
    else if( ubc_tick == UBC_FRAMES_PER_HKE_FRAME/2 ) {
        PIN_FRAME_CLK = 0;
    }
    ubc_tick++;


    // Read the frame count from UBC Sync box
    // The bitrate of the data coming in is 5 MHz, and we have a 16 bit shift register, so it takes 3.2 us to fill up.
    // Once it is full, the hardware transfers it to the SPIBUF and keeps shifting in.  We must read SPIBUF
    // before it has shifted in another 16 bits.  So we have 6.4 us or 128 instruction cycles to read out SPIBUF
    ubc_frame = psync_read_frame_count();
    new_frame = 1;

    // Blink LED3 on every 10th UBC frame
    blink_tick++;
    if (blink_tick >= 10) {
        PIN_LED3 = !PIN_LED3;
        blink_tick = 0;
    }

    PIN_OSCOPE1 = 0;
}


/*
 * Called once at boot up.  Delays until a precise multiple of the UBC frame count.
 * Only called if a sync box is detected.
 */
void phase_up_to_ubc() {

    while(1) {
        // Wait for a new frame count
        while( !new_frame );
        new_frame = 0;

        // Modulus - note the compiler is smart enough to turn this into a fast AND operation if UBC_FRAMES_PER_HKE_FRAME is a power of 2
        PIN_OSCOPE4 = 1;
        unsigned remainder = ubc_frame % ((unsigned)UBC_FRAMES_PER_HKE_FRAME);
        PIN_OSCOPE4 = 0;

        if ( remainder == 0 ) {
            ubc_tick = 0;
            return;
        }
    }
}


/*
 * Interrupt handler, called at 4 kHz, on the *falling* edge of the convert pulse
 */
void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void)
{
    PIN_OSCOPE2 = 1;

    IFS0bits.OC2IF = 0; // reset Output Compare 2 interrupt flag

    PIN_LED1 = PIN_FRAME_CLK; // LED1 on when Frame Clk is high (1 Hz)

    char isNewFrame = 0;

    if (SyncBoxPresent) {
        // Update address counters and transmit buffer in our time slot
        // If we have just started a new frame, then reset all our state variables (happens once per second)
        // The sync box has updated PIN_FRAME_CLK for us, will be read by update_communication
        isNewFrame = check_frame_clock();
        if (isNewFrame) {
            tick = 0;
        }
        if( tick == TICKS_PER_FRAME ) {
            PIN_LED4 = 1;       // SHOULD SET A STATUS BIT INSTEAD!
        }

        update_frame(); // writes to the data frame (uses tick to know when to write and swap)

        // Increment the tick counter
        tick++;
    }
    else {      // No Sync Box
        if (tick >= TICKS_PER_FRAME) // Happens roughly 1 time per second (1 per frame)
            tick = 0;

        // Generate the ~1 Hz backplane frame clock (if syncbox is present, INT1 handler does this)
        if (tick == 0) {
            PIN_FRAME_CLK = 1;
            isNewFrame = 1;     // Will only be 1 when tick == 0
        }
        else if (tick == TICKS_PER_HALF_FRAME) {
            PIN_FRAME_CLK = 0;
        }

        update_frame(); // writes to the data frame (uses tick to know when to write and swap)

        // Increment the tick counter
        tick++;
    }

    // Update address counters and transmit buffer in our time slot (does not look at tick)
    update_communication(isNewFrame);

    PIN_OSCOPE2 = 0;
}

// For debugging, obviously!
static void pulse_oscope3(int n)
{
    int i;
    
    for( i=0; i<n; ++i ) {
        PIN_OSCOPE3 = 1;        // DEBUG!
        Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
        PIN_OSCOPE3 = 0;        // DEBUG!
        Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
    }
    Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
}

static void update_frame()
{
    // The start of a new frame
    if (tick == 0) {
        // Write out the frame counter in new frame buffer
        fb_put8((unsigned char) frame_counter); // Save the bottom 8 bits of the frame counter
        // this is just for consistency with the other boards
        // Unused data nibble
        unsigned char unused = 0;
        fb_put4(unused);

        // Save the current board status
        unsigned char status = get_status_byte();
        fb_put4(status);

        // UBC frame count
        fb_put32(ubc_frame);

        // Internal frame count (1 Hz)
        fb_put32(frame_counter);

        // Update the internal frame_counter
        frame_counter++;
    }

    // The end of a frame
    else if (tick == TICKS_PER_FRAME - 10) {
        // This buffer is full, so swap to new one and queue up the old one for sending
        swap_active_frame_buf(); // Swap which frame buffer we're writing to
        
        if (data_on)
            enqueue_last_frame_buf();        
    }
}

/**
 *	HEX Character bits
 *	0 (LSB)	COM Mode (0 = RS232 or 1 = Fiber)
 *	1		UBC Frame Count present	(1 = present)
 *	2		CLK Source (0 = External or 1 = Internal)
 *	3 (MSB)	Unused
 *
 *	0000	0	Ext Clk, No Frame, RS232
 *	0001	1	Ext Clk, No Frame, Fiber
 *	0010	2	Ext Clk,    Frame, RS232
 *	0011	3	Ext Clk,    Frame, Fiber
 *	0100	4	Int Clk, No Frame, RS232
 *	0101	5	Int Clk, No Frame, Fiber
 *	0110	6	Int Clk,    Frame, RS232	// status not possible, Frame cannot exist without Ext Clk!
 *	0111	7	Int Clk,    Frame, Fiber	// status not possible, Frame cannot exist without Ext Clk!
 */
static unsigned char get_status_byte(void)
{
    unsigned char status = PIN_COM_MODE + (SyncBoxPresent << 1) + (PIN_CLK_MODE << 2);

    return status;
}

void set_data_frame(unsigned char state)
{
    data_on = state;
}

unsigned char data_frame_on(void)
{
    return data_on;
}
