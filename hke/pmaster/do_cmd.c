#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "pmaster.h"

/*
 * Called by cmd_gets() when it is just waiting for a user to hit a key.
 *
 * Use this for things that need to happen often, but are too much calculation
 * to do in the interrupt handler.
 */
void OnIdle( void )
{
   PIN_LED3 = 1;
}
