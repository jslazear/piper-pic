#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <libpic30.h>

#include "pmaster.h"
#include "version.h"        // Computer-generated version string (includes compile time and user name)

// Local function prototypes
static char do_cmd_bpreset( void );		// reset all the cards on the backplane
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_data( void );	// turn data stream on or off
static char do_cmd_p( void );		// prints status
static char do_cmd_echo( void );	// changes the echo mode of the board
static char do_cmd_version( void );	// prints version of the code

#define CMP	!strcmp


/*
 * Process a user command contained in the command buffer.
 */
void process_cmd( char *cmd_string )
{
	char *s;
	unsigned char r;

    // Parse command string and check address to see if it's for us
    s = bp_parse_cmd_string(cmd_string);
    if( s == NULL )     // Command is not for us
        return;

	if( CMP( s, "led" ))		r = do_cmd_led();		// set the LED
    else if( CMP( s, "bpreset" ))	r = do_cmd_bpreset();		// turn data stream on or off
	else if( CMP( s, "data" ))	r = do_cmd_data();		// turn data stream on or off
	else if( CMP( s, "p" ))		r = do_cmd_p();			// prints dac status
	else if( CMP( s, "echo" ))	r = do_cmd_echo();		// Sets echo on or off
	else if( CMP( s, "ver"))	r = do_cmd_version();	// prints software version
	else 						r = STATUS_FAIL_CMD;	// User typed unknown command

	// Print out the status, usually OK!
	print_status( r );
}

static char do_cmd_bpreset(void)
{
    PIN_BP_RESET_BAR = 0;       // Reset the cards

    // Pulse reset for 100 ms.
    // The boards have a 1 ms time constant RC filter on their bp reset lines, so need to be much longer than that
    // Being very long, like 100 ms doesn't hurt anything since this is a very, very infrequent command
    // and 100 ms is still short compared to the boot and framing time
    __delay32((long) (0.100 * CLOCKFREQ));

    PIN_BP_RESET_BAR = 1;       // Stop resetting the cards

   	return STATUS_OK;
}

/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/
/** LED
 *	Set LED light.
 *
 *	Usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led( void )
{
	char *arg;

	// Check that the next token is a valid number
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	arg = tok_get_str();
	if( CMP( arg, "1" ) )  {
		PIN_LED = 1;
	}
	else if( CMP( arg, "0" ) )  {
		PIN_LED = 0;
	}
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/** DATA
 *	Turns on/off data stream.
 *
 *	Usage:
 *	data on		-> Turns data stream on
 *	data off	-> Turns data stream off
 */
static char do_cmd_data( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		set_data_frame(1); // turn data stream on
	else if( CMP( arg, "off" ) )
		set_data_frame(0); // turn data stream off
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}


/** P - Prints board status.  Currently not implemented
 */
static char do_cmd_p( void )
{
	TTYPuts( "PMASTER\r\n" );

	return STATUS_OK;
}


/** ECHO
 *	Sets echo on or off.
 *
 *	Usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		cmd_set_echo( 1 );
	else if( CMP( arg, "off" ) )
		cmd_set_echo( 0 );
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;

}

/** VER
 *	Spits out a version string for the current code.
 *
 *	Usage:
 *	No arguments
 */
static char do_cmd_version( void )
{
 // Print the board name and mode
 TTYPuts("PMASTER ");

 // Print the standard version string and config name
 bp_print_version_info(VERSION_STRING);

 return STATUS_OK;
}
