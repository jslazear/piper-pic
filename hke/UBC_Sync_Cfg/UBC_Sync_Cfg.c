#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>
#include <stdlib.h>
#include <string.h>

#include "../common/stty.h"
#include "../common/tty_print.h"

#include "UBC_Sync_Cfg.h"


/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL4);          /* External 1.8432 MHz clock with 4xPLL oscillator --> 1.8432 MHz instruction cycle, Failsafe clock off */
_FWDT(WDT_OFF);                         /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN);           /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF);                    /* Code protect disabled */

void init_pic(void)
{
    // Set up values for GPIO pins
    PIN_LED = 1;		// Turn on LED1

    // Set the direction for GPIO
    TRISBbits.TRISB10 = 0;	// Set frame ack testpoint pin as an output
    TRISBbits.TRISB5 = 0;	// Set pic_ready_bar line to an output
    TRISCbits.TRISC1 = 0;	// Set LED as an output

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD4 = 0;
    TRISGbits.TRISG15 = 0;
    PIN_OSCOPE1 = 0;
    PIN_OSCOPE2 = 0;

    // Short delay to wait for power supply voltages to settle
    __delay32( (long)(0.01 * CLOCKFREQ) );

// Setup the UART - 9.6kbs
    unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
    unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
    OpenUART2( U2MODEvalue, U2STAvalue, BRG_SETTING );
    ConfigIntUART2( UART_RX_INT_DIS & UART_TX_INT_DIS );	// Disable TX and RX interrups

    PIN_LED = 0;		// Default to LED off for    power saving
}

void send_cmd (char* cmd)
{
    TTYPuts(cmd);
    TTYPutc('\r');

    __delay32( (long)(0.125*CLOCKFREQ));
    PIN_LED = 1;
    __delay32( (long)(0.125*CLOCKFREQ));
    PIN_LED = 0;
}

void config_UBC_Sync(void)
{
    PIN_LED = 1;
    __delay32( (long)(0.125*CLOCKFREQ));
    PIN_LED = 0;
    
    //Wait for long time after UBC Sync Box resets while it prints out "welcome" message
    __delay32( (long)(1.0*CLOCKFREQ));

    if( PIN_JP1_1 == 1 ) //PIPER Lab Config
    {
        send_cmd("rl 40");
        send_cmd("nr 25");
        send_cmd("fr 40");
    }
    else if(PIN_JP1_2 == 1 ) //BETTII Config
    {
        send_cmd("rl 64");
        send_cmd("nr 24");
        send_cmd("fr 40");
    }
}

/*
 * Get a string from UBC Sync Box.  Terminate on '\r' or '\n'
 */
void UBC_gets( char *str, int n )
{
    char c;
    unsigned char pos;

    pos = 0;				// set the position in string to be 0

    /* Clear the UART OVERRUN bit
     *
     * UBC Sync Box spits out a lot of characters when it is reset, which can overflow UART buffer
     *
     * If OERR bit is set, no further data is transmitted until OERR is cleared
     *
     * see dsPIC30F Family Reference Sec 16.5.1 (page 104)
     */
    U2STAbits.OERR=0;

    while( 1 )
    {
        //---> if no string, then we can do other stuff for a bit
	if( !DataRdyUART2() )
        {
            // Here's where we can do non-critical, idle-time tasks
            ClrWdt();			// Reset the watchdog timer

            continue;
	}

	//---> Character/Sring received, so deal with it
	c = getcUART2();		// get the input character
	switch( c )
        {
            // terminator
            case '\r':
            case '\n':
            case '\0':
                str[pos] = '\0';
                return;

            // backspace
            case '\b':
                if( pos > 0 )
                {
                    pos--;
                }

                continue;

            // otherwise, add the character to our string
            default:
                if( pos < n-1 )
                {
                    str[pos] = c;
                    pos++;
                }

                continue;
	}
    }
}

int main( void )
{
    init_pic(); 	// Setup the PIC internal peripherals

    // Delay for 1second then spit out the following
    __delay32( (long)(3.0*CLOCKFREQ));
    config_UBC_Sync();

    while (1)
    {
        char s[100];

        UBC_gets(s, sizeof (s)); // Get a command from the user

        if (!strcmp(s, "\tSyncoCmd-V1f")) //if UBC Sync Box Reset
        {
            config_UBC_Sync();
        }
    }

}


