#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>
#include <math.h>

#include "pmotor.h"

// Local function prototypes
static char do_cmd_amp(void);
static char do_cmd_lead_angle(void);
static char do_cmd_find_home(void);
static char do_cmd_enc_home(void);
static char do_cmd_search(void);

static char do_cmd_pid_group_set(void);//Change PID settings to predetermined ones

static char do_cmd_stop(void); //Set mode to free run set amplitude to stop

// Position PID commands
static char do_cmd_pid_pos_s(void); // Adjust setpoint
static char do_cmd_pid_pos_sign(void); // Set the sign for pterm and iterm
static char do_cmd_pid_pos_p(void); // Adjust proportional factor
static char do_cmd_pid_pos_i(void); // Adjust integral factor
static char do_cmd_pid_pos_d(void); // Adjust differential factor
static char do_cmd_pid_pos_p_shift(void); // Adjust fixed point basis for p
static char do_cmd_pid_pos_i_shift(void); // Adjust fixed point basis for i
static char do_cmd_pid_pos_d_shift(void); // Adjust fixed point basis for d
static char do_cmd_pid_pos_vslew(void); // Changes the trapezoid vslew parameter
static char do_cmd_pid_pos_a0(void); // Changes the trapezoid a0 parameter
static char do_cmd_pid_pos_on(void); // Enable the PID loop
static char do_cmd_pid_pos_off(void); // Terminate the PID loop
static char do_cmd_pid_pos_reset(void); // Terminate the PID loop
static char do_cmd_pid_pos_move(void);  // ramp the PID setpoint with a trapezoid velocity profile
static char do_cmd_pid_pos_trap_stop(void); //Ramp down the Trapizoid method

// Velocity PID commands
static char do_cmd_pid_vel_s(void); // Adjust setpoint
static char do_cmd_pid_vel_sign(void); // Set the sign for pterm and iterm
static char do_cmd_pid_vel_p(void); // Adjust proportional factor
static char do_cmd_pid_vel_i(void); // Adjust integral factor
static char do_cmd_pid_vel_p_shift(void); // Adjust fixed point basis for p
static char do_cmd_pid_vel_i_shift(void); // Adjust fixed point basis for i
static char do_cmd_pid_vel_on(void);    // Enable the PID loop
static char do_cmd_pid_vel_off(void);   // Terminate the PID loop
static char do_cmd_pid_vel_reset(void); // Terminate the PID loop
static char do_cmd_pid_vel_ramp(void);  // Velocity setpoint ramp
static char do_cmd_pid_vel_delay(void);
static char do_cmd_pid_vel_delta(void);

// Diagnostic commands
static char do_cmd_mdac(void);

static char do_cmd_led(void); // turn LED on or off
static char do_cmd_p(void); // prints status
static char do_cmd_echo(void); // echo on / off
static char do_cmd_data(void); // data on / off
static char do_cmd_debug1(void); // debug command 1

// helper functions
static void talk(char *s);      // Talks out the port, unless the silent flag is set




struct pidparams_both pid_groups[] = {
    {.vel_p =0, .vel_i=0, .vel_sign=0, .vel_sign=0, .vel_i_shift=0,.vel_p_shift=0,
    .pos_p=0, .pos_d=0, .pos_i=0, .pos_p_shift=0, .pos_d_shift=0, .pos_i_shift=0,
    .vel_ramp_delay=0, .vel_ramp_delta=0}, //ARRAY 0, All values 0

    {.vel_p =15, .vel_i=2, .vel_sign=0, .vel_sign=0, .vel_i_shift=5,.vel_p_shift=0,
    .pos_p=200, .pos_d=0, .pos_i=0, .pos_p_shift=0, .pos_d_shift=0, .pos_i_shift=0,
    .vel_ramp_delay=0, .vel_ramp_delta=0}, //ARRAY 1,Good for unloaded motor

     {.vel_p =25, .vel_i=35, .vel_sign=0, .vel_sign=0, .vel_i_shift=3,.vel_p_shift=0,
    .pos_p=200, .pos_d=0, .pos_i=0, .pos_p_shift=0, .pos_d_shift=0, .pos_i_shift=0,
    .vel_ramp_delay=15, .vel_ramp_delta=1} //ARRAY 2,Good for Big Wheel
};
/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */

void process_cmd(char *cmd)
{
    char *s;
    unsigned char r;

    // Parse the command string
    cmd_parse(cmd);

    // Parse the command string
    s = tok_get_str(); // first token is the command name

    // Discard empty commands
    if (!strcmp(s, ""))
        return;
    int valid_non_home_command = (!strcmp(s,"l")||!strcmp(s,"p")||!strcmp(s,"data")||!strcmp(s,"fh"));
    if (!get_found_home() && !valid_non_home_command){
        char str[100];
        sprintf(str, "YOU MUST FIND HOME BEFORE MOVING!\r\n");
        talk(str);
        return;
    }
    if (!strcmp(s, "a"))
        r = do_cmd_amp();
    else if (!strcmp(s, "l")) // "L" for lead
        r = do_cmd_lead_angle();
    else if (!strcmp(s, "fh"))
        r = do_cmd_find_home();
    else if (!strcmp(s, "enchome"))
        r = do_cmd_enc_home();
    else if (!strcmp(s, "search"))
        r = do_cmd_search();
        
    else if (!strcmp(s,"ppg")) r = do_cmd_pid_group_set();

    else if (!strcmp(s, "pps")) r = do_cmd_pid_pos_s(); // Changes PID setpoint
    else if (!strcmp(s, "ppm")) r = do_cmd_pid_pos_move(); // Changes PID setpoint with trapazoid velocity profile
    else if (!strcmp(s, "ppsign")) r = do_cmd_pid_pos_sign(); // Changes P and I sign
    else if (!strcmp(s, "ppp")) r = do_cmd_pid_pos_p(); // Changes PID p constant
    else if (!strcmp(s, "ppi")) r = do_cmd_pid_pos_i(); // Changes PID i constant
    else if (!strcmp(s, "ppd")) r = do_cmd_pid_pos_d(); // Changes PID d constant
    else if (!strcmp(s, "ppp_shift")) r = do_cmd_pid_pos_p_shift(); // Changes PID p fixed point basis
    else if (!strcmp(s, "ppi_shift")) r = do_cmd_pid_pos_i_shift(); // Changes PID i fixed point basis
    else if (!strcmp(s, "ppd_shift")) r = do_cmd_pid_pos_d_shift(); // Changes PID d fixed point basis
    else if (!strcmp(s, "ppvslew")) r = do_cmd_pid_pos_vslew(); // Changes the trapezoid vslew parameter
    else if (!strcmp(s, "ppm_stop")) r = do_cmd_pid_pos_trap_stop(); // stops the trapizoid method cleanly
    else if (!strcmp(s, "ppa0")) r = do_cmd_pid_pos_a0(); // Changes the trapezoid vslew parameter
    else if (!strcmp(s, "ppon")) r = do_cmd_pid_pos_on(); // Initializes a PID loop
    else if (!strcmp(s, "ppoff")) r = do_cmd_pid_pos_off(); // Terminate the PID loop
    else if (!strcmp(s, "ppreset")) r = do_cmd_pid_pos_reset(); // Terminate the PID loop

    else if (!strcmp(s, "stop")) r = do_cmd_stop(); //set mode to free run set amplitude to stop

    else if (!strcmp(s, "pvs")) r = do_cmd_pid_vel_s(); // Changes PID setpoint
    else if (!strcmp(s, "pvsign")) r = do_cmd_pid_vel_sign(); // Changes P and I sign
    else if (!strcmp(s, "pvp")) r = do_cmd_pid_vel_p(); // Changes PID p constant
    else if (!strcmp(s, "pvi")) r = do_cmd_pid_vel_i(); // Changes PID i constant
    else if (!strcmp(s, "pvp_shift")) r = do_cmd_pid_vel_p_shift(); // Changes PID p fixed point basis
    else if (!strcmp(s, "pvi_shift")) r = do_cmd_pid_vel_i_shift(); // Changes PID i fixed point basis
    else if (!strcmp(s, "pvon")) r = do_cmd_pid_vel_on(); // Initializes a PID loop
    else if (!strcmp(s, "pvoff")) r = do_cmd_pid_vel_off(); // Terminate the PID loop
    else if (!strcmp(s, "pvreset")) r = do_cmd_pid_vel_reset(); // Terminate the PID loop
    else if (!strcmp(s, "pvramp")) r = do_cmd_pid_vel_ramp(); // Initiate a velocity setpoint ramp
    else if (!strcmp(s, "pvramp_delay")) r = do_cmd_pid_vel_delay(); //set the pid velocity ramp delay time
    else if (!strcmp(s, "pvramp_delta")) r = do_cmd_pid_vel_delta();

    else if (!strcmp(s, "mdac"))    r = do_cmd_mdac(); // output a value to the motor dac

    else if (!strcmp(s, "led"))
        r = do_cmd_led(); // set the LED
    else if (!strcmp(s, "p")) // Full print command
        r = do_cmd_p();
    else if (!strcmp(s, "echo"))
        r = do_cmd_echo(); // Sets echo on or off
    else if (!strcmp(s, "data"))
        r = do_cmd_data(); // Sets data on or off
    else if (!strcmp(s, "d1"))
        r = do_cmd_debug1(); // Debug1 command -- who knows what this does???  Try it!  You'll get a kick out of it!
    else
        r = STATUS_FAIL_CMD; // User typed unknown command

    // Print the status, unless we're taking data
    if( !get_data_mode() )
        print_status(r);
}

void talk( char *s )
{
    if( get_data_mode() != DATA_MODE_RAW )
        TTYPuts(s);
}


/** A
 *      Set the sine amplitude in Volts
 *
 *      Arguments:
 *      <v=amplitude in volts>
 *
 *      v is allowed to be a FLOAT between -9.8 and 9.8 V.
 *
 */
static char do_cmd_amp(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(8, 1, 1))
        return STATUS_FAIL_INVALID;
    float a = tok_get_float();

    if (a > 9.8 || a < -9.8) {
        return STATUS_FAIL_RANGE;
    }

    int amp = AMP_TO_DAC(a);

    // set_sine_delta(delta);
    set_amplitude(amp);

    sprintf(str, "*PSG: amp -- Set to %f V (amp=%d counts)\r\n", a, amp);
    talk(str);

    return STATUS_OK;
}

/** L
 *      Set the commutation lead angle in degrees
 *
 *      Arguments:
 *      <la=lead angle in degrees>
 *
 *      la is an INT between -360 and 359
 *
 */
static char do_cmd_pid_group_set(void)
{
    char str[100];

    if (!tok_available())
        return STATUS_FAIL_NARGS;

    char *arg = tok_get_str();
    int array_index;
    if(!strcmp(arg,"zero"))
        array_index = 0;
    else if(!strcmp(arg,"no_load"))
        array_index = 1;
    else if(!strcmp(arg,"big_wheel"))
        array_index = 2;
    else
        return STATUS_FAIL_INVALID;
    
    set_pid_pos_parameter_group(pid_groups[array_index]);
    set_pid_vel_parameter_group(pid_groups[array_index]);



 
    return STATUS_OK;

}


static char do_cmd_lead_angle(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(3, 1, 0))
        return STATUS_FAIL_INVALID;
    int la = tok_get_int16();

    if (la < 0)
        la += 360;

    if (la < 0 || la >= 360) {
        return STATUS_FAIL_RANGE;
    }

    set_lead_angle(la);

    sprintf(str, "*PSG: lead angle -- Set to %d degrees\r\n", la);
    talk(str);

    return STATUS_OK;
}

/** FH
 *      Find the home position
 *
 *      Arguments:
 *      None
 */
static char do_cmd_find_home(void)
{
   char str[100];

    // what amplitude do you want to use to search for home
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(5, 1, 1))
        return STATUS_FAIL_INVALID;
    float amplitude = tok_get_float();
    int amp =AMP_TO_DAC(amplitude);


    sprintf(str, "*PSG: Finding Home -- With Amplitude %f V\r\n", amplitude);
    talk(str);

    find_home(1,amp);
    return STATUS_OK;
}

/** ENCHOME
 *      Set the current position to home.
 *
 *      Arguments:
 *      None
 */
static char do_cmd_enc_home(void)
{
    char str[100];

    set_abs_encoder_home();

    sprintf(str, "*PSG: encoder set to 0!\r\n");
    talk(str);

    return STATUS_OK;
}

/** SEARCH
 *      Search for the optimal lead angle
 *
 *      Arguments:
 *      None
 */
static char do_cmd_search(void)
{
    char str[100];
    int angle;

    angle = search(1);

    sprintf(str, "*PSG: Found optimal lead angle at %d degrees.\r\n", angle);
    talk(str);

    return STATUS_OK;
}

/*-----------------------------------------------------------
 * Position PID Commands
 *-------------------------------------------------------------*/

/* PPM
 *      Ramps the setpoint of the position PID loop towards a final target with
 *      a trapezoidal velocity profile.
 *
 *      Arguments:
 *      <sp_target=int32>
 *
 *      Changes current PID loop's setpoint.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_pos_move(void)
{
    char str[100];

    // Get target value of PID position setpoint
    if (!tok_valid_num(9, 1, 0))
        return STATUS_FAIL_INVALID;
    long sp_target = tok_get_int32();
    set_mode(MODE_PID_POSITION);
    trapezoid_init_move(sp_target);

    sprintf(str, "*PSG: moving to %ld\r\n", sp_target);
    talk(str);

    return STATUS_OK;
}

/* PPS
 *      Adjust the setpoint of current PID loop
 *
 *      Arguments:
 *      <sp=int32>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_pos_s(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(9, 1, 0))
        return STATUS_FAIL_INVALID;
    long setpoint = tok_get_int32();

    set_pid_pos_setpoint(setpoint);

    sprintf(str, "*PSG: pid_pos_s %ld\r\n", setpoint);
    talk(str);

    return STATUS_OK;
}

/* PPSIGN
 *      Adjust the sign (pos or neg) of the P and I terms of the PID loop.
 *
 *      Arguments:
 *      "pos" or "neg"
 *
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_pos_sign(void)
{
    char str[100];

    if (!tok_available())
        return STATUS_FAIL_NARGS;

    char *arg = tok_get_str();
    if (!strcmp(arg, "pos")) {
        set_pid_pos_sign(1);
    } else if (!strcmp(arg, "neg")) {
        set_pid_pos_sign(0);
    } else
        return STATUS_FAIL_INVALID;

    sprintf(str, "*PSG: pid_pos_sign %s\r\n", arg);
    talk(str);

    return STATUS_OK;
}

/* PPP
 *      Adjust the p constant of current PID loop
 *
 *      Arguments:
 *      <p=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_pos_p(void)
{
    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p = (unsigned int) tok_get_int32();

    set_pid_pos_p(p);

    sprintf(str, "*PSG: pid_pos_p\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPP_SHIFT
 *      Adjust the fixed point basis of the proportional term of the PID loop
 *
 *      Arguments:
 *      <p_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the proportional term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_pos_p_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p_shift = (unsigned int) tok_get_int32();

    set_pid_pos_p_shift(p_shift);

    sprintf(str, "*PSG: pid_pos_p_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPI
 *      Adjust the i constant of current PID loop
 *
 *      Arguments:
 *      <i=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_pos_i(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i = (unsigned int) tok_get_int32();

    set_pid_pos_i(i);

    sprintf(str, "*PSG: pid_pos_i\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPI_SHIFT
 *      Adjust the fixed point basis of the integral term of the PID loop
 *
 *      Arguments:
 *      <i_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the integral term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_pos_i_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i_shift = (unsigned int) tok_get_int32();

    set_pid_pos_i_shift(i_shift);

    sprintf(str, "*PSG: pid_pos_i_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPD
 *      Adjust the d constant of current PID loop
 *
 *      Arguments:
 *      <d=uint16>
 *
 *      Changes current PID loop's derivative coefficient. Does not reinitialize
 *      PID loop or change any other PID parameters.
 */
static char do_cmd_pid_pos_d(void)
{
    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int d = (unsigned int) tok_get_int32();

    set_pid_pos_d(d);

    sprintf(str, "*PSG: pid_pos_d\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPD_SHIFT
 *      Adjust the fixed point basis of the differential term of the PID loop
 *
 *      Arguments:
 *      <d_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the differential term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_pos_d_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int d_shift = (unsigned int) tok_get_int32();

    set_pid_pos_d_shift(d_shift);

    sprintf(str, "*PSG: pid_pos_d_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPA0
 *      Adjust the acceleration rate of the trapezoid move, specified in
 *      revs/sec^2.
 *
 *      Arguments:
 *      <a0=float32>
 *
 *      Changes the acceleration rate of the trapzeoid move. Re-initializes the
 *      trapezoid move based on the new acceleration rate. The input unit should
 *      be revolutions per second^2 (Hz^2/rev???).
 */
static char do_cmd_pid_pos_a0(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 1))
        return STATUS_FAIL_INVALID;
    float a0 = tok_get_float();

    a0 = a0*TRAPEZOID_AFACTOR;

    set_trap_a0(a0);
    trapezoid_reinitialize();

    sprintf(str, "*PSG: pid_pos_a0 %f rev/s^2 (%f internal units)\r\n", a0*TRAPEZOID_INVAFACTOR, a0);
    talk(str);

    return STATUS_OK;
}

/* PPM_STOP
 * Tell the trapizoid method to ramp down to zero imediatly
 */
static char do_cmd_pid_pos_trap_stop(void)
{
    trapezoid_deactivate();

    return STATUS_OK;
}

/* PPVSLEW
 *      Adjust the slew rate of the trapezoid move, specified in revs/sec.
 *
 *      Arguments:
 *      <vslew=float32>
 *
 *      Changes the slew rate of the trapzeoid move. Re-initializes the
 *      trapezoid move based on the new slew rate. The input unit should be
 *      revolutions per second (Hz).
 */
static char do_cmd_pid_pos_vslew(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 1))
        return STATUS_FAIL_INVALID;
    float vslew = tok_get_float();

    vslew = vslew*TRAPEZOID_VFACTOR;

    set_trap_vslew(vslew);
    trapezoid_reinitialize();

    sprintf(str, "*PSG: pid_pos_vslew %f Hz (%f internal units)\r\n", vslew*TRAPEZOID_INVVFACTOR, vslew);
    talk(str);

    return STATUS_OK;
}

/* PPON
 *      Enable the PID loop.
 *
 *      Arguments:
 *      None
 *
 *      Starts the PID loop.
 */
static char do_cmd_pid_pos_on(void)
{
    char str[100];

    set_mode(MODE_PID_POSITION);

    sprintf(str, "*PSG: mode = PID_POSITION\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPOFF
 *      Terminate the PID loop.
 *
 *      Arguments:
 *      None
 *
 *      Stops the PID loop. Does not re-initialize anything, since the PID
 *      initialization command initializes the PID variables.
 */
static char do_cmd_pid_pos_off(void)
{
    char str[100];

    set_mode(MODE_FREERUN);
    set_amplitude(0);

    sprintf(str, "*PSG: mode FREERUN\r\n");
    talk(str);

    return STATUS_OK;
}

/* PPRESET
 *      Resets the accumulator.
 *
 *      Arguments:
 *      None
 */
static char do_cmd_pid_pos_reset(void)
{
    char str[100];

    set_pid_pos_reset();

    sprintf(str, "*PSG: pp_reset\r\n");
    talk(str);

    return STATUS_OK;
}

/*-----------------------------------------------------------
 * Velocity PID Commands
 *-------------------------------------------------------------*/

/* PVS
 *      Adjust the setpoint of velocity PID loop
 *
 *      Arguments:
 *      <sp=int16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */

static char do_cmd_stop() //Get out of whatever mode your in and set amplitude to zero
{
    char str[100];
    set_mode(MODE_FREERUN);
    set_amplitude(0);
    sprintf(str, "*PSG: Stop!\r\n");
    talk(str);
}

static char do_cmd_pid_vel_s(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 1, 1))
        return STATUS_FAIL_INVALID;
    float setpoint = tok_get_float();

    set_pid_vel_setpoint(setpoint);

    sprintf(str, "*PSG: pid_vel_s %f\r\n", setpoint);
    talk(str);

    return STATUS_OK;
}

/* PVSIGN
 *      Adjust the sign (pos or neg) of the P and I terms of the PID loop.
 *
 *      Arguments:
 *      "pos" or "neg"
 *
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_vel_sign(void)
{
    char str[100];

    if (!tok_available())
        return STATUS_FAIL_NARGS;

    char *arg = tok_get_str();
    if (!strcmp(arg, "pos")) {
        set_pid_vel_sign(1);
    } else if (!strcmp(arg, "neg")) {
        set_pid_vel_sign(0);
    } else
        return STATUS_FAIL_INVALID;

    sprintf(str, "*PSG: pid_vel_sign %s\r\n", arg);
    talk(str);

    return STATUS_OK;
}

/* PVP
 *      Adjust the p constant of current PID loop
 *
 *      Arguments:
 *      <p=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_vel_p(void)
{
    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p = (unsigned int) tok_get_int32();

    set_pid_vel_p(p);

    sprintf(str, "*PSG: pid_vel_p\r\n");
    talk(str);

    return STATUS_OK;
}

/* PVP_SHIFT
 *      Adjust the fixed point basis of the proportional term of the PID loop
 *
 *      Arguments:
 *      <p_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the proportional term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_vel_p_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p_shift = (unsigned int) tok_get_int32();

    set_pid_vel_p_shift(p_shift);

    sprintf(str, "*PSG: pid_vel_p_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/* PVI
 *      Adjust the i constant of current PID loop
 *
 *      Arguments:
 *      <i=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_vel_i(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i = (unsigned int) tok_get_int32();

    set_pid_vel_i(i);

    sprintf(str, "*PSG: pid_vel_i\r\n");
    talk(str);

    return STATUS_OK;
}

/* PVI_SHIFT
 *      Adjust the fixed point basis of the integral term of the PID loop
 *
 *      Arguments:
 *      <i_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the integral term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_vel_i_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i_shift = (unsigned int) tok_get_int32();

    set_pid_vel_i_shift(i_shift);

    sprintf(str, "*PSG: pid_vel_i_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/* PVON
 *      Enable the PID loop.
 *
 *      Arguments:
 *      None
 *
 *      Starts the PID loop.
 */
static char do_cmd_pid_vel_on(void)
{
    char str[100];

    set_mode(MODE_PID_VELOCITY);

    sprintf(str, "*PSG: mode = PID_VELOCITY\r\n");
    talk(str);

    return STATUS_OK;
}

/* PVOFF
 *      Terminate the PID loop.
 *
 *      Arguments:
 *      None
 *
 *      Stops the PID loop. Does not re-initialize anything, since the PID
 *      initialization command initializes the PID variables.
 */
static char do_cmd_pid_vel_off(void)
{
    char str[100];

    set_mode(MODE_FREERUN);
    set_amplitude(0);

    sprintf(str, "*PSG: mode FREERUN\r\n");
    talk(str);

    return STATUS_OK;
}

/* PVRESET
 *      Resets the accumulator.
 *
 *      Arguments:
 *      None
 */
static char do_cmd_pid_vel_reset(void)
{
    char str[100];

    set_pid_vel_reset();

    sprintf(str, "*PSG: pv_reset\r\n");
    talk(str);

    return STATUS_OK;
}

/* PVRAMP
 *      Start a velocity pid setpoint ramp.
 *
 *      Arguments:
 *      
 */
static char do_cmd_pid_vel_ramp(void)
{
    char str[100];

    // Get final setpoint value
    if (!tok_valid_num(5, 1, 1))
        return STATUS_FAIL_INVALID;
    float sp_final = tok_get_float();


    set_mode(MODE_PID_VELOCITY);
    set_vel_setpoint_ramp(sp_final);

    sprintf(str, "*PSG: pv_ramp\r\n");
    talk(str);

    return STATUS_OK;
}

static char do_cmd_pid_vel_delay(void){
     // Get velocity ramp delay value
    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    int delay = tok_get_float();

    set_pid_vel_ramp_delay(delay);

    sprintf(str, "*PSG: pv_ramp_delay\r\n");
    talk(str);
}


static char do_cmd_pid_vel_delta(void){

    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    int delta = tok_get_float();

     set_pid_vel_ramp_delta(delta);

    sprintf(str, "*PSG: pv_ramp_delta\r\n");
    talk(str);
}

/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/

/** P
 * Prints current readings from all channels
 */
static char do_cmd_p(void)
{
    char str[100]; // max string len is about 51 chars, so 100 is very safe

    int la = get_lead_angle_degrees();
    long abs_pos = get_abs_encoder();
    long velocity = get_velocity();
    int amplitude = get_amplitude();
    int found_home = get_found_home();
    
    sprintf(str, "PSG: angle = %i, enc_pos = %ld, Poscount =%i, found_home = %s\r\n", la, abs_pos, POSCNT, found_home ?"YES" :"NO");
    talk(str);

    sprintf(str, "PSG: velocity = %f Hz (%ld counts/%d ticks)\r\n",
           // (float)velocity/VELOCITY_MEASUREMENT_INTERVAL*4000./5000., velocity, VELOCITY_MEASUREMENT_INTERVAL);
            (float)velocity*VELOCITY_HZ_PER_COUNTS, velocity, VELOCITY_MEASUREMENT_INTERVAL);
    talk(str);

    sprintf(str, "PSG: amplitude = %f V (%d counts)\r\n",
            DAC_TO_AMP(amplitude), amplitude);
    talk(str);

    unsigned char mode = get_mode();

    if (mode == MODE_PID_POSITION) {
        talk("PSG: MODE = PID_POSITION\r\n");
        pid_pos_status();
        trapezoid_status();
    }
    else if (mode == MODE_PID_VELOCITY) {
        talk("PSG: MODE = PID_VELOCITY\r\n");
        pid_vel_status();
    }
    else {
        talk("PSG: MODE = FREERUN\r\n");
        pid_pos_status();
        trapezoid_status();
        pid_vel_status();
    }
    
    return STATUS_OK;
}

/** MDAC
 *	Diagnostic command for setting a value to the MDAC.
 *  Also puts the board in DIAGNOSTIC mode which means that the motor commutation is disabled.
 *
 *	Usage:
 *	mdac 2 1000-> Send a decimal 1000 to the MDAC channel 2
 */
static char do_cmd_mdac(void)
{
    // Get the channel
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(1, 0, 0))
        return STATUS_FAIL_INVALID;
    int ch = tok_get_int16();

    // Get the dac value
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned val = (unsigned int)tok_get_int16();

    // Write the value to the DAC channel
    MDacWrite(ch, val);

    // Latch the values we just set into the MDAC
    PIN_MDAC_LOAD = 0;
    PIN_MDAC_LOAD = 1;

    return STATUS_OK;
}

/** LED
 *	Set LED light.
 *
 *	Usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led(void)
{
    char *arg;

    // Check that the next token is a valid number
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    arg = tok_get_str();
    if (!strcmp(arg, "on")) {
        PIN_LED1 = 1;
    } else if (!strcmp(arg, "off")) {
        PIN_LED1 = 0;
    } else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** ECHO
 *	Sets echo on or off.
 *
 *	Usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if (!strcmp(arg, "on"))
        cmd_set_echo(1);
    else if (!strcmp(arg, "off"))
        cmd_set_echo(0);
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** DATA
 *	Sets backplane data packet reporting on or off.
 *
 *	Usage:
 *	data on		-> sets data on
 *	data off	-> sets data off
 *	data raw	-> enables lightning mode
 */
static char do_cmd_data(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if (!strcmp(arg, "on"))
        set_data_mode(DATA_MODE_ON);
    else if (!strcmp(arg, "raw"))
        set_data_mode(DATA_MODE_RAW);
    else if (!strcmp(arg, "off"))
        set_data_mode(DATA_MODE_OFF);
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** DEBUG1
 *	First debug command.
 *
 *	Usage:
 *	???
 */
static char do_cmd_debug1(void)
{
    char str[100];

//    // Get value of DAC to set
//    if (!tok_valid_num(8, 1, 1))
//        return STATUS_FAIL_INVALID;
//    int a = tok_get_int16();

    DDacWrite(DACREG_DAC, 3, 0);


    sprintf(str, "*PSG: debug1 -- Reset!\r\n");
    talk(str);

    return STATUS_OK;
}

/* This function is really defined in common_functions.c
 * But we can't include that file in this project because it references a bunch of other stuff
 * that we don't need / want for this board.  Should really move print_status to its own
 * file in the /common directory.
 *
 * Print a human readable status message depending on the status code.
 */
void print_status(char status)
{
    switch (status) {
    case STATUS_OK:
        TTYPuts("OK");
        break;
    case STATUS_FAIL_CMD:
        TTYPuts("FAIL: unknown command");
        break;
    case STATUS_FAIL_NARGS:
        TTYPuts("FAIL: incorrect number of arguments");
        break;
    case STATUS_FAIL_INVALID:
        TTYPuts("FAIL: invalid argument format");
        break;
    case STATUS_FAIL_RANGE:
        TTYPuts("FAIL: argument out of range");
        break;
    case STATUS_FAIL_COULD_NOT_COMPLETE:
        TTYPuts("FAIL: the requested operation could not be completed");
        break;
    default:
        TTYPuts("FAIL: unknown error");
        break;
    }

    // All responses must end with ! to signal parser this is end of response
    TTYPuts("!\r\n");
}


