#include <stdio.h>
#include <stdlib.h>

#include "../common/math_utilities.h"
#include "pmotor.h"

// ---------------------
// ----VARIABLE DEFS----
// ---------------------

struct pidparams {
    unsigned p; // P term for PID
    unsigned i; // I term for PID
    unsigned d;  // D term for PID
    long setpoint; // PID setpoint (unrolled encoder units)
    long accumulator; // PID integrator term accumulator
    unsigned char sign; // Sign of P term and I term
    unsigned char i_shift; // I term is divided by 2^i_shift
    unsigned char p_shift; // P term is divided by 2^p_shift
    unsigned char d_shift; // D term is divided by 2^d_shift
    int amp_init; // Initial amplitude
};

static long error;
static long pterm;
static long iterm;
static long dterm;
static int vel;  // #DELME

/* Parameters for Flight Reaction Wheel
     p = 13, p_shift = 4, i = 0, dterm=70, d_shift = 0

 Parameters for bare motor
 p = 200, p_shift = 0, i = 0, d = 0
 */
static struct pidparams pid = {
    .p = 200,
    .i = 0,  // Good (possibly?): i=1, i_shift = 12
    .d = 0,
    .setpoint = 0,
    .accumulator = 0,
    .sign = 0,  // 0 = negative, 1 = positive
    .i_shift = 0,
    .p_shift = 0,
    .d_shift = 0,
    .amp_init = 0};

int compute_pid_pos(long abs_pos)
{
    static long sum;
    static int toret;

    error = abs_pos - pid.setpoint;

    // NOTE: pid.sign is handled here for i coefficient so that accumulator is
    // always continuous.
    if (pid.sign)
        pid.accumulator += pid.i * error;
    else
        pid.accumulator -= pid.i * error;

    if (pid.p != 0) {
        pterm = pid.p * error;

        pterm = fast_rshift_long(pterm, pid.p_shift);
    } else {
        pterm = 0;
    }

    // Compiler handles this switch structure by indexing i_shift into a look-up BRA table
    // so there is no performance cost to having all of the cases
    iterm = fast_rshift_long(pid.accumulator, pid.i_shift);

    vel = get_velocity();
    dterm = pid.d * (long)vel;  // WHY do we need the (long) cast???  //#DELME comment only
    dterm = fast_rshift_long(dterm, pid.d_shift);

    // Compute the full PID output
    // NOTE: Only p term of pid.sign is handled here, since i term handled above
    if (pid.sign)
        sum = (long)pid.amp_init + pterm + iterm + dterm;
    else
        sum = (long)pid.amp_init - pterm + iterm - dterm;

    // Coerce to signed 16-bit integer
    // PID algorithm should naturally fall into range, unless
    // specified set point is impossible to achieve, in which case
    // the controller will rail
    if (sum <= -32768L)
        toret = -32768;
    else if (sum >= 32767L)
        toret = 32767;
    else
        toret = (int)sum;

    return toret;
}



/* set_pid_pos_setpoint
 *
 * Adjusts the current PID loop to have a new setpoint
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * setpoint - value to which to rotate the wheel
 */
void set_pid_pos_setpoint(long setpoint)
{
    pid.setpoint = setpoint;
}

/* set_pid_pos_sign
 *
 * Valid arguments are 0 and 1.
 */
void set_pid_pos_sign(unsigned char sign)
{
    pid.sign = sign;
}

/* set_pid_pos_p
 *
 * Adjusts the current PID loop to have a new p constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_pos_p(unsigned p)
{
    pid.p = p;
}

/* set_pid_pos_p_shift
 *
 * Changes the fixed-point basis of the proportional term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_pos_p_shift(unsigned p_shift)
{
    pid.p_shift = p_shift;
}


/* set_pid_pos_i
 *
 * Adjusts the current PID loop to have a new i constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_pos_i(unsigned i)
{
    pid.i = i;
    if (i==0)
        pid.accumulator = 0;
}

/* set_pid_pos_i_shift
 *
 * Changes the fixed-point basis of the integral term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop. Does attempt to compensate the accumulator for the new scaling
 * factor so that there is not too large a discontinuity. 
 */
void set_pid_pos_i_shift(unsigned i_shift)
{
    int diff = (int)pid.i_shift - (int)i_shift;
    if (diff >=0) {
        pid.accumulator = pid.accumulator >> diff;
    } else {
        pid.accumulator = pid.accumulator << -diff;
    }

    pid.i_shift = i_shift;
}

/* set_pid_pos_d
 *
 * Adjusts the current PID loop to have a new d constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_pos_d(unsigned d)
{
    pid.d = d;
}

/* set_pid_pos_d_shift
 *
 * Changes the fixed-point basis of the derivative term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_pos_d_shift(unsigned d_shift)
{
    pid.d_shift = d_shift;
}

// Zeroes the accumulator.
void set_pid_pos_reset(void)
{
    pid.accumulator = 0;
}

void set_pid_pos_parameter_group(struct pidparams_both params){
    pid.p = params.pos_p;
    pid.d = params.pos_d;
    pid.i = params.pos_i;

    pid.p_shift = params.pos_p_shift;
    pid.d_shift = params.pos_d_shift;
    pid.i_shift = params.pos_i_shift;
}

void pid_pos_status(void)
{
    char str[100]; // max string len is about 51 chars, so 100 is very safe

    sprintf(str, "PSG: ---POSITION PID PARAMETERS---\r\n");
    TTYPuts(str);
    sprintf(str, "PSG: s = %ld, e = %ld\r\n", pid.setpoint, error);
    TTYPuts(str);
    sprintf(str, "PSG: accum = %ld, sign = %u\r\n", pid.accumulator, pid.sign);
    TTYPuts(str);
    sprintf(str, "PSG: p = %u, p_shift = %u, p_term = %ld\r\n", pid.p, pid.p_shift, pterm);
    TTYPuts(str);
    sprintf(str, "PSG: i = %u, i_shift = %u, i_term = %ld\r\n", pid.i, pid.i_shift, iterm);
    TTYPuts(str);
    sprintf(str, "PSG: v = %i, d = %u, d_shift = %u, d_term = %ld\r\n",
            vel, pid.d, pid.d_shift, dterm);
    TTYPuts(str);

}

//char *get_pid_pos_parameters(void)
//{
//    static char str[20];
//    sprintf(str, "%c%.2X%.2X%.2X%.1X%.1X%.1X", pid.sign ? '+' : '-',
//                                               pid.p,
//                                               pid.i,
//                                               pid.d,
//                                               pid.p_shift,
//                                               pid.i_shift,
//                                               pid.d_shift);
//    return str;
//}

long get_pid_pos_setpoint(void)
{
    return pid.setpoint;
}

void get_pid_pos_parameters(void)
{
    fb_putc(pid.sign ? '+' : '-');
    fb_put16(pid.p);
    fb_put16(pid.i);
    fb_put16(pid.d);
    fb_put4(pid.p_shift);
    fb_put4(pid.i_shift);
    fb_put4(pid.d_shift);
}