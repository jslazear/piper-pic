#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>
#include <qei.h>

#include "pmotor.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & ECIO_PLL16); /* External clock with 16xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */

/* Global variables */
unsigned char CardAddress, GroupAddress;

void init_pic()
{
    // Set up values for GPIO pins

    // PAPPA power supply turn-on time may be very slow, so wait 500 ms to
    // make sure the power supply rails have initialized correctly.
    // Without this, DAC was not initializing correctly.
    __delay32((long) (0.5 * CLOCKFREQ)); // Wait 500 ms for DACs to power up

    TRISBbits.TRISB11 = 0; // MUX Address lines
    TRISBbits.TRISB12 = 0; // MUX Address lines
    TRISBbits.TRISB13 = 0; // MUX Address lines
    TRISGbits.TRISG3 = 0; // GAIN Address lines
    TRISGbits.TRISG2 = 0; // GAIN Address lines
    TRISBbits.TRISB8 = 0; // MDAC_CS
    TRISBbits.TRISB9 = 0; // MDAC_LOAD
    TRISBbits.TRISB14 = 0; // DDAC_SYNC
    TRISBbits.TRISB15 = 0; // DDAC_LOAD
    TRISCbits.TRISC14 = 0; // RS-485 Enable
    TRISDbits.TRISD4 = 0; // OSCOPE2
    TRISDbits.TRISD5 = 0; // OSCOPE1
    TRISDbits.TRISD6 = 0; // OSCOPE0
    TRISBbits.TRISB0 = 0; // LED3
    TRISBbits.TRISB1 = 0; // LED2
    TRISBbits.TRISB2 = 0; // LED1
    TRISEbits.TRISE7 = 0; // LED4
    TRISDbits.TRISD8 = 0;  // DIGITAL OUT 1
    TRISCbits.TRISC15 = 0; // DIGITAL OUT 2

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

   
    // Oscope debug pins, set them to outputs
    PIN_OSCOPE0 = 0;
    PIN_OSCOPE1 = 0;
    PIN_OSCOPE2 = 0;

    PIN_LED1 = 1;

    // MUX channel 0
    PIN_A0 = 0;
    PIN_A1 = 0;
    PIN_A2 = 0;

    // Set instrumentation amplifer to Gain=1 (available gains are 1,2,5,10)
    PIN_GAIN0 = 0;
    PIN_GAIN1 = 0;

    // Short delay to wait for power supply voltages to settle
    __delay32((long) (0.01 * CLOCKFREQ));

    // Setup the UART - 115.2 kbs
    config_uart2();

    // Read in our card address and store in global variable
    CardAddress = (PIN_ADDR3 << 3) | (PIN_ADDR2 << 2) | (PIN_ADDR1 << 1) | PIN_ADDR0;
    GroupAddress = (PIN_GROUP2 << 2) | (PIN_GROUP1 <<1) | PIN_GROUP0;
    
    // Setup the SPI port1 (For ADC and DACs)
    OpenSPI1(FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON &
            MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
            PRI_PRESCAL_4_1,
            SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR);
    ConfigIntSPI1(SPI_INT_DIS); // NO SPI interrupts
    SPI1CONbits.SSEN = 0;       // Disable SS1 pin (which allows us to use RB2 pin for LED1)

    //------------------------------------------------------------------------------
    //The SPI module supports 4 different Serial Clock formats. The user can
    //select one of these 4 formats by configuring the Clock Polarity Select, or
    //CKP, and Clock Edge Select, or CKE, bits in the SPI Control Register.
    //See the dsPIC family reference for a description (see Fig. 20-3).
    //------------------------------------------------------------------------------
    SPI1CONbits.CKE = 1; // CKE=1 means SCLK *rising* edge is in the middle of the data bit
                         // CKE=0 means SCLK *falling* edig is in the middle of the data bit
                        // Note: the two dac chips on this board want different values of CKE, so
                        // their respective "write_dac" functions change this value
    SPI1CONbits.CKP = 0; // CKP=0 means SCLK sits low when the SPI port is not transmitting data (do not change this!)
    SPI1CONbits.SMP = 1; // If the SMP bit is set, then the input sampling is done at the end of the bit output

    // Power on and configure the AD5754 DAC chip
    DDacInit();

    OpenQEI(                            //REG QEICON
            QEI_DIR_SEL_QEB &           // Value of B channel determines direction (this is only valid choice)
            QEI_INT_CLK &               // Use main system clock (20 MHz)
//            QEI_INDEX_RESET_DISABLE &    // Reset count to 0 on index pulse  // #FIXME #DELME
            QEI_INDEX_RESET_ENABLE &    // Reset count to 0 on index pulse
            QEI_MODE_x2_PULSE &         // 2x mode (counts on Phase A rising and falling edges)
 //           QEI_MODE_x2_MATCH &       // 2x mode (counts on Phase A rising and falling edges)  // #FIXME #DELME
            QEI_NORMAL_IO &             // The UPDN pin functions as normal IO pin (RD7)
            QEI_INPUTS_NOSWAP &       // Do not swap Phase A and B inputs
//            QEI_INPUTS_SWAP &           // swap Phase A and B inputs
            QEI_IDLE_CON &              // Continue to operate in idle mode (we don't use idle mode, so irrelevant)
            QEI_CLK_PRESCALE_1          // Ignored for QEI mode (only used for timer mode)
            ,                            //REG DFLTCON
            POS_CNT_ERR_INT_DISABLE &   // No interrupts on encoder errors
//            QEI_QE_CLK_DIVIDE_1_64   &   // Digital filter must be stable for 3 scaled clock cycles  // #FIXME #DELME
            QEI_QE_CLK_DIVIDE_1_4   &   // Digital filter must be stable for 3 scaled clock cycles
                                        // 1:4 means must be stable for 0.60 us.
                                        // At 1000 RMP, phase A/B signals are stable for 12 us (24 us period)
            QEI_QE_OUT_ENABLE  &       // Enable the digital filter
            MATCH_INDEX_INPUT_LOW &     //Set Index to match on low  (IMV bit9)
            MATCH_INDEX_INPUT_PHASEB    //Set Index to match on B (IMV1 bit10) Default is bits 9-10 = 11
            );
    
    IEC2bits.QEIIE = 0;                 // Disable QEI interrupt on rollover / index
                                        // Note that MicroChip function ConfigIntQEI(QEI_INT_DISABLE) does NOT work!
                                        // We looked at its source code and it is setting the wrong bit


    MAXCNT = ENCODER_PULSE_PER_REV - 1;   // See Family Reference 16-13

    //#DELME?*  This is likely not needed
    __delay32((long) (0.5 * CLOCKFREQ));

    if( !PIN_CLK_MODE_INTERNAL ) {
        // Before we start the interrupt handler, wait for a rising edge on external frame clock
        // This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
        // Do this just before turning on the timer interrupt.
        sync_to_frame_clock();
    }

    config_timer2();
}

int main(void)
{
    init_pic(); // Setup the PIC internal peripherals

    // Setup our data frame buffers
    init_frame_buf_backplane('R', CardAddress);

    //DDacTest();   // for troubleshooting the diagnostic dac

    set_lead_angle(LEAD_ANGLE_OPTIMUM); // Set the default lead angle

    set_amplitude(AMP_TO_DAC(0));  // Start up in the off state

//    // Find home position on start-up
//    set_amplitude(AMP_TO_DAC(1));  // Must energize coils to be able to find home
////    find_home(1);
////    search(1);
//    set_amplitude(AMP_TO_DAC(1));

    PIN_LED1 = 0;

    PIN_LED1 = 1;

    TTYPutsDirect("Welcome!\r\n");

    /* Encoder testing loop
    while(1 ) {
        char s[100];
        sprintf(s, "%4d   %6ld\r\n", POSCNT, get_abs_encoder() );
        TTYPutsDirect(s);
        __delay32((long) (0.5 * CLOCKFREQ));
    }
    */
    
    while (1) {
        char s[100]; // Note the CH command can have long argument list (up to 90 characters)

        cmd_gets(s, sizeof (s)); // Get a command from the user

        process_cmd(s); // Deal with the command
    }
}

void OnIdle(void)
{
}

//#DELME - move this to a sensible location
// THIS IS A TEST TO SEE IF WE CAN GET PRINTF TO WORK!
int __attribute__((__section__(".libc.write")))
write(int handle, void *buffer, unsigned int len)
{
    // Note: ignore handle -- printf, fprintf(stderr,... will all go out our UART (where else would they go?)
    int i;

    for (i=0; i<len; ++i)
    {
        TTYPutc(*(char*)buffer);
        ++buffer;
    }
    return len;
}
