#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "pmotor.h"
#include "commutation_table.h"

static void update_motor_commutation(void);
static void update_abs_position(void);
static void write_motor_phase(int ch, int index);
static void update_velocity_measurement(void);
static void update_data_raw(void);
static void update_frame(void); // writes a new reading (if we have one) to our data frame
static void adc_update();
static int get_adc_value(int channel);
static unsigned char frame_counter = 0; // Which frame are we on?, a frame holds 16 readings and takes 1 seconds

int tick = 0;
static int lead_angle = 0;
long abs_pos = 0;
int velocity_measurement_tick = 0;
int trapezoid_tick = 0;
int velocity = 0;  // Latest measured velocity, in encoder counts per 1024 ticks
unsigned char new_velocity = 0;  // Whether or not a velocity measurement is current

int amplitude;  // Negative amplitude spins in reverse direction

static volatile unsigned char data_mode_request = DATA_MODE_OFF; // default: data stream is on
static volatile unsigned char data_mode = DATA_MODE_OFF;

static int finding_home=0;
static int adc_channel_tick =0;
unsigned char mode = MODE_FREERUN;
static int ADC_values[15];
static int mux_channel = 0;
static int init_amp = 1;
static int find_home_previous_amp = 0;

void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void)
{
    PIN_OSCOPE2 = 1;

    // Make sure POSCNT does not get above 5000! Not sure why this is necessary,
    // but we encountered a failure mode where POSCNT got up to ~9000 and caused
    // the commutation table index to exceed 625, indexing into random memory.
    // This works around that, though we should probably figure out why it's not
    // behaving at some point...
//    if (POSCNT >= ENCODER_PULSE_PER_REV+1)
//    {
//        POSCNT -= ENCODER_PULSE_PER_REV;
//        DDacWrite(DACREG_DAC, 3, 35000);
//    }
    IFS0bits.OC2IF = 0; // reset Output Compare 2 interrupt flag

    if (adc_channel_tick > ADC_DELAY){
        adc_update();
        adc_channel_tick = 0;
    }
    adc_channel_tick++;
   
    // See if we're starting a new frame
    char isNewFrame;
    if( PIN_CLK_MODE_INTERNAL ) {
        PIN_LED3 = 1;   // #DELME
        isNewFrame = (tick == 0);   // New frame starts on tick 0
    } else {
        // Check if a new frame has started by reading in PIN_FRAME_CLK (keeps all boards in lockstep)
        isNewFrame = check_frame_clock();
    }

    // If we have just started a new frame, then reset all our state variables (happens once per second)
    // This guarantees that we stay in sync with the rest of the backplane, (even if a cosmic ray corrupted tick)
    if (isNewFrame) {
        tick = 0;
        data_mode = data_mode_request;  // only allow data mode to change at the start of a new frame
                                        // this ensures that when switching from "raw" to "on"
                                        // that update_communication is called with isNewFrame == 1 which resets his internal state.
    }

    update_abs_position();

    // Update velocity measurement and PID loop
    ++velocity_measurement_tick;
    if (velocity_measurement_tick > VELOCITY_MEASUREMENT_INTERVAL) {
        velocity_measurement_tick = 0;
        update_velocity_measurement();      // measure velocity based on change in encoder readings
        if (mode == MODE_PID_VELOCITY) {
            update_pid_vel_ramp();          // Ramp the velocity setpoint (does nothing if we're not velocity ramping)
            amplitude = compute_pid_vel(velocity);  // Update our velocity PID loop with newest measurement
        }
    }

    if (mode == MODE_PID_POSITION) {
        ++trapezoid_tick;
        if( trapezoid_tick > TRAPEZOID_UPDATE_INTERVAL) {
            trapezoid_tick = 0;
            trapezoid_update_pid_setpoint();
        }
        amplitude = compute_pid_pos(abs_pos);
    }
    if (get_found_home() || finding_home ||init_amp){ //Have we found home or are we currently trying or are we setting the amplitude to 0 for init
        update_motor_commutation();                   //Otherwise don't allow DAC updates
        init_amp = 0;
    }
    
    // Write encoder position out to the dac so we can look at it with the scope
    int ddac0 = abs_pos + 32768;
    int ddac1 = (velocity<<2)+32768 ;
    DDacWrite(DACREG_DAC, 0, ddac0);
    DDacWrite(DACREG_DAC, 1, ddac1);
    DDacWrite(DACREG_DAC, 2, POSCNT + 32768);  // Write out the encoder position, should wrap every 5000. #DELME?
    DDacWrite(DACREG_DAC, 3, (int)(get_pid_pos_setpoint() + 32768));  // Write out the encoder position, should wrap every 5000. #DELME?

    // Write data
    if( data_mode == DATA_MODE_RAW ) {
       if (!(tick & 0b00001))  // (4000 Hz)/32 = 125 Hz data rate  // #FIXME
           update_data_raw();
       TTYUpdateTXNoFrameFast4();   // Look for characters in TX queue to send over UART
    }
    else {
        update_frame(); // writes a new reading (if we have one) to our data frame
        update_communication(isNewFrame); // Update address counters and transmit buffer in our time slot (does not look at tick)
    }

    // For backplane mode, this is a redundant check.  This is required for internal clock mode
    ++tick;
    if (tick >= TICKS_PER_FRAME ) {
        tick = 0;
    }
  
    PIN_OSCOPE2 = 0; 
}
static void adc_update(){ //channels 1-8 per data sheet
    
    SPI1CONbits.CKE = 1; // CKE=0 means SCLK falling edges are in the middle of the data bits (because AD5754 DAC reads on the falling edge)
    SPI1CONbits.MODE16 = 1; // Set to 16 bit mode
    int data_read =0;
    SPI1BUF = 0xFFFF;
    while (!SPI1STATbits.SPIRBF);
    data_read = SPI1BUF;
    ADC_values[mux_channel]=data_read;
    if (mux_channel<8)
        mux_channel++;
    else
        mux_channel = 0;
    
    PIN_A0 = mux_channel & 0b001;
    PIN_A1 = mux_channel & 0b010;
    PIN_A2 = mux_channel & 0b100;
}

static int get_adc_value(int channel){
    if (channel<0 || channel>8)
        return -1;
    return ADC_values[channel];
}

static void update_data_raw(void)
{
    // CHARS_PER_DATA_WRITE = 12.
    //TTYPutcFromInt('^');
    //TTYPut16FromInt(amplitude);
    //TTYPutcFromInt(' ');
    //TTYPut32FromInt(abs_pos);
  

        TTYPrintHex16(velocity);
        TTYPutcFromInt('\r');
        TTYPutcFromInt('\n');
}


// Phase offsets for indexing into the sine table to generate three-phase
#define PHASE_OFFSET2   ((unsigned)(COMMUTATION_TABLE_LEN * 1.0 / 3 + 0.5))
#define PHASE_OFFSET3   ((unsigned)(COMMUTATION_TABLE_LEN * 2.0 / 3 + 0.5))

static void update_motor_commutation(void)
{

    int enc = POSCNT;   // Grab the 16 bit encoder position. (copy into local variable because it is changing asynchronously)
                        // We don't need the 32 bit abs_pos value here, since just interested
                        // in the position within a rotation.
   
    // We need two electrical cycles per real rotation
    while (enc > ENCODER_MAX / 2) // ENCODER_MAX/2 == 2499
        enc -= ENCODER_MAX / 2 + 1; //Want 4999 to be sent to 2499
    if( enc < 0 )  // Before encoder has found home, it can read out negative
        enc = 0;
    
    // Divide by 4 to convert Encoder pos to table since table is only 625 long
    enc >>= 2;

    // Offset the phase by the lead angle
    enc += lead_angle;
    if (enc >= COMMUTATION_TABLE_LEN)
        enc -= COMMUTATION_TABLE_LEN;

    // Ouput the three phase
    // NOTE: swapping PHASE_OFFSET2 and PHASE_OFFSET3 reverses motor direction
    // WANT: if amplitude > 0, then encoder counts in the positive direction. PHASE_OFFSETn chosen to enforce this.
    write_motor_phase(0, enc);
    write_motor_phase(1, enc + PHASE_OFFSET2);      // enc + 120 degrees
    write_motor_phase(2, enc + PHASE_OFFSET3);      // enc + 240 degrees

    // amplitude has 1 sign bit and 15 value bits, but the amplitude DAC is 16-bit unipolar. Extract the absolute value
    // and convert to 16 bits of value (instead of 15 bits).
    unsigned int tamp;

    if( amplitude == -32768 ) {
        // -32768 is a very strange value in 16 bit arithmetic
        // -(-32768) == -32768
        tamp = 65535;
    }
    else {
        tamp = (amplitude >= 0) ? amplitude : -amplitude;
        tamp = tamp << 1;
    }
    MDacWrite(3, tamp);            // Sine wave amplitude

    // Load the values we just set into the MDAC
    PIN_MDAC_LOAD = 0;
    PIN_MDAC_LOAD = 1;
}

static void write_motor_phase(int ch, int index)
{
    if (index >= COMMUTATION_TABLE_LEN)
        index -= COMMUTATION_TABLE_LEN;

    int tval = commutation_table[index];
    if (amplitude < 0)
        tval = -tval;
    tval += 32768;      // For AD5544 channels 0-2, 32768 corresponds to 0 volts
    MDacWrite(ch, (unsigned int)tval);
}

static void update_abs_position(void)
{
    static int old_enc_pos = 0;
    int cur_enc_pos = POSCNT;   // POSCNT is updated asynchronous (by hardware), so snapshot it for this function

    int delta = cur_enc_pos - old_enc_pos;
    old_enc_pos = cur_enc_pos;

    // Check for encoder wrap-around. Starts aliasing at 8 kHz = 480 krpm for a 4 kHz interrupt rate.
    // Only works for max(delta) << 2^16.
    if (delta > ENCODER_PULSE_PER_REV/2)
        delta -= ENCODER_PULSE_PER_REV;
    else if (delta < -ENCODER_PULSE_PER_REV/2)
        delta += ENCODER_PULSE_PER_REV;

    abs_pos += delta;
}

// Measures change in encoder position (in encoder counts) since last called.
// Call this routine at a fixed cadence, to measure velocity.
static void update_velocity_measurement(void)
{
    long delta;
    static long last_pos = 0;
    long cur_pos = abs_pos;         // snapshot abs_pos
    delta = cur_pos - last_pos;
    last_pos = cur_pos;

    velocity = delta;
    new_velocity = 1;
}

static void update_frame() {
    // Start of the frame
    if( tick == 0 ) {
        fb_put8(frame_counter); // Save the frame counter as 1st entry in new frame buffer

        unsigned char status = 0; // get_cur_mode();
        fb_put8(status);
    }
    if( tick == 2 ) {
        fb_put32(abs_pos);      // Absolute position
        fb_put16(velocity);     // Velocity
        fb_put16(amplitude);    // Sine wave amplitude
        fb_putc(mode);          // Board mode ('F', 'V', or 'P')
        fb_put12((int)(((long)lead_angle*360/COMMUTATION_TABLE_LEN)));
    }
    if (tick == 3) {
        get_pid_pos_parameters();
    }
    if (tick == 4) {
        get_pid_vel_parameters();
    }

    // Check if we are on the last tick of the frame
    if (tick == TICKS_PER_FRAME-1 ) {
        PIN_LED2 = 1;
        // This buffer is full, so swap to new one and queue up the old one for sending
        swap_active_frame_buf(); // Swap which frame buffer we're writing to
        if (data_mode)
            enqueue_last_frame_buf();

        // Update frame_counter
        frame_counter++;
    }
}


char find_home(unsigned char verbose,int amplitude)
{
    char str[100];
    int la; // Lead angle
    find_home_previous_amp = get_amplitude(); //Save what the current amplitude is
    finding_home=1; //Alert the system that you are currently searching for home
    set_amplitude(amplitude); //Set the amplitude
    
    IFS2bits.QEIIF = 0;  // Reset found_home flag to make sure search starts fresh

    if (verbose) {
        sprintf(str, "*PSG: Searching for home. Please wait...\r\n");
        TTYPuts(str);
    }

    for (la = 0; la < 720; la += 15) {
        if (verbose) {
            sprintf(str, "*PSG: Setting lead angle to %d degrees...\r\n", la);
            TTYPuts(str);
        }

        set_lead_angle(la);
        __delay32((long) (1 * CLOCKFREQ)); // Wait 1 s for DACs to power up
        if (get_found_home()) {
            if (verbose) {
                sprintf(str, "*PSG: Found home!\r\n");
                TTYPuts(str);
            }
            set_lead_angle(LEAD_ANGLE_OPTIMUM); // Set to optimal lead angle.
            set_amplitude(find_home_previous_amp); //Reset the amplitude to what it was when you started (Usually 0)
            return EXIT_SUCCESS;
        }
    }
    if (verbose) {
        sprintf(str, "*PSG: Could not find home! Setting amplitude to 0.\r\n");
        TTYPuts(str);
    }
    set_amplitude(0); // Prevent power dissipation in boost since could not find home.
    finding_home=0; //Alert the system that you are no longer searching for home
    return EXIT_FAILURE;
}


//-------------- SETTERS / GETTERS --------------

void set_lead_angle(int degrees)
{
    lead_angle = (int) (((long) degrees * COMMUTATION_TABLE_LEN) / 360);
}

// Set sine wave amplitude in raw units (-32768 to 32767)
void set_amplitude(int amp)
{
    amplitude = amp;
}

// Set mode
void set_data_mode(char flag)
{
    data_mode_request = flag;
}

// Set mode
void set_mode(unsigned char new_mode)
{
    if (new_mode != MODE_PID_POSITION)
        trapezoid_deactivate();
    mode = new_mode;
}

// Set encoder to 0
void set_abs_encoder_home(void)
{
    abs_pos = 0;
}

int get_amplitude(void)
{
    return amplitude;
}

unsigned char get_mode(void)
{
    return mode;
}

char get_data_mode(void)
{
    return data_mode;
}

unsigned char get_found_home(void)
{
    //return found_home;
    return IFS2bits.QEIIF;      // This interrupt flag is set by the hardware whenever motor crosses the index mark
}

// Get the current value of the absolute encoder.
long get_abs_encoder(void)
{
    return abs_pos;
}

// Get the current value of the velocity (in encoder counts/tick).
int get_velocity(void)
{
    new_velocity = 0;
    return velocity;
}

// A blocking version of get_velocity... waits on next valid velocity measurement before returning
int get_velocity_blocking(void)
{
    while (!new_velocity) {}  // Block until a new velocity measurement is available
    new_velocity = 0;
    return velocity;
}

int get_lead_angle(void)
{
    return lead_angle;
}

int get_lead_angle_degrees(void)
{
    return (int)(((long)lead_angle*360/COMMUTATION_TABLE_LEN));
}

