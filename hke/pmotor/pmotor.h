#ifndef __PMOTOR_INCLUDED
#define __PMOTOR_INCLUDED

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

//---------> Some useful stuff
#define CLOCKFREQ 20000000L		// 20.000 MHz instruction clock (synthesised from 5 MHz crystal, 16x PLL)

// #define BRG_SETTING 2       // 416 kbs.  This is the fastest rate we can generate with a 20 MHz FCY that is
                            // compatible with the MAC using our FTDI chip USB to Fiber board.
                            // BRG = FCLK/(BaudRate*16) - 1
// Calculate the actual BAUD rate from the specified BRG setting
// #define BAUD_RATE           (CLOCKFREQ/(BRG_SETTING+1) >> 4)

#define BAUD_RATE	115200
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)- 1 + 0.5)))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Add 0.5 to convert to int from float
#define ENCODER_PULSE_PER_REV     5000      // 2500 lines per revolution and we run hardware in 2x mode
#define ENCODER_MAX       (ENCODER_PULSE_PER_REV - 1)
#define LEAD_ANGLE_OPTIMUM      33   // Guessed optimum lead angle

#define NUM_ADC_CHANNELS	8
#define CONVERT_PULSE_STOP_TICK 100  // 100 gives a 5 us pulse on the ADC convert pin

// Motor control modes (these character constants are sent out in the data packet over the serial port direclty)
#define MODE_FREERUN      'F'
#define MODE_PID_POSITION 'P'
#define MODE_PID_VELOCITY 'V'

#define DATA_MODE_OFF     0     // Board will report no data
#define DATA_MODE_ON      1     // Standard hex-encoded backplane-style data packets
#define DATA_MODE_RAW     2     // Super-fast, binary data (aka "lightning"  or "ronset" mode)

// Interval measured in ticks at which we compute velocity by differencing the encoder readings.
// An interval of 100 ticks gives a velocity loop update rate of 40 Hz.
// It gives a velocity precision of 0.008 revs / sec ~ 0.5 RPM
// calculated as (1 count)/(100 ticks)*(4000 ticks/s)/(5000 counts/rev) =
#define VELOCITY_MEASUREMENT_INTERVAL   100

// Multiply this by velocity in hardware units to get velocity in Hz.  NOTE THE FLOAT (slow!!!)
#define VELOCITY_HZ_PER_COUNTS  ((float) TICKS_PER_FRAME / ((float)VELOCITY_MEASUREMENT_INTERVAL * ((float)ENCODER_PULSE_PER_REV)))

// How long to dwell before incrementing the velocity ramp setpoint *in units of VELOCITY_MEASUREMENT_INTERVAL*
//#define VRAMP_UPDATE_DWELL 15 //Moved into pid_vel to make setable

// Interval measured in ticks at which we update the PID position setpoint for trapezoid slews
// An interval of 10 ticks gives an update rate of 400 Hz.
#define TRAPEZOID_UPDATE_INTERVAL 10

//------> INPUT PINS
#define PIN_ADDR0           PORTEbits.RE1   // Backplane card address HEX switch
#define PIN_ADDR1           PORTEbits.RE2   // Backplane card address HEX switch
#define PIN_ADDR2           PORTEbits.RE3   // Backplane card address HEX switch
#define PIN_ADDR3           PORTEbits.RE0   // Backplane card address HEX switch

#define PIN_GROUP0          PORTDbits.RD7   // Command Group OCT switch bit 0
#define PIN_GROUP1          PORTFbits.RF0   // Command Group OCT switch bit 1
#define PIN_GROUP2          PORTFbits.RF1   // Command Group OCT switch bit 2

#define PIN_SYNC_CLK        PORTDbits.RD3   // From the Backplane 96 pin connector
#define PIN_FRAME_CLK       PORTDbits.RD2   // From the Backplane 96 pin connector

#define PIN_CLK_MODE_INTERNAL     PORTCbits.RC13  // Low = External (Backplane) clock / High = on-board crystal clock

#define PIN_LIMIT_A         PORTDbits.RD0   // External limit switch input A (normally high / low = stop)
#define PIN_LIMIT_B         PORTDbits.RD11  // External limit switch input B (normally high / low = stop)
#define PIN_HOME            PORTDbits.RD10  // External home switch input (normally high / low = home)

#define PIN_EXT_SPARE       PORTDbits.RD9   // External switch - unknown purpose

//-----> OUTPUT PINS
#define PIN_A0			LATBbits.LATB11	// ADC input mux
#define PIN_A1			LATBbits.LATB12	// ADC input mux
#define PIN_A2			LATBbits.LATB13	// ADC input mux
#define PIN_GAIN0               LATGbits.LATG3	// ADC Instrumentation Amplifier LSB
#define PIN_GAIN1               LATGbits.LATG2	// ADC Instrumentation Amplifier MSB

#define PIN_MDAC_CS		LATBbits.LATB8	// Motor DAC Chip Select Bar (low = select)
#define PIN_MDAC_LOAD		LATBbits.LATB9	// Motor DAC Load Dac (low = load)

#define PIN_DDAC_SYNC		LATBbits.LATB14	// Diagnostic DAC Sync pin (active low)
#define PIN_DDAC_LOAD		LATBbits.LATB15	// Diagnostic DAC Load pin (low = load)

#define PIN_485_ENABLE          LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

#define PIN_DIG_OUT1		LATDbits.LATD8  // Digital Out 1 (to 96 pin connector)
#define PIN_DIG_OUT2            LATCbits.LATC15	// Digital Out 2 (to 96 pin connector)

// Diagnostic outputs
#define PIN_OSCOPE0		LATDbits.LATD6  // Diagnostic pin
#define PIN_OSCOPE1		LATDbits.LATD5  // Diagnostic pin
#define PIN_OSCOPE2		LATDbits.LATD4  // Diagnostic pin

#define PIN_LED1		LATBbits.LATB2	// Green LED
#define PIN_LED2		LATBbits.LATB1	// Yellow LED
#define PIN_LED3		LATBbits.LATB0	// Red LED
#define PIN_LED4		LATEbits.LATE7	// Red LED
#define PIN_LED                 PIN_LED1        // Standard backplane boards require a PIN_LED symbol (flashes on sync_to_frame_clock)

//  V_adc = ADC * 20480 mV / 65536 = ADC * 10 / 2^5
#define ADC_TO_MV(x) (((x>>4)*5)-10240)

// DAC Hardware
#define DACREG_DAC			0
#define DACREG_OUTPUT_RANGE		1
#define DACREG_POWER_CONTROL            2
#define DACREG_CONTROL			3

// DAC conversions
#define AMP_TO_DAC(x) (10000.0 * (x) / 3.00)
#define DAC_TO_AMP(x) (3.00 * (x) / 10000.0)

// ------------- TRAPEZOID STUFF ------------

// Trapezoid update frequency (~40 Hz)
#define TRAPEZOID_FREQ ((float)TICKS_PER_FRAME / TRAPEZOID_UPDATE_INTERVAL)

// Has units of (Encoder Counts / Trapezoid timestep) / (motor revs / second)
// Multiply this by a desired motor revs/second to get vslew
#define TRAPEZOID_VFACTOR   (ENCODER_PULSE_PER_REV / TRAPEZOID_FREQ)
#define TRAPEZOID_INVVFACTOR   (TRAPEZOID_FREQ / ENCODER_PULSE_PER_REV)
#define TRAPEZOID_VSLEW0 (8 * TRAPEZOID_VFACTOR)

// Has units of (Encoder Counts / Trapezoid timestep^2) / (motor revs / second^2)
// Multiply this by a desired motor revs/second^2 to get a0
#define TRAPEZOID_AFACTOR   (TRAPEZOID_VFACTOR / TRAPEZOID_FREQ)
#define TRAPEZOID_INVAFACTOR   (TRAPEZOID_FREQ / TRAPEZOID_VFACTOR)
#define TRAPEZOID_A0 (4 * TRAPEZOID_AFACTOR)

#define ADC_DELAY 100 //delay between channel switches in ticks


//---------> Function Prototypes
// psyncadc.c
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// process_cmd.c
void process_cmd( char *cmd );

// do_cmd.c
void write_mux( unsigned short ch );

// cmd_gets.c
void cmd_gets( char *str, int n );

// dac.c
void DDacInit( void );
void DDacTest( void );
void DDacWrite( int reg, int ch, int data );
void MDacWrite(int ch, unsigned int data);

// int_handler.c
long measure_velocity(void);
char find_home(unsigned char verbose, int amplitude);
void set_lead_angle( int degrees );
void set_amplitude(int amp);
void set_mode(unsigned char new_mode);
void set_data_mode(char flag);
void set_abs_encoder_home(void);
unsigned char get_mode(void);
char get_data_mode(void);
unsigned char get_found_home(void);
int get_amplitude(void);
long get_abs_encoder(void);
int get_velocity(void);
int get_velocity_blocking(void);
int get_lead_angle(void);
int get_lead_angle_degrees(void);

// sinegen.c
void update_sines( void );
void update_freq_ramp( void );

void set_sine_delta(long d);
long compute_delta( float freq );
void set_freq_ramp(long target_delta, int seconds);

// pid_pos.c
int compute_pid_pos(long abs_pos);
void set_pid_pos_setpoint(long setpoint);
void set_pid_pos_sign(unsigned char sign);
void set_pid_pos_p(unsigned p);
void set_pid_pos_p_shift(unsigned p_shift);
void set_pid_pos_i(unsigned i);
void set_pid_pos_i_shift(unsigned i_shift);
void set_pid_pos_d(unsigned d);
void set_pid_pos_d_shift(unsigned d_shift);
void set_pid_pos_reset(void);
void pid_pos_status(void);
long get_pid_pos_setpoint(void);
void get_pid_pos_parameters(void);
void set_pid_pos_parameter_group(pidparams_both);

// pid_vel.c
int compute_pid_vel(int velocity);
void update_pid_vel_ramp();
void set_pid_vel_setpoint(float setpoint);
void set_pid_vel_sign(unsigned char sign);
void set_pid_vel_p(unsigned p);
void set_pid_vel_p_shift(unsigned p_shift);
void set_pid_vel_i(unsigned i);
void set_pid_vel_i_shift(unsigned i_shift);
void set_pid_vel_reset(void);
void pid_vel_status(void);
void set_vel_setpoint_ramp(float setpoint_final);
void set_pid_vel_ramp_delay(unsigned delay);
void set_pid_vel_ramp_delta(unsigned delta);
void get_pid_vel_parameters(void);
void set_pid_vel_parameter_group(pidparams_both);

// trapezoid.c
void trapezoid_init_move(long end_pos);
void trapezoid_reinitialize(void);
void trapezoid_update_pid_setpoint(void);
float trapezoid_compute_velocity(void);
void trapezoid_deactivate(void);
void set_trap_vslew(float vslew);
void set_trap_a0(float a0);
void trapezoid_status(void);
void trapezoid_stop(void);

// search.c
int search(unsigned char verbose);

struct pidparams_both {
    unsigned vel_p; // P term for PID
    unsigned vel_i; // I term for PID
    unsigned char vel_sign; // Sign of P term and I term
    unsigned char vel_i_shift; // I term is divided by 2^i_shift
    unsigned char vel_p_shift; // P term is divided by 2^p_shift

    unsigned pos_p; // P term for PID
    unsigned pos_i; // I term for PID
    unsigned pos_d;  // D term for PID
    unsigned char pos_sign; // Sign of P term and I term
    unsigned char pos_i_shift; // I term is divided by 2^i_shift
    unsigned char pos_p_shift; // P term is divided by 2^p_shift
    unsigned char pos_d_shift; // D term is divided by 2^d_shift

    int vel_ramp_delay;
    int vel_ramp_delta;
};

#endif //__PMOTOR_INCLUDED