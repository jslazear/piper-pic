#include <stdio.h>
#include <stdlib.h>

#include "../common/math_utilities.h"
#include "pmotor.h"


// ---------------------
// ----VARIABLE DEFS----
// ---------------------

struct pidparams {
    unsigned p; // P term for PID
    unsigned i; // I term for PID
    int setpoint; // PID setpoint (velocity units = counts/1024ticks)
    long accumulator; // PID integrator term accumulator
    unsigned char sign; // Sign of P term and I term
    unsigned char i_shift; // I term is divided by 2^i_shift
    unsigned char p_shift; // P term is divided by 2^p_shift
    int amp_init; // Initial amplitude
    long error_last;
    int ramp_delay;
    int ramp_delta;
};

static struct pidparams pid = {
    .p = 25,
    .i = 35,
    .setpoint = 0,
    .accumulator = 0,
    .sign = 0,
    .i_shift = 3,
    .p_shift = 0,
    .amp_init = 0,
    .error_last = 0,
    .ramp_delay = 0,
    .ramp_delta = 0
};

struct vrampparms {
    int sp_final;   // destination setpoint
    int delta;      // delta is added onto setpoint at each update step
                    // Delta = 0 means that we are not ramping!
    int dwell_count;    // Dwells N VELOCITY_MEASUREMENT_INTERVALS before advancing to the next setpoint value
};

static struct vrampparms vrp = {
    .sp_final = 0,
    .delta = 0,
    .dwell_count = 0,
};

int compute_pid_vel(int velocity)
{
    static long error;
    static long pterm;
    static long iterm;
    static long sum;
    static int toret;
    //    static char buf[100];
   
    error = velocity - pid.setpoint;

    // NOTE: pid.sign is handled here for i coefficient so that accumulator is
    // always continuous.

    //Derivitive part
    //FIXME? DELME?

    //Anti Wind-up
    int amplitude = get_amplitude();
    int abs_amplitude = amplitude >= 0 ?amplitude :-amplitude;
    if(abs_amplitude < 30000){
        if (pid.sign)
            pid.accumulator += pid.i * error;
        else
            pid.accumulator -= pid.i * error;
    }

    if (pid.p != 0) {
        pterm = pid.p * error;

        pterm = fast_rshift_long(pterm, pid.p_shift);
    } else {
        pterm = 0;
    }

    // Compiler handles this switch structure by indexing i_shift into a look-up BRA table
    // so there is no performance cost to having all of the cases
    iterm = fast_rshift_long(pid.accumulator, pid.i_shift);

    // Compute the full PID output
    // NOTE: Only p term of pid.sign is handled here, since i term handled above
    if (pid.sign)
        sum = (long)pid.amp_init + pterm + iterm;
    else
        sum = (long)pid.amp_init - pterm + iterm;

    // Coerce to signed 16-bit integer
    // PID algorithm should naturally fall into range, unless
    // specified set point is impossible to achieve, in which case
    // the controller will rail
    if (sum <= -32768L)
        toret = -32768;
    else if (sum >= 32767L)
        toret = 32767;
    else
        toret = (int)sum;

    return toret;
}

/*
 * Updates the velocity PID setpoint if we are in vramp mode.
 * Does nothing if not in vramp mode.
 */
void update_pid_vel_ramp()
{
    // Delta == 0 means we are not ramping
    if(vrp.delta == 0)
        return;

    // We only update the velocity ramp setpoint ever so often
    ++vrp.dwell_count;
    if( vrp.dwell_count  < pid.ramp_delay )
        return;
    vrp.dwell_count = 0;

    // convert to a 32-bit value for the comparisons to avoid 16-bit integer wrap around errors
    long sp = pid.setpoint;

    
    // Add in the delta
//    long error = get_velocity() - pid.setpoint;
//    if (abs(error)<=abs(.15*pid.setpoint)) //DELME? FIXME
//        sp += vrp.delta;

    sp += vrp.delta;
    
    if( vrp.delta > 0 && sp >= vrp.sp_final ) {
        sp = vrp.sp_final;  // Set to exactly what the user requested
        vrp.delta = 0;      // Turn off ramping
    }
    else if( vrp.delta < 0 && sp <= vrp.sp_final) {
        sp = vrp.sp_final;  // Set to exactly what the user requested
        vrp.delta = 0;      // Turn off ramping
    }

    // Set the actual setpoint variable -- need to cast back to 16-bit integer
    pid.setpoint = (int)sp;
}

/* set_pid_vel_setpoint
 *
 * Adjusts the current PID loop to have a new setpoint
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * setpoint - value to which to rotate the wheel
 */
void set_pid_vel_setpoint(float setpoint) //passed in as Hz
{
    float setpoint_counts_per_ticks;
    //setpoint_counts_per_ticks=setpoint*VELOCITY_MEASUREMENT_INTERVAL*5000./4000.;
    setpoint_counts_per_ticks=setpoint/VELOCITY_HZ_PER_COUNTS;
    pid.setpoint = (int) setpoint_counts_per_ticks;
}

/* set_pid_vel_sign
 *
 * Valid arguments are 0 and 1.
 */
void set_pid_vel_sign(unsigned char sign)
{
    pid.sign = sign;
}

/* set_pid_vel_p
 *
 * Adjusts the current PID loop to have a new p constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_vel_p(unsigned p)
{
    pid.p = p;
}

/* set_pid_vel_p_shift
 *
 * Changes the fixed-point basis of the proportional term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */

void set_pid_vel_p_shift(unsigned p_shift)
{
    pid.p_shift = p_shift;
}


/* set_pid_vel_i
 *
 * Adjusts the current PID loop to have a new i constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_vel_i(unsigned i)
{
    pid.i = i;
}

/* set_pid_vel_i_shift
 *
 * Changes the fixed-point basis of the integral term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop. Does attempt to compensate the accumulator for the new scaling
 * factor so that there is not too large a discontinuity. 
 */
void set_pid_vel_i_shift(unsigned i_shift)
{
    int diff = (int)pid.i_shift - (int)i_shift;
    if (diff >=0) {
        pid.accumulator = pid.accumulator >> diff;
    } else {
        pid.accumulator = pid.accumulator << -diff;
    }

    pid.i_shift = i_shift;
}

void set_pid_vel_ramp_delay(unsigned delay)
{
    pid.ramp_delay = delay;
}

void set_pid_vel_ramp_delta(unsigned delta)
{
    pid.ramp_delta = delta;
}

/* set_vel_setpoint_ramp
 * 
 * Initiate a velocity setpoint ramp
 */
void set_vel_setpoint_ramp(float setpoint_final) // In Hz
{

    float setpoint_final_ticks;
    // Take the absolute value of delta
    pid.ramp_delta = pid.ramp_delta >= 0 ? pid.ramp_delta : -pid.ramp_delta;
    // If we are ramping down in velocity, ensure that delta is negative
    if( setpoint_final < pid.setpoint )
        pid.ramp_delta = -pid.ramp_delta;
    //setpoint_final_ticks=setpoint_final*VELOCITY_MEASUREMENT_INTERVAL*5000./4000.;
    //delta_ticks=delta*VELOCITY_MEASUREMENT_INTERVAL*5000./4000.;
    setpoint_final_ticks=setpoint_final/VELOCITY_HZ_PER_COUNTS;   
    vrp.sp_final = (int)setpoint_final_ticks;
    vrp.delta = pid.ramp_delta;
    vrp.dwell_count = 0;

    char str[100]; //DELME
    sprintf(str, "final_set %f (%d), delta %i\r\n", setpoint_final, vrp.sp_final,vrp.delta);
    TTYPuts(str); //DELME
}

// Zeroes the accumulator.
void set_pid_vel_reset(void)
{
    pid.accumulator = 0;
}
void set_pid_vel_parameter_group(struct pidparams_both params){
    pid.p = params.vel_p;
    pid.i = params.vel_i;
    pid.p_shift = params.vel_p_shift;
    pid.i_shift = params.vel_i_shift;
    pid.ramp_delay = params.vel_ramp_delay;
    pid.ramp_delta = params.vel_ramp_delta;
}

void pid_vel_status(void)
{

    char str[100]; // max string len is about 51 chars, so 100 is very safe

    long error = (long)get_velocity() - (long)pid.setpoint;
    long iterm = fast_rshift_long(pid.accumulator, pid.i_shift);
    long pterm;



    if (pid.p != 0) {
        pterm = pid.p * error;

        pterm = fast_rshift_long(pterm, pid.p_shift);
    } else {
        pterm = 0;
    }

    sprintf(str, "PSG: ---VELOCITY PID PARAMETERS---\r\n");
    TTYPuts(str);
    sprintf(str, "PSG: s = %d, e = %ld\r\n", pid.setpoint, error);
    TTYPuts(str);
    sprintf(str, "PSG: accum = %ld, sign = %u\r\n", pid.accumulator, pid.sign);
    TTYPuts(str);
    sprintf(str, "PSG: p = %u, p_shift = %u, p_term = %ld\r\n", pid.p, pid.p_shift, pterm);
    TTYPuts(str);
    sprintf(str, "PSG: i = %u, i_shift = %u, i_term = %ld\r\n", pid.i, pid.i_shift, iterm);
    TTYPuts(str);
}

void get_pid_vel_parameters(void)
{
    fb_putc(pid.sign ? '+' : '-');
    fb_put16(pid.p);
    fb_put16(pid.i);
    fb_put4(pid.p_shift);
    fb_put4(pid.i_shift);
}