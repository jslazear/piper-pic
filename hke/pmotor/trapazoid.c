#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "../common/math_utilities.h"
#include "pmotor.h"

struct trapezoid_parameters {
    int active;     // 0 = not doing trapezoid slew; 1 = doing trapezoid ramp
    long start_pos;
    long distance;   // end position in encoder counts
    float vstart;   // Initial velocity at the start of the trapazoid motion in counts
    float a0;       // acceleration in encoder counts / timestep^2 (magnitude only)
    float vslew0;    // velocity in encoder counts / timestep (magnitude only)
    float a1;       // Effective acceleration (including sign) for the initial acceleration phase
    float a2;       // Effective acceleration (including sign) for the final deceleration phase
    float vslew;  // Effective slew rate (including sign)
    long end_pos;   // The requested end position in counts

    long t;     // Time in trapezoid timesteps (TRAPEZOID_UPDATE_INTERVAL / 4000 in seconds)
    
    // Recomputed for each motion (should go in a different structure!!)
    long ta;    // Time to switch ...
    long tb;
    long tf;
    long xa;    // Position at ta
    long xb;    // Position at tb
};

static struct trapezoid_parameters p = {
    .active = 0,
    .vslew0 = 8*TRAPEZOID_VFACTOR, // TRAPEZOID_VSLEW0,
    .a0 = 4*TRAPEZOID_AFACTOR //  TRAPEZOID_A0
};

void trapezoid_init_move(long end_pos)
{
    // Should compute this (if computing it) before changing any of the parameters
    // Or else you might not compute it correctly!
    if (p.active) { // If trapezoid is active, compute from model
        p.vstart = trapezoid_compute_velocity();  // Already in trapezoid counts/s
    } else { // Otherwise measure it. This kinda sucks.
        p.vstart = get_velocity() * VELOCITY_HZ_PER_COUNTS; // Convert velocity counts to Hz
        p.vstart *= TRAPEZOID_VFACTOR;  // Convert from Hz to trapezoid counts
    }

    p.end_pos = end_pos;
    p.t = 0;
    p.start_pos = get_abs_encoder();    // We always start where we are
    p.distance = p.end_pos - p.start_pos;

    // compute the distance needed to decelerate at a0 to a complete stop
    float xstop = (p.vstart > 0 ?1.0 :-1.0)*(p.vstart*p.vstart)/(2*p.a0);

    // The slewing portion of our motion may actually need to be in reverse
    // if we would otherwise overshoot the target (because we can't decelerate fast enough)
    p.vslew = (xstop < p.distance) ? p.vslew0 : -p.vslew0;

    // Calculate a1 from vslew and vstart
    p.a1 = (p.vslew - p.vstart > 0) ? p.a0 : -p.a0;
    p.a2 = (p.vslew < 0) ?p.a0 : -p.a0;

    // Compute the time at which the acceleration ramp up will complete
    p.ta = (p.vslew - p.vstart) / p.a1;
    p.xa = p.vstart*p.ta + 0.5*p.a1*(p.ta*p.ta);

    // Compute the time at which we have to start decelerating to stop at the right spot
    float dtf = p.vslew0/p.a0;  // dtf = tf - tb
    float dxf = p.vslew*dtf + 0.5*p.a2*dtf*dtf;  // x_f - x_b
    p.xb = p.distance - dxf;

    // Compute how long we need to slew for
    float dtslew = (p.xb - p.xa) / p.vslew;
    p.tb = p.ta + dtslew;
    if (dtslew < 0) { // then triangle thing
        float sign = p.a1 > 0 ?1.0 :-1.0;
        p.ta = (sign*sqrt(0.5*p.vstart*p.vstart + p.a1*p.distance) - p.vstart)/p.a1;  //
        p.tb = p.ta;        // Triangle case -> ta = tb (no slewing here)
        p.vslew = p.vstart + p.a1*p.ta;  // This is the velocity at turn-around (the "tip of the triangle")
                                         // which is smaller in magnitude than the vslew0.
                                         // Calling this vslew is a little weired because we never slew in this case
        // Recompute quantities based on new values of ta, tb, and vslew
        dtf = p.vslew/p.a1;     // tf - tb
        if (dtf < 0)
            dtf = -dtf;
        p.xa = p.vstart*p.ta + 0.5*p.a1*p.ta*p.ta;
        p.xb = p.xa;
    }

    p.tf = p.tb + dtf;

    char str[100];
    sprintf(str, "start_pos: %ld\r\n", p.start_pos );
    TTYPuts(str);
    sprintf(str, "distance: %ld\r\n", p.distance );
    TTYPuts(str);
    sprintf(str, "vslew: %f\r\n", p.vslew );
    TTYPuts(str);
    sprintf(str, "a1: %f\r\n", p.a1 );
    TTYPuts(str);
    sprintf(str, "a2: %f\r\n", p.a2 );
    TTYPuts(str);
    sprintf(str, "ta: %ld\r\n", p.ta );
    TTYPuts(str);
    sprintf(str, "tb: %ld\r\n", p.tb );
    TTYPuts(str);
    sprintf(str, "tf: %ld\r\n", p.tf );
    TTYPuts(str);
    sprintf(str, "xa: %ld\r\n", p.xa );
    TTYPuts(str);
    sprintf(str, "xb: %ld\r\n", p.xb );
    TTYPuts(str);

    // Do this last!  As soon as this bit is set, interrupt handler will start motion, so all parameters better be set!
    p.active = 1;           // start trapezoid motion
}

void trapezoid_reinitialize(void)
{
    if (p.active)
        trapezoid_init_move(p.end_pos);
}

void trapezoid_update_pid_setpoint(void)
{
    // Only do these calculations if we are trapezoiding
    if( !p.active )
        return;

    long pos, t = p.t;

    if( t < p.ta ) {       // Acceleration ramp
        pos = (p.vstart * t) + (0.5 * p.a1) * (t * t);
    }
    else if( t < p.tb ) {  // Constant velocity slew
        float dt = t - p.ta;
        pos = p.xa + (long)(p.vslew * dt);
    }
    else if( t < p.tf ) {      // Deceleration ramp
        float dt = t - p.tb;
        pos = p.xb + (long)((p.vslew*dt) + (0.5 * p.a2) * (dt * dt));
    }
    else {      // We're done!
        pos = p.distance;   // Ensure that the final PID position setpoint is the requested destination
        p.active = 0;      // trapezoidal motion complete!
    }

    ++p.t;  // increment to the next timestep

    // Change the position PID setpoint
    set_pid_pos_setpoint(p.start_pos + pos);
}

float trapezoid_compute_velocity(void)
{
    // Only do these calculations if we are trapezoiding
    if( !p.active )
        return 0.;

    long t = p.t;

    if( t < p.ta ) {       // Acceleration ramp
        return p.vstart + p.a1 * t;
    }
    else if( t < p.tb ) {  // Constant velocity slew
        return p.vslew;
    }
    else if( t < p.tf ) {      // Deceleration ramp
        return p.vslew + p.a2 * (t - p.tb);
    }
    else {      // We're done!
        return 0.;
    }
}

void trapezoid_deactivate(void)
{
    p.active = 0;
}

void set_trap_vslew(float vslew)
{
    p.vslew0 = vslew;
}

void set_trap_a0(float a0)
{
    p.a0 = a0;
}

void trapezoid_status(void)
{
    char str[100]; // max string len is about 51 chars, so 100 is very safe

    sprintf(str, "PSG: ---TRAPEZOID PARAMETERS---\r\n");
    TTYPuts(str);
    sprintf(str, "PSG: ACTIVE = %c, t = %ld\r\n", p.active ? 'T' : 'F', p.t);
    TTYPuts(str);
    sprintf(str, "PSG: a0 = %f rev/s^2, vslew = %f Hz\r\n", p.a0*TRAPEZOID_INVAFACTOR, p.vslew0*TRAPEZOID_INVVFACTOR);
    TTYPuts(str);
    sprintf(str, "PSG: start_pos = %ld, end_pos = %ld\r\n", p.start_pos, p.end_pos);
    TTYPuts(str);
    sprintf(str, "PSG: ta (xa) = %ld (%ld), tb (xb) = %ld (%ld), tf (xf) = %ld (%ld)\r\n", p.ta, p.xa, p.tb, p.xb, p.tf, p.end_pos);
    TTYPuts(str);
}
