/* These functions are the low-level interface to an AD5754 Quad 16-bit DAC.
 * On this board, this DAC is used for diagnostic output only.
 */
#include <libpic30.h>
#include <spi.h>
#include "pmotor.h"

void DDacWrite(int reg, int ch, int data)
{
    int data_read = 0; // Data read back from the DAC

    PIN_DDAC_LOAD = 0; // Single DAC update mode

    SPI1CONbits.CKE = 0; // CKE=0 means SCLK falling edges are in the middle of the data bits (because AD5754 DAC reads on the falling edge)
    SPI1CONbits.MODE16 = 0; // Set to 8 bit mode
    PIN_DDAC_SYNC = 0; // Set DAC mode to low (means dac output is updated immediately upon rising edge of DAC_SYNC)

    // Send control byte
    unsigned char cmd = ((reg & 0x7) << 3) | (ch & 0x7);
    SPI1BUF = cmd;
    while (!SPI1STATbits.SPIRBF); // Wait for xmit to be finished
    data_read = SPI1BUF; // Read in data from DAC, necessary to clear SPIRBF

    // Send data bytes
    SPI1CONbits.MODE16 = 1; // Set to 16 bit mode (also what's needed for ADC)
    SPI1BUF = data;
    while (!SPI1STATbits.SPIRBF); // Wait for xmit to be finished
    data_read = SPI1BUF; // Read in data from DAC, necessary to clear SPIRBF

    PIN_DDAC_SYNC = 1; // Latch the data in the DAC
}

void DDacInit()
{
    // Set the DAC output ranges (4 = +/- 10V)
    // Datasheet says first communication with DAC should be to set output voltage ranges
    DDacWrite(DACREG_OUTPUT_RANGE, 0, 4);
    DDacWrite(DACREG_OUTPUT_RANGE, 1, 4);
    DDacWrite(DACREG_OUTPUT_RANGE, 2, 4);
    DDacWrite(DACREG_OUTPUT_RANGE, 3, 4);

    DDacWrite(DACREG_POWER_CONTROL, 0, 0x0f); // Power on all 4 DACS + voltage reference

    DDacWrite(DACREG_CONTROL, 1, 0b1100); // Enable thermal shutdown, 20 mA current clamp; all others disabled.
}

void DDacTest()
{
    unsigned i;

    __delay32((long) (0.01 * CLOCKFREQ));

    while (1) {
        for (i = 0; i < 65535; ++i) {
            DDacWrite(DACREG_DAC, 0, i);
            __delay32(250);
        }
    }
}

/*
 * Writes to one of the AD5544 4-channel multiplying DAC latch registers.
 * Note: You must strobe MDAC_LOAD *low* for one clock cycle in order to actually change the DAC outputs.
 */
void MDacWrite(int ch, unsigned int data)
{
    int data_read = 0; // Data read back from the DAC

    PIN_MDAC_LOAD = 1; // Ensure load pin is high (strobe low when all dacs are written to)
    PIN_MDAC_CS = 0;   // Chip select must be low throughout write

    // Set the CKE bit -- AD5544 DAC reads on the rising edge
    SPI1CONbits.CKE = 1; // CKE=1 means SCLK rising edges are in the middle of the data bit (for CKP=0)
    SPI1CONbits.MODE16 = 0; // Set to 8 bit mode

    // Send control byte
    SPI1BUF = ch;
    while (!SPI1STATbits.SPIRBF); // Wait for xmit to be finished
    data_read = SPI1BUF; // Read in data from DAC, necessary to clear SPIRBF

    // Send data bytes
    SPI1CONbits.MODE16 = 1; // Set to 16 bit mode (also what's needed for ADC)
    SPI1BUF = data;
    while (!SPI1STATbits.SPIRBF); // Wait for xmit to be finished
    data_read = SPI1BUF; // Read in data from DAC, necessary to clear SPIRBF

    PIN_MDAC_CS = 1;   // Latch the data in the DAC
}

// Clear all MDAC channels
void MDacClear()
{
    MDacWrite(0,0);
    MDacWrite(1,0);
    MDacWrite(2,0);
    MDacWrite(3,0);

    // Load the values we just set into the MDAC
    PIN_MDAC_LOAD = 0;
    PIN_MDAC_LOAD = 1;
}
