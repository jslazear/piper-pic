#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <math.h>

#include "psquid.h"
#include "version.h"
#include "../common/ptty.h"
#include "../common/common.h"

// Local function prototypes
static char do_cmd_dac(void); // Set a DAC
static char do_cmd_sweep(void); // Sweep a DAC
static char do_cmd_sweep_off(void);  // Terminate any sweep
static char do_cmd_data(void); // Report data until interrupted
static char do_cmd_raw_data(void); // Report data until interrupted
static char do_cmd_data_off(void);  // Terminate the data reporting
static char do_cmd_raw_data_off(void);  // Terminate the raw data reporting
static char do_cmd_rs(void); // Select stage 1 row
static char do_cmd_p(void); // prints status
static char do_cmd_echo(void); // changes the echo mode of the board
static char do_cmd_version(void); // prints version of the code
static char do_cmd_speriod(void); // Adjust sampling rate
static char do_cmd_pid_ch(void); // Adjust setpoint
static char do_cmd_pid_s(void); // Adjust setpoint
static char do_cmd_pid_sign(void);  // Set the sign for pterm and iterm
static char do_cmd_pid_p(void); // Adjust proportional factor
static char do_cmd_pid_i(void); // Adjust integral factor
static char do_cmd_pid_p_shift(void); // Adjust fixed point basis for p
static char do_cmd_pid_i_shift(void); // Adjust fixed point basis for i
static char do_cmd_pid(void); // Adjust setpoint
static char do_cmd_pid_off(void);  // Terminate the PID loop
//static char do_cmd_data_test(void);  // Testing command for data rewrite #DELME!
//static char do_cmd_output_mode(void);  // Sets whether binary or ascii-hex output should be used #DELME!

static void print_interrupt_rate(char *str); // Helper function prints info about interrupt rate


#define CMP	!strcmp

/*
 * Process a user command contained in the command buffer.
 */
void process_cmd(char *cmd)
{
    char *s;
    unsigned char r;
    char silent;

    // Parse the command string
    cmd_parse(cmd);

    s = tok_get_str(); // next token is the command name

    if (CMP(s, "dac")) r = do_cmd_dac(); // Set a DAC
    else if (CMP(s, "sweep")) r = do_cmd_sweep(); // Sweep a DAC
    else if (CMP(s, "sweep_off")) r = do_cmd_sweep_off(); // Terminate sweep
    else if (CMP(s, "data")) r = do_cmd_data(); // Report data until interrupted
    else if (CMP(s, "raw_data")) r = do_cmd_raw_data(); // Report raw data until interrupted
    else if (CMP(s, "data_off")) r = do_cmd_data_off(); // Interrupt the data reporting.
    else if (CMP(s, "raw_data_off")) r = do_cmd_raw_data_off(); // Interrupt the raw data reporting.
    else if (CMP(s, "rs")) r = do_cmd_rs(); // Select stage 1 row
    else if (CMP(s, "p")) r = do_cmd_p(); // prints dac status
    else if (CMP(s, "echo")) r = do_cmd_echo(); // Sets echo on or off
    else if (CMP(s, "ver")) r = do_cmd_version(); // prints software version
    else if (CMP(s, "pid_ch")) r = do_cmd_pid_ch(); // Changes PID channel
    else if (CMP(s, "speriod")) r = do_cmd_speriod();  // Changes the sampling rate
    else if (CMP(s, "pid_s")) r = do_cmd_pid_s(); // Changes PID setpoint
    else if (CMP(s, "pid_sign")) r = do_cmd_pid_sign(); // Changes P and I sign
    else if (CMP(s, "pid_p")) r = do_cmd_pid_p(); // Changes PID p constant
    else if (CMP(s, "pid_i")) r = do_cmd_pid_i(); // Changes PID i constant
    else if (CMP(s, "pid_p_shift")) r = do_cmd_pid_p_shift(); // Changes PID p fixed point basis
    else if (CMP(s, "pid_i_shift")) r = do_cmd_pid_i_shift(); // Changes PID i fixed point basis
    else if (CMP(s, "pid")) r = do_cmd_pid(); // Initializes a PID loop
    else if (CMP(s, "pid_off")) r = do_cmd_pid_off(); // Terminate the PID loop
//    else if (CMP(s, "datatest")) r = do_cmd_data_test();  // Testing command for data rewrite #DELME!
//    else if (CMP(s, "output_mode")) r = do_cmd_output_mode();  // Testing command for data rewrite #DELME!
    else r = STATUS_FAIL_CMD; // User typed unknown command

    // Print out the status, usually "OK!\r\n"
    silent = get_silent_flag();
    if (!silent) {
        print_status(r);
    }
}

/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/

/** P
 *      Prints board status.
 *
 *      Usage:
 *      p   -> Prints board status
 *
 */
static char do_cmd_p(void)
{
    char str[160];

    int ch = get_pid_ch();
    unsigned int s = get_pid_s();
    unsigned int p = get_pid_p();
    unsigned int i = get_pid_i();
    unsigned char p_shift = get_pid_p_shift();
    unsigned char i_shift = get_pid_i_shift();
    unsigned char mux = get_mux();
    //    unsigned char binary_flag = get_binary_flag();

    sprintf(str, "*PSQUID\r\nPID.ch = %d\r\n", ch);
    TTYPuts(str);

    sprintf(str, "PID.s = %x\r\n", s);
    TTYPuts(str);

    char *sign = (get_pid_sign() == PID_SIGN_POS) ?"pos" :"neg";
    sprintf(str, "PID.sign = %s\r\nPID.p = %d\r\nPID.i = %d\r\n", sign, p, i);
    TTYPuts(str);

    sprintf(str, "PID.p_shift = %d\r\nPID.i_shift = %d\r\n", p_shift, i_shift);
    TTYPuts(str);

    sprintf(str, "mux_ch = %d\r\n", mux);
    TTYPuts(str);

    // Print out the interrupt rate info
    print_interrupt_rate(str);
    
    return STATUS_OK;
}

static void print_interrupt_rate(char *str)
{
    unsigned p = get_interrupt_period();

    // Calculate the rate (for display and validation only)
    float rate = (float)CLOCKFREQ / p;

    sprintf(str, "Interrupt rate = %.2f Hz\r\n", rate);
    TTYPuts(str);

    // Sanity check the rate
    if( rate >= MAX_RAW_DATA_RATE ) {
        sprintf(str, "Warning, rate exceeds max raw data rate (%ld Hz)\r\n", MAX_RAW_DATA_RATE);
        TTYPuts(str);
    }

    // Calculate the minimum rate / dwell
    int minDataDwell = (int)ceil( (rate * CHARS_PER_DATA_WRITE)/(BAUD_RATE/10) );
    int minSweepDwell = (int)ceil( (rate * CHARS_PER_SWEEP_WRITE)/(BAUD_RATE/10) );

    sprintf(str, "min data dwell = %d\r\n", minDataDwell);
    TTYPuts(str);

    sprintf(str, "min sweep dwell = %d\r\n", minSweepDwell);
    TTYPuts(str);
}

/** ECHO
 *	Sets echo on or off.
 *
 *	Usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if (CMP(arg, "on"))
        cmd_set_echo(1);
    else if (CMP(arg, "off"))
        cmd_set_echo(0);
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;

}

/** VER
 *	Spits out a version string for the current code.
 *
 *	Usage:
 *	No arguments
 */
static char do_cmd_version(void)
{
    /* Just for debuggin' new baud rates
    int i;

    while(1) {
        for( i=0; i<127; ++i)
            TTYPutc(i);
    }
    */
    
    // Print the board name and mode
    char silent = get_silent_flag();
    if (!silent) {
        TTYPuts("*PSQUID ");

        TTYPuts(VERSION_STRING);

    }
    return STATUS_OK;
}

/*-----------------------------------------------------------
 * PSquid Commands
 *-------------------------------------------------------------*/

/* SPERIOD
 *   Adjust the interrupt period and thus the ADC sampling rate PID control loop rate.
 *
 * Arguments:
 *   <p=uint16>
 */
static char do_cmd_speriod(void)
{
    char silent = get_silent_flag();

    // Get the period
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p = (unsigned int) tok_get_int16();

    // Set it
    set_interrupt_period(p);

    if( !silent ) {
        char str[100];

        sprintf(str, "*PSQUID: speriod %u.\r\n", p);
        TTYPuts(str);

        print_interrupt_rate(str);
    }

    return STATUS_OK;
}

/* DAC
 *      Set a DAC
 *
 *      Arguments:
 *      <address=integer dac identifier>
 *      <DAC code>
 *
 *      Set the output of the specified DAC (all DACs are included) to the specified code
 */
static char do_cmd_dac(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    int ch = tok_get_int16();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get value of DAC to set
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    int val = tok_get_int16();

    set_dac(val, ch);
    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: dac\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* SWEEP
 *      Sweep a DAC
 *
 *      Arguments:
 *      <writeaddress=integer dac identifier>
 *      <readaddress=integer dac identifier>
 *      <low=DAC code>
 *      <high=DAC code>
 *      <delta=DAC code>
 *      <dwell=int>
 *      <ntimer=int>
 *      <nsweeps=int>
 *
 *      Sweep the the specified DAC between low code and high code, using steps
 *      of delta (in code), dwelling at each step location for dwell integer
 *      samples. Waits for ntimer OC2 interrupts before accumulating. Sweeps
 *      nsweeps times; if nsweeps=0, sweeps continuously until issued a
 *      sweep_stop command or a data command.
 */
static char do_cmd_sweep(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    int ch = tok_get_int16();
    if (ch > 8) {
        return STATUS_FAIL_RANGE;
    }

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    unsigned char read_ch = (unsigned char) tok_get_int16();
    if (read_ch > 8) {
        return STATUS_FAIL_RANGE;
    }

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int low = (unsigned int) tok_get_int32();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get high value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int high = (unsigned int) tok_get_int32();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get step size of sweep
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    long delta = tok_get_int32();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get dwell period
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int dwell = (unsigned int) tok_get_int16();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get wait period ntimer
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int ntimer = (unsigned int) tok_get_int16();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get number of sweeps, nsweeps
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int nsweeps = (unsigned int) tok_get_int16();

    nsweeps *= 2;       // set_sweep take number of half-periods (since switch to triangle wave sweeps)
    set_sweep(ch, read_ch, low, high, delta, dwell, ntimer, nsweeps);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: sweep\r\n");
        TTYPuts(str);
    }

    //    sprintf(str, "ch: %d\r\n", ch);
    //    TTYPuts(str);
    //
    //    sprintf(str, "read_ch: %d\r\n", read_ch);
    //    TTYPuts(str);
    //
    //    sprintf(str, "low: %u\r\n", low);
    //    TTYPuts(str);
    //
    //    sprintf(str, "high: %u\r\n", high);
    //    TTYPuts(str);
    //
    //    sprintf(str, "delta: %ld\r\n", delta);
    //    TTYPuts(str);
    //
    //    sprintf(str, "dwell: %u\r\n", dwell);
    //    TTYPuts(str);
    //
    //    sprintf(str, "ntimer: %u\r\n", ntimer);
    //    TTYPuts(str);
    //
    //    sprintf(str, "nsweeps: %u\r\n", nsweeps);
    //    TTYPuts(str);

    return STATUS_OK;
}

/* SWEEP_OFF
 *      Terminate the currently active sweep.
 *
 *      Arguments:
 *      None
 *
 *      Stops any active sweep. Completely resets the sweep state.
 */
static char do_cmd_sweep_off(void)
{
    char str[100];

    set_sweep(-1, 0, 0, 0, 1, 1, 0, 1);

    sprintf(str, "*PSQUID: sweep_off\r\n");
    TTYPuts(str);

    return STATUS_OK;
}

/* DATA
 *      Report data until interrupted
 *
 *      Arguments:
 *      <ch=integer dac identifier>
 *      <navg=int>
 *
 *      Reports back (ADC sample, read DAC code) pairs until interrupted.
 *      Averages together navg samples before reporting back.
 */
static char do_cmd_data(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    unsigned char ch = (unsigned char) tok_get_int16();
    if (ch > 8) {
        return STATUS_FAIL_RANGE;
    }

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get dwell period
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int dwell = (unsigned int) tok_get_int16();

    set_data(ch, dwell);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: data\r\n");
        TTYPuts(str);
    }
    
    return STATUS_OK;
}

/* RAW_DATA
 *      Report raw data until interrupted
 *
 *      Arguments:
 *      <ch=integer dac identifier>
 *
 *      Reports back raw DAC or ADC values until interrupted. Reports back
 *      the DAC value of the specified channel if the PID loop is active,
 *      otherwise reports back the ADC value.
 *
 *      Does no accumulation or averaging on the values before reporting them
 *      back.
 */
static char do_cmd_raw_data(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    unsigned char ch = (unsigned char) tok_get_int16();
    if (ch > 8) {
        return STATUS_FAIL_RANGE;
    }

    set_raw_data(ch);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: raw_data\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* DATA_OFF
 *      Terminate the data reporting
 *
 *      Arguments:
 *      None
 *
 *      Terminates the reporting of data. Resets all data parameters.
 */
static char do_cmd_data_off(void)
{
    char str[100];

    set_data(-1, 1);

    sprintf(str, "*PSQUID: data_off\r\n");
    TTYPuts(str);

    return STATUS_OK;
}

/* RAW_DATA_OFF
 *      Terminate the raw_data reporting
 *
 *      Arguments:
 *      None
 *
 *      Terminates the reporting of raw data.
 */
static char do_cmd_raw_data_off(void)
{
    char str[100];

    set_raw_data(-1);

    sprintf(str, "*PSQUID: raw_data_off\r\n");
    TTYPuts(str);

    return STATUS_OK;
}

/* PID_CH
 *      Adjust the channel of current PID loop
 *
 *      Arguments:
 *      <ch=uint16>
 *
 *      Changes current PID loop's channel. Does not reinitialize PID loop
 *      or change any other PID parameters. Does not allow for a negative
 *      channel, so issue pid_off to disable PID loop.
 */
static char do_cmd_pid_ch(void)
{
    char str[100];

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    int ch = (int) tok_get_int16();

    if (ch > 8) {
        return STATUS_FAIL_RANGE;
    }

    set_pid_ch(ch);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_ch\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* PID_S
 *      Adjust the setpoint of current PID loop
 *
 *      Arguments:
 *      <sp=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_s(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int setpoint = (unsigned int) tok_get_int32();

    set_pid_setpoint(setpoint);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_s\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* PID_SIGN
 *      Adjust the sign (pos or neg) of the P and I terms of the PID loop.
 *
 *      Arguments:
 *      "pos" or "neg"
 *
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_sign(void)
{
    char str[100];

    // Get low value of sweep
     if( !tok_available() )
        return STATUS_FAIL_NARGS;

    char *arg = tok_get_str();
    if( CMP(arg,"pos") ) {
        set_pid_sign(PID_SIGN_POS);
    }
    else if( CMP(arg,"neg"))  {
        set_pid_sign(PID_SIGN_NEG);
    }
    else
        return STATUS_FAIL_INVALID;

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_sign %s\r\n", arg);
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* PID_P
 *      Adjust the p constant of current PID loop
 *
 *      Arguments:
 *      <p=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_p(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p = (unsigned int) tok_get_int32();

    set_pid_p(p);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_p\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* PID_P_SHIFT
 *      Adjust the fixed point basis of the proportional term of the PID loop
 *
 *      Arguments:
 *      <p_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the proportional term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_p_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p_shift = (unsigned int) tok_get_int32();

    set_pid_p_shift(p_shift);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_p_shift\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}


/* PID_I
 *      Adjust the i constant of current PID loop
 *
 *      Arguments:
 *      <i=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_i(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i = (unsigned int) tok_get_int32();

    set_pid_i(i);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_i\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* PID_I_SHIFT
 *      Adjust the fixed point basis of the integral term of the PID loop
 *
 *      Arguments:
 *      <i_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the integral term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_i_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i_shift = (unsigned int) tok_get_int32();

    set_pid_i_shift(i_shift);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_i_shift\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* PID
 *      Initialize and begin a PID loop
 *
 *      Arguments:
 *      <ch=int16[0, 8]>
 *      <s=uint16>
 *      <p=uint16>
 *      <i=uint16>
 *      <p_shift=uint8>
 *      <i_shift=uint8>
 *
 *      Sets the PID parameters to the specified values and then enables the
 *      PID loop. Reinitializes the accumulator to 0.
 */
static char do_cmd_pid(void)
{
    char str[100];

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    int ch = (int) tok_get_int16();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get setpoint
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int s = (unsigned int) tok_get_int32();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get p constant
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p = (unsigned int) tok_get_int32();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get i constant
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i = (unsigned int) tok_get_int32();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get p_shift
    if (!tok_valid_num(3, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned char p_shift = (unsigned char) tok_get_int32();

    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get i_shift
    if (!tok_valid_num(3, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned char i_shift = (unsigned char) tok_get_int32();

    set_pid(ch, p, i, s, i_shift, p_shift);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* PID_OFF
 *      Terminate the PID loop.
 *
 *      Arguments:
 *      None
 *
 *      Stops the PID loop. Does not re-initialize anything, since the PID
 *      initialization command initializes the PID variables.
 */
static char do_cmd_pid_off(void)
{
    char str[100];

    set_pid_ch(-1);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: pid_off\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}

/* DATATEST
 *      Convenient testing command for data rewrite.
 *
 *      #DELME!
 */
//static char do_cmd_data_test(void)
//{
//    char str[100];
//
//    set_data(0, 1000);
//
//    char silent = get_silent_flag();
//    if (!silent) {
//        sprintf(str, "*PSQUID: data_test\r\n");
//        TTYPuts(str);
//    }
//
//    return STATUS_OK;
//}

/* OUTPUT_MODE
 *      Sets the output mode, 0 for ascii-hex or non-zero for binary
 *
 *      #DELME!
 */
//static char do_cmd_output_mode(void)
//{
//    char str[100];
//
//    // Ensure that token is available
//    if (!tok_available())
//        return STATUS_FAIL_NARGS;
//
//    // Get the channel (0-8)
//    if (!tok_valid_uint(1))
//        return STATUS_FAIL_INVALID;
//    char value = tok_get_uint8();
//
//    set_binary_flag(value);
//
//    char silent = get_silent_flag();
//    if (!silent) {
//        sprintf(str, "*PSQUID: output_mode\r\n");
//        TTYPuts(str);
//    }
//
//    return STATUS_OK;
//}

/* RS
 *      Select stage 1 row
 *
 *      Arguments:
 *      <row=int>
 *
 *      Select which row address line to drive, specified by integer.
 */
static char do_cmd_rs(void)
{
    char str[100];
    
    // Ensure that token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the channel (0-8)
    if (!tok_valid_uint(1))
        return STATUS_FAIL_INVALID;
    unsigned char ch = tok_get_uint8();

    set_mux(ch);

    char silent = get_silent_flag();
    if (!silent) {
        sprintf(str, "*PSQUID: rs\r\n");
        TTYPuts(str);
    }

    return STATUS_OK;
}
