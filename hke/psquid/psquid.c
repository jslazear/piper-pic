/*TAKEN FROM PSYNCADC CODE, MAY BE NONSENSICAL ERRORS!*/
#include <p30F5011.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>
#include <libpic30.h>

#include "psquid.h"
#include "../common/ptty.h"
//#include "../common/psync_common.h"
//#include "../common/frame_buf.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL16); /* External clock with 16xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF); /* Code protect disabled */

void init_pic()
{
    // Set up values for GPIO pins
    TRISBbits.TRISB5 = 0; // LED_1 (LOCK)
    TRISBbits.TRISB4 = 0; // LED_2 (SWEEP)
    TRISBbits.TRISB3 = 0; // LED_3 (MISC/DATA)

    TRISGbits.TRISG2 = 0; // ZAP

    // Set up the output pins for GPIO
    TRISCbits.TRISC1 = 0; // Front panel LED
    TRISBbits.TRISB8 = 0; // S1 bias DAC /CS
    TRISBbits.TRISB9 = 0; // S2 bias DAC /CS
    TRISBbits.TRISB10 = 0; // S3 bias DAC /CS
    TRISBbits.TRISB11 = 0; // Det bias DAC /CS
    TRISBbits.TRISB12 = 0; // S1 FB DAC /CS
    TRISBbits.TRISB13 = 0; // S2 FB DAC /CS
    TRISBbits.TRISB14 = 0; // S3 FB DAC /CS
    TRISBbits.TRISB15 = 0; // S3 output pre-amp offset DAC /CS
    TRISGbits.TRISG3 = 0; // S3 output adjustable gain DAC /CS
    TRISDbits.TRISD8 = 0; // Address select MUX LSB
    TRISDbits.TRISD9 = 0; // Address select MUX middle bit
    TRISDbits.TRISD10 = 0; // Address select MUX MSB

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD5 = 0;
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD3 = 0;
    TRISDbits.TRISD2 = 0;
    PIN_OSCOPE1 = 0;
    PIN_OSCOPE2 = 0;
    PIN_OSCOPE3 = 0;
    PIN_OSCOPE4 = 0;

    // Short delay to wait for power supply voltages to settle
    __delay32((long) (0.01 * CLOCKFREQ));

    // Setup the UART - 115.2 kbs
    unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
    unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
    OpenUART2(U2MODEvalue, U2STAvalue, BRG_SETTING);
    ConfigIntUART2(UART_RX_INT_DIS & UART_TX_INT_DIS); // Disable TX and RX interrups

    // Setup the SPI port2 (For DACs)
    OpenSPI2(FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON &
            MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
            PRI_PRESCAL_4_1,
            SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR);
    ConfigIntSPI2(SPI_INT_DIS); // NO SPI interrupts

    //------------------------------------------------------------------------------
    //The SPI module supports 4 different Serial Clock formats. The user can
    //select one of these 4 formats by configuring the Clock Polarity Select, or
    //CKP, and Clock Edge Select, or CKE, bits in the SPI Control Register.
    //See the dsPIC family reference for a description.
    //NOTE: the settings here are a bit off from our usual, the AD5724 DAC chip needed different settings
    //  -- it wants valid data on the falling edge of sclk.  Other DACs, like the DAC7715 (on the AO board) want valid
    // data on the rising SCLK edge
    //------------------------------------------------------------------------------
    // Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
    // PSQUID -- I think CKE=0, CKP=0 is active-high, data valid on rising
    // PSQUID -- edge mode, which I think is correct for the AD5543.
    // PSQUID -- Will likely need to verify this.
    // UPDATE: Changed to 1/0/1 to match TRead, not sure if other settings (0/0/0) will work
    //         Check with scope!
    SPI2CONbits.CKE = 1; // CKE=1 means SDO changes on RISING SCLK edges, because AD5754 DAC reads on the falling edge
    SPI2CONbits.CKP = 0; // CKP=0 means SCLK is active high
    SPI2CONbits.SMP = 1; // If the SMP bit is set, then the input sampling is done at the end of the bit output

    // Setup Timer2, our one and only interrupt source
    // PSQUID -- Will use Timer2 as are one and only interrupt handler
    // PSQUID -- 10 kHz cycle frequency. Timer is set to ON.
    OpenTimer2(T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, 2000);
    set_interrupt_period(CLOCKFREQ / 10000L);       // Sets the interrupt rate correctly (includes the off by 1 correction)

    // Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
    // these pulses will trigger the convert pin of the ADC
    // PSQUID -- The ADC is read every interrupt, so use the OC2 pulse for
    // PSQUID -- ADC convert pulse
    OpenOC2(OC_IDLE_CON & OC_TIMER2_SRC & OC_CONTINUE_PULSE, 200, 0);

    // Finally, turn on OC2 interrupt
    ConfigIntOC2(OC_INT_ON & OC_INT_PRIOR_2);
}

int main(void)
{
    PIN_ZAP = 0; // Make sure zap is disabled
    init_pic(); // Setup the PIC internal peripherals

    // DACs are initialized by the dac_dirty_values array values, so no need
    // to initialize them explicitly.

    PIN_OSCOPE3 = 1; //DEBUG
    PIN_OSCOPE3 = 0; //DEBUG

    while (1) {
        char s[100]; // Note the CH command can have long argument list (up to 90 characters)

        cmd_gets(s, sizeof(s)); // Get a command from the user

        process_cmd(s); // Deal with the command
    }

}

void OnIdle(void)
{
    // pass
}