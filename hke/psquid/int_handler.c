#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "psquid.h"
#include "../common/ptty.h"
//#include "../common/psync_common.h"
//#include "../common/frame_buf.h"
#include "../common/tty_print.h"
#include "../common/math_utilities.h"

static void update_sweep_sum(void);
static void update_data(void);
static void update_raw_data(void);
static void sweep_write(unsigned code, unsigned long dac_accum, unsigned long adc_accum);
//static void sweep_write2(unsigned code, unsigned long dac_accum, unsigned long adc_accum);
static void data_write(unsigned long dac_accum, unsigned long adc_accum);
//static void data_write2(unsigned long dac_accum, unsigned long adc_accum);
static void raw_data_write(unsigned int value);
static unsigned compute_pid(unsigned data);

// NOTES
//int_handler
//
//    Read the ADC
//    adc_sum += adc
//    update_serial_port() // write till hardware buffer full, do this while waiting for ADC read to finish
//    if( locked_ch >= 0 )
//        error = adc - set_point
//        dac[locked_ch] = locked_p * error + locked_i * accumulator
//        spi write to locked DAC channel
//        lock_dac_sum += dac[locked_ch]
//    if( sweep >= 0 )
//        sw_counts += sweep_delta
//        dac[sweep_dac] = sweep_counts
//        dac_dirty[sweep_dac] = 1;
//        check stop condition
//        Print some hex data (see below)
//        spi write to sweep DAC channel
//    else if( data )
//        if( n_sum == block_len )
//            print data ( adc_sum >> block_len_ln2, lock_dac_sum >> block_len_ln2 )
//            n_sum = 0, adc_sum = 0, lock_dac_sum = 0
//    ++dac_update_ch;  if(dac_update_ch > NUM_DACS) dac_update_ch = 0;  // for the DAC command
//    if( dac_dirty[ dac_update_ch ] )
//        spi write to dac_update_ch, set dirty = 0;

//#ifdef INTS_DEFINED //DEBUG

// ---------------------
// ----VARIABLE DEFS----
// ---------------------
static unsigned int tick = 0; // Incremented each time we get a timer2 interrupt

struct pidparams {
    int ch; // Specifies the locked register; -1 means none locked
    unsigned p; // P term for PID
    unsigned i; // I term for PID
    unsigned int setpoint; // PID setpoint (demod units)
    long accumulator; // PID integrator term accumulator
    unsigned char sign;     // Sign of P term and I term
    unsigned char i_shift; // I term is divided by 2^i_shift
    unsigned char p_shift; // P term is divided by 2^p_shift
    unsigned int dac_init; // Initial dac value
};

struct sweepparams {
    // User-specified sweep parameters
    int ch; // Channel to sweep; -1 means not in sweep mode
    unsigned char read_ch; // Additional dac channel to report while sweeping
    unsigned int low; // Start dac code for sweep
    unsigned int high; // End dac code for sweep
    unsigned int delta; // Step size for sweep
    unsigned int dwell; // How many samples to collect at each sweep location
    unsigned int ntimer; // Sample once every ntimer OC2 interrupts
    unsigned int nsweeps; // Number of sweeps to perform

    // Internal utility variables
    unsigned int code; // Current dac code
    unsigned int dwell_counter;
    unsigned int ntimer_counter;
    unsigned int nsweeps_counter;
    unsigned char reverse; // Whether the sweep is ascending or descending
    unsigned long dac_accum; // Read DAC accumulator
    unsigned long adc_accum; // ADC accumulator
};

struct dataparams {
    // User-specified data parameters
    int ch; // Channel to sweep; -1 means not in sweep mode
    unsigned int dwell; // How many samples to collect at each sweep location

    // Internal utility variables
    unsigned int dwell_counter;
    unsigned long dac_accum; // Read DAC accumulator
    unsigned long adc_accum; // ADC accumulator
};

struct rawdataparams {
    // User-specified raw_data parameters
    int ch; // Channel to sweep; -1 means not in sweep mode
};

static struct pidparams pid = {.ch = -1,
                               .p = 1,
                               .i = 1,
                               .setpoint = 0,
                               .accumulator = 0,
                               .sign = PID_SIGN_POS,
                               .i_shift = 15,
                               .p_shift = 8,
                               .dac_init = 0};

static struct sweepparams sweep = {.ch = -1,
                                   .read_ch = 0,
                                   .low = 0,
                                   .high = 0,
                                   .delta = 1,
                                   .dwell = 1,
                                   .ntimer = 0,
                                   .nsweeps = 1,
                                   .code = 0,
                                   .dwell_counter = 0,
                                   .ntimer_counter = 0,
                                   .nsweeps_counter = 0,
                                   .reverse = 0,
                                   .dac_accum = 0,
                                   .adc_accum = 0};

static struct dataparams data = {.ch = -1,
                                 .dwell = 1,
                                 .dwell_counter = 0,
                                 .dac_accum = 0,
                                 .adc_accum = 0};

static struct rawdataparams raw_data = {.ch = -1};


static unsigned int adc_value = 0x0000;
static unsigned int new_dac = 0; // New DAC setting after PID loop/scheduled iteration

static unsigned dac_values[9] = {0}; // Array of DAC values
static int dac_dirty[9] = {1, 1, 1, 1, 1, 1, 1, 1, 1}; // Array of DAC update statuses
static unsigned dac_dirty_values[9] = {0, 0, 0, 0, 0, 0, 0, 0, 65535}; // Array of to-be-updated DAC values

static unsigned int dac_update_ch = 0; // The DAC channel that is scheduled to be updated

//static unsigned char binary_flag = 0;  // Flag for whether binary or hex output should be used #DELME


// ---------------------
// -----INT HANDLER-----
// ---------------------
void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void)
{
    IFS0bits.OC2IF = 0; // reset Output Compare 1 interrupt flag
    PIN_OSCOPE2 = 1;

    if (tick >= 0xffff) {
        tick = 0;
    }

    TTYUpdateTXNoFrameFast4();
    
    PIN_OSCOPE4 = 1;        //# DELME

    //-----> ADC Read
    // Do the actual SPI write/read
    SPI2STATbits.SPIROV = 0; // Clear SPI receive overflow bit
    SPI2BUF = adc_value; // Write to the SPI port


    while (!SPI2STATbits.SPIRBF) {
    }; // Wait for data to be ready on SPI port
    adc_value = SPI2BUF; // Read in data from ADC converter

    PIN_OSCOPE4 = 0;        //# DELME

    // Update PID-controlled DAC
//    if (!(tick & 0xff)) {
        if (pid.ch >= 0) {
            // Compute, store, and set new DAC value for locked channel
            new_dac = compute_pid(adc_value);
            dac_values[pid.ch] = new_dac;   // Update the local dac shadow variable
            write_dac(new_dac, pid.ch);     // Bypass dirty flag, just update the dac directly
            PIN_LED_LOCK = 1;
        } else {
            PIN_LED_LOCK = 0;
        }
//    }

    // Update sweep, if sweeping
    if (sweep.ch >= 0) {
        update_sweep_sum();
        PIN_LED_SWEEP = 1;
        PIN_LED_MISC = 0;
    } else if (data.ch >= 0) {
        update_data();
        PIN_LED_SWEEP = 0;
        PIN_LED_MISC = 1;
    } else if (raw_data.ch >= 0) {
        update_raw_data();
        PIN_LED_SWEEP = 0;
        PIN_LED_MISC = 1;
    } else { 
        PIN_LED_SWEEP = 0;
        PIN_LED_MISC = 0;
    }

    // Update scheduled DAC (we only update one dac per interrupt)
    if (dac_dirty[dac_update_ch]) {
        new_dac = dac_dirty_values[dac_update_ch];
        dac_values[dac_update_ch] = new_dac;

        write_dac(new_dac, dac_update_ch);

        dac_dirty[dac_update_ch] = 0;
    }

    // Iterate DAC schedule
    dac_update_ch++;
    if (dac_update_ch >= 9)
        dac_update_ch = 0;

    tick++;

    PIN_OSCOPE2 = 0;
}

/* TTY Speeds
 *
 *  TTYPrintHex16  = 14.1 us
 *  TTYPutc        = 6.18 us
 *  TTYPutcFromInt = 2.45 us
 *
 *
 * Total Interrupt Handler Time
 *  No TTY, No Sweep = 8 us
 *  No TTY, Sweep = 14.2 us
 *  No TTY, PID = 21 us
 *  PID + 2 * TTYPutcFromInt ~ 25 us  = 40 kHz
 *
 * Interrupt Handler Parts Times
 *   TTYUpdateTXNoFrameFast = 1.9 us (could go inside wait for ADC result???)
 *   Wait for ADC = 3.8 us
 *
 * PID Timing
 *  Compute_pid = 4.73 us (pshift:0 ishift:0, optimize level==0)
 *  Compute_pid = 5.95 us (pshift:0 ishift:4, optimize level==0)
 *  Compute_pid = 2.97 us (pshift:0 ishift:0, optimize level==1)
 *  Compute_pid = 4.14 us (pshift:0 ishift:4, optimize level==1)
 *  write_dac = 4 us (called every cycle to write out pid value)
 *
 */


/* update_sweep_prod (private)
 *
 * Updates the sweep, if enabled, in the OC2 interrupt handler.
 * Step period = ntimer*dwell in units of OC2 interrupt counts
 * See update_sweep_sum for version where (step period) = ntimer + dwell.
 *
 * 1) Write DAC
 * 2) Report data every ntimer*dwell
 * 3) Check stop condition
 * 4) If (not stop), increment code
 */
/* static void update_sweep_prod(void)
{
    if (sweep.ntimer_counter == 0) {
        // --(1) Write DAC only on first cycle in step--
        dac_values[sweep.ch] = sweep.code;
        write_dac(sweep.code, sweep.ch);
    }

    if (sweep.ntimer_counter + 1 >= sweep.ntimer) {
        sweep.ntimer_counter = 0;

        sweep.dac_accum += dac_values[sweep.read_ch];
        sweep.adc_accum += adc_value;

        if (sweep.dwell_counter + 1 >= sweep.dwell) {
            // --(2) Report data every ntimer*dwell--
            sweep_write(sweep.code, sweep.dac_accum, sweep.adc_accum);
            sweep.dwell_counter = 0;
            sweep.dac_accum = 0;
            sweep.adc_accum = 0;

            if (!sweep.reverse) {
                // --(3) Check stop condition--
                // Must not add delta to code before check -- will overflow and
                // wrongly continue if (end + delta) >= 2^16!
                if (sweep.high - sweep.code < sweep.delta) {
                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
                        sweep.nsweeps_counter = 0;
                        sweep.ch = -1;

//                        PIN_OSCOPE3 = 1; //DEBUG
//                        PIN_OSCOPE3 = 0; //DEBUG
                    } else { // --(4) If (not stop), increment code--
                        sweep.nsweeps_counter += 1;
                        sweep.code = sweep.low;
                    }
                } else {
                    sweep.code += sweep.delta;
                }
            } else {
                // --(3) Check stop condition--
                // Must not add delta to code before check -- will overflow and
                // wrongly continue if (end + delta) >= 2^16!
                if (sweep.code - sweep.low < sweep.delta) {
                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
                        sweep.nsweeps_counter = 0;
                        sweep.ch = -1;

//                        PIN_OSCOPE3 = 1; //DEBUG
//                        PIN_OSCOPE3 = 0; //DEBUG
                    } else { // --(4) If (not stop), increment code--
                        sweep.nsweeps_counter += 1;
                        sweep.code = sweep.high;
                    }
                } else {
                    sweep.code -= sweep.delta;
                }
            }
        } else {
            sweep.dwell_counter += 1;
        }
    } else {
        sweep.ntimer_counter += 1;
    }
}
*/

/* update_sweep_sum (private)
 *
 * Updates the sweep, if enabled, in the OC2 interrupt handler.
 * Step period = ntimer + dwell in units of OC2 interrupt counts
 * See update_sweep_prod for version where (step period) = ntimer*dwell.
 *
 * 1) Write DAC
 * 2) Report data every ntimer*dwell
 * 3) Check stop condition
 * 4) If (not stop), increment code
 */
static void update_sweep_sum(void)
{
    if (sweep.ntimer_counter == 0) {
        // --(1) Write DAC only on first cycle in step--
        dac_values[sweep.ch] = sweep.code;
        write_dac(sweep.code, sweep.ch);
    }

    if (sweep.ntimer_counter >= sweep.ntimer) {
        sweep.dac_accum += dac_values[sweep.read_ch];
        sweep.adc_accum += adc_value;


        if (sweep.dwell_counter + 1 >= sweep.dwell) {
            // --(2) Report data every ntimer*dwell--
            sweep_write(sweep.code, sweep.dac_accum, sweep.adc_accum);
            sweep.ntimer_counter = 0;
            sweep.dwell_counter = 0;
            sweep.dac_accum = 0;
            sweep.adc_accum = 0;

            if (!sweep.reverse) {
                // --(3) Check stop condition--
                // Must not add delta to code before check -- will overflow and
                // wrongly continue if (end + delta) >= 2^16!
                if (sweep.high - sweep.code < sweep.delta) {
                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
                        sweep.nsweeps_counter = 0;
                        sweep.ch = -1;

                        // Send termination string at end of sweep
                        TTYPuts("*PSQUID: sweep_off\r\nOK!\r\n");
                    } else { // --(4) If (not stop), increment code--
                        sweep.nsweeps_counter += 1;
                        sweep.reverse = 1;      // switch direction for triangle wave
                    }
                } else {
                    sweep.code += sweep.delta;
                }
            } else {
                // --(3) Check stop condition--
                // Must not add delta to code before check -- will overflow and
                // wrongly continue if (end + delta) >= 2^16!
                if (sweep.code - sweep.low < sweep.delta) {
                    if (sweep.nsweeps && (sweep.nsweeps_counter + 1 >= sweep.nsweeps)) {
                        sweep.nsweeps_counter = 0;
                        sweep.ch = -1;

                        // Send termination string at end of sweep
                        TTYPuts("*PSQUID: sweep_off\r\nOK!\r\n");
                    } else { // --(4) If (not stop), increment code--
                        sweep.nsweeps_counter += 1;
                        sweep.reverse = 0;      // switch direction of sweep for triangle wave
                    }
                } else {
                    sweep.code -= sweep.delta;
                }
            }
        } else {
            sweep.dwell_counter += 1;
        }
    } else {
        sweep.ntimer_counter += 1;
    }
}

/* update_data (private)
 *
 * Updates the data reporting, if enabled, in the OC2 interrupt handler.
 *
 * 2) Accumulate data every cycle
 * 1) Report data every dwell cycles
 */
static void update_data(void)
{
    // --(1) Accumulate data every cycle--
    data.dac_accum += dac_values[data.ch];
    data.adc_accum += adc_value;

    if (data.dwell_counter + 1 >= data.dwell) {
        // --(2) Report data every dwell cycles--
        data_write(data.dac_accum, data.adc_accum);

        data.dwell_counter = 0;
        data.dac_accum = 0;
        data.adc_accum = 0;
    } else {
        data.dwell_counter += 1;
    }
}

/* update_raw_data (private)
 *
 * Updates the raw_data reporting, if enabled, in the OC2 interrupt handler.
 *
 * 1) Report data every cycle
 */
static void update_raw_data(void)
{
    if (pid.ch == -1) {
        raw_data_write(adc_value);
    } else {
        raw_data_write(dac_values[raw_data.ch]);
    }
}


/* sweep_write (private)
 *
 * Writes sweep data to the data buffer for trickling out the serial port.
 */
static void sweep_write(unsigned code, unsigned long dac_accum, unsigned long adc_accum)
{
    // CHARS_PER_SWEEP_WRITE = 15.  Update this in psquid.h if you change this function!!!
    TTYPutcFromInt('^');
    TTYPut16FromInt(code);
    TTYPutcFromInt(' ');
    TTYPut32FromInt(dac_accum);
    TTYPutcFromInt(' ');
    TTYPut32FromInt(adc_accum);
    TTYPutcFromInt('\r');
    TTYPutcFromInt('\n');
}

/* data_write (private)
 *
 * Writes data data to the data buffer for trickling out the serial port.
 *
 * Only uses last 2 elements of data buffer array.
 */
static void data_write(unsigned long dac_accum, unsigned long adc_accum)
{
    // CHARS_PER_DATA_WRITE = 12.  Update this in psquid.h if you change this function!!!
    TTYPutcFromInt('^');
    TTYPut32FromInt(dac_accum);
    TTYPutcFromInt(' ');
    TTYPut32FromInt(adc_accum);
    TTYPutcFromInt('\r');
    TTYPutcFromInt('\n');
}


static void raw_data_write(unsigned int value)
{
    // Currently, this is 3 characters per datum (caret + 16 bit value)
    // If you change this, MUST update the MAX_RAW_DATA_RATE macro in psquid.h
    TTYPutcFromInt('^');
    TTYPut16FromInt(value);
}

static unsigned compute_pid(unsigned data)
{
    static long error;
    static long pterm;
    static long iterm;
    static long sum;
    static unsigned toret;
//    static char buf[100];

    PIN_OSCOPE4 = 1;        //# DELME

    // error = int17; may want to think about throwing away 1 bit to get down to int16
    // Will need to look at disassembly code to figure out if worthwhile
    error = (long)data - (long)pid.setpoint;

    // pid_i*error should be a 16x16 = 32 multiply; check disassembly
    // default assembly code is not at all optimized...
    // NOTE: pid.sign is handled here for i coefficient so that accumulator is
    // always continuous.
    if( pid.sign == PID_SIGN_POS)
        pid.accumulator += pid.i*error;
    else
        pid.accumulator -= pid.i*error;

    // Optimized version of above (but wrong!).  Todo: fix this!
//    pid.accumulator += __builtin_muluu(pid.i, error);

    // pid_p*error should be a 16x16=32 multiply; check disassembly.
    // But error is 32 bit, so this doesn't work as written!!!!
    // Default assembly code is not at all optimized...
    // but at least it works!
    if( pid.p != 0) {
        pterm = pid.p * error;

        pterm = fast_rshift_long(pterm, pid.p_shift);  // #DELME?
    }
    else {
        pterm = 0;
    }

    // Compiler handles this switch structure by indexing i_shift into a look-up BRA table
    // so there is no performance cost to having all of the cases
    iterm = pid.accumulator;
    iterm = fast_rshift_long(iterm, pid.i_shift);

    // Optimized version of above (saves 7 cycles!)  But wrong!
//    pterm = __builtin_muluu(pid.p, error) >> pid.p_shift;

    // Compute the full PID output
    // NOTE: Only p term of pid.sign is handled here, since i term handled above
    if( pid.sign == PID_SIGN_POS)
        sum = (long)pid.dac_init + pterm + iterm;
    else
        sum = (long)pid.dac_init - pterm + iterm;

    // Coerce to unsigned 16-bit integer
    // PID algorithm should naturally fall into range, unless
    // specified set point is impossible to achieve, in which case
    // the controller will rail
    if (sum <= 0)
        toret = 0;
    else if (sum >= 65535)
        toret = 65535;
    else
        toret = (unsigned)sum;

    PIN_OSCOPE4 = 0;        //# DELME

//    sprintf(buf, "d: %08x - s: %08x\r\n", data, pid.setpoint);  //#DELME DEBUG
//    TTYPutsFromInt(buf);  //#DELME DEBUG
//
//    sprintf(buf, "dL: %08lx - sL: %08lx - e: %08lx\r\n", (long)data, (long)pid.setpoint, error);  //#DELME DEBUG
//    TTYPutsFromInt(buf);  //#DELME DEBUG
//
//    sprintf(buf, "p: %08x - pshft: %08x - i: %08x - ishft: %08x\r\n", pid.p, pid.p_shift, pid.i, pid.i_shift);  //#DELME DEBUG
//    TTYPutsFromInt(buf);  //#DELME DEBUG
//
//    sprintf(buf, "D: %08x - P: %08lx - I: %08lx - R: %08x\r\n", pid.dac_init, pterm, pid.accumulator, toret);  //#DELME DEBUG
//    TTYPutsFromInt(buf);  //#DELME DEBUG
//
//    if (!(tick & 0x00f)) {  //DEBUG
//        TTYPutsFromInt("e: ");  //DEBUG
//        TTYPrintHex32(error);  //DEBUG
//        TTYPutsFromInt(" - p: ");  //DEBUG
//        TTYPrintHex32(pterm);  //DEBUG
//        TTYPutsFromInt(" - i: ");  //DEBUG
//        TTYPrintHex32(pid.accumulator);  //DEBUG
//        TTYPutsFromInt(" - r: ");  //DEBUG
//        TTYPrintHex16(toret);  //DEBUG
//        TTYPutsFromInt("\r\n");  //DEBUG
//    }

    return toret;
}



// ---------------------
// ----SET FUNCTIONS----
// ---------------------

/* set_interrupt_period
 */
void set_interrupt_period(unsigned p)
{
    PR2 = p - 1;        // The PR2 hardware register determines the interrupt rate of Timer2.
                        // - 1 is because TIMER2 counts up to PR2 *inclusive* (not intuitive)
}

/* set_dac
 *
 * Signals for a DAC to be set to a specified code.
 *
 * Signals the OC2 interrupt handler that the DAC specified by ch
 * should be changed to the code specified by val. Does not actually
 * update the DAC. The OC2 interrupt will update the DAC according
 * to its pre-defined schedule.
 *
 * Does not report back anything over the serial port.
 *
 * val - Code to which the specified DAC should be set to
 * ch  - Specifies the DAC to be changed (see table below)
 *
 * ch - DAC
 * 0  - S1 Bias (aka Row Select)
 * 1  - S2 Bias
 * 2  - S3 Bias (aka Series Array Bias)
 * 3  - Detector Bias
 * 4  - S1 Feedback
 * 5  - S2 Feedback
 * 6  - S3 Feedback (aka Series Array Feedback)
 * 7  - S3 Offset (aka Series Array Offset)
 * 8  - S3 Gain (aka Series Array Gain)
 */
void set_dac(unsigned val, int ch)
{
    dac_dirty[ch] = 1;
    dac_dirty_values[ch] = val;
}

/* set_pid
 *
 * Signals for a DAC to be PID-controlled to a specified setpoint.
 *
 * Signals the OC2 interrupt handler that the DAC specified by ch
 * should be PID controlled to the specified setpoint. All parameters
 * are in DAC units, if relevant.
 *
 * ch       - DAC channel to be swept
 * p        - proportional parameter of PID loop
 * i        - integral parameter of PID loop
 * setpoint - value to which specified DAC should be controlled
 * i_shift  - log_2(divisor) on integral contribution to sum
 * p_shift  - log_2(divisor) on proportional contribution to sum
 */
void set_pid(int ch, unsigned p, unsigned i, unsigned setpoint,
             unsigned char i_shift, unsigned char p_shift)
{
    pid.p = p;
    pid.i = i;
    pid.setpoint = setpoint;
    pid.i_shift = i_shift;
    pid.p_shift = p_shift;

    pid.dac_init = dac_values[ch];
    pid.accumulator = 0;

    pid.ch = ch;  // Set this last to prevent PID loop from starting prematurely
}

/* set_pid_ch
 *
 * Adjusts the current PID loop to have a new channel
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * Note that this command may be used to enable/disable the PID loop without
 * changing the PID parameters, since ch = -1 indicates the the PID loop is
 * off.
 *
 * ch - DAC channel which should be actuated
 */
void set_pid_ch(int ch)
{
    pid.ch = ch;
}

/* set_pid_setpoint
 *
 * Adjusts the current PID loop to have a new setpoint
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * setpoint - value to which specified DAC should be controlled
 */
void set_pid_setpoint(unsigned setpoint)
{
    pid.setpoint = setpoint;
}

/* set_pid_sign
 *
 * Valid arguments are PID_SIGN_POS and PID_SIGN_NEG
 */
void set_pid_sign(unsigned char sign)
{
    pid.sign = sign;
}

/* set_pid_p
 *
 * Adjusts the current PID loop to have a new p constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * p - value to which specified DAC should be controlled
 */
void set_pid_p(unsigned p)
{
    pid.p = p;
}

/* set_pid_p_shift
 *
 * Changes the fixed-point basis of the proportional term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * p_shift - value to which specified DAC should be controlled
 */

void set_pid_p_shift(unsigned p_shift)
{
    pid.p_shift = p_shift;
}


/* set_pid_i
 *
 * Adjusts the current PID loop to have a new i constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * i - value to which specified DAC should be controlled
 */

void set_pid_i(unsigned i)
{
    pid.i = i;
}

/* set_pid_i_shift
 *
 * Changes the fixed-point basis of the integral term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop. Does attempt to compensate the accumulator for the new scaling
 * factor so that there is not too large a discontinuity. 
 *
 * i_shift - value to which specified DAC should be controlled
 */

void set_pid_i_shift(unsigned i_shift)
{
    int diff = (int)pid.i_shift - (int)i_shift;
    if (diff >=0) {
        pid.accumulator = pid.accumulator >> diff;
    } else {
        pid.accumulator = pid.accumulator << -diff;
    }

    pid.i_shift = i_shift;
}

/* set_sweep
 *
 * Initialize a sweep and signal for it to begin.
 *
 * Begins a sweep of DAC identified by ch. Steps the DAC code through
 * the range [low, high] using the step size delta. Sits at each step
 * location for dwell ADC samplings. Samples the ADC every ntimer
 * OC2 interrupts. Performs nsweeps of these sweeps, in series.
 *
 * Reports back over the serial port the triplets
 * (dac_sweep, dac_read, adc_value) every time an ADC value is recorded,
 * i.e. every ntimer OC2 cycles.
 *
 * Note that the sweep may be ascending or descending in DAC code. This
 * property depends solely on the sign of delta, not on the relative
 * values of low and high. While delta is a long, it will be coerced into
 * the range [-UINT16_MAX, UINT16_MAX], i.e. [-65535, 65535].
 *
 * The values of low and high are sorted in the function, so may have
 * low > high without consequence. Note that low > high will not reverse
 * the orientation of the sweep.
 *
 * ch      - DAC channel to be swept
 * read_ch - Extra DAC channel to be read and reported
 * low     - Lower of the DAC values in sweep range
 * high    - Higher of the DAC values in sweep range
 * delta   - Step size of sweep
 * dwell   - Number of times to sample at each step
 * ntimer  - ADC is sampled once every ntimer OC2 interrupts
 * nsweeps - Number of half-sweeps to perform
 */
void set_sweep(int ch, unsigned char read_ch, unsigned int low,
        unsigned int high, long delta, unsigned int dwell,
        unsigned int ntimer, unsigned int nsweeps)
{
    // Disable data reporting if active
    if (data.ch >= 0) {
        data.ch = -1;
    }

    // Disable raw_data reporting if active
    if (raw_data.ch >= 0) {
        raw_data.ch = -1;
    }

    if (high < low) {
        unsigned temp = low;
        low = high;
        high = temp;
    }

    sweep.read_ch = read_ch;
    sweep.low = low;
    sweep.high = high;
    sweep.dwell = dwell;
    sweep.ntimer = ntimer;
    sweep.nsweeps = nsweeps;

    sweep.dwell_counter = 0;
    sweep.nsweeps_counter = 0;
    sweep.ntimer_counter = 0;
    sweep.dac_accum = 0;
    sweep.adc_accum = 0;

    if (delta == 0)
        delta = 1;

    if (delta > 0) {
        sweep.reverse = 0;
        sweep.code = sweep.low;
    } else if (delta < 0) {
        delta = -delta;
        sweep.reverse = 1;
        sweep.code = sweep.high;
    }

    if (delta > 65535)
        delta = 65535;
    sweep.delta = (int) delta;

    sweep.ch = ch;  // Set last to prevent sweep from starting prematurely
}

/* set_data
 *
 * Initializes and signals that data reporting should begin
 *
 * Begins reporting data in (dac, adc) pairs.
 *
 * The ch specifies which DAC should be reported back. This is normally
 * the locked DAC, since otherwise there is nothing changing the DAC value.
 *
 * The data is accumulated dwell times before being reported.
 *
 * ch      - DAC channel to be reported
 * dwell   - Number of times to accumulate before reporting
 */
void set_data(int ch, unsigned int dwell)
{
    // Disable sweeping if active
    if (sweep.ch >= 0) {
        sweep.ch = -1;
    }

    // Disable raw_data reporting if active
    if (raw_data.ch >= 0) {
        raw_data.ch = -1;
    }

    data.dwell = dwell;
    data.dwell_counter = 0;

    data.dac_accum = 0;
    data.adc_accum = 0;

    data.ch = ch;  // Set last to prevent data from starting prematurely
}

/* set_raw_data
 *
 * Initializes and signals that raw_data reporting should begin
 *
 * Begins reporting data in adc or dac raw values, depending on whether
 * or not the PID loop is enabled. If PID is enabled, then reports dac
 * values of the specified channel. Otherwise reports adc values.
 *
 * The ch specifies which DAC should be reported back. This should normally
 * be chosen to be the locked DAC, since otherwise there is nothing changing
 * the DAC value.
 *
 * The data is not accumulated at all and is instead reported back in raw
 * form every cycle.
 *
 * ch      - DAC channel to be reported
 */
void set_raw_data(int ch)
{
    // Disable sweeping if active
    if (sweep.ch >= 0) {
        sweep.ch = -1;
    }

    // Disable data reporting if active
    if (data.ch >= 0) {
        data.ch = -1;
    }

    raw_data.ch = ch;  // Set last to prevent data from starting prematurely
}

/* set_mux
 *
 * Muxes the S1B DAC output to the specified RS channel.
 *
 * Only changes the mux, does not reset the DAC or anything else.
 *
 * ch      - channel to which to mux
 */
void set_mux(char ch)
{
    unsigned temp;
    temp = LATD & ~(0b111 << 8);  // Set all the address bits to 0
    LATD = temp | (ch << 8);      // Enable only address bits that are 1
}


// ---------------------
// ----GET FUNCTIONS----
// ---------------------
unsigned get_interrupt_period(void)
{
    return PR2 + 1;     // PR2 is special function register that sets TIMER2 interrupt rate
                        // + 1 is because TIMER2 counts up to PR2 *inclusive* (not intuitive)
                        // Note it's -1 in the setter and +1 in the getter
}

unsigned int get_adc_value(void)
{
    return adc_value;
}

unsigned int get_tick(void)
{
    return tick;
}

int get_pid_ch(void)
{
    return pid.ch;
}

unsigned int get_pid_s(void)
{
    return pid.setpoint;
}

unsigned char get_pid_sign(void)
{
    return pid.sign;
}

unsigned int get_pid_p(void)
{
    return pid.p;
}

unsigned int get_pid_i(void)
{
    return pid.i;
}

unsigned char get_pid_p_shift(void)
{
    return pid.p_shift;
}

unsigned char get_pid_i_shift(void)
{
    return pid.i_shift;
}

unsigned char get_silent_flag(void)
{
    if (sweep.ch >= 0 || data.ch >= 0 || raw_data.ch >= 0) {
        return 1;
    } else {
        return 0;
    }
}

unsigned char get_mux(void)
{
    return (LATD >> 8) & 0b111;
}


// #DELME
//unsigned char get_binary_flag(void)
//{
//    return binary_flag;
//}

//#endif //DEBUG