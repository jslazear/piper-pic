"""
The manual lock control panel for PSquid.
"""
import wx
# from wx.lib.scrolledpanel import ScrolledPanel
from misc import DACsChoice, make_gbsizer_from_array
import numpy as np


class ManualLockPanel(wx.Panel):
# class SweepPanel(ScrolledPanel): #DELME?
    """
    The manual lock control panel for PSquid.
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)
        # ScrolledPanel.__init__(self, parent=parent, id=wx.ID_ANY) #DELME?

        self.fmf = self.GetTopLevelParent()
        self.timer = wx.Timer(self)
        self.timer.Start(500)  # Send timer event every 50 ms  #UN#DELME

        self.helptext = ("Lock a DAC.")

        self.create_sizers()
        self.populate_top()

        # self.SetupScrolling(scroll_x=False, scroll_y=True) #DELME?

    def create_sizers(self):
        """
        Create the sizers in the panel.
        """
        self.bsMain = wx.BoxSizer(wx.VERTICAL)

        self.bsTop = wx.BoxSizer(wx.VERTICAL)

        self.bsMain.Add(self.bsTop, 1, wx.EXPAND)

        self.SetSizer(self.bsMain)

    def populate_top(self, sizer=None):
        """
        Populate the top sizer.
        """
        if sizer is None:
            sizer = self.bsTop

        # Create controls
        # self.txtHelp = StaticWrapText(self, wx.ID_ANY, label=self.helptext)

        txtfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        txtsize = wx.Size(70, 17)
        self.lblADC = wx.StaticText(self, label='ADC Value')
        self.txtADC = wx.TextCtrl(self, size=txtsize)
        self.txtADC.SetFont(txtfont)
        self.sldADC = wx.Slider(self, minValue=0, maxValue=65535)

        self.lblDACs = wx.StaticText(self, label='Lock DAC ')
        self.chDACs = DACsChoice(self, wx.ID_ANY)

        self.lblInit = wx.StaticText(self, label='Init Value')
        self.txtInit = wx.TextCtrl(self, size=txtsize)
        self.txtInit.SetFont(txtfont)
        self.sldInit = wx.Slider(self, minValue=0, maxValue=65535)

        self.sbPID = wx.StaticBox(self, label='PID')
        self.sbsPID = wx.StaticBoxSizer(self.sbPID, wx.VERTICAL)
        self.PIDctrls = self.make_PID()

        self.bLock = wx.Button(self, label='Lock',
                               style=wx.EXPAND | wx.ALIGN_CENTER)

        arr = np.array([[(-1, 1), None, None],
                        [self.lblADC, self.sldADC, self.txtADC],
                        [self.lblDACs, self.chDACs, self.chDACs],
                        [self.lblInit, self.sldInit, self.txtInit],
                        [self.sbsPID, self.sbsPID, self.sbsPID],
                        [self.bLock, self.bLock, self.bLock],
                        [(-1, 1), None, None]],
                       dtype='object')

        styles = np.zeros_like(arr, dtype=object)
        styles.fill(None)
        styles[5, 0] = wx.ALIGN_CENTER

        bs = make_gbsizer_from_array(self, arr, styles, hgap=1, vgap=1,
                                     vertmargin=True, vminsize=5)
        bs.AddGrowableCol(0, 1)
        bs.AddGrowableCol(4, 1)
        bs.AddGrowableRow(6, 1)
        bs.SetEmptyCellSize((1, 1))

        sizer.Add(bs, 1, wx.EXPAND)

    def make_PID(self, sizer=None):
        if sizer is None:
            sizer = self.sbsPID

        controls = {}

        self.lblPeq = wx.StaticText(self, label='P =')
        self.lblPover = wx.StaticText(self, label='/2^')
        self.lblIeq = wx.StaticText(self, label='I  =')
        self.lblIover = wx.StaticText(self, label='/2^')

        self.txtP = wx.TextCtrl(self)
        self.txtPshift = wx.TextCtrl(self)
        self.txtI = wx.TextCtrl(self)
        self.txtIshift = wx.TextCtrl(self)

        self.chkNegative = wx.CheckBox(self, wx.ID_ANY, "Negative?")

        self.bSendPID = wx.Button(self, label='Not Locked')
        self.bResetPID = wx.Button(self, label='Reset')

        controls['lblPeq'] = self.lblPeq
        controls['lblPover'] = self.lblPover
        controls['lblIeq'] = self.lblIeq
        controls['lblIover'] = self.lblIover

        controls['txtP'] = self.txtP
        controls['txtPshift'] = self.txtPshift
        controls['txtI'] = self.txtI
        controls['txtIshift'] = self.txtIshift

        controls['chkNegative'] = self.chkNegative

        controls['bSendPID'] = self.bSendPID
        controls['bResetPID'] = self.bResetPID

        bsP = wx.BoxSizer(wx.HORIZONTAL)
        bsP.AddMany([(self.lblPeq, 0, wx.EXPAND),
                     (self.txtP, 0, wx.EXPAND),
                     (self.lblPover, 0, wx.EXPAND),
                     (self.txtPshift, 0, wx.EXPAND)])

        bsI = wx.BoxSizer(wx.HORIZONTAL)
        bsI.AddMany([(self.lblIeq, 0, wx.EXPAND),
                     (self.txtI, 0, wx.EXPAND),
                     (self.lblIover, 0, wx.EXPAND),
                     (self.txtIshift, 0, wx.EXPAND)])

        bsN = wx.BoxSizer(wx.HORIZONTAL)
        bsN.Add(self.chkNegative, 0, wx.EXPAND)

        bsButtons = wx.BoxSizer(wx.HORIZONTAL)
        bsButtons.AddMany([(self.bSendPID, 0, wx.EXPAND),
                           (self.bResetPID, 0, wx.EXPAND)])

        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(bsP, 0, wx.EXPAND)
        bs.Add((-1, 3), 0, wx.EXPAND)
        bs.Add(bsI, 0, wx.EXPAND)
        bs.Add((-1, 3), 0, wx.EXPAND)
        bs.Add(bsN, 0, wx.EXPAND)
        bs.Add((-1, 3), 0, wx.EXPAND)
        bs.Add(bsButtons, 0, wx.ALIGN_CENTER)

        sizer.Add(bs, 1, wx.EXPAND)

        return controls

