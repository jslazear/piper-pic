#!/bin/env python

"""
utility.py
jlazear
2013-07-17

Utility functions for working with PSquid from the command line.

A utility class is required for connecting the PSquid interface to plotting
routines because they were individually designed to be maximally decoupled
and modular. So we cannot simply plug a plotter into PSquid. We instead need
a class to organize the communication between PSquid and PyOscope. Utility
does this.

Example:

from psquid import PSquid
from psquidemulator import PSquidEmulator

p = PSquid('/Users/jlazear/pty1')
pse = PSquidEmulator('/Users/jlazear/pty2')

u = Utility(p)
u.sweep(4)
u.data()
"""
version = 20130717
releasestatus = 'beta'

from numpy import *
from pyoscope import PyOscope
from readers import HexReader
import time


class Utility(object):
    def __init__(self, psquid, pyoscope=None):
        self.p = psquid
        self.rt = pyoscope

    def sweep(self, ch, read_ch=None, low=0., high=1., delta=.01,
              navg=100, ntimer=100, nsweeps=1, raw=False, f=None,
              windowsize=None, xy=True, legend=True):
        state = self.p.connection.state
        if state == 'data':
            self.p.data_off()
            time.sleep(0.2)
        elif state == 'sweep':
            self.p.sweep_off()
            time.sleep(0.2)

        newf = self.p.sweep(ch, read_ch, low, high, delta, navg, ntimer,
                            nsweeps, raw, f)

        time.sleep(1)  # Don't want to read an empty file

        if self.rt is None:
            self.rt = PyOscope(newf, reader=HexReader)
        else:
            self.rt.switch_file(newf)

        if not raw:
            def callback(s):
                s.data = s.data/65535.
        else:
            def callback(s):
                pass

        self.rt.set_callback(callback)

        if xy:
            self.rt.plot('swept', ['locked', 'adc'], windowsize=windowsize,
                         legend=legend)
        else:
            self.rt.plot(windowsize=windowsize, legend=legend)

    def data(self, ch=None, navg=1000, f=None, raw=False, windowsize=None,
             xy=False, legend=True):
        state = self.p.connection.state
        if state == 'data':
            self.p.data_off()
            time.sleep(.2)
        elif state == 'sweep':
            self.p.sweep_off()
            time.sleep(.2)

        newf = self.p.data(ch, navg, f)

        time.sleep(1)  # Don't want to read an empty file

        if self.rt is None:
            self.rt = PyOscope(newf, reader=HexReader)
        else:
            self.rt.switch_file(newf)

        if not raw:
            def callback(s):
                s.data = s.data/65535.
        else:
            def callback(s):
                pass

        self.rt.set_callback(callback)

        if xy:
            self.rt.plot('locked', 'adc', windowsize=windowsize,
                         legend=legend)
        else:
            self.rt.plot(windowsize=windowsize, legend=legend)

