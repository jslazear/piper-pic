import psquidreaders as psr
import sys
import os

PSRDR = psr.PSquidRawDataReader

fname = sys.argv[1]
newfname = os.path.splitext(fname)[0]

psrdr = PSRDR(fname).init_data()

psrdr.to_csv(newfname + '.txt')