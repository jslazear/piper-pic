#!/bin/env python

"""
psquid.py
jlazear
2013-06-20

Python interface classes for PSquid board

Long description

Example:

p = PSquid('/dev/tty.usbserial-A10180PC')
p.lock('s3fb', 1.2)
p.dac('s2b', 0.7)
p.sweep('s2fb')
"""
version = 20130618
releasestatus = 'beta'

from psquiderrors import PSquidError
from numpy import *
import serial
import inspect
import time
from collections import Iterable
from types import StringTypes
import cereal
from serial import SerialException
import os
from tempfile import NamedTemporaryFile


class PSquid(object):
    """
    An interface class for interacting with the PSquid board.
    """
    def __init__(self, port=None, baudrate=115200, timeout=1, throttle=True,
                 verbose=False,
                 **kwargs):
        self.connection = PSquidSerial(port, baudrate, timeout,
                                       verbose=verbose,
                                       **kwargs)
        self.connected = self.connection.ser.isOpen()

        self.locked = False
        self.locked_ch = None

        self.throttle = bool(throttle)
        self.verbose = verbose

    def __del__(self):
        try:
            self.connection.close()
            self.connection.stop()
        except:
            pass
        del self.connection

    def __repr__(self):
        idstr = hex(id(self))
        state = self.connection.state
        toret = ('<psquid.PSquid object at {id}: state = '
                 '{state}>'.format(id=idstr, state=state))
        return toret

    def close(self, verbose=None):
        if verbose is None:
            verbose = self.verbose
        if verbose: print "Closing PSquid..." #DELME
        if self.connection.state == 'data':
            if verbose: print "state = DATA, sending data_off" #DELME
            try:
                self.data_off()
                time.sleep(0.2)
            except:
                pass
        elif self.connection.state == 'sweep':
            if verbose: print "state = SWEEP, sending sweep_off" #DELME
            try:
                self.sweep_off()
                time.sleep(0.2)
            except:
                pass
        try:
            if verbose: print "Stopping Cereal thread..." #DELME
            self.connection.stop()
            time.sleep(0.2)
            if verbose: print "Closing Cereal thread..." #DELME
            self.connection.close()
        except:
            if verbose:
                print "Failed to stop or close. May be already done." #DELME
            pass
        if verbose: print "PSquid closed." #DELME


    def set_port(self, port):
        """
        Sets the port and attempts to open the connection.
        """
        self.connection.port = port
        try:
            self.connection.open()
            self.connected = self.connection.ser.isOpen()
        except SerialException:
            raise ValueError('Invalid port: {port}'.format(port=port))

    #-----------------------------------------------------------#
    #--------------------- User Commands -----------------------#
    #-----------------------------------------------------------#
    chdict = {'s1b': 0,
              's2b': 1,
              's3b': 2,
              'sab': 2,
              'detb': 3,
              's1fb': 4,
              's2fb': 5,
              's3fb': 6,
              'offset': 7,
              'gain': 8,
              0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8}

    def dac(self, ch, mv, raw=False):
        """
        Set the specified DAC `ch` to the specified value `mv` in
        millivolts.

        :Arguments:
            ch - ([array]int/str) Integer or string identifier of DAC
                 channel. May be an iterable type to set multiple
                 channels at once.

            mv - ([array]float) Value to which to set the DAC, in
                 millivolts between 0 and 1000 mV. May be an iterable
                 type to set multiple channels at once. If `raw` is
                 True, then this is in DAC units and has a full range
                 of 0 to 65535.

            raw - (bool) If False, `mv` is in volts and has a full
                  range of 0 to 1 V. If True, `mv` is in DAC units and
                  has a full range of 0 to 65535.

        :Returns:
            None

        Channel listing
        ch (int)   --    ch (str)   --   description
           0             's1b'           Stage 1 Bias
           1             's2b'           Stage 2 Bias
           2             's3b'/'sab'     Series Array (S3) Bias
           3             'detb'          Detector Bias
           4             's1fb'          Stage 1 Feedback
           5             's2fb'          Stage 2 Feedback
           6             's3fb'          Series Array (S3) Feedback
           7             'offset'        Output Offset
           8             'gain'          Output Gain

        Note that `ch` and `mv` may be scalars or vectors, but if `mv`
        is a vector, then `ch` must have matching length. The reverse
        is not true: if `ch` is a vector and `mv` is a scalar, then
        all channels in `ch` will be set to the value `mv`.

        This function only allows 1D `ch` and `mv`.
        """
        # Argument checking
        if ((not isinstance(ch, Iterable))
            or isinstance(ch, StringTypes)):
            ch = [ch]
        if not isinstance(mv, Iterable):
            mv = array([float(mv)]*len(ch), dtype=float)
        else:
            mv = array(mv, dtype=float)

        # mv = floor(65535*mv).astype(int)  # Convert to DAC units
        if not raw:
            mv = self._mv_to_dac(mv)
        ch = [self.chdict[c] for c in ch]  # Convert to int index

        # Set the DACs
        for i in range(len(ch)):
            c = ch[i]
            m = mv[i]
            self._dac(c, m)

    def data(self, ch=None, navg=1000, f=None, delete=True):
        """
        Enable data reporting mode.

        :Arguments:
            ch - (int/str) Integer or string identifier of DAC
            channel. See PSquid.dac command for channel listing. By
            default attempts to use the locked channel, if one exists.
            Otherwise uses Stage 3 Feedback (SA FB).

            navg - (int) Number of interrupt cycles to accumulate the
            data over before reporting a data point. Defaults to 1000.

            f - ([optional]file handle/str) The file to record the
            data into. If None, creates a temporary file in the
            current directory.

            delete - ([optional]bool) If a temporary file is used, whether
            or not the file should be deleted when closed.

        :Returns:
            f - (file handle) File handle of file to which data is
            being saved.
        """
        if ch is None:
            ch = (self.locked_ch
                  if (self.locked_ch is not None)
                  else 's3fb')
        elif (ch == -1) or (ch is False):
            self._data_off()

        if f is None:
            f = NamedTemporaryFile(mode='w', delete=delete)

        ch = self.chdict[ch]   # Convert to int index

        hdict = {'ch': ch, 'navg': navg,
                 'locked': self.locked, 'locked_ch': self.locked_ch,
                 'columns': ['locked', 'adc']}

        self.connection.set_state('data', f, hdict)
        self._data(ch, navg)
        return f

    def raw_data(self, ch=None, f=None, delete=True):
        """
        Enable raw_data reporting mode.

        'raw_data' mode outputs raw ADC or FB counts. It differs from 'data'
        in that the 'raw_data' mode reported values are un-accumulated, and
        so may be contained in an int16 rather than an int32, thus allowing
        a faster data rate.

        :Arguments:
            ch - (int/str) Integer or string identifier of DAC
            channel. See PSquid.dac command for channel listing. By
            default attempts to use the locked channel, if one exists.
            Otherwise uses Stage 3 Feedback (SA FB).

            f - ([optional]file handle/str) The file to record the
            data into. If None, creates a temporary file in the
            current directory.

            delete - ([optional]bool) If a temporary file is used, whether
            or not the file should be deleted when closed.

        :Returns:
            f - (file handle) File handle of file to which data is
            being saved.
        """
        if ch is None:
            ch = (self.locked_ch
                  if (self.locked_ch is not None)
                  else 's3fb')
        elif (ch == -1) or (ch is False):
            self._data_off()

        if f is None:
            f = NamedTemporaryFile(mode='w', delete=delete)

        ch = self.chdict[ch]   # Convert to int index

        navg = 1
        hdict = {'ch': ch,
                 'locked': self.locked, 'locked_ch': self.locked_ch,
                 'columns': ('dac' if self.locked else 'adc')}

        self.connection.set_state('raw_data', f, hdict)
        self._raw_data(ch)
        return f

    def data_off(self):
        """
        Turn off any data reporting.
        """
        self._data_off()

    def raw_data_off(self):
        """
        Turn off any raw data reporting.
        """
        self._raw_data_off()

    def sweep(self, ch, read_ch=None, low=0., high=1., delta=.01,
              navg=100, ntimer=100, nsweeps=1, raw=False, f=None,
              delete=True):
        """
        Begin a sweep.

        :Arguments:
            ch - (int/str) Integer or string identifier of DAC
            channel to be swept. See PSquid.dac command for channel
            listing.

            read_ch - (int/str) Integer or string identifier of DAC
            channel to be read during the sweep. If None, attempts to
            find the PID-controlled (locked) channel and uses that.
            Otherwise uses same channel as specified by `ch`.

            low - (float/int) Low end of the voltage range of the
            sweep. Defaults to 0 V. If `raw` is True, then this is in
            DAC units (code).

            high - (float/int) High end of the voltage range of the
            sweep. Defaults to 1 V. If `raw` is True, then this is in
            DAC units (code).

            delta - (float/int) Step size of the sweep in volts.
            Defaults to 0.01 V. If `raw` is True, then this is in DAC
            units (code).

            navg - (int) Number of interrupt cycles to accumulate the
            data over before reporting a data point.

            ntimer - (int) Number of cycles to wait to allow the DAC
            to settle before beginning to accumulate data.

            nsweep - (int) Number of times to execute the sweep. A
            value of 0 indicates that the sweep should repeat forever.

            raw - (bool) If False, `low`/`high`/`delta` are specified
            in voltage units, and the full range of the DAC is 0 -> 1
            volt. If True, `low`/`high`/`delta` are specified in DAC
            units, and the full range of the DAC is 0 -> 65535.

            f - ([optional]file handle/str) The file to record the
            data into. If None, creates a temporary file in the
            current directory.

            delete - ([optional]bool) If a temporary file is used, whether
            or not the file should be deleted when closed.

        :Returns:
            f - (file handle) File handle of file to which data is
            being saved.
        """
        if (ch == -1) or (ch is False):
            self._sweep_off()

        if f is None:
            f = NamedTemporaryFile(mode='w', delete=delete)

        if read_ch is None:
            read_ch = (self.locked_ch
                       if (self.locked_ch is not None)
                       else ch)

        # Convert low/high/delta units if necessary
        if not raw:
            low, high, delta = self._mv_to_dac((low, high, delta))

        ch = self.chdict[ch]   # Convert to int index
        read_ch = self.chdict[read_ch]   # Convert to int index

        hdict = {'ch': ch, 'read_ch': read_ch,
                 'low': low, 'high': high, 'delta': delta,
                 'navg': [1, navg, navg], 'ntimer': ntimer,
                 'nsweeps': nsweeps, 'locked': self.locked,
                 'locked_ch': self.locked_ch,
                 'columns': ['swept', 'locked', 'adc']}

        self.connection.set_state('sweep', f, hdict)
        self._sweep(ch, read_ch, low, high, delta, navg, ntimer,
                    nsweeps)
        return f

    def sweep_off(self):
        """
        Turn off any active sweeps.
        """
        self._sweep_off()

    def lock(self, ch, setpoint=0., p=10, i=100, i_shift=8, p_shift=4,
             init_dac=None, raw=False):
        """
        Lock a channel.

        :Arguments:
            ch - (int/str) Integer or string identifier of DAC
            channel to be locked. See PSquid.dac command for channel
            listing. If ch is None, unlocks any feedback.

            setpoint - (float/int) Value of the ADC to which to lock
            to. sweep. Defaults to 0 V. If `raw` is True, then this is
            in DAC units (code).

            p - (int) The p coefficient of the PID loop.

            i - (int) The i coefficient of the PID loop.

            p_shift - (int) log_2 of the p divisor of the proportional
            term in the PID loop.

            i_shift - (int) log_2 of the i divisor of the integral
            term in the PID loop.

            init_dac - (float/int) Value to set the to-be-locked DAC
            to before attempting to lock.

            raw - (bool) If False, `low`/`high`/`delta` are specified
            in voltage units, and the full range of the DAC is 0 -> 1
            volt. If True, `low`/`high`/`delta` are specified in DAC
            units, and the full range of the DAC is 0 -> 65535.

        :Notes:
            The PID update step is
                DAC = DAC_0 + p*error/2^p_shift + i*error/2^i_shift
        """
        if ch is None:
            self._pid_off()
            return

        # Convert low/high/delta units if necessary
        if not raw:
            setpoint = self._mv_to_adc(setpoint)

        # Initialize locked DAC
        if init_dac is not None:
            self.dac(ch, init_dac, raw=raw)
            time.sleep(.2)

        ch = self.chdict[ch]   # Convert to int index

        self._pid(ch, setpoint, p, i, p_shift, i_shift)

    def pid_params(self, p=None, p_shift=None, i=None, i_shift=None,
                   sign=None):
        """
        Set the PID parameters of the PID loop without modifying its state.

        :Arguments:
            p - (int) The p coefficient of the PID loop.

            i - (int) The i coefficient of the PID loop.

            p_shift - (int) log_2 of the p divisor of the proportional
            term in the PID loop.

            i_shift - (int) log_2 of the i divisor of the integral
            term in the PID loop.

            sign - (int/float/bool/{'positive', 'negative'}) Sign of the
            pid coefficients. Positive is True or >0.

        :Notes:
            The PID update step is
                DAC = DAC_0 + p*error/2^p_shift + i*error/2^i_shift

        """
        if p is not None:
            self._pid_p(p)
            time.sleep(.01)
        if p_shift is not None:
            self._pid_p_shift(p_shift)
            time.sleep(.01)
        if i is not None:
            self._pid_i(i)
            time.sleep(.01)
        if i_shift is not None:
            self._pid_i_shift(i_shift)
            time.sleep(.01)
        if sign is not None:
            if isinstance(sign, StringTypes):
                sign = sign.lower()
                sign = 'positive'.startswith(sign)
            elif isinstance(sign, (int, float)):
                sign = (sign > 0)
            else:
                sign = bool(sign)
            self._pid_sign(sign)

    def unlock(self):
        """
        Unlock any active feedback.
        """
        self._pid_off()

    def p(self):
        """
        Print status of the board.
        """
        self._p()

    def gain(self, gain=1):
        """
        Sets the gain of the user-adjustable gain DAC.

        Valid values for gain are 0.25 < gain < 16384.
        """
        gdac = int(floor(16384./gain))
        gdac = min(65535, gdac)
        gdac = max(1, gdac)

        self._dac(8, gdac)

    def ver(self, readresponse=True):
        """
        Queries the board for its PIC code version.
        """
        response = self._ver(readresponse)
        response = response.lstrip('*PSQUID: ')
        return response

    #-----------------------------------------------------------#
    #-------------------- Serial Functions ---------------------#
    #-----------------------------------------------------------#
    def write(self, *args, **kwargs):
        """
        Send an arbitrary string to the board. Simply calls
        self.connection.write.
        """
        return self.connection.write(*args, **kwargs)

    def read(self, *args, **kwargs):
        """
        Reads an arbitrary string from the board. Simply calls
        self.connection.read.
        """
        return self.connection.read(*args, **kwargs)

    def send(self, command, args=(), readresponse=False,
             terminator='\r', throttle=None, verbose=None):
        """
        Sends a command to the board. Arguments to the command may be
        included in arglist. Reads the response if readresponse is
        True. Correctly appends termination strings.
        """
        if verbose is None:
            verbose = self.verbose
        if not isinstance(args, str):
            try:
                argstr = ' '.join((str(arg) for arg in args))
            except TypeError:
                return
        else:
            argstr = args

        if throttle is None:
            throttle = self.throttle

        space = ' ' if argstr else ''
        towrite = command + space + argstr + terminator
        if verbose:
            print "towrite = ", repr(towrite) #DELME
        self.write(towrite)
        if throttle:
            time.sleep(0.05)  # Only allow one command every 50 ms

        if readresponse:
            response = self.readresponse()
            return response

    def readresponse(self, terminator='OK!\r\n', timeout=2.):
        """
        Reads the response to a command. Continues reading until it
        encounters the terminator string or reads for longer than the
        timeout period.
        """
        t0 = time.time()
        t = time.time()
        buf = ''
        char = None
        while not buf.endswith(terminator):
            t = time.time()
            if (t - t0 > timeout):
                raise PSquidTimeoutError
            char = self.connection.read(1)
            buf += char
            time.sleep(.001)
            # print "buf = ", repr(buf) #DELME

        buf = buf.rstrip(terminator)

        return buf

    #-----------------------------------------------------------#
    #---------------------- Raw Commands -----------------------#
    #-----------------------------------------------------------#
    def _dac(self, ch, value):
        """
        Set a DAC

        Arguments:
        <ch=integer dac identifier>
        <value=DAC code>

        Set the output of the specified DAC (all DACs are included) to
        the specified code
        """
        ch = int(ch)
        value = int(value)

        args = (ch, value)
        atypes = ('uch', 'uint16')
        if self._check_types(args, atypes):
            self.send('dac', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _sweep(self, ch, read_ch, low, high, delta, dwell, ntimer,
               nsweeps):
        """
        Sweep a DAC

        Arguments:
        <writeaddress=integer dac identifier>
        <readaddress=integer dac identifier>
        <low=DAC code>
        <high=DAC code>
        <delta=DAC code>
        <dwell=int>
        <ntimer=int>
        <nsweeps=int>

        Sweep the the specified DAC between low code and high code,
        using steps of delta (in code), dwelling at each step location
        for dwell integer samples. Waits for ntimer OC2 interrupts
        before accumulating. Sweeps nsweeps times, if nsweeps=0; if
        nsweeps=0, sweeps continuously until issued a sweep_stop
        command or a data command.
        """
        # Check for bad arguments
        ch = int(ch)
        read_ch = int(read_ch)
        low = int(low)
        high = int(high)
        delta = int(delta)
        dwell = int(dwell)
        ntimer = int(ntimer)
        nsweeps = int(nsweeps)

        args = (ch, read_ch, low, high, delta, dwell, ntimer, nsweeps)
        atypes = ('uch', 'uch', 'uint16', 'uint16', 'int17', 'uint16',
                  'uint16', 'uint16')
        if self._check_types(args, atypes):
            self.send('sweep', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _sweep_off(self):
        """
        Terminate the currently active sweep.

        Arguments:
        None

        Stops any active sweep. Completely resets the sweep state.
        """
        self.send('sweep_off')

    def _data(self, ch, navg):
        """
        Report data until interrupted

        Arguments:
        <ch=integer dac identifier>
        <navg=int>

        Reports back (ADC sample, read DAC code) pairs until interrupted.
        Averages together navg samples before reporting back.
        """
        ch = int(ch)
        navg = int(navg)

        args = (ch, navg)
        atypes = ('uch', 'uint16')
        if self._check_types(args, atypes):
            self.send('data', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _raw_data(self, ch):
        """
        Report raw data until interrupted

        Arguments:
        <ch=integer dac identifier>

        'raw_data' mode outputs raw ADC or FB counts. It differs from 'data'
        in that the 'raw_data' mode reported values are un-accumulated, and
        so may be contained in an int16 rather than an int32, thus allowing
        a faster data rate.
        """
        ch = int(ch)

        args = (ch,)
        atypes = ('uch',)
        if self._check_types(args, atypes):
            self.send('raw_data', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _data_off(self):
        """
        Terminate the data reporting.

        Arguments:
        None

        Terminates the reporting of data. Resets all data parameters.
        """
        self.send('data_off')

    def _raw_data_off(self):
        """
        Terminate the raw data reporting.

        Arguments:
        None

        Terminates the reporting of data. Resets all data parameters.
        """
        self.send('raw_data_off')

    def _pid(self, ch, s, p, i, p_shift, i_shift):
        """
        Initialize and begin a PID loop

        Arguments:
        <ch=int16[0, 8]>
        <s=uint16>
        <p=uint16>
        <i=uint16>
        <p_shift=uint8>
        <i_shift=uint8>

        Sets the PID parameters to the specified values and then enables the
        PID loop. Reinitializes the accumulator to 0.
        """
        # Check for bad arguments
        ch = int(ch)
        s = int(s)
        p = int(p)
        i = int(i)
        p_shift = int(p_shift)
        i_shift = int(i_shift)

        args = (ch, s, p, i, p_shift, i_shift)
        atypes = ('uch', 'uint16', 'uint16', 'uint16', 'uint8',
                  'uint8')
        if self._check_types(args, atypes):
            self.send('pid', args=args)
            self.locked = True
            self.locked_ch = ch
        else:
            raise PSquidArgumentError(args, atypes)

    def _pid_ch(self, ch):
        """
        Adjust the channel of current PID loop

        Arguments:
        <ch=uint16>

        Changes current PID loop's channel. Does not reinitialize PID loop
        or change any other PID parameters. Does not allow for a negative
        channel, so issue pid_off to disable PID loop.
        """
        # Check for bad arguments
        ch = int(ch)

        args = (ch,)
        atypes = ('uch',)
        if self._check_types(args, atypes):
            self.send('pid_ch', args=args)
            self.locked_ch = ch
        else:
            raise PSquidArgumentError(args, atypes)

    def _pid_s(self, sp):
        """
        Adjust the setpoint of current PID loop

        Arguments:
        <sp=uint16>

        Changes current PID loop's setpoint. Does not reinitialize PID loop
        or change any other PID parameters.
        """
        # Check for bad arguments
        sp = int(sp)

        args = (sp,)
        atypes = ('uint16',)
        if self._check_types(args, atypes):
            self.send('pid_s', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _pid_sign(self, positive):
        """
        Sets the sign of the PID loop parameters.

        Arguments:
        <positive=bool>

        Sets the current PID loop's sign. Does not reinitialize PID loop or
        change any other PID parameters. Both the P and I terms always share
        the same sign. positive=True indicates a positive sign,
        positive=False indicates a negative sign.
        """
        positive = bool(positive)

        sign = 'pos' if positive else 'neg'
        args = (sign,)
        self.send('pid_sign', args=args)

    def _pid_p(self, p):
        """
        Adjust the p constant of current PID loop

        Arguments:
        <p=uint16>

        Changes current PID loop's setpoint. Does not reinitialize PID loop
        or change any other PID parameters.
        """
        # Check for bad arguments
        p = int(p)

        args = (p,)
        atypes = ('uint16',)
        if self._check_types(args, atypes):
            self.send('pid_p', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _pid_p_shift(self, p_shift):
        """
        Adjust the fixed point basis of the proportional term of the PID loop

        Arguments:
        <p_shift=uint16>

        Changes current PID loop's fixed point basis for the proportional
        term.

        Does not reinitialize PID loop or change any other PID parameters.
        """
        # Check for bad arguments
        p_shift = int(p_shift)

        args = (p_shift,)
        atypes = ('uint16',)
        if self._check_types(args, atypes):
            self.send('pid_p_shift', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _pid_i(self, i):
        """
        Adjust the i constant of current PID loop

        Arguments:
        <i=uint16>

        Changes current PID loop's setpoint. Does not reinitialize PID loop
        or change any other PID parameters.
        """
        # Check for bad arguments
        i = int(i)

        args = (i,)
        atypes = ('uint16',)
        if self._check_types(args, atypes):
            self.send('pid_i', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _pid_i_shift(self, i_shift):
        """
        Adjust the fixed point basis of the integral term of the PID loop

        Arguments:
        <i_shift=uint16>

        Changes current PID loop's fixed point basis for the integral term.
        Does not reinitialize PID loop or change any other PID parameters.
        """
        # Check for bad arguments
        i_shift = int(i_shift)

        args = (i_shift,)
        atypes = ('uint16',)
        if self._check_types(args, atypes):
            self.send('pid_i_shift', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _pid_off(self):
        """
        Terminate the PID loop.

        Arguments:
        None

        Terminates the PID control in the board.
        """
        self.locked = False
        self.locked_ch = None
        self.send('pid_off')

    def _rs(self, row):
        """
        Select stage 1 row

        Arguments:
        <row=int>

        Select which row address line to drive, specified by integer.
        """
        # Check for bad arguments
        row = int(row)

        args = (row,)
        atypes = ('row',)
        if self._check_types(args, atypes):
            self.send('rs', args=args)
        else:
            raise PSquidArgumentError(args, atypes)

    def _p(self):
        """
        Prints board status.

        Usage:
        p   -> Prints board status
        """
        self.send('p')

    def _ver(self, readresponse=False):
        """
        Spits out a version string for the current code.

        Usage:
        No arguments
        """
        return self.send('ver', readresponse=readresponse)

    def _set_binary(self, binary=True):
        """
        Turns on/off binary output mode.

        If `binary` is True, output mode is changed to binary. Otherwise,
        output mode is changed to ASCII-Hex.
        """
        arg = 1 if binary else 0
        self.send('output_mode {0}'.format(arg))

    #-----------------------------------------------------------#
    #-------------------- Utility Functions --------------------#
    #-----------------------------------------------------------#
    vartypes = {'int': (-2**15, 2**15 - 1),
                'int16': (-2**15, 2**15 - 1),
                'uint': (0, 2**16),
                'uint16': (0, 2**16),
                'int17': (-2**16, 2**16 - 1),
                'int8': (-2**7, 2**7 - 1),
                'uint8': (0, 2**8),
                'char': (-2**7, 2**7 - 1),
                'uchar': (0, 2**8),
                'uchannel': (0, 8),
                'uch': (0, 8),
                'row': (0, 7),
                'channel': (-1, 8),
                'ch': (-1, 8)}

    @classmethod
    def _check_type(cls, value, vartype):
        """
        Checks that the value is within the accepted range for the
        specified vartype. See PSquid.vartypes for available vartypes.
        """
        low, high = cls.vartypes[vartype]
        return (low <= value <= high)

    @classmethod
    def _check_types(cls, values, vartypes, summed=True):
        """
        Checks many types at once. Returns True iff all checks pass.
        """
        checks = (cls._check_type(values[i], vartypes[i])
                  for i in range(len(values)))

        if summed:
            sumcheck = False if (False in checks) else True
            return sumcheck
        else:
            return checks

    @staticmethod
    def _mv_to_dac(mv):
        if isinstance(mv, ndarray):
            mv = floor(mv*65535)
            mv = minimum(mv, mv*0 + 65535)
            mv = maximum(mv, mv*0 + 0)
        elif isinstance(mv, Iterable):
            mvs = []
            for m in mv:
                m = floor(m*65535)
                m = min(m, 65535)
                m = max(m, 0)
                mvs.append(m)
            mv = mvs
        else:
            mv = floor(mv*65535)
            mv = min(mv, 65535)
            mv = max(mv, 0)
        return mv

    @staticmethod
    def _mv_to_adc(mv):
        if isinstance(mv, ndarray):
            mv = floor((mv + 2.048)/4.096*65535)
            mv = minimum(mv, mv*0 + 65535)
            mv = maximum(mv, mv*0 + 0)
        elif isinstance(mv, Iterable):
            mvs = []
            for m in mv:
                m = floor((m + 2.048)/4.096*65535)
                m = min(m, 65535)
                m = max(m, 0)
                mvs.append(m)
            mv = mvs
        else:
            mv = floor((mv + 2.048)/4.096*65535)
            mv = min(mv, 65535)
            mv = max(mv, 0)
        return mv


class PSquidSerial(cereal.Cereal):
    """
    A stateful class that is meant to handle serial monitoring and
    communication for the PSquid interface.

    Owns a serial connection, a state, and (sometimes) a file handle.

    The serial connection allows it to communicate with the board.

    The state indicates how the communication should be handled (i.e.
    what should SerialThread be doing while communicating, e.g. does
    SerialThread need to record data from a sweep command?).

    The file handle is the location to which PSquidSerial will stream
    data if it needs to read data.

    A file is used as the storage location because it maximally
    decouples any operation PSquid wants to do on the data from the
    recording of the data by PSquidSerial.

    Subclasses cereal.Cereal because Cereal is already a buffered
    serial port driver. All we need to do is dump the buffer to the
    file handle and add statefulness.

    Note that PSquidSerial is run in a separate thread because we do
    not want any modes of communication that require monitoring and
    polling the serial port (e.g. reading the output during sweeps) to
    be blocking to the PSquid class.
    """
    # Variables with these names should not be passed to the
    # underlying serial connection object.
    localvars = ('ser', 'timeout', 'buffer', 'running', 'state',
                 'filehandle', 'realtime', '_statedict', '_setstatedict')

    def __init__(self, port=None, baudrate=115200, timeout=1, realtime=False,
                 verbose=False,
                 **kwargs):
        super(PSquidSerial, self).__init__(port=port,
                                           baudrate=baudrate,
                                           timeout=timeout, **kwargs)

        self.state = 'do nothing'
        self.filehandle = None
        self.realtime = realtime  # Write data immediately to disk?

        self.verbose = verbose

        # Dictionary of actions to take according to the state
        self._statedict = {'do nothing': self._do_nothing,
                           'sweep': self._sweep,
                           'data': self._data,
                           'raw_data': self._raw_data}

        # Dictionary of functions to call to change the state
        self._setstatedict = {'do nothing': self._set_do_nothing,
                              'sweep': self._set_sweep,
                              'data': self._set_data,
                              'raw_data': self._set_raw_data}

        self.tempindex = 0  #DELME

        self.times = []  #DELME
        self.times2 = []  #DELME
        self.lens = []  #DELME
        self.lens2 = []  #DELME

    def run(self):
        """
        Essentially a copy of Cereal.run(), except with
        self.handledata() added into the loop.
        """
        while not self.stopped.isSet():
            try:
                if self.openflag:
#                    self.ser.open()
                    self._read_port()  #DELME?
                    # with self.bufferlock:
                    #     try:
                    #         iw = self.ser.inWaiting()
                    #         iw = max(1, int(iw))
                    #         toadd = self.ser.read(iw)
                    #     except (ValueError, TypeError):
                    #         toadd = ''
                    #     except (OSError, serial.SerialException):
                    #         toadd = ''
                    #     self.buffer += toadd
                time.sleep(.01)
                self.handledata()   # Deal with data by state
            except KeyboardInterrupt:
                self.stopped.set()

    def _read_port(self):
        """
        Read any characters waiting in the port and append them to the
        buffer.
        """
        with self.bufferlock:
            try:
                iw = self.ser.inWaiting()
                iw = max(1, int(iw))
                toadd = self.ser.read(iw)
            except (ValueError, TypeError):
                toadd = ''
            except (OSError, serial.SerialException):
                toadd = ''
            self.buffer += toadd

    def stop(self, verbose=None):
        """
        Adds a lock around the port access so there are no race conditions
        between closing the port in stop() and reading the port in run().
        """
        # DELME Shutdown procedure seems to cause segfault/illegal
        # instruction, need to sort that out at some point.
        # Not a big deal right now since happens near end of shutdown
        # procedure and everything has already been safely closed by that
        # point
        if verbose is None:
            verbose = self.verbose
        if verbose: print "Acquiring bufferlock..." #DELME
        with self.bufferlock:
            if verbose: print "Stopping Cereal loop..." #DELME
            # super(PSquidSerial, self).stop()
            if self.isAlive():
                if verbose:
                    print "Thread is alive, setting stop flag." #DELME
                self.stopped.set()
                time.sleep(0.2) #DELME?
                # print "Joining process." #DELME
                # print 'Flag = ', self.stopped.isSet() #DELME
                # self.join()  # Hangs on join? Why? #FIXME
            if verbose: print "Closing serial port connection." #DELME
            self.ser.close()
            if verbose: print "Successfully stopped thread." #DELME
        if verbose: print "Released bufferlock." #DELME


    #-----------------------------------------------------------#
    #-------------------- Utility Functions --------------------#
    #-----------------------------------------------------------#
    def set_state(self, newstate,  *args, **kwargs):
        """
        Changes the state.
        """
        if self.verbose:
            print 'Trying to change state to "{0}"'.format(newstate) #DELME
        try:
            self._setstatedict[newstate](*args, **kwargs)
        except KeyError:
            raise PSquidInvalidStateError(newstate)

    def _set_do_nothing(self, *args, **kwargs):
        """
        Change to the 'do nothing' state.
        """
        if self.filehandle is not None:
            self.filehandle.close()
            self.filehandle = None

        self.flushInput()
        self.state = 'do nothing'

    def _set_sweep(self, f, headerdict=None, *args, **kwargs):
        """
        Change to the 'sweep' state. Must pass in a file handle or
        filename.
        """
        # Close the currently used file handle
        if self.filehandle is not None:
            self.filehandle.close()

        # Make new file handle
        if isinstance(f, file):
            self.filehandle = f
        else:
            try:
                self.filehandle = open(f, 'w')
            except TypeError as e:
                try:
                    f.file
                    self.filehandle = f
                except AttributeError:
                    raise e

        if headerdict:
            self._write_header(headerdict)
        self.flushInput()
        self.state = 'sweep'

    def _set_data(self, f, headerdict=None, *args, **kwargs):
        """
        Change to the 'data' state. Must pass in a file handle or
        filename.
        """
        # Close the currently used file handle
        if self.filehandle is not None:
            self.filehandle.close()

        # Make new file handle
        if isinstance(f, file):
            self.filehandle = f
        else:
            try:
                self.filehandle = open(f, 'w')
            except TypeError as e:
                try:
                    f.file
                    self.filehandle = f
                except AttributeError:
                    raise e

        if headerdict:
            self._write_header(headerdict)
        self.flushInput()
        self.state = 'data'

    def _set_raw_data(self, f, headerdict=None, *args, **kwargs):
        """
        Change to the 'raw_data' state. Must pass in a file handle or
        filename.
        """
        # Close the currently used file handle
        if self.filehandle is not None:
            self.filehandle.close()

        # Make new file handle
        if isinstance(f, file):
            self.filehandle = f
        else:
            try:
                self.filehandle = open(f, 'w')
            except TypeError as e:
                try:
                    f.file
                    self.filehandle = f
                except AttributeError:
                    raise e

        if headerdict:
            self._write_header(headerdict)
        self.flushInput()
        self.state = 'raw_data'

    def handledata(self):
        """
        Handle the data according to the state
        """
        self._statedict[self.state]()  # Call appropriate function

    def _do_nothing(self):
        """
        The 'do nothing' state. Used when the PSquid board is not
        reporting anything back except command responses.
        """
        # if self.inWaiting(): print 'OUT: ', repr(self.buffer) #DELME
        # if self.buffer.endswith('\r\n'):
        #     print self.read(self.inWaiting())
        pass

    def _sweep(self, verbose=None):
        """
        The 'sweep' state. Used when the PSquid board is performing a
        sweep and reporting back data triplets.
        """
        if verbose is None:
            verbose = self.verbose
        # This code is adapted from pixiereadout's data loop.
        self.times.append(time.time())  #DELME
        if self.filehandle is None:
            return

        linelen = 15  # each line is ('^xx yyyy zzzz\r\n')
        checkstr = "*PSQUID: sweep_off\r\n"[:linelen]
        cont = True

        with self.bufferlock:
            self.lens.append(len(self.buffer))  #DELME
            while len(self.buffer) >= linelen:
                self.times2.append(time.time())  #DELME
                self.lens2.append(len(self.buffer))  #DELME
                # print "BUFFER: \n", repr(self.buffer)  #DELME
                lentoread = (len(self.buffer)/linelen)*linelen
                if (self.buffer[0] == '^'
                    and self.buffer[lentoread-linelen] == '^'):
                    line = self._unsafe_read(lentoread)
                    self._write_to_file(line)
                elif self.buffer[0] == '^':
                    line = self._unsafe_read(linelen)
                    self._write_to_file(line)
                elif self.buffer[:linelen] == checkstr:
                    iw = self.ser.inWaiting()

                    # Occasionally the state machine leaves the sweep state
                    # prematurely, truncating the file. This ensures that
                    # this does not happen.
                    while iw:
                        self._read_port() #DELME?
                        time.sleep(0.1) #DELME
                        iw = self.ser.inWaiting()
                    if cont:  #DELME
                        cont = not cont   #DELME
                        time.sleep(0.1) #DELME
                        continue  #DELME
                    line = self.readline()
                    if verbose: print "line = ", repr(line) #DELME
                    self._write_to_file(line)
                    self.set_state('do nothing')
                else:
                    try:
                        caret = self.buffer.index('^')
                    except ValueError:
                        caret = 'none'
                    try:
                        star = self.buffer.index('*')
                    except ValueError:
                        star = 'none'

                    next = min(caret, star)
                    if isinstance(next, int):
                        line = self._unsafe_read(next)
                        # self._write_to_file(line)  #DELME?

                # Crash protection...
                self.tempindex += 1  #DELME
                if self.tempindex >= 100000:
                    print repr("Sending: sweep_off\r") #DELME
                    self.write('sweep_off\r') #DELME
                    self.set_state('do nothing')  #DELME


        # OLD
        # if '*PSQUID: sweep_off' in self.buffer:
        #     self.readline(eol='\r\nOK!\r\n')
        #     self.set_state('do nothing')
        # else:
        #     while '\r\n' in self.buffer:
        #         towrite = self.readline(eol='\r\n')
        #         if not (('PSQUID' in towrite) or ('OK!' in towrite)):
        #             self._write_to_file(towrite)

    def _data(self):
        """
        The 'data' state. Used when the PSquid board is in the data
        averaging state and is reporting back data pairs.
        """
        # This code is adapted from pixiereadout's data loop.
        self.times.append(time.time())  #DELME
        if self.filehandle is None:
            return

        linelen = 12  # each line is ('^xxxx yyyy\r\n')
        checkstr = "*PSQUID: data_off\r\n"[:linelen]

        with self.bufferlock:
            self.lens.append(len(self.buffer))  #DELME
            while len(self.buffer) >= linelen:
                self.times2.append(time.time())  #DELME
                self.lens2.append(len(self.buffer))  #DELME
                # print "BUFFER: \n", repr(self.buffer)  #DELME
                lentoread = (len(self.buffer)/linelen)*linelen
                if (self.buffer[0] == '^'
                    and self.buffer[lentoread-linelen] == '^'):
                    line = self._unsafe_read(lentoread)
                    self._write_to_file(line)
                elif self.buffer[0] == '^':
                    line = self._unsafe_read(linelen)
                    self._write_to_file(line)
                elif self.buffer[:linelen] == checkstr:
                    line = self.readline()
                    self._write_to_file(line)
                    self.set_state('do nothing')
                else:
                    try:
                        caret = self.buffer.index('^')
                    except ValueError:
                        caret = 'none'
                    try:
                        star = self.buffer.index('*')
                    except ValueError:
                        star = 'none'

                    next = min(caret, star)
                    if isinstance(next, int):
                        line = self._unsafe_read(next)
                        # self._write_to_file(line)  #DELME?

                # # Crash protection...
                # self.tempindex += 1  #DELME
                # if self.tempindex >= 100000:
                #     print repr("Sending: data_off\r") #DELME
                #     self.write('data_off\r') #DELME
                #     self.set_state('do nothing')  #DELME

    def _raw_data(self, verbose=None):
        """
        The 'raw_data' state. Used when the PSquid board is in the raw_data
        reporting state and is reporting back raw ADC or FB counts.

        This mode differs from the 'data' state in that, since there is no
        averaging in this state, only an int16 is reported rather than an
        int32, thus allowing a higher data rate.
        """
        if verbose is None:
            verbose = self.verbose
        # This code is adapted from pixiereadout's data loop.
        self.times.append(time.time())  #DELME
        if self.filehandle is None:
            return

        linelen = 3  # each line is ('^' + 2 bytes)

        with self.bufferlock:
            self.lens.append(len(self.buffer))  #DELME
            while len(self.buffer) >= linelen:
                self.times2.append(time.time())  #DELME
                self.lens2.append(len(self.buffer))  #DELME
                # print "BUFFER: \n", repr(self.buffer)  #DELME
                lentoread = (len(self.buffer)/linelen)*linelen
                if (self.buffer[0] == '^'
                    and self.buffer[lentoread-linelen] == '^'):
                    line = self._unsafe_read(lentoread)
                    self._write_to_file(line)
                elif self.buffer[0] == '^':
                    line = self._unsafe_read(linelen)
                    self._write_to_file(line)
                elif self.buffer[:linelen] == '*PS':
                    if verbose: print "Found '*PS', stopping loop." #DELME
                    line = self.readline()
                    self._write_to_file(line)
                    self.set_state('do nothing')
                else:
                    try:
                        caret = self.buffer.index('^')
                    except ValueError:
                        caret = 'none'
                    try:
                        star = self.buffer.index('*')
                    except ValueError:
                        star = 'none'

                    next = min(caret, star)
                    if isinstance(next, int):
                        line = self._unsafe_read(next)
                        # self._write_to_file(line)  #DELME?

                # # Crash protection...
                # self.tempindex += 1  #DELME
                # if self.tempindex >= 100000:
                #     print repr("Sending: raw_data_off\r") #DELME
                #     self.write('raw_data_off\r') #DELME
                #     self.set_state('do nothing')  #DELME

    def _write_to_file(self, towrite):
        """
        Forces a write to disk, i.e. writes to the file buffer then
        flushes the file buffer to disk.

        This guarantees that the string written to the file will
        appear immediately in the file, which is useful if some other
        program is monitoring the file (e.g. kst).
        """
        try:
            self.filehandle.write(towrite)
            if self.realtime:
                self.filehandle.flush()
                os.fsync(self.filehandle.fileno())
        except AttributeError:
            print "Attempted to write to closed file..." #DELME?

    def _write_header(self, headerdict, comments='#'):
        """
        Writes the key-value pairs in headerdict to the file header.
        """
        for key, value in headerdict.iteritems():
            # ['adc', 'locked'] -> '[adc, locked]'
            if isinstance(value, Iterable) and \
               not isinstance(value, StringTypes):
                value = [str(v) for v in value]
                value = '[{0}]'.format(', '.join(value))
            towrite = "{com} {key}: {value}\n".format(com=comments, key=key,
                                                      value=value)
            self._write_to_file(towrite)

    #DELME
    def write(self, data, verbose=None):
        if verbose is None:
            verbose = self.verbose
        if verbose: print "WRITING: ", repr(data)
        return super(PSquidSerial, self).write(data)

#-----------------------------------------------------------#
#---------------------- Error Classes ----------------------#
#-----------------------------------------------------------#
class PSquidArgumentError(PSquidError):
    """
    An error indicating that there was an error with the arguments
    passed to a PSquid function.
    """
    def __init__(self, values, vartypes):
        if isinstance(values, (int, float)):
            values = [values]
        if isinstance(vartypes, (int, float)):
            vartypes = [vartypes]

        # Get method that raised error
        callername = inspect.stack()[1][3]
        method = getattr(PSquid, callername)

        # Get arguments of method
        argnames = inspect.getargspec(method)[0]
        argnames = argnames[1:]  # Get rid of 'self' argument

        self.msg = ("Invalid argument passed to "
                    "PSquid.{0}. Expected (Got): ".format(callername))
        toappend = []
        checks = PSquid._check_types(values, vartypes, summed=False)
        for i, check in enumerate(checks):
            if not check:
                vartype = vartypes[i]
                value = values[i]
                low, high = PSquid.vartypes[vartype]
                name = argnames[i]
                temp = "{0} <= {1} ({2}) <= {3}".format(low, name,
                                                        value, high)
                toappend.append(temp)

        toappend = ' and '.join(toappend)
        self.msg += toappend

    def __str__(self):
        return self.msg


class PSquidTimeoutError(PSquidError):
    """
    An error indicating that something in PSquid timed out.
    """
    def __init__(self):
        self.msg = "PSquid timed out!"

    def __str__(self):
        return self.msg


class PSquidInvalidStateError(PSquidError):
    """
    An error indicating the specified state is invalid.
    """
    def __init__(self, newstate):
        self.msg = "Attempted to change to an invalid state: "
        self.msg += str(newstate)

    def __str__(self):
        return self.msg
