"""
The graphic lock control panel for PSquid.
"""
import wx
# from wx.lib.scrolledpanel import ScrolledPanel
from misc import DACsChoice, make_gbsizer_from_array
import numpy as np


class GraphicalLockPanel(wx.Panel):
# class SweepPanel(ScrolledPanel): #DELME?
    """
    The graphical lock control panel for PSquid.
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)
        # ScrolledPanel.__init__(self, parent=parent, id=wx.ID_ANY) #DELME?

        self.fmf = self.GetTopLevelParent()
        self.timer = wx.Timer(self)
        self.timer.Start(500)  # Send timer event every 50 ms  #UN#DELME

        self.helptext = ("Lock a DAC.")
        # self.helptext = ("Specify channel you want to monitor, the number of "
        #                  "samples to average (must be >100), and the location"
        #                  " to save the data to.\n\nChecking 'Temp File' will "
        #                  "use a throw-away temporary file, so a filename is "
        #                  "not necessary in that case.\n\nThen click 'Collect "
        #                  "Data' to begin collecting and plotting data.\n\n"
        #                  "The 'Window Size' parameter specifies how many "
        #                  "post-averaging samples should be plotted. The "
        #                  "latest 'Window Size' samples will be plotted.\n\n"
        #                  "You may toggle whether or not the ADC or DAC is "
        #                  "shown with their check boxes.")

        self.create_sizers()
        self.populate_top()

        # self.SetupScrolling(scroll_x=False, scroll_y=True) #DELME?

    def create_sizers(self):
        """
        Create the sizers in the panel.
        """
        self.bsMain = wx.BoxSizer(wx.VERTICAL)

        self.bsTop = wx.BoxSizer(wx.VERTICAL)

        self.bsMain.Add(self.bsTop, 1, wx.EXPAND)

        self.SetSizer(self.bsMain)

    def populate_top(self, sizer=None):
        """
        Populate the top sizer.
        """
        if sizer is None:
            sizer = self.bsTop

        # Create controls
        # self.txtHelp = StaticWrapText(self, wx.ID_ANY, label=self.helptext)

        lblfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.BOLD)

        self.sbStep = wx.StaticBox(self, label='Step')
        self.sbStep.SetFont(lblfont)
        self.sbsStep = wx.StaticBoxSizer(self.sbStep, wx.VERTICAL)
        self.stepctrls = self.make_step()

        self.sbSweep = wx.StaticBox(self, label='Sweep')
        self.sbSweep.SetFont(lblfont)
        self.sbsSweep = wx.StaticBoxSizer(self.sbSweep, wx.VERTICAL)
        self.sweepctrls = self.make_sweep()

        self.bStart = wx.Button(self, label='Collect Data')

        txtfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        txtsize = wx.Size(70, 22)
        self.lblStepValue = wx.StaticText(self, label='Step Value')
        self.txtStepValue = wx.TextCtrl(self, size=txtsize)
        self.txtStepValue.SetFont(txtfont)
        self.sldStepValue = wx.Slider(self, minValue=0, maxValue=0,
                                      size=(150, -1))

        self.bSend = wx.Button(self, label='Send to DAC')

        self.sbPID = wx.StaticBox(self, label='PID')
        self.sbsPID = wx.StaticBoxSizer(self.sbPID, wx.VERTICAL)
        self.PIDctrls = self.make_PID()

        # self.lblLock = wx.StaticText(self, label='Click on plot to lock.',
        #                              style=wx.EXPAND | wx.ALIGN_CENTER)
        blocktext = 'Click on plot to select lock location'
        self.bLock = wx.Button(self, label=blocktext,
                               style=wx.EXPAND | wx.ALIGN_CENTER)

        # arr = np.array([[(-1, 1), None, None],
        #                 [self.sbsStep, self.sbsStep, self.sbsStep],
        #                 [self.sbsSweep, self.sbsSweep, self.sbsSweep],
        #                 [(-1, 1), None, None]],
        #                dtype='object')
        arr = np.array([[(-1, 1), None, None],
                        [self.lblStepValue, self.sldStepValue,
                         self.sldStepValue],
                        [None, self.txtStepValue, self.bSend],
                        [self.sbsPID, self.sbsPID, self.sbsPID],
                        # [self.lblLock, self.lblLock, self.lblLock],
                        [self.bLock, self.bLock, self.bLock],
                        [(-1, 1), None, None],
                        ['hline', 'hline', 'hline'],
                        [(-1, 1), None, None],
                        [self.sbsStep, self.sbsStep, self.sbsStep],
                        [self.sbsSweep, self.sbsSweep, self.sbsSweep],
                        [self.bStart, self.bStart, self.bStart],
                        [(-1, 1), None, None]],
                       dtype='object')

        lflag = wx.EXPAND | wx.RIGHT
        rflag = wx.EXPAND | wx.LEFT
        cflag = wx.ALIGN_CENTER

        # styles = np.array([[None, None, None],
        #                    [None, None, None],
        #                    [None, None, None],
        #                    [None, None, None]],
        #                   dtype='object')
        # styles = np.array([[None, None, None],
        #                    [0, 0, 0],
        #                    [None, 0, 0],
        #                    [cflag, cflag, cflag],
        #                    [None, None, None],
        #                    [None, None, None],
        #                    [None, None, None],
        #                    [rflag, None, 0],
        #                    [None, rflag, 0],
        #                    [None, None, None],
        #                    [cflag, cflag, cflag],
        #                    [None, None, None]],
        #                   dtype='object')
        styles = np.zeros_like(arr, dtype=object)
        # styles[1, 1] = wx.EXPAND
        styles[4, 0] = wx.ALIGN_CENTER
        styles[6, 0] = wx.EXPAND
        styles[10, 0] = wx.ALIGN_CENTER

        bs = make_gbsizer_from_array(self, arr, styles, hgap=1, vgap=1,
                                     vertmargin=True, vminsize=5)
        bs.AddGrowableCol(0, 1)
        bs.AddGrowableCol(4, 1)
        bs.AddGrowableRow(4, 1)
        # bs.AddGrowableRow(3-1, 1)
        bs.SetEmptyCellSize((1, 1))

        sizer.Add(bs, 1, wx.EXPAND)

    def make_step(self, sizer=None):
        if sizer is None:
            sizer = self.sbsStep

        controls = {}

        self.lblStep = wx.StaticText(self, label='Step DAC ')
        self.chStep = DACsChoice(self, wx.ID_ANY)

        txtfont = wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        txtsize = wx.Size(55, 14)

        # sldsize = (120, -1)
        self.lblStartStep = wx.StaticText(self, label='Step from ')
        self.txtStartStep = wx.TextCtrl(self, size=txtsize)
        self.txtStartStep.SetFont(txtfont)

        self.lblEndStep = wx.StaticText(self, label=' to ')
        self.txtEndStep = wx.TextCtrl(self, size=txtsize)
        self.txtEndStep.SetFont(txtfont)

        self.lblStepStep = wx.StaticText(self, label=' by ')
        self.txtStepStep = wx.TextCtrl(self, size=txtsize)
        self.txtStepStep.SetFont(txtfont)

        controls['lblStep'] = self.lblStep
        controls['chStep'] = self.chStep
        controls['lblStartStep'] = self.lblStartStep
        controls['txtStartStep'] = self.txtStartStep
        controls['lblEndStep'] = self.lblEndStep
        controls['txtEndStep'] = self.txtEndStep
        controls['lblStepStep'] = self.lblStepStep
        controls['txtStepStep'] = self.txtStepStep

        bsStepDAC = wx.BoxSizer(wx.HORIZONTAL)
        bsStepDAC.Add(self.lblStep, 0, wx.EXPAND)
        bsStepDAC.Add(self.chStep, 0, wx.EXPAND)

        bsStepParams = wx.BoxSizer(wx.HORIZONTAL)
        bsStepParams.AddMany([(self.lblStartStep, 0, wx.EXPAND | wx.RIGHT),
                              (self.txtStartStep, 0, wx.EXPAND),
                              (self.lblEndStep, 0, wx.EXPAND),
                              (self.txtEndStep, 0, wx.EXPAND),
                              (self.lblStepStep, 0, wx.EXPAND),
                              (self.txtStepStep, 0, wx.EXPAND)])

        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(bsStepDAC, 0, wx.EXPAND)
        bs.Add((-1, 5))
        bs.Add(bsStepParams, 0, wx.EXPAND)

        sizer.Add(bs, 1, wx.EXPAND)

        return controls

    # def make_step(self, sizer=None):
    #     if sizer is None:
    #         sizer = self.sbsStep

    #     controls = {}

    #     self.lblStep = wx.StaticText(self, label='Step DAC')
    #     self.chStep = DACsChoice(self, wx.ID_ANY)

    #     txtfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
    #     txtsize = wx.Size(70, 17)
    #     # sldsize = (120, -1)
    #     self.lblStartStep = wx.StaticText(self, label='Start Value')
    #     self.txtStartStep = wx.TextCtrl(self, size=txtsize)
    #     self.txtStartStep.SetFont(txtfont)
    #     self.sldStartStep = wx.Slider(self, minValue=0, maxValue=65535)

    #     self.lblEndStep = wx.StaticText(self, label='End Value')
    #     self.txtEndStep = wx.TextCtrl(self, size=txtsize)
    #     self.txtEndStep.SetFont(txtfont)
    #     self.sldEndStep = wx.Slider(self, minValue=0, maxValue=65535)

    #     self.lblStepStep = wx.StaticText(self, label='Step Size')
    #     self.txtStepStep = wx.TextCtrl(self, size=txtsize)
    #     self.txtStepStep.SetFont(txtfont)
    #     self.sldStepStep = wx.Slider(self, minValue=0, maxValue=65535)

    #     controls['lblStep'] = self.lblStep
    #     controls['chStep'] = self.chStep
    #     controls['lblStartStep'] = self.lblStartStep
    #     controls['sldStartStep'] = self.sldStartStep
    #     controls['txtStartStep'] = self.txtStartStep
    #     controls['lblEndStep'] = self.lblEndStep
    #     controls['sldEndStep'] = self.sldEndStep
    #     controls['txtEndStep'] = self.txtEndStep
    #     controls['lblStepStep'] = self.lblStepStep
    #     controls['sldStepStep'] = self.sldStepStep
    #     controls['txtStepStep'] = self.txtStepStep

    #     arr = np.array([[(-1, 1), None, None],
    #                     [self.lblStep, self.chStep, self.chStep],
    #                     [self.lblStartStep, self.sldStartStep,
    #                      self.txtStartStep],
    #                     [self.lblEndStep, self.sldEndStep, self.txtEndStep],
    #                     [self.lblStepStep, self.sldStepStep,
    #                      self.txtStepStep],
    #                     [(-1, 1), None, None]],
    #                    dtype='object')

    #     lflag = wx.EXPAND | wx.RIGHT
    #     rflag = wx.EXPAND | wx.LEFT

    #     styles = np.array([[None, None, None],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [None, None, None]],
    #                       dtype='object')

    #     bs = make_gbsizer_from_array(self, arr, styles, vertmargin=False)
    #     bs.AddGrowableCol(1, 1)
    #     bs.AddGrowableRow(5, 1)
    #     bs.SetEmptyCellSize((1, 1))

    #     sizer.Add(bs, 1, wx.EXPAND)

    #     return controls

    def make_sweep(self, sizer=None):
        if sizer is None:
            sizer = self.sbsSweep

        controls = {}

        self.lblSweep = wx.StaticText(self, label='Sweep DAC   ')
        self.chSweep = DACsChoice(self, wx.ID_ANY)

        self.lblMonitor = wx.StaticText(self, label='Monitor DAC ')
        self.chMonitor = DACsChoice(self, wx.ID_ANY, auto=True)

        txtfont = wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        txtsize1 = wx.Size(55, 14)
        txtsize2 = wx.Size(50, 14)
        # sldsize = (120, -1)
        self.lblStartSweep = wx.StaticText(self, label='Sweep from ')
        self.txtStartSweep = wx.TextCtrl(self, size=txtsize1)
        self.txtStartSweep.SetFont(txtfont)

        self.lblEndSweep = wx.StaticText(self, label=' to ')
        self.txtEndSweep = wx.TextCtrl(self, size=txtsize1)
        self.txtEndSweep.SetFont(txtfont)

        self.lblStepSweep = wx.StaticText(self, label=' by ')
        self.txtStepSweep = wx.TextCtrl(self, size=txtsize1)
        self.txtStepSweep.SetFont(txtfont)

        self.lblNsweeps = wx.StaticText(self, label='Nsweeps')
        self.txtNsweeps = wx.TextCtrl(self, size=txtsize2)
        self.txtNsweeps.SetFont(txtfont)

        self.lblNavg = wx.StaticText(self, label=' Navg')
        self.txtNavg = wx.TextCtrl(self, size=txtsize2)
        self.txtNavg.SetFont(txtfont)

        self.lblNsettle = wx.StaticText(self, label=' Nsettle')
        self.txtNsettle = wx.TextCtrl(self, size=txtsize2)
        self.txtNsettle.SetFont(txtfont)

        controls['lblSweep'] = self.lblSweep
        controls['chSweep'] = self.chSweep
        controls['lblStartSweep'] = self.lblStartSweep
        controls['txtStartSweep'] = self.txtStartSweep
        controls['lblEndSweep'] = self.lblEndSweep
        controls['txtEndSweep'] = self.txtEndSweep
        controls['lblStepSweep'] = self.lblStepSweep
        controls['txtStepSweep'] = self.txtStepSweep
        controls['lblNsweeps'] = self.lblNsweeps
        controls['txtNsweeps'] = self.txtNsweeps
        controls['lblNavg'] = self.lblNavg
        controls['txtNavg'] = self.txtNavg
        controls['lblNsettle'] = self.lblNsettle
        controls['txtNsettle'] = self.txtNsettle

        bsSweepDAC = wx.BoxSizer(wx.HORIZONTAL)
        bsSweepDAC.Add(self.lblSweep, 0, wx.EXPAND)
        bsSweepDAC.Add(self.chSweep, 0, wx.EXPAND)

        bsMonitorDAC = wx.BoxSizer(wx.HORIZONTAL)
        bsMonitorDAC.Add(self.lblMonitor, 0, wx.EXPAND)
        bsMonitorDAC.Add(self.chMonitor, 0, wx.EXPAND)

        bsSweepParams = wx.BoxSizer(wx.HORIZONTAL)
        bsSweepParams.AddMany([(self.lblStartSweep, 0, wx.EXPAND | wx.RIGHT),
                               (self.txtStartSweep, 0, wx.EXPAND),
                               (self.lblEndSweep, 0, wx.EXPAND),
                               (self.txtEndSweep, 0, wx.EXPAND),
                               (self.lblStepSweep, 0, wx.EXPAND),
                               (self.txtStepSweep, 0, wx.EXPAND)])

        bsSweepParams2 = wx.BoxSizer(wx.HORIZONTAL)
        bsSweepParams2.AddMany([(self.lblNsweeps, 0, wx.EXPAND | wx.RIGHT),
                                (self.txtNsweeps, 0, wx.EXPAND),
                                (self.lblNavg, 0, wx.EXPAND),
                                (self.txtNavg, 0, wx.EXPAND),
                                (self.lblNsettle, 0, wx.EXPAND),
                                (self.txtNsettle, 0, wx.EXPAND)])

        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(bsSweepDAC, 0, wx.EXPAND)
        bs.Add(bsMonitorDAC, 0, wx.EXPAND)
        bs.Add((-1, 5))
        bs.Add(bsSweepParams, 0, wx.EXPAND)
        bs.Add((-1, 2))
        bs.Add(bsSweepParams2, 0, wx.EXPAND)

        sizer.Add(bs, 1, wx.EXPAND)

        return controls

    # def make_sweep(self, sizer=None):
    #     if sizer is None:
    #         sizer = self.sbsSweep

    #     controls = {}

    #     self.lblSwept = wx.StaticText(self, label='Sweep DAC')
    #     self.chSwept = DACsChoice(self, wx.ID_ANY)
    #     self.lblMonitor = wx.StaticText(self, label='Monitor DAC')
    #     self.chMonitor = DACsChoice(self, wx.ID_ANY, auto=True)

    #     txtfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
    #     txtsize = wx.Size(70, 17)
    #     self.lblStartSweep = wx.StaticText(self, label='Start Value')
    #     self.txtStartSweep = wx.TextCtrl(self, size=txtsize)
    #     self.txtStartSweep.SetFont(txtfont)
    #     self.sldStartSweep = wx.Slider(self, minValue=0, maxValue=65535)

    #     self.lblEndSweep = wx.StaticText(self, label='End Value')
    #     self.txtEndSweep = wx.TextCtrl(self, size=txtsize)
    #     self.txtEndSweep.SetFont(txtfont)
    #     self.sldEndSweep = wx.Slider(self, minValue=0, maxValue=65535)

    #     self.lblStepSweep = wx.StaticText(self, label='Step Size')
    #     self.txtStepSweep = wx.TextCtrl(self, size=txtsize)
    #     self.txtStepSweep.SetFont(txtfont)
    #     self.sldStepSweep = wx.Slider(self, minValue=0, maxValue=65535)

    #     self.lblNsweeps = wx.StaticText(self, label='N Sweeps')
    #     self.spinNsweeps = wx.SpinCtrl(self, min=0, max=65535, initial=1)

    #     self.lblNavg = wx.StaticText(self, label='N Average')
    #     self.spinNavg = wx.SpinCtrl(self, min=100, max=65535, initial=100)

    #     self.lblNsettle = wx.StaticText(self, label='N Settle')
    #     self.spinNsettle = wx.SpinCtrl(self, min=10, max=65535, initial=100)

    #     controls['lblSwept'] = self.lblSwept
    #     controls['chSwept'] = self.chSwept
    #     controls['lblMonitor'] = self.lblMonitor
    #     controls['chMonitor'] = self.chMonitor
    #     controls['lblStartSweep'] = self.lblStartSweep
    #     controls['sldStartSweep'] = self.sldStartSweep
    #     controls['txtStartSweep'] = self.txtStartSweep
    #     controls['lblEndSweep'] = self.lblEndSweep
    #     controls['sldEndSweep'] = self.sldEndSweep
    #     controls['txtEndSweep'] = self.txtEndSweep
    #     controls['lblStepSweep'] = self.lblStepSweep
    #     controls['sldStepSweep'] = self.sldStepSweep
    #     controls['txtStepSweep'] = self.txtStepSweep
    #     controls['lblNsweeps'] = self.lblNsweeps
    #     controls['spinNsweeps'] = self.spinNsweeps
    #     controls['lblNavg'] = self.lblNavg
    #     controls['spinNavg'] = self.spinNavg
    #     controls['lblNsettle'] = self.lblNsettle
    #     controls['spinNsettle'] = self.spinNsettle

    #     arr = np.array([[(-1, 1), None, None],
    #                     [self.lblSwept, self.chSwept, self.chSwept],
    #                     [self.lblMonitor, self.chMonitor, self.chMonitor],
    #                     [self.lblStartSweep, self.sldStartSweep, self.txtStartSweep],
    #                     [self.lblEndSweep, self.sldEndSweep, self.txtEndSweep],
    #                     [self.lblStepSweep, self.sldStepSweep, self.txtStepSweep],
    #                     [self.lblNsweeps, self.spinNsweeps, self.spinNsweeps],
    #                     [self.lblNavg, self.spinNavg, self.spinNavg],
    #                     [self.lblNsettle, self.spinNsettle, self.spinNsettle],
    #                     [(-1, 1), None, None]],
    #                     dtype='object')

    #     lflag = wx.EXPAND | wx.RIGHT
    #     rflag = wx.EXPAND | wx.LEFT

    #     styles = np.array([[None, None, None],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [rflag, lflag, lflag],
    #                        [None, None, None]],
    #                        dtype='object')

    #     bs = make_gbsizer_from_array(self, arr, styles, vertmargin=False)
    #     bs.AddGrowableCol(1, 1)
    #     bs.AddGrowableRow(9, 1)
    #     bs.SetEmptyCellSize((1, 1))

    #     sizer.Add(bs, 1, wx.EXPAND)

    #     return controls

    # def make_PID(self, sizer=None):
    #     if sizer is None:
    #         sizer = self.sbsPID

    #     controls = {}

    #     self.lblPeq = wx.StaticText(self, label='P =')
    #     self.lblPover = wx.StaticText(self, label='/2^')
    #     self.lblIeq = wx.StaticText(self, label='I =')
    #     self.lblIover = wx.StaticText(self, label='/2^')

    #     self.spinP = wx.SpinCtrl(self)
    #     self.spinPshift = wx.SpinCtrl(self)
    #     self.spinI = wx.SpinCtrl(self)
    #     self.spinIshift = wx.SpinCtrl(self)

    #     controls['lblPeq'] = self.lblPeq
    #     controls['lblPover'] = self.lblPover
    #     controls['lblIeq'] = self.lblIeq
    #     controls['lblIover'] = self.lblIover

    #     controls['spinP'] = self.spinP
    #     controls['spinPshift'] = self.spinPshift
    #     controls['spinI'] = self.spinI
    #     controls['spinIshift'] = self.spinIshift

    #     arr = np.array([[(-1, 1), None, None, None, (-1, 1)],
    #                     [self.lblPeq, self.spinP, self.lblPover,
    #                      self.spinPshift, None],
    #                     [self.lblIeq, self.spinI, self.lblIover,
    #                      self.spinIshift, None],
    #                     [(-1, 1), None, None, None, None]],
    #                    dtype='object')

    #     lflag = wx.EXPAND | wx.RIGHT
    #     rflag = wx.EXPAND | wx.LEFT

    #     styles = np.array([[None, None, None, None],
    #                        [rflag, lflag, rflag,lflag],
    #                        [rflag, lflag, rflag,lflag],
    #                        [None, None, None, None]],
    #                       dtype='object')

    #     bs = make_gbsizer_from_array(self, arr, styles, vertmargin=False)
    #     bs.AddGrowableCol(4, 1)
    #     bs.AddGrowableRow(3, 1)
    #     bs.SetEmptyCellSize((1, 1))

    #     sizer.Add(bs, 1, wx.EXPAND)

    #     return controls

    def make_PID(self, sizer=None):
        if sizer is None:
            sizer = self.sbsPID

        controls = {}

        self.lblPeq = wx.StaticText(self, label='P =')
        self.lblPover = wx.StaticText(self, label='/2^')
        self.lblIeq = wx.StaticText(self, label='I  =')
        self.lblIover = wx.StaticText(self, label='/2^')

        self.txtP = wx.TextCtrl(self)
        self.txtPshift = wx.TextCtrl(self)
        self.txtI = wx.TextCtrl(self)
        self.txtIshift = wx.TextCtrl(self)

        self.chkNegative = wx.CheckBox(self, wx.ID_ANY, "Negative?")

        self.bSendPID = wx.Button(self, label='Not Locked')
        self.bResetPID = wx.Button(self, label='Reset')

        controls['lblPeq'] = self.lblPeq
        controls['lblPover'] = self.lblPover
        controls['lblIeq'] = self.lblIeq
        controls['lblIover'] = self.lblIover

        controls['txtP'] = self.txtP
        controls['txtPshift'] = self.txtPshift
        controls['txtI'] = self.txtI
        controls['txtIshift'] = self.txtIshift

        controls['chkNegative'] = self.chkNegative

        controls['bSendPID'] = self.bSendPID
        controls['bResetPID'] = self.bResetPID

        bsP = wx.BoxSizer(wx.HORIZONTAL)
        bsP.AddMany([(self.lblPeq, 0, wx.EXPAND),
                     (self.txtP, 0, wx.EXPAND),
                     (self.lblPover, 0, wx.EXPAND),
                     (self.txtPshift, 0, wx.EXPAND)])

        bsI = wx.BoxSizer(wx.HORIZONTAL)
        bsI.AddMany([(self.lblIeq, 0, wx.EXPAND),
                     (self.txtI, 0, wx.EXPAND),
                     (self.lblIover, 0, wx.EXPAND),
                     (self.txtIshift, 0, wx.EXPAND)])

        bsN = wx.BoxSizer(wx.HORIZONTAL)
        bsN.Add(self.chkNegative, 0, wx.EXPAND)

        bsButtons = wx.BoxSizer(wx.HORIZONTAL)
        bsButtons.AddMany([(self.bSendPID, 0, wx.EXPAND),
                           (self.bResetPID, 0, wx.EXPAND)])

        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(bsP, 0, wx.EXPAND)
        bs.Add((-1, 3), 0, wx.EXPAND)
        bs.Add(bsI, 0, wx.EXPAND)
        bs.Add((-1, 3), 0, wx.EXPAND)
        bs.Add(bsN, 0, wx.EXPAND)
        bs.Add((-1, 3), 0, wx.EXPAND)
        bs.Add(bsButtons, 0, wx.ALIGN_CENTER)

        sizer.Add(bs, 1, wx.EXPAND)

        return controls

        # self.lblFile = wx.StaticText(self, label='File')
        # self.txtFile = wx.TextCtrl(self, value='sweep.txt')
        # self.chkFile = wx.CheckBox(self, label='Temp File')

        # self.lblMonitor = wx.StaticText(self, label='Monitor')
        # self.chMonitor = DACsChoice(self, auto=True)
        # self.lblWSize = wx.StaticText(self, label='Window Size')
        # self.spinWSize = wx.SpinCtrl(self, max=int(1.e9))

        # self.lblShowx = wx.StaticText(self, label='X axis')
        # self.chkIndexx = wx.CheckBox(self, wx.ID_ANY, "Index")
        # self.chkSweptx = wx.CheckBox(self, wx.ID_ANY, "Swept")
        # self.chkDACx = wx.CheckBox(self, wx.ID_ANY, "Monitor")
        # self.chkADCx = wx.CheckBox(self, wx.ID_ANY, "ADC")
        # bsChkx = wx.BoxSizer(wx.VERTICAL)
        # bsChkx.AddMany([self.chkIndexx,
        #                 self.chkSweptx,
        #                 self.chkDACx,
        #                 self.chkADCx])

        # self.lblShowy = wx.StaticText(self, label='Y axis')
        # self.chkIndexy = wx.CheckBox(self, wx.ID_ANY, "Index")
        # self.chkSwepty = wx.CheckBox(self, wx.ID_ANY, "Swept")
        # self.chkDACy = wx.CheckBox(self, wx.ID_ANY, "Monitor")
        # self.chkADCy = wx.CheckBox(self, wx.ID_ANY, "ADC")
        # bsChky = wx.BoxSizer(wx.VERTICAL)
        # bsChky.AddMany([self.chkIndexy,
        #                 self.chkSwepty,
        #                 self.chkDACy,
        #                 self.chkADCy])
        # bsChk = wx.BoxSizer(wx.HORIZONTAL)
        # bsChk.AddMany([self.lblShowx, bsChkx, ((20, -1), 1),
        #                self.lblShowy, bsChky])

        # self.lblAutoscale = wx.StaticText(self, label='Autoscale')
        # self.chkAutoscalex = wx.CheckBox(self, label='X axis')
        # self.chkAutoscaley = wx.CheckBox(self, label='Y axis')
        # bsChkAuto = wx.BoxSizer(wx.VERTICAL)
        # bsChkAuto.AddMany([self.chkAutoscalex, self.chkAutoscaley])

        # # Add controls to sizers
        # arr = np.array([[(-1, 1), None, None],
        #                 [self.lblSwept, self.chSwept, self.chSwept],
        #                 [self.lblStart, self.sldStart, self.txtStart],
        #                 [self.lblEnd, self.sldEnd, self.txtEnd],
        #                 [self.lblStep, self.sldStep, self.txtStep],
        #                 [self.lblNsweeps, self.spinNsweeps, self.spinNsweeps],
        #                 [(-1, 1), None, None],
        #                 [self.lblNavg, self.spinNavg, self.spinNavg],
        #                 [self.lblNsettle, self.spinNsettle, self.spinNsettle],
        #                 [(-1, 1), None, None],
        #                 [self.lblFile, self.txtFile, self.txtFile],
        #                 [None, self.chkFile, self.chkFile],
        #                 [(-1, 1), None, None],
        #                 [self.bStart, self.bStart, self.bStart],
        #                 [(-1, 1), None, None],
        #                 ['hline', 'hline', 'hline'],
        #                 [self.lblMonitor, self.chMonitor, self.chMonitor],
        #                 [self.lblWSize, self.spinWSize, self.spinWSize],
        #                 [bsChk, bsChk, bsChk],
        #                 [(-1, 1), None, None],
        #                 [self.lblAutoscale, bsChkAuto, bsChkAuto],
        #                 [(-1, 1), None, None]],
        #                 dtype='object')

        # lflag = wx.EXPAND | wx.RIGHT
        # rflag = wx.EXPAND | wx.LEFT
        # cflag = wx.ALIGN_CENTER

        # styles = np.array([[None, None, None],
        #                    [rflag, lflag, lflag],
        #                    [rflag, None, lflag],
        #                    [rflag, None, lflag],
        #                    [rflag, None, lflag],
        #                    [rflag, lflag, lflag],
        #                    [None, None, None],
        #                    [rflag, None, lflag],
        #                    [rflag, None, lflag],
        #                    [None, None, None],
        #                    [rflag, lflag, lflag],
        #                    [None, lflag, lflag],
        #                    [None, None, None],
        #                    [cflag, cflag, cflag],
        #                    [None, None, None],
        #                    [lflag, lflag, lflag],
        #                    [rflag, lflag, lflag],
        #                    [rflag, lflag, lflag],
        #                    [cflag, cflag, cflag],
        #                    [None, None, None],
        #                    [rflag, lflag, lflag],
        #                    [None, None, None]],
        #                    dtype='object')

        # self.gbs = make_gbsizer_from_array(self, arr, styles, vertmargin=True,
        #                                    vminsize=5)

        # # self.gbs.Add((30, -1), (0, 3)) #DELME?
        # # self.gbs.AddGrowableCol(3, 1) #DELME?
        # self.gbs.AddGrowableCol(0, 1)
        # self.gbs.AddGrowableCol(4, 1)
        # self.gbs.AddGrowableRow(14, 1)

        # sizer.Add(self.gbs, 1, wx.EXPAND)
