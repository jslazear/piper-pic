import serial
import time

def test(port='/dev/tty.usbserial-DAWVBF3H', baudrate=115200, totransmit='U'):
	ser = serial.Serial(port, baudrate=baudrate)

	flag = True
	while flag:
		try:
			ser.write(totransmit)
			print ser.read(1)
			time.sleep(.1)
		except KeyboardInterrupt:
			flag = False

	ser.close()


# pid = 0x6015 = 24597
# vid = 0x0403 = 1027