#include <p30F5011.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>

#include "tread.h"
#include "../common/common.h"
#include "../common/stty.h"
#include "../common/ee_io.h"
#include "../common/tty_print.h"


#define SETTINGS_VERSION 0x0001     // Increment this integer each time the structure of the settings
                                    // are changed.  This will cause restore_settings to fail, which is
                                    // what we want if the settings are changed.

/*
 * POS  LEN   DESCRIPTION
 *   0    2   Settings Version
 *   2    1   Data on flag
 *   3    1   Echo on flag
 *   4    1   Tmux auto
 *   5    1   Tmux setting (0 if tmux auto is true)
 *   6   64   16 x (Amplitude / Gain DAC setting pairs)
 *
 *       70   TOTAL LENGTH
 */
#define EE_DATA_LEN    70

unsigned char _EEDATA(_EE_ROW) settingsEE[EE_DATA_LEN + EE_CHECKSUM_LEN];		// Reserve memory for the state data in EEPROM


/**
 * Save all board settings to EEPROM
 */
void save_settings()
{
    unsigned word;
    unsigned char byte;

    // Get the address of our EE block
    _prog_addressT ee;
    _init_prog_address(ee, settingsEE);

    eeOpenWrite(ee);
    eeWriteWord(SETTINGS_VERSION);  // Write out the version

    // Write out the DATA ON / OFF Flag
    byte = data_frame_on();
    eeWriteByte(byte);

    // Write out the ECHO ON / OFF Flag
    byte = cmd_get_echo();
    eeWriteByte(byte);

    // Write out the TMUX Setting
    unsigned char tmux_auto = tmux_auto_on();      // 1 means TMUX is in auto state and get_tmux() should be ignored
    eeWriteByte(tmux_auto);
    if( !tmux_auto )
        byte = get_tmux();
    else
        byte = 0;
    eeWriteByte(byte);
    
    // Write out AMPLITUDE and GAIN settings for each channel
    int i;
    for( i=0; i<NCHANNELS; ++i )  {
        word = read_amp_dac(i);
        eeWriteWord(word);
     
        word = read_gain_dac(i);
        eeWriteWord(word);
    }

    eeWriteChecksum();
    eeCloseWrite();
}

/**
 * Restores board settings from EEProm.
 * Returns 1 if settings are restored successfully.
 *         0 if not restored (device blank, wrong version, wrong checksum)
 */
int load_settings( void )
{
    unsigned word;
    unsigned char byte;

    // Get the address of our EE block
    _prog_addressT ee;
    _init_prog_address(ee, settingsEE);


    // Verify Checksum
    if( !eeVerifyChecksum(ee, EE_DATA_LEN) ) {
        // If the EE memory is blank, then we will just use our standard, default settings.
        if( eeCheckIsBlank(ee, EE_DATA_LEN))
            return 0;
        
        TTYPuts( "Wrong checksum!\r\n");
        return 0;
    }

    eeOpenRead(ee);

    // Read in VERSION
    word = eeReadWord();
    if( word != SETTINGS_VERSION )  {
        TTYPuts( "Wrong settings version!\r\n");
        return 0;
    }

    // Read the DATA ON / OFF Flag
    byte = eeReadByte();
    set_data_frame(byte);

    // Read the ECHO ON / OFF Flag
    byte = eeReadByte();
    cmd_set_echo(byte);

    // Read the TMUX Setting
    unsigned char tmux_auto = eeReadByte();      // 1 means TMUX is in auto state and get_tmux() should be ignored
    set_tmux_auto(tmux_auto);
    byte = eeReadByte();
    if( !tmux_auto )
        set_tmux(byte);

    // Read AMPLITUDE and GAIN settings for each channel
    int i;
    for( i=0; i<NCHANNELS; ++i )  {
        word = eeReadWord();
        write_amp_dac(word, i);

        word = eeReadWord();
        write_gain_dac(word, i);
    }

    return 1;
}

void dump_ee()
{
    // Get the address of our EE block
    _prog_addressT ee;
    _init_prog_address(ee, settingsEE);

    int j, i, word;

    for( j=0; j<8; ++j )  {             // Rows
        for( i=0; i<16; ++i)  {         // 16 words per row
            _memcpy_p2d16(&word, ee, 2);

            TTYPrintHex16(word);
            TTYPutc(' ');

            ee += 2;           // Advance to the next word
        }
        TTYPutc('\r');
        TTYPutc('\n');
    }
}