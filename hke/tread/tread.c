#include <p30F5011.h>
#include <dsp.h>
#include <spi.h>
#include <stdio.h>
#include <libpic30.h>

#include "tread.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & XT_PLL16); /* XT with 8xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF); /* Code protect disabled */


/* Global variables */
unsigned char CardAddress;

/*
 * Set all the GPIO leads on the pic to be either inputs or outputs
 * as needed.
 *
 * Set up the internal pic hardware, like the timers to generate square wave
 * for the convert input of the ADC.
 *
 * Set up the PIC SPI and UART (RS-232) port
 */
void init_pic()
{
    // Set up values for GPIO pins
    PORTCbits.RC1 = 1; // Turn on the LED

    // Set the TMUX Address lines and Enable lines as outputs
    TRISBbits.TRISB8 = 0;
    TRISBbits.TRISB9 = 0;
    TRISBbits.TRISB10 = 0;
    TRISBbits.TRISB11 = 0;
    TRISBbits.TRISB12 = 0;

    // Set up the thermometry mux to point to a CAL resistor
    // so we don't fry a sensitive thermometer if DACs are set to high current on startup
    write_tmux(15);

    // Set the direction for GPIO
    TRISCbits.TRISC14 = 0; // Set the RS-485 Enable pin as an output
    TRISCbits.TRISC1 = 0; // Set the LED as an output

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

    // Load pins for the DACs
    TRISBbits.TRISB14 = 0; // Gain DAC load pin
    TRISBbits.TRISB15 = 0; // Excitation (amplitude) DAC load pin
    PIN_ADAC_LD = 1; // Turn off the CHIP_SELECT for both dacs
    PIN_GDAC_LD = 1;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD9 = 0;
    PIN_LED = 0; // turn off LED

    // PIC control over AC excitation chop state
    TRISDbits.TRISD8 = 0; // PIC lead to control AC excitation chop state
    PIN_CHOP_CLK_PIC = 1;

    TRISDbits.TRISD1 = 0; // ADC convert pin

    // Short delay to wait for power supply voltages to settle
    __delay32((long) (RESET_DELAY_SECONDS * CLOCKFREQ));

    // Setup the SPI port
    OpenSPI2(FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON &
             MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
             PRI_PRESCAL_4_1,
             SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR);
    ConfigIntSPI2(SPI_INT_DIS); // NO SPI interrupts

    //------------------------------------------------------------------------------
    //The SPI module supports 4 different Serial Clock formats. The user can
    //select one of these 4 formats by configuring the Clock Polarity Select, or
    //CKP, and Clock Edge Select, or CKE, bits in the SPI Control Register.
    //The CKP bit determines whether the Serial Clock is at a high logic level or
    //low logic level when the SPI module is in an idle state.
    //If CKP is set, then SCK is high when idle, which means the SCK signal is
    //interpreted as �active-low�.
    //If CKP is cleared, then SCK is high when active, which means the SCK
    //signal is interpreted as �active-high�.
    //The CKE bit determines whether the Serial Data Output changes its state on
    //an idle-to-active transition of the Serial Clock or on an active-to-idle transition
    //of the Serial Clock.
    //If CKE is set, SDO changes on an active-to-idle SCK transition.
    //If CKE is cleared, SDO changes on an idle-to-active SCK transition.
    //------------------------------------------------------------------------------
    // Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
    SPI2CONbits.CKE = 1; // CKE=1 (see above)
    SPI2CONbits.CKP = 0; // CKP=0 means SCLK is active high
    SPI2CONbits.SMP = 1; // If the SMP bit is set, then the input sampling is done at the end of the bit output

    // Figure out what mode we're in and set constants appropriately (standard, high res, low res...)
    init_mode_params();

    // Init all the DACS on the board to sensible initial values
    set_initial_excitation();

    // Setup the UART with our standard backplane settings
    config_uart2();

    // Read in our card address and store in global variable
    CardAddress = get_addr();

    // Attempt to load stored configuration settings from EEPROM
    load_settings();

    // Before we start the interrupt handler, wait for a rising edge on external frame clock
    // This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
    // Do this just before turning on the timer interrupt.
    sync_to_frame_clock();

    // Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
    // Setup timer 2 and set its period (exact period depends on configuration, but it will be roughly a few KHZ)
    // turn on ~4 kHz interrupt handler.  Do this last.
    config_timer2();
}

int main(void)
{
    // Setup the PIC internal peripherals
    init_pic();

    // Setup our data frame buffers
    init_frame_buf_backplane('T', CardAddress);

    // Main loop, wait for command from user, do it, repeat forever...
    while (1) {
        char s[80];

        cmd_gets(s, sizeof (s)); // Get a command from the user
        process_cmd(s); // Deal with the command
    }

}
