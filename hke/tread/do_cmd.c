#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "tread.h"


/*
 * Set which thermometer to read
 */
void write_tmux( unsigned short ch )
{
	if (ch >= 16)
		return;
	
	//turn off mux
	PIN_TMUX_EN2 = 0;
	PIN_TMUX_EN1 = 0;
	
	// Set address bits
	// NOTE! Because of MUX wiring, Address bit patern is bitwise inverted
	if( ch & 0b0001 )			//set LSB of address
		PIN_TMUX_A0 = 0;
	else
		PIN_TMUX_A0 = 1;
	
	if( ch & 0b0010 )			//set middle bit of address
		PIN_TMUX_A1 = 0;
	else
		PIN_TMUX_A1 = 1;
	
	if( ch & 0b0100 )			//set MSB of address
		PIN_TMUX_A2 = 0;
	else
		PIN_TMUX_A2 = 1;
	
	// set enable bits
	if( ch < 8 )
		PIN_TMUX_EN1 = 1;
	else
		PIN_TMUX_EN2 = 1;
}

void disable_tmux( void )
{
	//turn off mux
	PIN_TMUX_EN2 = 0;
	PIN_TMUX_EN1 = 0;
}

/* 
 * Gets the hex address of the card.
 */
unsigned char get_addr(void)
{
	unsigned char addr = (PIN_ADDR3 << 3) | (PIN_ADDR2 << 2) | (PIN_ADDR1 << 1) | PIN_ADDR0;
	return addr;
}

/* 
 * Sets the current, in nanoamps, of a particular channel.
 */
void set_ina( unsigned long ina, unsigned short ch )
{
	//ina is in nano amps
	//I=V_ref/R * ADAC/65536
	//ADAC=(R*65536/V) ina * 10^-9
	//V_ref=4.096V, R=220k-ohm
	unsigned int adac = ina_to_adac(ina);
	write_amp_dac( adac, ch );
	//set gdac ??
}



/*
 * Sets gain so that delta is at 50% of full amplitude, if it is <20% or >80%
 * JRH --NOT USED-- !!!NOT SURE IF THIS EVEN WORKS OR IS A GOOD IDEA!!!
 */
void auto_set_gain( unsigned char ch ) {
	unsigned long demod = read_demod( ch );
	unsigned long delta = demod/SUM_TICKS;
	unsigned int gain = read_gain_dac( ch );
	// If delta is < 20% or > 80% of full amplitude, set gain such that delta is 50%
	if ( delta < 13107 ) {
		unsigned long dg = delta * gain;
		unsigned long g_new = dg/32768;
		write_gain_dac( (unsigned int) g_new, ch );
	} else if ( delta > 52429 ) {
		unsigned long ratio = delta / 32768;
		unsigned int g_new = gain * ((int) ratio);
		write_gain_dac( g_new, ch );
	}
}



static long data_counter = 0;
static long int data_sum = 0;
static long int sq_sum = 0;
/*
 * Updates counters and prints data when necessary
 */
void adc_noise_update( int skip, long int num, char format )
{

	short data = read_adc();
	data_sum += data;
	sq_sum += (data - data_sum/(data_counter+1) ) * (data - data_sum /(data_counter+1));
	// Print out data in correct format
	char str[10];	// Longest string is 65535\r\n\0 is 8 characters
	switch (format) {
		case 'd':					// Print number in Decimal, takes about 3 ms
			sprintf(str, "%i\r\n", data); 
			TTYPutsDirect( str );
			break;
		case 'x':
			//TTYPrintHex16(data);	// Print number in Hex, takes about 450 us
			break;
		case 'b':					// Print number in binary, takes about 90 us
			TTYPutc(data);
			TTYPutc(data>>8);
			break;
		default:
			break;
	}
}

/** Prints out sample of ADC data
 *  skip    -> Number of samples to skip between reported data points
 *  num     -> Total number of samples to print
 *  format  -> Format of the data (decimal, hex, or binary)
 */
void adc_noise_test( int skip, long int num, char format )
{
	while (data_counter < num) {

		// Check to see if we should print data
		if (get_sample_count() >= skip )  {
			zero_sample_count();
			data_counter++;

			// If new sample is ready, update counters and print if appropriate
			adc_noise_update(skip, num, format);
		}
	}
	long int mean = data_sum / num;
	long int var =  sq_sum / num;
	char mean_str[16];
	char stdev_str[16];
	sprintf(mean_str, "%li\r\n", mean); 
	TTYPuts( mean_str );
	sprintf(stdev_str, "%li\r\n", var);
	TTYPuts( stdev_str );
	zero_sample_count();
	data_counter = 0;
	data_sum = 0;
	sq_sum = 0;
}


/*
 * Called by cmd_gets() when it is just waiting for a user to hit a key.
 *
 * Use this for things that need to happen often, but are too much calculation
 * to do in the interrupt handler.
 */
void OnIdle( void )
{
}
