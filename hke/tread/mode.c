#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "tread.h"

struct ModeParams {
	unsigned char mode;

	int na_initial;		// initial setting of excitation current in nA
	unsigned short gdac_initial;	// initial setting of the gain setting dac

	float alpha;		// R52/(R50+R52), SCALING FACTOR FOR EXCITATION CURRENT
	float na_per_bit;	// Nano-amps per bit of amplitude dac
	float R_load;		// Total load resistance
};
static struct ModeParams P;

// Local functions
static unsigned char read_cur_mode( void );


/*
 * Set all the parameters that depend on the board mode.
 */ 
void init_mode_params()
{
	unsigned char mode = read_cur_mode();
	P.mode = mode;

	// AMPS per bit = 2 * alpha * VREF / (R_load * 65536) where VREF = 4.096 volts
	switch( mode ) {
		// 1 Meg resistors (signal conditioner box)
		case MODE_HIGH_RES:
			P.na_initial = 15;			// 15 nA, Imax = 50 nA
			P.gdac_initial = 16384;		// Gain = 4
			P.alpha = 1.0 / 72.5;
			P.na_per_bit = 0.000776604;
			P.R_load = 2220.1 * 1000;	// 2.2 M Ohms
			break;

		// 1 Ohm resistors (High current mode)
		case MODE_LOW_RES:
			P.na_initial = 1000;		// 1 uA default current, Imax = 2080 uA (2.8 mA)
			P.gdac_initial = 8192;		// Gain = 8
			P.alpha = 1.0;				// No scaling in high-current mode
			P.na_per_bit = 31.7259;		// 
			P.R_load = 3940.0;			// 2 * [1.82k + 100 (input res network) + 100 (mux)]
			break;
		
		// Diode mode
		case MODE_DIODE:
			P.na_initial = 0;			// 0 nA, turn off excitation current (diode mode uses on-board 10 uA source)
			P.gdac_initial = DIODE_MODE_GDAC_SETTING;
			P.alpha = 1.0;				// unused
			P.na_per_bit = 1.0;			// unused
			P.R_load = 1.0;				// unused
			break;

		// Standard 20k RuOx resistors
		case MODE_STANDARD:
		default:
			P.na_initial = 32;			// 32 nA, Imax = 500 nA
			P.gdac_initial = 8192;		// Gain = 8
			P.alpha = 1.0 / 72.5;
			P.na_per_bit = 0.007833;
			P.R_load = 220.1 * 1000;	// 220k Ohms
			break;
	}
}

/*
 * Set amplitude and gain dacs to mode-dependent values
 */
void set_initial_excitation()
{
	int i;
	
	for ( i = 0; i < 16; i++ ) 
	{
		write_gain_dac( P.gdac_initial, i );	// Set gain
		set_ina( P.na_initial, i );
	}

}


/*
 * Read the current mode of the board from the Jumpers (standard / high current mode) 
 */
static unsigned char read_cur_mode( void )
{
	if( !PIN_JP5_2_MODE && !PIN_JP5_3_MODE)
		return MODE_STANDARD;
	else if( PIN_JP5_2_MODE  && !PIN_JP5_3_MODE)
		return MODE_HIGH_RES;
	else if( !PIN_JP5_2_MODE && PIN_JP5_3_MODE)
		return MODE_LOW_RES;
	else
		return MODE_DIODE;
}

/*
 * Gets the current board mode from our mode params structure
 */
unsigned char get_cur_mode(void)
{
	return P.mode;
}

char * get_mode_string()
{
	if( P.mode == MODE_STANDARD )
		return "STANDARD";
	else if( P.mode == MODE_HIGH_RES )
		return "HIGH RES";
	else if( P.mode == MODE_LOW_RES )
		return "LOW RES";
	else if( P.mode == MODE_DIODE )
		return "DIODE";
	else
		return "UNKNOWN";
}

/*
 * Excitation current functions
 *
 * Translate from NanoAmps to ADAC setting
 */
unsigned int ina_to_adac( unsigned long ina ) {
	return (unsigned int)(ina / P.na_per_bit + 0.5);
}

unsigned long adac_to_ina( unsigned int adac ) {
	return (unsigned long)(adac*P.na_per_bit + 0.5);// + 0.5 for to round to nearest integer
}

unsigned long get_max_current() {
	return (unsigned long)(65535.0 * P.na_per_bit);
}

/* 
 * Calculates the resistance across a particular channel.
 */
float calc_r_val( unsigned short ch )
{
	// Read demodulated data from interrupt hanlder
	long data;
	data = read_demod( ch );
	// delta is the difference in adc value when chop is high and low (demodulated)
	// delta = demod value, averaged over N_SUM readings
	float delta = data/SUM_TICKS;
	unsigned int adac = read_amp_dac( ch );
	unsigned int gdac = read_gain_dac( ch );
	if (gdac == 0)
		return 0.0;
	float ratio = (float)adac / (float)gdac;

    // Preamp Gain = 100.0, 4.99 is gain of ADC scaling network
	float r = P.R_load*delta/( (100.0*65536.0/4.99*ratio*4 * P.alpha) - delta );

	return r;
}

/*
 * Calculates the voltage across diode
 */
float calc_diode_volts ( float delta )
{
	// convert delta from ADC units into volts 
	// then multiply by level shifter gain of 4.99 and divide by the GDAC gain of 10.0
	float v = delta*(4.096/65536)*(4.99/DIODE_MODE_GDAC_GAIN);

	return v;
}