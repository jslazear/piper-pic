#!/bin/env python

"""
timings2.py
jlazear
2013-05-16

A short script for finding timing values for HKE-UBC synchronization.

Edit the values between EDIT START and EDIT END, then run the script:

    python timings.py

The values are only suggested and should be double-checked, and then
will need to be inputted into the PIC code manually.
"""
version = 20130516
releasestatus = 'beta'

from numpy import *
from itertools import chain
from collections import OrderedDict

bettii = 64*24*40
bettiiM = 512
piper = 40*25*40
piperM = 625

MHz = 1000*1000
# ================================
# ---------- EDIT START ----------
# ================================

# Define UBC and HKE clock speeds and multipliers
# UBC Clock and multipliers
Fubc_clk = 25*MHz  # 25 MHz UBC Sync Box clock frequency

ubcdivisor = piper
M = piperM
# ubcdivisor = 60*28*40  # (cycles/sample) = (row dwell)*(# rows)*(coadded frames)
# M = 511  # number of UBC samples per sweep
Mubcdivisor = M*ubcdivisor

# HKE clock; suggested multipliers will be computed
Fhke_clk = 20*MHz

# ================================
# ----------- EDIT END -----------
# ================================

# Leave the code below here alone if you don't know what you're doing!

Fubc_clk = int(Fubc_clk)     # Note: code relies on this being an integer!

def factors2(n):
    result = []
    # test 2 and all of the odd numbers
    # xrange instead of range avoids constructing the list
    for i in chain([2], xrange(3, n+1, 2)):
        s = 0
        while n % i == 0:  # a good place for mod
            n /= i
            s += 1
        result.extend([i]*s)  # avoid another for loop
        if n == 1:
            return result


def round2(x):
    return round(x/2.)*2


def makelinestr(num, justify='^', digits=20):
    if not isinstance(justify, (list, tuple)):
        justify = [justify]*num
    if not isinstance(digits, (list, tuple)):
        digits = [digits]*num

    linestr = ''.join(['{' + str(i) + ':' + justify[i] +
                       str(digits[i]) + '}' for i in range(num)])
    return linestr


def estimate60Hz(n_period, t_sample, n_gap_min=0):
    """
    Computes the GAP_TICKS (n_gap) and SUM_TICKS (n_len) that
    maximizes 60 Hz rejection by fitting as many full 60 Hz periods
    into n_len as possible. Allows for specifying a minimum n_gap
    length.

    Arguments:
        n_period - number of samples in one full demod period, e.g.
                   (samples per frame)/16
        t_sample - the period of one sample in seconds
        n_gap_min - the minimum number of gap ticks allowed

    Returns (n_len, n_gap).
    """
    n_half_period = n_period/2
    t_60 = 1/60.
    n_60 = floor(t_60/t_sample)  # Should this be round, not floor???
    m_60 = int(floor((n_half_period)/n_60))  # here too

    n_gap = -1
    m_60 += 1
    while n_gap < n_gap_min:
        m_60 -= 1

        if m_60 != 0:
            n_len = n_60*m_60
            n_gap = n_half_period - n_len
        else:
            print "WARNING: No 60 Hz rejection configurations found!"
            n_gap = n_gap_min
            n_len = n_half_period - n_gap

    return n_len, n_gap, m_60


class HKEchoices(object):
    """
    Finds appropriate choices for the HKE timing parameters.
    """
    def __init__(self, Mubcdivisor, Fubc_clk, Fhke_clk):

                 #periodUBC, Thke):
        self.boards = {'tread': self.tread,
                       'pmaster': self.pmaster,
                       'analogin': self.analogin,
                       'dspid': self.dspid,
                       'quit': self.quit,
                       'common': self.user_choose}

        self.Mubcdivisor = Mubcdivisor
        self.Fubc_clk = Fubc_clk
        self.Fhke_clk = Fhke_clk
        self.period = float(Mubcdivisor)/Fubc_clk
        self.clockperiodHKE = 1./Fhke_clk
        self.clockperiodUBC = 1./Fubc_clk

        self.clockratio = round(self.period/self.clockperiodHKE, 8)
        # print "ratio of clocks = ", self.clockratio
        if self.clockratio % 1 != 0:
            print("Warning: HKE and UBC clocks may not be rational! Proceeding"
                  " with nearest integer multiple...")
            self.clockratio = int(self.clockratio)

        self.factors = factors2(int(self.clockratio))
        # print "factor list = ", self.factors

        cpts = arange(500, 16000, dtype='int64')
        spfs = (1.*self.clockratio)/cpts
        #valids = where(Mubcdivisor*Fhke_clk == cpts*)[0]
        DMFh = Mubcdivisor*Fhke_clk
        Fu = Fubc_clk
        self.valids, = where(DMFh == cpts*around(DMFh/(cpts*Fu))*Fu)

        self.cpts = cpts[self.valids]
        self.spfs = spfs[self.valids]

        self.samplingperiodHKE = self.clockperiodHKE*self.cpts
        self.samplingrateHKE = 1/self.samplingperiodHKE

        self.print_warning()

    def user_choose(self):
        self._user_choose()
        self.quit = False
        while not self.quit:
            self.user_choose_board()

    def print_warning(self):
        toprint = \
        """WARNING: UBC Clock is 50 MHz, but SyncBox clock is 25 MHz.
        Make sure you have the UBC multipliers specified to the
        correct clock!
        """

    def _user_choose(self):
        print "Frame Period = {0} s".format(self.period)

        linestr = makelinestr(4, digits=(10, 20, 20, 20))
        titles = ['Choice # | ',
                  'Sampling Rate (Hz) | ',
                  'Sampling Period (us) | ',
                  'Samples per Frame']
        print linestr.format(*titles)
        for i in range(len(self.cpts)):
            rate = self.samplingrateHKE[i]
            period = self.samplingperiodHKE[i]*1.e6
            n = self.spfs[i]
            print linestr.format(i, rate, period, n)

        i = int(raw_input('Choose Choice #: '))
        self.choice = i
        self.bp_common()

    def user_choose_board(self):
        print "\nAvailable Boards:\n"
        for b in self.boards:
            print b

        choice = raw_input("\nChoose Board ('quit' to exit): ").lower()
        if choice in self.boards.keys():
            self.boards[choice]()
        else:
            print "Unknown board."

    def bp_common(self):
        spf = self.spfs[self.choice]
        cps = self.cpts[self.choice]
        rate = self.samplingrateHKE[self.choice]
        p = self.samplingperiodHKE[self.choice]

        master = ceil(.01/p)  # (10 ms)/(sampling period)
        card = int(floor((spf - master)/20))
        master = int(spf - 20*card)

        toprint = OrderedDict([
            ('Frame Period (s)', self.period),
            ('CYCLES_PER_TICK', cps),
            ('NUM_ADDRESSES', 20),
            ('MASTER_TICKS', master),
            ('Master Duration (ms)', master*p*1.e3),
            ('TICKS_PER_ADDRESS', card),
            ('Card Duration (ms)', card*p*1.e3)])

        print "\nbp_common.h PARAMETERS"
        print '-'*80
        for name, value in toprint.items():
            print "{0:<20}{1:^3}{2:<20}".format(name, '=', value)

        print

    def quit(self):
        self.quit = True

    def tread(self):
        spf = self.spfs[self.choice]
        cps = self.cpts[self.choice]
        rate = self.samplingrateHKE[self.choice]
        p = self.samplingperiodHKE[self.choice]

        tpd = spf/16.
        tsample = cps*self.clockperiodHKE
        nlt, ngt, m_60 = estimate60Hz(tpd, tsample)

        toprint = OrderedDict([
            ('Frame Period (s)', self.period),
            ('Sampling Rate (Hz)', rate),
            ('Cycles per Sample', cps),
            ('Samples per Frame', spf),
            ('Ticks per Demod', tpd),
            ('Num 60 Hz Periods', m_60),
            ('GAP_TICKS', ngt),
            ('SUM_TICKS', nlt)])

        print "\nTREAD PARAMETERS (for tread.h)"
        print '-'*80
        for name, value in toprint.items():
            print "{0:<20}{1:^3}{2:<20}".format(name, '=', value)

        print

    def pmaster(self):
        spf = self.spfs[self.choice]
        cps = self.cpts[self.choice]
        rate = self.samplingrateHKE[self.choice]
        p = self.samplingperiodHKE[self.choice]

        toprint = OrderedDict([
        #  None!
        ])

        print "\nPMASTER PARAMETERS (for pmaster.h)"
        print '-'*80
        if not toprint:
            print "None!"
        for name, value in toprint.items():
            print "{0:<20}{1:^3}{2:<20}".format(name, '=', value)

        print

    def analogin(self):
        spf = self.spfs[self.choice]
        cps = self.cpts[self.choice]
        rate = self.samplingrateHKE[self.choice]
        p = self.samplingperiodHKE[self.choice]

        tpd = spf/32.
        nstlog2 = floor(log2(tpd - 1))
        nst = 2**nstlog2

        toprint = OrderedDict([
            ('Frame Period (s)', self.period),
            ('Sampling Rate (Hz)', rate),
            ('Channels', 32),
            ('Samples per Frame', spf),
            ('N_PERIOD_TICKS', tpd),
            ('N_SAMPLE_TICKS', nst),
            ('N_SAMPLE_TICKS_LOG2', nstlog2)])

        print "\nANALOGIN PARAMETERS (for analogin.h)"
        print '-'*80
        if not toprint:
            print "None!"
        for name, value in toprint.items():
            print "{0:<20}{1:^3}{2:<20}".format(name, '=', value)

        print

    def dspid(self):
        spf = self.spfs[self.choice]
        cps = self.cpts[self.choice]
        rate = self.samplingrateHKE[self.choice]
        p = self.samplingperiodHKE[self.choice]

        tpd = spf/8.
        tsample = cps*self.clockperiodHKE
        # N_GAP >= 16 because of a switch block in DSPID code, would
        # need to update code if wanted N_GAP < 16
        nlt, ngt, m_60 = estimate60Hz(tpd, tsample, n_gap_min=16)

        toprint = OrderedDict([
            ('Frame Period (s)', self.period),
            ('Sampling Rate (Hz)', rate),
            ('Cycles per Sample', cps),
            ('Samples per Frame', spf),
            ('Ticks per Demod', tpd),
            ('Num 60 Hz Periods', m_60),
            ('GAP_TICKS', ngt),
            ('SUM_TICKS', nlt)])

        print "\nDSPID PARAMETERS (for dspid.h)"
        print '-'*80
        if not toprint:
            print "None!"
        for name, value in toprint.items():
            print "{0:<20}{1:^3}{2:<20}".format(name, '=', value)

        print

if __name__ == '__main__':
    hc = HKEchoices(Mubcdivisor, Fubc_clk, Fhke_clk)
    hc.user_choose()