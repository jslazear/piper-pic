#include <p30F5011.h>
#include <stdio.h>

#include "tread.h"


// Local functions for the interrupt handler
//static void rotate_cal_settings( void );
static void update_spi_and_sum();
static void update_chopper();
static void update_frame(); // writes a new reading (if we have one) to our data frame
static void update_tmux(); // increments to next tmux channel if it is time

// Local variables for the interrupt handler
static long sum = 0; // The accumulator for demodulating thermometer readings
static unsigned char frame_counter = 0; // Which frame are we on?, a frame holds 16 readings and takes 1 seconds
static unsigned int tick = 0; // Increments on each interrupt, resets after reading each channel (~250)
static volatile unsigned char demod_counter = 0; // Counts which reading we're doing within the current frame (from 0 to 15)
// In standard operation, this is the same as TMux setting
// tagged 'volatile' because referenced by 'set_tmux_auto' function that is called by process_cmd()

// Variables used to communicate with the outside world
static volatile unsigned char chopper_on = 1; // default: chopper is on
static volatile unsigned char data_on = 1; // default: data stream is on
static volatile unsigned char tmux_auto = 1; // default: auto cycling through channels is on
static volatile unsigned char tmux_setting = 0; // default: tmux set to channel 0
static volatile unsigned int sample_counter = 0; // Increments on every ADC sample (for ADC noise test only)
static volatile unsigned int adac_setting[16], gdac_setting[16], adc_value = 0;
static volatile long demod[16];
static volatile unsigned int denable_mask = 0xFFFF; // 16-bit number determines if diode channels are on/off (1 = on, 0 = off), default = all on

/*
 * Interrupt handler, called at 4 kHz, on the *falling* edge of the convert pulse
 */
void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void) {
    // 20090720 - Int handler measured to take 10 us normally, with some about 25 us
    // with data on, takes 13 us, so not much more. ~10% duty cycle  (interrupt rate is 4 kHZ, or 250 us per interrupt)
    PIN_OSCOPE2 = 1;

    IFS0bits.OC2IF = 0; // reset Output Compare 2 interrupt flag

    // Check if a new frame has started by reading in PIN_FRAME_CLK
    char isNewFrame = check_frame_clock();

    // If we have just started a new frame, then reset all our state variables (happens once per second)
    // This guarantees that we stay in sync with the rest of the backplane, (even if a cosmic ray corrupted tick)
    if (isNewFrame) {
        demod_counter = 0;
        tick = 0;
        // Note: do not need to reset sum or demod, since they are reset on each reading
        // tick and demod_counter are also reset in other places in the code, but doing it here
        // *forces* them to stay in *phase* with the external frame_clk pin (generated by PMaster)
    }

    update_chopper(); // Set the thermometer chop clock high or low depending on tick
    update_spi_and_sum(); // Reads in the ADC, writes to the DACs, sums up ADC readings into a demod value
    update_frame(); // writes a new reading (if we have one) to our data frame
    update_tmux(); // sets tmux to next channel (**must** call after update_frame() )
    update_communication(isNewFrame); // Update address counters and transmit buffer in our time slot (does not look at tick)


    // Increment the tick counter
    tick++;
    if (tick >= DEMOD_TICKS) { // Happens 16 times per frame
        tick = 0;
    }

    PIN_OSCOPE2 = 0;
}

/*
 * Chop the sample high/low at the appropriate time
 */
static void update_chopper() {
    //----> Chop the chop clock and blink the LED
    if (chopper_on) {
        if (tick == 0) {
            PIN_CHOP_CLK_PIC = 0;
            PIN_LED = 0;
        }
        if (tick == CHOP_TICKS) {
            PIN_CHOP_CLK_PIC = 1;
            PIN_LED = 1;
        }
    }
}

//---------------------------------------
// Read / Write to the SPI port
// Add the newest ADC reading to the global "sum" variable
// Called once per tick
//---------------------------------------

static void update_spi_and_sum() {
    unsigned int data = 0xFFFF;

    // Update the Gain DAC on tick 0
    // Update the Amplitude DAC on tick 1
    // For TRead, each of the 16 channels has its own GDac and ADac setting
    // Just read in the ADC on all other ticks
    if (tick == 0) {
        PIN_GDAC_LD = 0; // Chip select the Gain DAC
        data = gdac_setting[tmux_setting];
    }
    if (tick == 1) {
        PIN_ADAC_LD = 0; // Chip select the DAC
        data = adac_setting[tmux_setting];
    }

    SPI2STATbits.SPIROV = 0; // Clear SPI receive overflow bit
    SPI2BUF = data; // Write to the SPI port
    while (!SPI2STATbits.SPIRBF); // Wait for data to be ready on SPI port
    data = SPI2BUF; // Read in data from ADC converter

    adc_value = data;
    sample_counter++; // We have one more ADC sample,
    // Used by adc_noise_test() for timing

    if (tick == 0) {
        PIN_GDAC_LD = 1; // Load the DAC
    }
    if (tick == 1) {
        PIN_ADAC_LD = 1; // Load the DAC
    }

    //---------------------------------------
    // Do the demodulation
    //---------------------------------------
    unsigned char mode = get_cur_mode();
    if (mode == MODE_DIODE) // Diode mode does not demodulate, it just sums!
    {
        if (tick >= GAP_TICKS + GAP_TICKS && tick < DEMOD_TICKS)
            sum += adc_value;
    } else // Standard, High R, and Low R modes demodulate!
    {
        if (tick >= GAP_TICKS && tick < CHOP_TICKS) {
            sum += adc_value;
        } else if (tick >= CHOP_TICKS + GAP_TICKS && tick < DEMOD_TICKS) {
            sum -= adc_value;
        }
    }
}

static void update_frame() {
    // Check if we are at the end of a DEMOD cycle (i.e., we just finished a complete reading on a channel)
    if (tick == DEMOD_TICKS - 1) { // tick == TICKS_PER_DEMOD-1 on the last ADC sample for this reading

        // Write frame buffer with frame counter, tmux & status bits before writing data
        if (demod_counter == 0) {
            fb_put8(frame_counter); // Save the frame counter as 1st entry in new frame buffer

            // Save the tmux setting
            if (tmux_auto)
                fb_putc('@');
            else
                fb_put4(tmux_setting);

            // Save the current mode of the board
            unsigned char status;
            status = get_cur_mode();
            fb_put4(status);
        }

        demod[tmux_setting] = sum;
        sum = 0;

        // Save the just completed reading in the frame buffer
        fb_put32(demod[tmux_setting]); // Save our most recent demod value in the frame buffer
        fb_put16(adac_setting[tmux_setting]); // save the Amplitude DAC setting that was used for this reading
        fb_put16(gdac_setting[tmux_setting]); // save the Gain DAC setting that was used for this reading


        // Check if we just did the last demod for this data frame
        demod_counter++;
        if (demod_counter >= DEMODS_PER_FRAME) {
            //rotate_cal_settings();

            // Write out remaining info to the frame
            fb_put8(SUM_TICKS);

            // This buffer is full, so swap to new one and queue up the old one for sending
            swap_active_frame_buf(); // Swap which frame buffer we're writing to
            if (data_on)
                enqueue_last_frame_buf();

            // Update frame_counter
            frame_counter++;

            // Reset the number of readings in this frame
            demod_counter = 0;
        }
    }
}

static void update_tmux() {
    static unsigned int denable_cur;

    // Set the mux hardware to the correct channel
    if (tick == 0) {
        if (demod_counter == 0) {
            denable_cur = denable_mask;
        }

        if (denable_cur & 0x1)
            write_tmux(tmux_setting); // Will set tmux and enable
        else
            disable_tmux(); // Disable mux entirely

        // Shift over our mask so we'll work on the next channel next iteration
        denable_cur >>= 1;
    }

    // tick == TICKS_PER_DEMOD-1 on the last ADC sample for this reading
    if (tick == DEMOD_TICKS - 1) {
        // tmux "auto" means cycle through the available channels
        if (tmux_auto) {
            tmux_setting = demod_counter;
        }
    }
}



/*
 * Change ADAC/GDAC settings for cal resistors using the frame counter
 * So on frame 0, cal resistor gain and excitation are set like Channel 0
 * So on frame 1, cal resistor gain and excitation are set like Channel 1...
 *
 * Only happens when tmux_auto == 1
 *
 * Currently, this function is NOT used (LNL 20110908)
 */

/*static void rotate_cal_settings( void )
{
        // Rotate adac/gdac settings on cal channels each frame
        unsigned char ch = frame_counter&0xf;
        unsigned int adac = adac_setting[ch];
        unsigned int gdac = gdac_setting[ch];
        adac_setting[CAL_CHANNEL_01] = adac;
        adac_setting[CAL_CHANNEL_02] = adac;
        adac_setting[CAL_CHANNEL_03] = adac;
        adac_setting[CAL_CHANNEL_04] = adac;
        gdac_setting[CAL_CHANNEL_01] = gdac;
        gdac_setting[CAL_CHANNEL_02] = gdac;
        gdac_setting[CAL_CHANNEL_03] = gdac;
        gdac_setting[CAL_CHANNEL_04] = gdac;
}
 */
void set_tmux(unsigned short ch) {
    tmux_setting = ch;
}

unsigned char get_tmux(void) {
    return tmux_setting;
}

void set_chop(int state) {
    if (state == 0) {
        chopper_on = 0; // Turn off the auto chopper
        PIN_CHOP_CLK_PIC = 0; // Set low
    } else if (state == 1) {
        chopper_on = 0; // Turn off the auto chopper
        PIN_CHOP_CLK_PIC = 1; // Set low
    } else
        chopper_on = 1; // Enable the auto chopper (default mode)
}

long read_demod(unsigned short ch) {
    if (ch >= 16)
        return -1;
    return demod[ch];
}

void write_gain_dac(unsigned int val, unsigned short ch) {
    if (ch >= 16)
        return;
    else
        gdac_setting[ch] = val;
}

void write_amp_dac(unsigned int val, unsigned short ch) {
    if (ch >= 16)
        return;
    else
        adac_setting[ch] = val;
}

unsigned int read_gain_dac(unsigned short ch) {
    if (ch >= 16)
        return -1;
    return gdac_setting[ch];
}

unsigned int read_amp_dac(unsigned short ch) {
    if (ch >= 16)
        return -1;
    return adac_setting[ch];
}

short read_adc(void) {
    return adc_value;
}

void set_data_frame(unsigned char state) {
    data_on = state;
}

unsigned char data_frame_on(void) {
    return data_on;
}

void set_tmux_auto(unsigned char state) {
    tmux_auto = state;
    // sync to demod_counter, so tmux channels are reported correctly to the frame
    if (tmux_auto)
        tmux_setting = demod_counter;
}

unsigned char tmux_auto_on(void) {
    return tmux_auto;
}


// Used by ADC Noise Test

unsigned int get_sample_count(void) {
    return sample_counter;
}

// Used by ADC Noise Test

void zero_sample_count(void) {
    sample_counter = 0;
}

// Used by DENABLE (enable/disable diode channels)

unsigned int get_denable_mask(void) {
    return denable_mask;
}

void set_denable_mask(unsigned m) {
    denable_mask = m;
}
