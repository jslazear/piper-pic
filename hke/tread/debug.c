/*
 * Original, simple implementation of basic hardware functions
 */

#include <p30F5011.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>

#include "tread.h"
#include "../common/common.h"



// #define DONT_USE_THESE 1

#ifdef DONT_USE_THESE

/*
 * Set the Amplifier GAIN DAC (12 bit DAC)
 */
void write_gain_dac( unsigned short val )
{
// val |= 0xF000;			//%^%^$^%^^%#@$@#!@$#! Set top bits high so PIC will send????
 PIN_GDAC_LD = 0;			// Chip select the DAC
 
 SPI2STATbits.SPIROV = 0;	// clear receive overflow bit

 WriteSPI2( val );			// write value to SPI port (all 16 bits!)
							// Transmits MS bit first in time
 while( !DataRdySPI2() );	// Wait for data to be ready on SPI port 


 PIN_GDAC_LD = 1;			// Load the DAC

 ReadSPI2();	// read the data from SPI transfer and discard
				// clears data ready bit flag
}

/*
 * Set the Excitation Amplitude DAC (12 bit DAC)
 */
void write_amp_dac( unsigned short val )
{
// val |= 0xF000;			//%^%^$^%^^%#@$@#!@$#! Set top bits high so PIC will send????
 PIN_ADAC_LD = 0;			// Chip select the DAC
 WriteSPI2( val );			// write value to SPI port (all 16 bits!)
							// Transmits MS bit first in time

 while( !DataRdySPI2() );	// Wait for data to be ready on SPI port 
 PIN_ADAC_LD = 1;			// Load the DAC

 ReadSPI2();	// read the data from SPI transfer and discard
				// clears data ready bit flag
}

short read_adc( void )
{
	// Long convert pulses indicate that we don't want a "busy" indicator
	// on the SPO pin of AD7685.  First SCLK pulse will get valid data
	 PIN_CONVERT = 1;
	 __delay32( 200 );;	// 200 cycles = 10 us (Tconv max = 2.2 us)
	 PIN_CONVERT = 0;	// Our instruction cycle is 50 nsec

	SPI2STATbits.SPIROV = 0;	// clear receive overflow bit
	WriteSPI2(0xFFFF);	// Dummy write
	while( !DataRdySPI2() );	// Wait for data to be ready on SPI port 
	return ReadSPI2();	// read the data and return it
}

#endif
