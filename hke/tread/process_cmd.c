#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "tread.h"
#include "version.h"        // Computer-generated version string (includes compile time and user name)


// Local function prototypes
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_adac( void );	// set thermometer excitation amplitude DAC
static char do_cmd_gdac( void );	// set thermometer gain DAC
static char do_cmd_gain( void );	// set thermometer gain
static char do_cmd_tmux( void );    // set which thermometer to read
static char do_cmd_adc( void );    	// read the ADC
static char do_cmd_demod( void );  	// read the demodulated thermometer signal
static char do_cmd_chop( void );  	// set the thermometer excitation low/high/auto
static char do_cmd_r( void );		// calculate resistance
static char do_cmd_addr( void );	// read hex address
static char do_cmd_ina( void );		// set current in nA
static char do_cmd_iua( void );		// set current in uA
static char do_cmd_data( void );	// turn data stream on or off
static char do_cmd_p( void );		// prints dac status
static char do_cmd_cmode( void );	// reports the current mode of the board
static char do_cmd_echo( void );	// changes the echo mode of the board
static char do_cmd_version( void );	// prints version of the code
static char do_cmd_denable( void );	// enables or disables diode channels

static char do_cmd_save( void );	// Saves settings to non-volatile EEPROM memory
static char do_cmd_load( void );	// Load settings from non-volatile EEPROM memory
static char do_cmd_eedump( void );	// dumps EE memory where settings are stored

static void print_diode_line (int i, char* str);
static void print_standard_line (int i, char* str);


#define CMP	!strcmp

/*
 * Process a user command contained in the command buffer.
 */
void process_cmd( char *cmd_string )
{
	char *s;
	unsigned char r;
	
    // Parse command string and check address to see if it's for us
    s = bp_parse_cmd_string(cmd_string);
    if( s == NULL )     // Command is not for us
        return;
	
	if( CMP( s, "led" ))		r = do_cmd_led();		// set the LED
	else if( CMP( s, "adac" ))	r = do_cmd_adac();		// set the amplitude DAC
	else if( CMP( s, "gdac" ))	r = do_cmd_gdac();		// set the gain DAC
	else if( CMP( s, "gain" ))	r = do_cmd_gain();		// set the gain
	else if( CMP( s, "tmux" ))	r = do_cmd_tmux();		// set which thermometer to read
	else if( CMP( s, "a" ))		r = do_cmd_adc();		// read the ADC
	else if( CMP( s, "adc" ))	r = do_cmd_adc();		// read the ADC
	else if( CMP( s, "d" ))		r = do_cmd_demod();		// read the demodulated value
	else if( CMP( s, "chop" ))	r = do_cmd_chop();		// set chopper state
	else if( CMP( s, "r" ))		r = do_cmd_r();			// read resistance
	else if( CMP( s, "addr" ))	r = do_cmd_addr();		// read in hex address
	else if( CMP( s, "ina" ))	r = do_cmd_ina();		// set current in nA
	else if( CMP( s, "iua" ))	r = do_cmd_iua();		// set current in uA

	else if( CMP( s, "echo" ))	r = do_cmd_echo();		// Sets echo on or off
	else if( CMP( s, "ver"))	r = do_cmd_version();	// prints software version
    else if( CMP( s, "data" ))	r = do_cmd_data();		// turn data stream on or off
	else if( CMP( s, "p" ))		r = do_cmd_p();			// prints dac status
    else if( CMP( s, "save" ))	r = do_cmd_save();		// Saves settings to non-volatile EEPROM memory
    else if( CMP( s, "load" ))	r = do_cmd_load();		// Loads settings from non-volatile EEPROM memory
    else if( CMP( s, "eedump")) r = do_cmd_eedump();	// dumps EE memory where settings are stored

	else if( CMP( s, "cmode" )) r = do_cmd_cmode();		// reports the current mode of the board
	else if( CMP( s, "denable"))r = do_cmd_denable();	// enables or disables diode channels
	else 						r = STATUS_FAIL_CMD;	// User typed unknown command

	// Print out the status, usually OK!
	print_status( r );
}

/*-----------------------------------------------------------
 * Excitation and Gain Commands
 *-------------------------------------------------------------*/

/** INA
 *	Sets current through specified channels in nanoamps.
 *
 *	Usage:
 *	ina 10 11 12 1000	-> Sets current to 1000 nA in channels 10, 11, 12
 */
static char do_cmd_ina( void )
{
	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 2);    // -2 for cmd + value
	if (ch_mask==0)
		return STATUS_FAIL_INVALID;
	if( !tok_valid_uint( 10 ) )
		return STATUS_FAIL_INVALID;
	unsigned long ina = (unsigned long)tok_get_int32();
	unsigned long max = get_max_current();
	if (ina > max)
		return STATUS_FAIL_RANGE;
	int i;
	for (i = 0; i < 16; i++) {
		if ( (ch_mask >> i) & 0x1 )
			set_ina( ina, i );
	}
	return STATUS_OK;
}

/** IUA
 *	Sets current through specified channels in microamps.
 *
 *	Usage:
 *	ina 10 11 12 500	-> Sets current to 500 uA in channels 10, 11, 12
 */
static char do_cmd_iua( void )
{
	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 2);        // -2 for cmd + value
	if (ch_mask==0)
		return STATUS_FAIL_INVALID;
	if( !tok_valid_uint( 7 ) )
		return STATUS_FAIL_INVALID;
	unsigned long iua = (unsigned long)tok_get_int32();
	unsigned long max = get_max_current();
	if (iua*1000 > max)
		return STATUS_FAIL_RANGE;
	int i;
	for (i = 0; i < 16; i++) {
		if ( (ch_mask >> i) & 0x1 )
			set_ina( iua*1000, i );
	}
	return STATUS_OK;
}


/** GAIN
 *	Sets the second stage amplifier gain.
 *  	Available gains are 1, 2, 4, 8, ... 128.
 *
 *	Usage:
 *	gain 12 2	-> Sets gain on channel 12 to 2, GDAC to 32768
 */
static char do_cmd_gain( void )	
{
	unsigned char mode = get_cur_mode();

	// GAIN cmd not supported in diode mode
	if(mode == MODE_DIODE)
	{
		TTYPuts("Command not supported in diode mode");
		return STATUS_FAIL_INVALID;
	}

	// Get channels
	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 2);     // -2 for cmd + value
	
	if (ch_mask == 0)
		return STATUS_FAIL_INVALID;

	// Get value of gain to set
	if( !tok_valid_uint( 3 ) )
		return STATUS_FAIL_INVALID;

	unsigned short u16 = (unsigned short)tok_get_int16();
	unsigned int gdac = 0;
	switch (u16) {
		case 1: gdac = 65535; break;
		case 2: gdac = 32768; break;
		case 4: gdac = 16384; break;
		case 8: gdac = 8192; break;
		case 16: gdac = 4096; break;
		case 32: gdac = 2048; break;
		case 64: gdac = 1024; break;
		case 128: gdac = 512; break;
		default: gdac = 0;
	}
	
	// Set gain for each channel
	int i;
	for (i=0; i<16; i++) {
		if ( (ch_mask >> i) & 0x1 )
			write_gain_dac( gdac, i );
	}
	
	return STATUS_OK;
}

/*-----------------------------------------------------------
 * Low Level Excitation and Gain Commands
 *-------------------------------------------------------------*/

/** ADAC
 *	Set thermometer excitation amplitude DAC.
 *
 * Usage:
 * 	adac ch_list      
 *	adac 12 1024	-> Sets channel 12 ADAC to 1024
 */
static char do_cmd_adac( void )	
{
	// Get channels
	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 2);        // -2 for cmd + value
	
	if (ch_mask == 0)
		return STATUS_FAIL_INVALID;

	// Get value of ADAC to set
	if( !tok_valid_uint( 5 ) )
		return STATUS_FAIL_INVALID;
	unsigned int u16 = (unsigned int)tok_get_int16();

	// Set channels
	int i;
	for (i=0; i<16; i++) {
		if ( (ch_mask >> i) & 0x1 )
			write_amp_dac( u16, i );
	}
	
	return STATUS_OK;
}

/** GDAC
 *	Set s the 2nd stage amplifier gain in DAC units
 *
 *	Usage:
 *	gdac 12 1024	-> Sets the GDAC for channel 12 to 1024
 */
static char do_cmd_gdac( void )	
{
	// Get channels
	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 2);        // -2 for cmd + value
	
	if (ch_mask == 0)
		return STATUS_FAIL_INVALID;

	// Get value of GDAC to set
	if( !tok_valid_uint( 5 ) )
		return STATUS_FAIL_INVALID;
	unsigned int u16 = (unsigned long)tok_get_int16();

	// Set GDAC in channels
	int i;
	for (i=0; i<16; i++) {
		if ( (ch_mask >> i) & 0x1 )
			write_gain_dac( u16, i );
	}
	return STATUS_OK;
}


/*-----------------------------------------------------------
 * Diode Mode Commands
 *-------------------------------------------------------------*/

/** DENABLE
 *	Enables or disables the diode channels.
 *
 *	When a diode channel is disabled, the output MUX is disabled during its readout interval
 *	so that no excitation current flows through the diode.
 *
 * Usage:
 *	denable 10 11 12 on	-> enables channels 10, 11, & 12
 *	denable 1 2 3 off 	-> disables channels 1, 2, & 3
 *	denable all on		-> enables all channels
 *	denable all off		-> disables all channels
 *
 * Notes:
 *    	In diode mode, the standard excitation current commands, (i.e. INA) do not function.
 *    	This is because a dedicated, DC 10 uA current source is used instead of the standard
 * 	16 Hz AC square wave excitation.
 */
static char do_cmd_denable( void )
{
	// Get channel list
	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 2);        // -2 for cmd + value

	if (ch_mask==0)
		return STATUS_FAIL_INVALID;

	unsigned denable_mask = get_denable_mask();

	char *arg = tok_get_str();
	if( CMP( arg, "on" ) )  
	{
		set_denable_mask( denable_mask | ch_mask );
	}
	else if( CMP( arg, "off" ) )  
	{
		set_denable_mask( denable_mask & ~ch_mask );
	}
	else
		return STATUS_FAIL_INVALID;

	return STATUS_OK;
}


/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/

/** DATA
 *	Turns on/off data stream.
 *
 *	Usage:
 *	data on		-> Turns data stream on
 *	data off	-> Turns data stream off
 */
static char do_cmd_data( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		set_data_frame(1); // turn data stream on
	else if( CMP( arg, "off" ) )
		set_data_frame(0); // turn data stream off
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}


/** P
 *	Prints a table of data for specified channels:
 *	GDAC, ADAC, Demod, Delta, InA, r.
 *
 *	Usage:
 *	p			-> Prints all channels
 *	p 10			-> Prints data for channel 10
 *	p 10 11 12	-> Prints data for channel 10, 11, 12
 *	p cal		-> Prints calibration resistor data
 */
static char do_cmd_p( void )
{
	char str[100];	// max string len is about 52 chars, so 100 is very safe

	sprintf( str, "TREAD (%s)\r\n", get_mode_string() );
	TTYPuts(str);

	unsigned char mode = get_cur_mode();
	unsigned short ch_mask = 0;
	int nargs = tok_get_num_args() - 1;         // -1 for cmd

	if (nargs == 0)
		ch_mask = 0xFFFF;
	else
		ch_mask = parse_channel_list(nargs);

	if (mode == MODE_DIODE)
		TTYPuts("Ch   GDAC      Demod    Delta  Volts\r\n");
	else
		TTYPuts("Channel Gain  ADAC  Demod    Delta      I       r\r\n");
	
	int i = 0;
	for (i = 0; i < 16; i++) 
	{
		if ((ch_mask >> i) & 0x1) 
		{
			if (mode == MODE_DIODE)
				print_diode_line(i,str);
			else
				print_standard_line(i,str);
		}
	}
	return STATUS_OK;
}

static void print_diode_line (int ch, char* str)
{
	long int demod;
	float delta=0.0;
	float v;
	unsigned int gdac = read_gain_dac( ch );

	demod = read_demod( ch );
	delta = ((float)demod)/(SUM_TICKS+SUM_TICKS);
	
	v = calc_diode_volts( delta );

	sprintf( str, "%2i  %5d  %9li  %7.1f  %5.3f\r\n", ch, gdac, demod, delta, v );
	TTYPuts(str);
}

static void print_standard_line (int i, char* str)
{
	unsigned int gdac, adac;
	long int demod;
	long int ina;
	int delta;
	float r;

	gdac = read_gain_dac( i );
	unsigned char gain = 1;
	switch (gdac) {
		case 65535: gain = 1; break;
		case 32768: gain = 2; break;
		case 16384: gain = 4; break;
		case 8192: gain = 8; break;
		case 4096: gain = 16; break;
		case 2048: gain = 32; break;
		case 1024: gain = 64; break;
		case 512: gain = 128; break;
		default: gain = 0;
	}
	adac = read_amp_dac( i );
	demod = read_demod( i );
	r = calc_r_val( i );
	ina = adac_to_ina(adac);
	delta = demod/SUM_TICKS;

	sprintf( str, "%6i%6u%6u%9li%6u", i, gain, adac, demod, delta );
	TTYPuts(str);

	// PRINT CURRENT
	if (ina > 1000) {	// print current in micro amps
		sprintf( str, "%8.2lf%c", ina/1000.0, 'u' );
		TTYPuts(str);
	}
	else {		// Print current in nano-amps
		sprintf( str, "%8li%c", ina, 'n' );
		TTYPuts(str);
	}

	// PRINT R last
	sprintf( str, " %10.3f\r\n", r );
	TTYPuts(str);
}

/** ADDR
 *	Prints hex address of the board.
 *
 *	Usage:	
 *	No arguments
 */
static char do_cmd_addr( void )
{
	unsigned char addr = get_addr();
	char str[16];
	sprintf( str, "%x\r\n", addr );
	TTYPuts( str );
	return STATUS_OK;
}


/** ECHO
 *	Sets echo on or off.
 *
 *	Usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( CMP( arg, "on" ) )
		cmd_set_echo( 1 );
	else if( CMP( arg, "off" ) )
		cmd_set_echo( 0 );
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;

}


/** VER
 *	Spits out a version string for the current code.
 *
 *	Usage:
 *	No arguments
 */
static char do_cmd_version( void )
{
 // Print the board name and mode
 TTYPuts("TREAD (");
 TTYPuts( get_mode_string() );
 TTYPuts( ")  " );

 // Print the standard version string and config name
 bp_print_version_info(VERSION_STRING);

 return STATUS_OK;
}

/** LED
 *	Set the green front panel LED light.
 *
 *	Usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led( void )
{
	char *arg;

	// Check that the next token is a valid number
	if( !tok_available() )
		return STATUS_FAIL_NARGS;

	arg = tok_get_str();
	if( CMP( arg, "on" ) )  {
		PIN_CHOP_CLK_PIC = 1;
		PIN_LED = 1;
	}
	else if( CMP( arg, "off" ) )  {
		PIN_CHOP_CLK_PIC = 0;
		PIN_LED = 0;
	}
	else
		return STATUS_FAIL_INVALID;

	return STATUS_OK;
}

/** SAVE
 *	Saves the board settings to non-volatile EEPROM memory.
 *
 *	Usage:
 *	  No arguments
 */
static char do_cmd_save( void )
{
    save_settings();
	TTYPuts("Settings Saved!\r\n");
    
    return STATUS_OK;
}

/** LOAD
 *	Loads the board settings from non-volatile EEPROM memory.
 *
 *	Usage:
 *	  No arguments
 */
static char do_cmd_load( void )
{
    if( load_settings() )
    	TTYPuts("Settings Loaded!\r\n");
    else
    	TTYPuts("Settings not loaded!\r\n");

    return STATUS_OK;
}

/** EEDUMP
 *	Dumps the EEPROM memory where the board settings are stored.
 */
static char do_cmd_eedump( void )
{
    dump_ee();
	return STATUS_OK;
}


/*-----------------------------------------------------------
 * Test / Diagnostic Commands
 *-------------------------------------------------------------*/

 /** CMODE
 *	Reports the current mode of the board (standard / high res / low res / diode).
 *
 *	Usage:
 *	No arguments
 *  Notes:
 *  This information is also reported by the standard VER command.
 */
static char do_cmd_cmode( void )
{	
	unsigned char mode = get_cur_mode();
	char str[32];
	sprintf(str, "%i %s\r\n", mode, get_mode_string() );
	TTYPuts(str);
	return STATUS_OK;
}

/** ADC
 *	Report current ADC value.
 *
 *	Usage:
 *	adc				-> Prints adc value
 *	adc 10 10000 d	-> Prints every 10th reading for a total of 10000 readings, in decimal format
 *					   (other formats: x=hex, b=binary)
 */
static char do_cmd_adc( void )
{
	int count = tok_get_num_args() - 1;
	if (count == 0) {
		char str[16];
		unsigned short data;
		
		data = read_adc();
		
		sprintf( str, "%u\r\n", data );
		TTYPuts( str );
	} else {
		int nskip;
		long int num;
		char format;
		if (!tok_valid_uint(4))
			return STATUS_FAIL_INVALID;
		nskip = tok_get_int16();
		if (!tok_valid_uint(6))
			return STATUS_FAIL_INVALID;
		num = tok_get_int32();
		char *arg = tok_get_str();
		if ( CMP( arg, "d" ))
			format = 'd';
		else if ( CMP ( arg, "i" ))
			format = 'd';
		else if ( CMP( arg, "x" ))
			format = 'x';
		else if ( CMP( arg, "b" ))
			format = 'b';
		else
			return STATUS_FAIL_INVALID;
		// Check to see if data rate/format is possible, then set parameters of test
		adc_noise_test(nskip, num, format);
	}
	return STATUS_OK;
}

/** D
 *	Prints demod value for channel.
 *
 *	Usage:
 *	demod 12		-> Prints demod value for channel 12
 */
static char do_cmd_demod( void )
{
	unsigned short ch_mask = parse_channel_list(tok_get_num_args() - 1);        // -1 for cmd
	if (ch_mask == 0)
		return STATUS_FAIL_INVALID;
	int i;
	for (i = 0; i < 16; i++) {
		if ( (ch_mask >> i) & 0x1 ) {
			long data = read_demod( i );
			char str[16];
			sprintf( str, "%i: %li\r\n", i, data );
			TTYPuts( str );
		}
	}
	return STATUS_OK;
}


/** R
 *	Calculates resistance values for channels.
 *
 *	Usage:
 *	r cal	-> Prints values of r for channels 12,13,14,15
 *	r 12	-> Prints value of r for channel 12
 */
static char do_cmd_r( void )
{
	// Get channels
	unsigned short ch_mask = parse_channel_list(tok_get_num_args()-1);
	if (ch_mask == 0)
		return STATUS_FAIL_INVALID;
	
	int i;
	for (i = 0; i < 16; i++) {
		if ( (ch_mask >> i) & 0x1 ) {
			float r = calc_r_val( i );
			char str[20];	// 0: 1007.431519\r\n\0 is 17 characters
			sprintf( str, "%i: %f\r\n", i, r );
			TTYPuts( str );
		}
	}


	return STATUS_OK;
}

/** CHOP
 *	Sets chopper state.
 *
 *	Usage:
 *	chop low	-> Sets chop to low
 *	chop high	-> Sets chop to high
 *	chop auto	-> Sets chop to auto
 */
static char do_cmd_chop( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	arg = tok_get_str();
	if( CMP( arg, "low" ) )
		set_chop( 0 );
	else if( CMP( arg, "high" ) )
		set_chop( 1 );
	else if( CMP( arg, "auto" ) )
		set_chop( 2 );
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/** TMUX
 *	Set TMUX channel.
 *
 *	Usage:
 *	tmux 2	-> Sets tmux to channel 2
 *	tmux auto	-> Sets tmux to cycle through channels (this is the default setting)
 */
static char do_cmd_tmux( void )	
{	
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	if( !tok_valid_uint( 2 ) ) {
		char *arg;
		arg = tok_get_str();
		if( CMP( arg, "auto" ) ) {
			// auto cycle through channels
			set_tmux_auto(1);
		} else 
			return STATUS_FAIL_INVALID;
		return STATUS_OK;
	}
	set_tmux_auto(0);
	
	unsigned short u16;
	u16 = (unsigned short)tok_get_int16();
	write_tmux( u16 );		// Write tmux directly to hardware (not really needed)
	set_tmux( u16 );		// Tell int_handler() to set TMUX for us
	
	return STATUS_OK;
}

