#ifndef __TREAD_INCLUDED
#define __TREAD_INCLUDED

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

// Timing information and configurations
/*     |----------------------|                        |------------
       |                      |                        |
       |                      |                        |
 CHOP  |                      |                        |
       |                      |                        |
       |                      |------------------------|

     +1     |-----------------|                             |------
            |                 |                             |
 Demod      |                 |                             |
      0 ----|                 |----|                   |-----
                                   |                   |
     -1                            |-------------------|

       |----|-----------------|----|-------------------|----|
       0    A                 B    C                   D

	A = GAP_TICKS
	B = GAP_TICKS + SUM_TICKS
        C = 2*GAP_TICKS + SUM_TICKS
	D = DEMOD_TICKS  (2*GAP_TICKS + 2*SUM_TICKS)

        DEMOD_TICKS = Number of samples for a full demod reading (0 to D)
        DEMODS_PER_FRAME = Number of readings per frame
                   Number of samples per frame = (TICKS_PER_DEMOD)*(DEMODS_PER_FRAME)
        SUM_TICKS = Number of samples over which the demod value is summed (A to B)
                   per half cycle.  So a full demod reading actually contains 2*N_SUM samples
        GAP_TICKS = Number of samples discarded while response settles
        CHOP_TICKS = SUM_TICKS + GAP_TICKS (0 to B)
 */

// Constants used for timing/demodulation. Refer to timing diagram / definitions above
// Constants generated using timings.py script, optimizing to exclude 60 Hz noise.
// For flight, could change to a longer N_LEN for less noise

#define DEMODS_PER_FRAME             16 // Number of thermometer readings in 1 frame
                                        // Not necessarily the number of distinct channels read

#ifdef PIPER
  #define SUM_TICKS                  66
  #define GAP_TICKS                  59
#endif

#ifdef BETTII
  #define SUM_TICKS                 108
  #define GAP_TICKS                  20
#endif

#define CHOP_TICKS                (GAP_TICKS + SUM_TICKS)
#define DEMOD_TICKS               (2*CHOP_TICKS) // Full demod reading period (0 to D)

// Sanity checks on the timing constants, may catch some dumb errors
#if (DEMOD_TICKS*DEMODS_PER_FRAME) != TICKS_PER_FRAME
    #error "Invalid timing constant definitions"
#endif

// Constants
#define NCHANNELS               16
#define R52_DIVIDE_FACTOR		72.5	// Divide ratio of therm. excitation before ADAC	


#define DIODE_MODE_GDAC_GAIN	5	// set GDAC gain to 10 for diode mode
#define DIODE_MODE_GDAC_SETTING	(65536/DIODE_MODE_GDAC_GAIN)

// Definitions
#define CAL_CHANNEL_01          12
#define CAL_CHANNEL_02          13
#define CAL_CHANNEL_03          14
#define CAL_CHANNEL_04          15

//-----> OUTPUT PINS
#define PIN_LED                 LATCbits.LATC1	// Front pannel LED

#define PIN_TMUX_A0             LATBbits.LATB8	// Thermometer MUX address bits
#define PIN_TMUX_A1             LATBbits.LATB9
#define PIN_TMUX_A2             LATBbits.LATB10
#define PIN_TMUX_EN1            LATBbits.LATB11
#define PIN_TMUX_EN2            LATBbits.LATB12

#define PIN_GDAC_LD             LATBbits.LATB14	// DAC load/enable pins
#define PIN_ADAC_LD             LATBbits.LATB15

#define PIN_CONVERT             LATDbits.LATD1	// ADC convert pin
#define PIN_485_ENABLE          LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

#define PIN_CHOP_CLK_PIC        LATDbits.LATD8	// Pin to chop therm. excitation

#define PIN_OSCOPE1             LATDbits.LATD4	// Debug pins for looking at with scope
#define PIN_OSCOPE2             LATDbits.LATD9


//------> INPUT PINS
#define PIN_ADDR0               PORTFbits.RF1
#define PIN_ADDR1               PORTGbits.RG1
#define PIN_ADDR2               PORTGbits.RG0
#define PIN_ADDR3               PORTFbits.RF0
#define PIN_FRAME_CLK		PORTDbits.RD2
#define PIN_JP5_1_MODE		PORTGbits.RG14	// Set's board mode (low-res, standard, high-res)
#define PIN_JP5_2_MODE		PORTGbits.RG12	// Set's board mode (low-res, standard, high-res)
#define PIN_JP5_3_MODE		PORTGbits.RG13	// Set's board mode (low-res, standard, high-res)

// Possible modes for TRead
#define MODE_STANDARD           0
#define MODE_HIGH_RES           1
#define MODE_LOW_RES            2
#define MODE_DIODE              3




//---------> Function Prototypes
// TREAD.C
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// int_handler.c
void write_amp_dac( unsigned int val, unsigned short ch );
void write_gain_dac( unsigned int val, unsigned short ch );

unsigned int read_gain_dac( unsigned short ch );
unsigned int read_amp_dac( unsigned short ch );

void set_tmux( unsigned short ch );
short read_adc( void );
long read_demod( unsigned short ch );
void set_data_frame( unsigned char state );
unsigned char data_frame_on( void );
void set_tmux_auto( unsigned char state );
unsigned char tmux_auto_on( void );
void set_chop( int state );
unsigned char get_tmux( void );
unsigned int get_sample_count( void );
void zero_sample_count( void );
unsigned int get_denable_mask( void );
void set_denable_mask ( unsigned m );

// process_cmd.c
void process_cmd( char *str ) ;

//do_cmd.c
void write_tmux( unsigned short ch );
void disable_tmux( void );

unsigned char get_addr( void );
void set_ina( unsigned long ina, unsigned short ch );
void auto_set_gain( unsigned char ch );
void adc_noise_test(int skip, long int n, char f);

// mode.c
void init_mode_params( void );
void set_initial_excitation( void );
char * get_mode_string();

unsigned char get_cur_mode( void );
unsigned long get_max_current( void );
unsigned int ina_to_adac( unsigned long ina );
unsigned long adac_to_ina( unsigned int adac );
float calc_r_val( unsigned short ch );
float calc_diode_volts ( float delta );

// save_settings.c
void save_settings(void);
int load_settings( void );
void dump_ee(void);

#endif	// __TREAD_INCLUDED
