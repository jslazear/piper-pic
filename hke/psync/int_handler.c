#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>

#include "../common/psync_common.h"
#include "psync.h"

// frame count global variable
volatile unsigned long frame_count;


/*
 * Interrupt handler
 */
void __attribute__((__interrupt__, __auto_psv__)) _INT0Interrupt( void ) 
{
 	IFS0bits.INT0IF = 0;		// reset INT0 interrupt flag -- needed or int handler will not work right!!!

	PIN_OSCOPE2 = 1;

	// Read in the UBC frame count (takes 8 us)
	frame_count = psync_read_frame_count();

	PIN_OSCOPE2 = 0;
}


/*----------------------------- 1 kHz Timer -------------------------*/
static volatile unsigned int timer_tick=0;

/* Timer 2 Interrupt Handler.  Called at a rate of 1 kHz
 */
void __attribute__((__interrupt__, __auto_psv__)) _T2Interrupt( void )
{
	IFS0bits.T2IF = 0;		// Clear the interrupt flag
	timer_tick++;
}

void ClearTimer()
{
	timer_tick=0;
}

unsigned int ReadTimer()
{
	return timer_tick;
}
