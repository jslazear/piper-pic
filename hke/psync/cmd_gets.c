#include <p30f5011.h>
#include <uart.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../common/stty.h"

void OnIdle( void );		// Must be defined in another file in the project

static char cmd_echo=0;		// default to echo typed characters

/*
 * Setting e=1 will result in all characters received being re-transmitted.
 * This helps users using "hyperterminal" or such, but is very irritating to computers.
 */
void cmd_set_echo( char e )
{
 cmd_echo = e;
}

/*
 * Return the status of the echo setting.  1 means we will echo all typed characters
 */
char cmd_get_echo( void )
{
 return cmd_echo;
}

/*
 * put a character to the UART, only if echo is turned on
 */
static void cmd_putc( char c )
{
	if( cmd_echo )
		TTYPutc( c );
}


/*
 * Get a string from the user.  Terminate on '\r' or '\n'
 */
void cmd_gets( char *str, int n )
{
 char c;
 unsigned char pos;

 // Print out a prompt
 cmd_putc( '>' );
 cmd_putc( ' ' );

 pos = 0;					// set the position in string to be 0

 while( 1 ) {
	//---> if no one hit a key, then we can do other stuff for a bit
	if( !DataRdyUART2() ) {
		// Here's where we can do non-critical, idle-time tasks
		OnIdle();

		continue;
	}

	//---> Key pressed, so deal with it
	c = getcUART2();		// get the input character
	switch( c ) {
		// terminator
		case '\r':
		case '\n':
		case '\0':
			str[pos] = '\0';
			cmd_putc( '\r' );
			cmd_putc( '\n' );
			return;
		
		// backspace
		case '\b':
			if( pos > 0 ) {
				pos--;
				cmd_putc('\b');
				cmd_putc(' ');
				cmd_putc('\b');
			}
			continue;
		
		// otherwise, add the character to our string
		default:
			if( pos < n-1 ) {
				str[pos] = c;
				pos++;
				cmd_putc( c );		// echo the character
			}
			else
				cmd_putc( '%' );		// buffer is full, ignore character
			continue;
	}
 }
}

