#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "../common/common.h"
#include "../common/stty.h"
#include "psync.h"

/*
 * Spit the frame count out over the port forever
 */
void SpitFrameCounts()
{
	extern volatile unsigned long frame_count;
 	unsigned long last_count = frame_count;

	while( 1 )  {
		if( frame_count != last_count )  {
			last_count = frame_count;

			PrintFrameCount( last_count );

			asm volatile ("pwrsav #1");	// Put CPU in IDLE mode
		}
	}
}

void PrintFrameCount( unsigned long count )
{
	char *hex = "0123456789ABCDEF";	// array to numbers into ascii values in HEX

	// Spit the data out over the port in hex
	// Don't use sprintf, it's too slow (sprintf takes 5 ms to convert one value)			
	TTYPutc(hex[((unsigned char)(count>>28) & 0xF)]);
	TTYPutc(hex[((unsigned char)(count>>24) & 0xF)]);
	TTYPutc(hex[((unsigned char)(count>>20) & 0xF)]);
	TTYPutc(hex[((unsigned char)(count>>16) & 0xF)]);
	TTYPutc(hex[((unsigned char)(count>>12) & 0xF)]);
	TTYPutc(hex[((unsigned char)(count>>8)  & 0xF)]);
	TTYPutc(hex[((unsigned char)(count>>4)  & 0xF)]);
	TTYPutc(hex[((unsigned char)(count)     & 0xF)]);
	TTYPutc('\r');
	TTYPutc('\n');		
	while( BusyUART2() );	// Wait till UART done transmitting
}

