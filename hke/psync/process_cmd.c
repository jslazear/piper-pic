#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "../common/common.h"
#include "../common/stty.h"
#include "psync.h"

// Local function prototypes
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_p( void );		// prints status
static char do_cmd_spit( void );		// spit data forever
static char do_cmd_idle( void );		// 

/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */
void process_cmd( char *cmd )
{
	char *s;
	unsigned char r;
	
	// Parse the command string
	cmd_parse(cmd);

	// Parse the command string
	s = tok_get_str();	// first token is the command name
	
	if( !strcmp( s, "led" ))		
		r = do_cmd_led();		// set the LED
	else if( !strcmp( s, "p" ))
		r = do_cmd_p();
	else if( !strcmp( s, "spit" ))
		r = do_cmd_spit();
	else if( !strcmp( s, "idle" ))
		r = do_cmd_idle();
	else 							
		r = STATUS_FAIL_CMD;	// User typed unknown command
	
	print_status( r );
}

/*----------------------------------------------------------------------
 * PSync Commands
 *----------------------------------------------------------------------*/

/** spit
 * Puts the card in 'spit' mode where it prints out the frame count over the serial port
 * as soon as it arrives from the UBC Sync box.
 * In non-spit mode, you need to query the board (via the 'p' command) for the most recent frame count.
 * The startup mode (spit / query) is selectable via hardware jumper.
 */
static char do_cmd_spit( void )
{
	SpitFrameCounts();

	return STATUS_OK;
}


/*----------------------------------------------------------------------
 * Standard Commands
 *----------------------------------------------------------------------*/

/** LED
 *	Set LED light.
 *
 *	usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led( void )
{
	char *arg;
	
	// Check that the next token is a valid number
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	arg = tok_get_str();
	if( !strcmp( arg, "on" ) )
		PIN_LED = 1;
	else if( !strcmp( arg, "off" ) )
		PIN_LED = 0;
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}



/** P
 * Prints the most recent frame count received.
 */
static char do_cmd_p( void )
{
	extern unsigned long frame_count;	// Set by the interrupt handler

	PrintFrameCount( frame_count );

	return STATUS_OK;
}


/*----------------------------------------------------------------------
 * Diagnostic / Test Commands
 *----------------------------------------------------------------------*/

/** idle
 * Idle the processor to test power savings
 */
static char do_cmd_idle( void )
{
	PIN_LED = 0;

	asm volatile ("pwrsav #1");

	return STATUS_OK;
}


/* This function is really defined in common_functions.c
 * But we can't include that file in this project because it references a bunch of other stuff
 * that we don't need / want for this board.  Should really move print_status to its own
 * file in the /common directory.
 *
 * Print a human readable status message depending on the status code.
 */
void print_status( char status )
{
	switch( status )  {
		case STATUS_OK:
			TTYPuts( "OK" );
			break;
		case STATUS_FAIL_CMD:
			TTYPuts( "FAIL: unknown command" );
			break;
		case STATUS_FAIL_NARGS:
			TTYPuts( "FAIL: incorrect number of arguments" );
			break;
		case STATUS_FAIL_INVALID:
			TTYPuts( "FAIL: invalid argument format" );
			break;
		case STATUS_FAIL_RANGE:
			TTYPuts( "FAIL: argument out of range" );
			break;
		default:
			TTYPuts( "FAIL: unknown error" );
			break;
	}

	// All responses must end with ! to signal parser this is end of response
	TTYPuts( "!\r\n" );
}
