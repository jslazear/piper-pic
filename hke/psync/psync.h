//---------> Some useful stuff
#undef BAUD_RATE	// We are overriding our standard baud rate for this board!!!
#define BAUD_RATE		56000	// This board runs at slower clock speed (save power) so use lower baud rate
#define CLOCKFREQ 10000000L		// 10.000 MHz instruction clock (synthesised from 5 MHz crystal, 8x PLL)
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)) - 1.00 + 0.5 ))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Adds 0.5 to convert from float to integer correctly

//-----> OUTPUT PINS
#define PIN_LED				LATCbits.LATC1	// Front pannel LED
#define PIN_PIC_READY_BAR	LATBbits.LATB5	// Drive low to re-enable frame count capture circuit
#define PIN_FRAME			LATBbits.LATB10	// "Frame" pulse output (one pulse per UBC frame count)

// Diagnostic outputs
#define PIN_OSCOPE1			LATDbits.LATD4	// Debug pins for looking at with scope
#define PIN_OSCOPE2			LATGbits.LATG15
#define PIN_FRAME_ACK		LATBbits.LATB10	// Connected to "Frame" test point, pulsed high when frame received

//------> INPUT PINS
#define PIN_GATE			PORTFbits.RF6	// Clock gate input (also INT0, triggers int_handler)
#define PIN_CAMERA_SHUTTER 	PORTBbits.RB1	// Connected to "Flash Sync" of star camera

#define PIN_JP1_1			PORTDbits.RD3
#define PIN_JP1_2			PORTDbits.RD2
#define PIN_JP1_3			PORTDbits.RD1
#define PIN_JP1_4			PORTDbits.RD0


//---------> Function Prototypes
// psync.c
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// process_cmd.c
void process_cmd( char *cmd );

// do_cmd.c
void SpitFrameCounts( void );
void PrintFrameCount( unsigned long count );

// cmd_gets.c
void cmd_gets( char *str, int n );

// int_handler.c
void ClearTimer();
unsigned int ReadTimer();

