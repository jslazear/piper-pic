import gui
import bindings
import psyncadc


if __name__ == "__main__":
    # Create the objects
    guiobject = gui.PsyncadcApp(0)
    guithread = psyncadc.GUIThread()
    sthread = psyncadc.SerialThread(guithread)

    # Bind the program to the GUI
    bindings = bindings.Bindings(guiobject, sthread, guithread)
    bindings.set_bindings()

    # Enter the event handler loop
    guiobject.MainLoop()


