#include <p30F5011.h>
#include <libpic30.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <math.h>
#include "psync_common.h"
#include "psyncadc.h"
#include "ptty.h"

static void disable_frame_ints();
static void enable_frame_ints();

static unsigned _SAV_PR2;
/*
 * Disable the interrupts having to do with receiving frame pulses from the UBC Sync box
 * and triggering the ADC.
 */
static void disable_frame_ints()
{
	// Turn off Int1 interrupts (from UBC Sync box)
	// important to do this first, in case we were just about to start the next acq sequence
	IEC1bits.INT1IE = 0;

	// Wait for the current acquisition sequence to be over, if we're in one
	while( IsDoingAdcAcqSequence() );

	IEC0bits.OC1IE = 0;		// Turn off output compare module interupts
							// In these routines we take direct control of the 
							// output compare module to drive the ADC convert pin
							// and don't want it to generate any interrupts
	_SAV_PR2 = PR2;			// Save the value of the PR2 register
}

static void enable_frame_ints()
{
	PR2 = _SAV_PR2;			// Restore the value of the PR2 register

	T2CONbits.TON = 0;					// turn off timer2 (INT1 handler will turn on as needed)
	IEC0bits.OC1IE = 1;					// Turn on output compare module interupts (For ADC sweep sequence)	

	init_psync();			// Init psync hardware (will enable INT1)
}

static void wait_for_sample()
{
	//--> Wait for falling edge of convert pulse
	int prev = PIN_CONVERT;
	while( 1 ) {
		int now = PIN_CONVERT;
		if( prev == 1 && now == 0 )
			break;
		prev = now;
	}
}

// Read the ADC directly
static unsigned read_adc_raw()
{
	PIN_OSCOPE1 = 1;
	unsigned data = 0x0000;
	SPI1STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
	SPI1BUF = data;					// Write to the SPI port
	while( !SPI1STATbits.SPIRBF );	// Wait for data to be ready on SPI port
	data = SPI1BUF;					// Read in data from ADC converter
	PIN_OSCOPE1 = 0;

	return data;
}


/*
 * Reads a specified number of samples from the ADC and outputs them to the serial port.
 * For testing only.
 */
void adc_noise_test( int nSamples )
{
	int i;
	long long sum=0;
	long long sum_sq=0;					// long longs are 64 bits (needed to not overflow)
	char str[100];

	// Turn off interrupts
	disable_frame_ints();

	write_mux( 30 );						// Switch to our 4.096V calibration channel
	__delay32( (long)(0.01 * CLOCKFREQ) );	// Wait for filter to settle

	// Setup timer 2 for our sampling rate
	T2CONbits.TON = 0;					// turn off timer2 
	PR2 = 20000;						// 20,000 is 1 khz sampling rate
	TMR2 = 0;
	T2CONbits.TON = 1;					// turn on timer2 

	for( i=0; i<nSamples; ++i )  {
		wait_for_sample();

		//--> ADC Read
		unsigned short data = read_adc_raw();

		//--> Output the data sample over the port (optional)
		// sprintf( str, "%u\r\n", data ); 
		// TTYPuts( str );
		
		//--> Compute statistics
		long lData = (long)(data - 32768);	// Subtract 0 level and convert to long
		sum += lData;
		sum_sq += lData*lData;
	}

	// Compute N^2 * Variance.  Note as long as sum and sum_sq are 64 bits
	// and N < 65536, this won't overflow
	long long nnvar = nSamples*sum_sq - sum*sum;

	// Compute avg and std as floats
	float fN = (float)nSamples;
	float avg = (float)sum / fN;
	float std = sqrt( (float)(nnvar) / (fN*fN) );

	// Debug printout
	// sprintf( str, "sum=%lld, sum_sq=%lld, nnvar=%lld\r\n", sum, sum_sq, nnvar ); 
	// TTYPuts( str );

	// Print out statistics
	sprintf( str, "avg=%f stddev=%f bits\r\n", avg, std ); 
	TTYPuts( str );

	// Reset the timer2 period for normal operation
	enable_frame_ints();
}


/*
 * Sets MUX to GND lets settle, switches to +4.096V, Reads and prints samples from the ADC.
 * For testing only.
 */
#define N_FT_SAMPLES	20
void filter_settle_test()
{
	int i=0;
	char str[100];
/*
	while( 1 )  {
		disable_frame_ints();
		__delay32( (long)(0.02 * CLOCKFREQ) );	// Wait for filter to settle
		
		enable_frame_ints();
		__delay32( (long)(0.02 * CLOCKFREQ) );	// Wait for filter to settle

		++i;
		sprintf( str, "%d\r\n", i ); 
		TTYPuts( str );
	}

*/

	// Turn off interrupts
	disable_frame_ints();

	write_mux( 31 );						// Switch to our 0.00V calibration channel
	__delay32( (long)(0.01 * CLOCKFREQ) );	// Wait for filter to settle

	// Setup timer 3, he will be used just to keep time (one tick per us)
	//OpenTimer3( T3_OFF & T3_IDLE_CON & T3_GATE_OFF & T3_PS_1_1 & T3_SOURCE_INT, 20);

	// Setup timer 2 for our sampling rate (drives CONVERT pin via OC1 hardware)
	T2CONbits.TON = 0;					// turn off timer2 
	PR2 = 200;							// 200 is 100 khz sampling rate, is 10 us per sample
										// 10 us is as fast as we can go with this ADC (4us to convert, 2.5us to readout)
	TMR2 = 0;

	write_mux( 30 );					// Switch to 4.096 Volts
	T2CONbits.TON = 1;					// turn on timer2 (first ADC sample is taken here)
	// T3CONbits.TON = 1;					// start our time keeping timer

	int data[N_FT_SAMPLES];

	// Aquire the data
	for( i=0; i<N_FT_SAMPLES; ++i )  {
		
		wait_for_sample();				// Wait for the 4 us conversion process to complete

		// ADC Read
		unsigned x = read_adc_raw();

		data[i] = (int)(x - 32768U);
	}

	// Print out a header
	sprintf( str, "%8s %8s %8s\r\n", "us", "ADC Val", "Percent" ); 
	TTYPuts( str );

	// Print out the data
	float delta_t = PR2 / 20.0;			// Delta T between samples in us
	float final_value = (float)data[N_FT_SAMPLES-1];

	for( i=0; i<N_FT_SAMPLES; ++i )  {
		float t = delta_t * i;
		float percent = 100.0 * (float)(data[i]) / final_value;
		sprintf( str, "%8.1f %8d %8.1f\r\n", t, data[i], percent ); 
		TTYPuts( str );
	}

	// Compute time constant from the first 2 samples
	float alpha = (float)data[1] / final_value;
	float tau = -delta_t / log( 1.0 - alpha );
	sprintf( str, "Tau from first 2 samples is %4.2f us\r\n", tau ); 
	TTYPuts( str );

	// Reset the timer2 period for normal operation
	enable_frame_ints();
}