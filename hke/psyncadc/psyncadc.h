// Define the version of the board this program is for
//#define PSYNCADC1
#define PSYNCADC2

//---------> Some useful stuff
//#define BAUD_RATE		115200	// Standard baud rate of 115.2 kbs
#define CLOCKFREQ 20000000L		// 20.000 MHz instruction clock (synthesised from 5 MHz crystal, 16x PLL)
//#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)) - 1.00 + 0.5 ))	// BRG (baud rate generator) setting
//								// BRG = FCLK/(BaudRate*16) - 1
//								// Adds 0.5 to convert from float to integer correctly
#define BRG_SETTING 2       // 416 kbs.  This is the fastest rate we can generate with a 20 MHz FCY that is
                            // compatible with the MAC using our FTDI chip USB to Fiber board.
                            // BRG = FCLK/(BaudRate*16) - 1

// Calculate the actual BAUD rate from the specified BRG setting
#define BAUD_RATE           (CLOCKFREQ/(BRG_SETTING+1) >> 4)


#define NUM_ADC_CHANNELS	32

#define CONVERT_PULSE_STOP_TICK 100		// 100 gives a 5 us pulse on the ADC convert pin

//-----> OUTPUT PINS (Common to Version 1 and Version 2)
#define PIN_A0			LATBbits.LATB11	// ADC input mux
#define PIN_A1			LATBbits.LATB12	// ADC input mux
#define PIN_A2			LATBbits.LATB13	// ADC input mux
#define PIN_A3			LATBbits.LATB14	// ADC input mux
#define PIN_A4			LATBbits.LATB15	// ADC input mux
#define PIN_TESTPOINT1		LATBbits.LATB1  // test point 1 output
#define PIN_TESTPOINT2		LATBbits.LATB0  // test point 2 output

//------> INPUT PINS
#define PIN_GATE		PORTDbits.RD8	// For READING Gate pin (normally triggers INT1)
#define PIN_CONVERT		PORTDbits.RD0	// For READING the ADC Convert pin (output is from OC1 hardware)

#ifdef PSYNCADC1    // Version 1 Pin Assignments
	// Outputs
    #define PIN_LED1		LATCbits.LATC1	// Front pannel LED
    #define PIN_FRAME		LATBbits.LATB10 // FRAME test point output
    #define PIN_PIC_READY_BAR	LATBbits.LATB5	// Drive low to re-enable frame count capture circuit

    #define PIN_DAC_SYNC	LATFbits.LATF1	// DAC Sync BAR pin
    #define PIN_LOAD_DAC	LATFbits.LATF0	// LDAC BAR pin

	// Inputs
    #define PIN_JP1_1		PORTDbits.RD3   // Unallocated Jumper inputs
    #define PIN_JP1_2		PORTDbits.RD2
    #define PIN_JP1_3		PORTDbits.RD1

    // Diagnostic outputs
    #define PIN_OSCOPE1		LATDbits.LATD4	// Debug pins for looking at with scope
    #define PIN_OSCOPE2		LATGbits.LATG15

    // Compatibility shims (Version 1 hardware doesn't have these features)
    // By defining these macros and pointing them to an unused pin, we allow the software to be compiled for v1
    #define PIN_LED2		LATCbits.LATC13	// Front pannel LED
    #define PIN_LED3		LATCbits.LATC13	// Front pannel LED
    #define PIN_AMP_GAIN_A0     LATCbits.LATC13     // Instrumentation amp gain 0 (new for V2)
    #define PIN_AMP_GAIN_A1     LATCbits.LATC13     // Instrumentation amp gain 1 (new for V2)
    #define PIN_OSCOPE3		LATCbits.LATC13
    #define PIN_OSCOPE4		LATCbits.LATC13

    // Compatibility inputs - hardcode to sensible values (PSyncADC v1 doesn't have these inputs)
    #define PIN_CLK_MODE	1               // Hardcode this to internal clock

#endif
#ifdef PSYNCADC2    // Version 2 changes
	// Outputs
    #define PIN_LED1            LATBbits.LATB5      // Moved for V2
    #define PIN_LED2            LATBbits.LATB4      // Yellow - ON = internal clock, OFF = ext clock (New for V2)
    #define PIN_LED3            LATBbits.LATB3      // Red - ON = sync box missing, OFF = sync box detected (New for V2)

    #define PIN_FRAME           LATBbits.LATB8      // FRAME test point output (moved for V2)
    #define PIN_PIC_READY_BAR   LATBbits.LATB2      // Drive low to re-enable frame count capture circuit (moved for V2)

    #define PIN_AMP_GAIN_A0     LATDbits.LATD10     // Instrumentation amp gain 0 (new for V2)
    #define PIN_AMP_GAIN_A1     LATDbits.LATD11     // Instrumentation amp gain 1 (new for V2)

    // Inputs
    #define PIN_FRAME_CLK	PORTDbits.RD2       // ~1 Hz frame clock (from PMaster)
    #define PIN_SYNC_CLK	PORTDbits.RD3       // Reserved for future use (from PMaster)

    #define PIN_JP1_1		PORTGbits.RG13      // Unallocated Jumper inputs
    #define PIN_JP1_2		PORTGbits.RG12
    #define PIN_JP1_3		PORTGbits.RG14
    #define PIN_JP1_4		PORTGbits.RG0

    #define PIN_CLK_MODE	PORTCbits.RC14      // 0 = EXT (backplane) clock.  1 = INT clock.

    // Diagnostic outputs
    #define PIN_OSCOPE1		LATDbits.LATD7      // Debug pins for looking at with scope
    #define PIN_OSCOPE2		LATDbits.LATD6      // Debug pins for looking at with scope
    #define PIN_OSCOPE3		LATDbits.LATD5      // Debug pins for looking at with scope
    #define PIN_OSCOPE4		LATDbits.LATD4      // Debug pins for looking at with scope
#endif          // End Version 2 changes

//  V_adc = ADC * 20480 mV / 65536 = ADC * 10 / 2^5
#define ADC_TO_MV(x) (((x>>4)*5)-10240)

// Frame rate measurement
#define FRAME_RATE_CLK_PRESCALE	8			// Must match the timer45 prescaler value set in init_pic()

// Board clock source modes (UBC Sync box, or internal osc)
#define FRAME_SOURCE_INTERNAL	0
#define FRAME_SOURCE_EXTERNAL	1
#define INTERNAL_FRAME_TICK_PERIOD	10		// Period, in 0.25 ms (4 kHz) units.  i.e. 10 = 400 Hz

//---------> Function Prototypes
// psyncadc.c
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// process_cmd.c
void process_cmd( char *cmd );

// do_cmd.c
void write_mux( unsigned short ch );

// cmd_gets.c
void cmd_gets( char *str, int n );

// dac.c
void DacTest( void );

// int_handler.c
short GetAdcReading( int ch );
unsigned long GetSystemTime( void );
unsigned long GetFrameCount( void );
float GetFrameRate( void );
unsigned long GetFramePeriod( void );
void SetDPer( int );
int GetDPer( void );
void SetDataSpit( int on );
int GetDataSpit();
int SetFrameSource( int frmSrc );
int GetFrameSource( void );
unsigned char IsDoingAdcAcqSequence( void );
void EnableAcquisition( void );
void DisableAcquisition(void);
void SetChannelList( unsigned char *list, int len );
int GetChannelListLength( void );
unsigned char* GetChannelList( void );

// save.c
void SaveState( void );
int RestoreState( void );

// test_cmd.c
void adc_noise_test( int nSamples );
void filter_settle_test( void );


// PSync Functions (from psync_common.h)
void init_psync(void);