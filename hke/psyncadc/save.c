#include <p30F5011.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>

#include "../common/common.h"
#include "../common/stty.h"
#include "../common/frame_buf.h"

#include "psyncadc.h"

#define VERSION 101		// Increment this every time you make a change to psyncAdcState definition

struct psyncAdcState {
	unsigned int version;
	unsigned int checksum;		// 16 bit checksum

    unsigned char echoOn;
	unsigned char dataOn;
	unsigned char dataPeriod;
	unsigned char frameSource;
	unsigned char channelListLength;
	unsigned char channelList[NUM_ADC_CHANNELS];
};

#define STATE_BYTES	(sizeof( struct psyncAdcState ))	// Note: may be larger than sum of lengths of fields due to alignment

unsigned char _EEDATA(2) stateDataEE[STATE_BYTES];		// Reserve memory for the state data in EEPROM

static char rowBuf[_EE_ROW] __attribute__((aligned(2)));	// Regular RAM to hold one "row" of data while copying to EEPROM


// Local functions
static unsigned computeChecksum( char *p, int n );

static int readState( struct psyncAdcState *pState );
static void writeState( struct psyncAdcState *pState );
static void getBoardState( struct psyncAdcState *pState );
static void setBoardState( struct psyncAdcState *pState );
static int compareStates( struct psyncAdcState *p, struct psyncAdcState *q );


// Returns 1 if two state objects are equal, 0 if not
static int compareStates( struct psyncAdcState *p, struct psyncAdcState *q )
{
	int i, n;

	if( p->version != q->version )
		return 0;

    if( p->echoOn != q->echoOn)
            return 0;
	if( p->dataOn != q->dataOn )
		return 0;
	if( p->dataPeriod != q->dataPeriod )
		return 0;
	if( p->frameSource != q->frameSource )
		return 0;
	if( p->channelListLength != q->channelListLength )
		return 0;

	n = p->channelListLength;
	for( i=0; i<n; ++i )  {
		if( p->channelList[i] != q->channelList[i] )
			return 0;
	}

	return 1;
}

// Gets the board's state and stores it in a state buffer
static void getBoardState( struct psyncAdcState *pState )
{
	int i, len;

	// Set the version
	pState->version = VERSION;

        pState->echoOn = cmd_get_echo();
	pState->dataOn = GetDataSpit();
	pState->dataPeriod = GetDPer();
	pState->frameSource = GetFrameSource();

	// Copy channel list length
	len = GetChannelListLength();
	pState->channelListLength = (unsigned char)len;

	// Copy channel list
	unsigned char *channelList = GetChannelList();
	for( i=0; i<len; ++i )
		pState->channelList[i] = channelList[i];
	for( ; i<NUM_ADC_CHANNELS; ++i )
		pState->channelList[i] = 'C';	// Safeguard in case of readback error

	// Compute the checksum (note, set checksum to 0 first)
	pState->checksum = 0;
	unsigned ck = computeChecksum( (char*)pState, STATE_BYTES );
	pState->checksum = ck;
}

// Sets the board's state given a state buffer
// MUST have called DisableAcquisiton prior to calling this
static void setBoardState( struct psyncAdcState *pState )
{
	// Note: set channel list does a copy, so it's OK to pass a reference to the local varialbe state.channelList
	SetChannelList( pState->channelList, pState->channelListLength );

    cmd_set_echo(pState->echoOn);
        
	// Set the board's data period
	SetDPer( pState->dataPeriod );

	// Set data packet transmission on/off
	SetDataSpit( pState->dataOn );

	// Set the frame source
	SetFrameSource( pState->frameSource );
}



// Saves up all board properties to the EEPROM memory
// MUST call DisableAcquisition before calling this
void SaveState()
{
	int s;
	struct psyncAdcState state __attribute__((aligned(2)));
	struct psyncAdcState stateInEE __attribute__((aligned(2)));

	getBoardState( &state );

	// Read in the stored state first, and don't overwrite if it was the same
	s = readState( &stateInEE );
	if( s == 0 )  {		// 0 means we read it OK (non-zero is error code)
		if( compareStates( &state, &stateInEE ) != 0 )  {	// 0 means different, 1 means the same
			TTYPuts("Unchanged, not saving to EEPROM.\r\n");
			return;
		}
	}

	// If we couldn't read old settings, or the settings have changed, save
	TTYPuts("Changed, going to actually save.\r\n");
	writeState( &state );
}

static int readState( struct psyncAdcState *pState )
{
    // initialize a variable to represent the Data EEPROM address
    _prog_addressT EE_addr;
    _init_prog_address(EE_addr, stateDataEE);

    // Copy from EEPROM to RAM
    _memcpy_p2d16(pState, EE_addr, STATE_BYTES);

	// Check the version, if it's wrong, we can't restore anything
	if( pState->version != VERSION )  {
                char s[80];
                sprintf(s,"Wrong EEPROM version (%d)\r\n",pState->version);
		TTYPuts(s);
		return 1;
	}

	// Compute the checksum (note, set checksum to 0 first)
	unsigned ckRead = pState->checksum;	// save a copy of the checksum we read in
	pState->checksum = 0;					// set checksum to 0 (we don't include checksum in the checksum)
	unsigned ckCompute = computeChecksum( (char*)pState, STATE_BYTES );
	if( ckRead != ckCompute )  {
		TTYPuts("Bad checksum\r\n");
		return 2;
	}

	return 0;		// Success
}

static void writeState( struct psyncAdcState *pState )
{
	int i;

    // initialize a variable to represent the Data EEPROM address
    _prog_addressT EE_addr;
    _init_prog_address(EE_addr, stateDataEE);

	// Write the state buffer out to EEPROM
	char *p = (char*)pState;			// Get a pointer to the start of the state buffer
	
	while( 1 )  {
		int bytes_left = STATE_BYTES - (p - (char*)pState);	// Bytes left to write

		if( bytes_left <= 0 )
			break;						// We're done, we wrote out all the bytes

		// If we have more than one row left to write, copy the bytes into our row buffer
		if( bytes_left >= _EE_ROW )  {
			for( i=0; i<_EE_ROW; ++i )
				rowBuf[i] = *p++;
		}
		else {	// Less than one row
			for( i=0; i<bytes_left; ++i )	// Copy over the bytes we have
				rowBuf[i] = *p++;
			for( ; i<_EE_ROW; ++i )			// Blank out the rest of the row buffer
				rowBuf[i] = 'J';
		}

		// Erase a row in Data EEPROM
	    _erase_eedata(EE_addr, _EE_ROW);
	    _wait_eedata();
	
	    // Write a RAM to a row in EEPROM
	    _write_eedata_row(EE_addr, (void*)rowBuf);
	    _wait_eedata();

		EE_addr += _EE_ROW;					// Advance to the next row
	}
}


// Loads up all board properties from the EEPROM memory
// MUST call DisableAcquisition before calling this
// Returns 0 on succes, non-zero on failure
int RestoreState()
{	
	struct psyncAdcState state;
	int s;

	// Read the board state from EEPROM
	s = readState( &state );
	if( s )
		return s;			// Failed to read -- return error code

	// Set the board properties
	setBoardState(&state);

	return 0;
}

// Compute a simple, 16 bit checksum for an array
static unsigned computeChecksum( char *p, int n )
{
	unsigned sum = 0;
	int i;
	for( i=0; i<n; ++i )
		sum += p[i];

	return sum;
}
