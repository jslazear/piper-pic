#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "../common/psync_common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"
#include "psyncadc.h"

/**********************************************************************************
Interrupt processing is very tricky in this board.  See discussion above init_pic()
function and above the 3 interrupt handler functions in this file
BEFORE CHANGING ANYTHING!!!
***********************************************************************************/

// frame count
unsigned long frame_count;		// Frame count decoded from the UBC Sync box
static char data_on = 0;		// Set to true to spit data packets
static int data_period = 10;	// The data period (10 means take data every 10th UBC frame)
static unsigned short adc_data[NUM_ADC_CHANNELS];	// The most recent ADC readings for each mux channel

// List of the channels to actually scan during our acq. sequence
static unsigned char channelList[NUM_ADC_CHANNELS];	// Will be set to all 0's
static unsigned int channelListLength = 0;

static unsigned char dper_count = 0;	// Inc on each UBC sync pulse, take data when it's 0
static unsigned long framePeriod = 0;	// Measured value of the frame rate
static unsigned char frameSource = FRAME_SOURCE_EXTERNAL;	// INTERNAL = our clock, EXTERNAL = UBC Sync box

// For the 4 kHz timer interrupt handler
static unsigned long system_time=0;
static unsigned int internalFrameTicker=0;		// used in internal frame mode

// Local functions
static void putDataInFrame( void );
static void measureFramePeriod( void );
static void startAquisitionSequence( void );


/*
 * Int1 handler.  UBC Sync box triggers INT1 when a new frame count arrives.
 */
void __attribute__((__interrupt__, __shadow__, __auto_psv__)) _INT1Interrupt( void ) 
{
 	IFS1bits.INT1IF = 0;		// reset INT1 interrupt flag -- needed or int handler will not work right!!!
	
	if( frameSource == FRAME_SOURCE_EXTERNAL )
		startAquisitionSequence();
}

/*
 * Called when we get an external interrupt from the UBC box (normal mode), or
 * when our internal timer interrupt (internal mode) says it's time to take data
 *
 * Note: it is essential that this routine does the minimum possible.  He needs to
 * read in the first word of the 40 bit frame count from the SPI port before the 2nd and 3rd arrive.
 * Also, he really needs to get timer2 going at a reliable (stable) time after this interrupt handler
 * is called.  SO DO AS LITTLE AS POSSIBLE IN THIS ROUTINE!!!
 */
static void startAquisitionSequence()
{
	//---> Read in the frame count from the UBC Sync Box (takes 8 us)
	PIN_OSCOPE2 = 1;
	if( dper_count == 0 )
		PIN_FRAME = 1;				// Set the FRAME test point

	// Read the frame count from UBC Sync box, or use our internal counter
	// Note: read of UBC box *must* happen within 128 instruction cycles, so this must be near top of int handler
	if( frameSource == FRAME_SOURCE_EXTERNAL )
		frame_count = psync_read_frame_count();

	PIN_OSCOPE2 = 0;
	if( dper_count == 0 )
		PIN_FRAME = 0;				// Clear the FRAME test point

	measureFramePeriod();			// Uses timer 4/5 to measure the frame period

	//---> Start the ADC aquisition by enabling timer 2
	if( dper_count == 0 && channelListLength != 0 )  {
		TMR2 = 0;			// Reset timer 2 to 0
		T2CONbits.TON = 1;	// Turn on timer 2, this initiates an aquisition off all 32 channels
							// Because this routine operates at a higher priority than the timer2 interrupt
							// timer2 won't interrupt us.
	}

	//---> Increment the sync pulse count
	++dper_count;
	if( dper_count >= data_period )
		dper_count = 0;
}


/*
 * Initiated by the INT1 handler above.  Takes ADC data.
 * Runs N times after each INT1, then turns itself off, where N = number of channels we're reading out (0 - 32)
 * Timer Interrupt handler, called on the *falling* edge of the convert pulse.
 * Note that the ADC convert pin is triggered by HARDWARE (timer2 / OC1), so latency/jitter in responding
 * to this interrupt don't really matter so much
 */
static unsigned char tick = 0;				// Incremented each time we get a timer2 interrupt

void __attribute__((__interrupt__, __auto_psv__)) _OC1Interrupt( void ) 
{
	PIN_OSCOPE2 = 1;
 	IFS0bits.OC1IF = 0;		// reset Output Compare 1 interrupt flag

	//--> ADC Read
	unsigned int data = 0x0000;


	// Do the actual SPI write/read  (Note PSyncADC uses SPI1 to talk to ADC / DAC)
	SPI1STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
	SPI1BUF = data;					// Write to the SPI port
	while( !SPI1STATbits.SPIRBF );	// Wait for data to be ready on SPI port
	data = SPI1BUF;				// Read in data from ADC converter

	// Figure out what channel we just read
	int ch = channelList[tick];

	// Save the ADC data
	adc_data[ch] = data;

	// Inc our tick count and see if we're done
	tick++;
	if( tick >= channelListLength )  {
		tick = 0;
		T2CONbits.TON = 0;			// turn off timer 2, we're done sampling all channels

		// Send out our data frame
		putDataInFrame();
	}

	// Set the input mux for our next aquisition -- important to do this even if we just finished an acq. sequence (tick == 0)
	// so that we leave it set to the first channel for the next acq. sequence.
	// Note: if we get here, channel list has at least one element (if it were 0 elements, we wouldn't be in this int handler)
	ch = channelList[tick];			// tick has been incremented already
	write_mux( ch );

	PIN_OSCOPE2 = 0;
}

/*
 * Loads all the ADC data into the frame buffer to be transmitted
 * Clocked at 150 us to send all 32 ADC samples
 * 		+ ~10 us for the channel list stuff
 * Does nothing if data_on is set to false!!!
 */
static void putDataInFrame()
{
	int i;

	if ( !data_on )
		return;

	swap_active_frame_buf();	// Swap which frame buffer we're writing to
	enqueue_last_frame_buf();

	fb_put32( frame_count );	// UBC frame count
	fb_putc('-');

	// Write out the channels that we just measured
	for( i=0; i<channelListLength; ++i )  {
		int ch = channelList[i];
		fb_put16( (unsigned)adc_data[ch] );
	}
}

// Updates the *Measured* frame rate by reading Timer45
static void measureFramePeriod()
{
/*  Alternate method that never resets counter to 0
	static unsigned long last_timer;
	unsigned long timer = TMR4; // Copy Timer4 into timer low 16bit
    timer |= ((unsigned long)TMR5HLD)<<16; // shift  16 time as TMR 5 

	unsigned long newPer = timer - last_timer;
	if( newPer > 0x80000000 )
		newPer += 0x80000000;

	last_timer = timer;
*/

	// newPer = ReadTimer45();  copied from library source code
	unsigned long newPer = TMR4; // Copy Timer4 into timer low 16bit
    newPer |= ((unsigned long)TMR5HLD)<<16; // shift  16 time as TMR 5 

	// WriteTimer45(0);  Copied from library source
    TMR5HLD = 0;				// Write to the hold register first
    TMR4 = 0;					// Next write to TMR4

	framePeriod = newPer;
}


//-------------------------------------------------------------------
// Timer 3 int handler.  Runs at 4 kHz
//-------------------------------------------------------------------

/*
 * Timer 3 interrupt handler.  Runs at 4 kHz at lowest priority.  
 *
 * Two purposes:
 *    * shove characters from the software TX queues into the UART hardware.
 *    * initiate data aquisition cycle when we're in the FRAME_SOURCE_INTERNAL mode.
 */
void __attribute__((__interrupt__, __auto_psv__)) _T3Interrupt( void )
{
	IFS0bits.T3IF = 0;		// Clear the interrupt flag

	PIN_OSCOPE1 = 1;

	// In internal frame mode, we start ADC aquisition sequence here, do this *before* TTYUpdateTX
	// Note if "internalFrameTicker" is negative, this is a flag NOT to start an acq. sequence
	if( frameSource == FRAME_SOURCE_INTERNAL && internalFrameTicker >= 0 )  {
		internalFrameTicker++;
		if( internalFrameTicker >= INTERNAL_FRAME_TICK_PERIOD )  {
			startAquisitionSequence();
			internalFrameTicker = 0;
			frame_count++;
		}
	}

	TTYUpdateTX();		// Look for characters in TX queue to send over UART

	system_time++;

	PIN_OSCOPE1 = 0;
}


//------------------------------------------------------
// External interface functions
//------------------------------------------------------

// Gets the 4 khz system timer tick value
unsigned long GetSystemTime()
{
	return system_time;
}


// Returns an ADC reading (mostly for "p" command)
short GetAdcReading( int ch )
{
	if( ch >= 0 && ch < NUM_ADC_CHANNELS )
		return adc_data[(int)ch];
	return 0;
}

// Returns the most recent UBC frame count
unsigned long GetFrameCount()
{
	return frame_count;
}

// Returns the *measured* frame rate in Hz
float GetFrameRate()
{
	return (CLOCKFREQ/FRAME_RATE_CLK_PRESCALE) / (float)framePeriod;
}
unsigned long GetFramePeriod()
{
	return framePeriod;
}

// Returns true if we're taking data
unsigned char IsDoingAdcAcqSequence()
{
	// Timer 2 is on during ADC sweep (he generates 32 interrupts to switch channels and take data)
	// This is true whether we are in internal or external mode
	return T2CONbits.TON;
}

// Sets the "data period" ie, 10 -> take data every 10th UBC frame
void SetDPer( int dper )
{
	data_period = dper;
}
int GetDPer()
{
	return data_period;
}

// Turns data on/off
void SetDataSpit( int on )
{
	data_on = on;
}
int GetDataSpit()
{
	return data_on;
}

void DisableAcquisition()
{
	if( frameSource == FRAME_SOURCE_INTERNAL )  {
		// Set a flag to prevent timer interrupt from starting a new acq. sequence
		internalFrameTicker = -1;	// timer interrupt handler checks internalFrameTicker>=0 before starting an acq sequence
	}
	else if( frameSource == FRAME_SOURCE_EXTERNAL ) {
		// Turn off Int1 interrupts (from UBC Sync box)
		// This will prevent a new acq. sequence from starting
		IEC1bits.INT1IE = 0;	
	}

	// Wait for the current acq. sequence to finish (in the unlikely event that were were in an acq. seq. when this routine was called)
	while( IsDoingAdcAcqSequence() );

	// If we were sending a frame buffer, wait for it to finish
	while( TTYIsSendingFrameBuf() );
}

void EnableAcquisition()
{
	if( frameSource == FRAME_SOURCE_INTERNAL )  {
		internalFrameTicker = 0;	// allow timer int handler to initiate acquisition sequences again
	}
	else if( frameSource == FRAME_SOURCE_EXTERNAL ) {
		// Re-init the psync stuff
		init_psync();			// Init psync hardware (will enable INT1)
	}
}

// Sets frame source to internal or external
// MUST call DisableAcquisition() before using this routine (except in init_pic where acq. hasn't started yet)
// RETURNS: 1 on success, 0 on failure
int SetFrameSource( int frmSrc )
{
	if( frmSrc == FRAME_SOURCE_INTERNAL )  {
		frameSource = FRAME_SOURCE_INTERNAL;		// This is a single-cyle op, can't be interrupted
        PIN_LED3 = 1;   // No sync box -- light up the red LED
		return 1;		// success
	}
    else {      // FRAME_SOURCE_EXTERNAL
		frameSource = FRAME_SOURCE_EXTERNAL;
        PIN_LED3 = 0;   // Sync box detected -- turn off the red LED

		if( test_psync() ) {			// Important - don't let people change to external with no UBC box present
			return 1;		// success
		}
		else {
			frameSource = FRAME_SOURCE_INTERNAL;	// Don't set to external if SyncBox could not be detected
            PIN_LED3 = 1;   // No sync box -- light up the red LED
			return 0;		// failed to set desired mode
		}
	}
}
int GetFrameSource()
{
	return frameSource;
}

// Sets the channel list
// MUST call DisableAcquisition() before using this routine
void SetChannelList( unsigned char *list, int len )
{
	// Copy the new channel list into the master channel list
	int i;

	// Zero out all the ADC readings
	for( i=0; i<NUM_ADC_CHANNELS; ++i ) 
		adc_data[i] = 0;

	channelListLength = len;
	for( i=0; i<len; ++i )
		channelList[i] = list[i];

	// Switch mux to first channel in list (if there is one)
	if( channelListLength > 0 )
		write_mux( channelList[0] );
	
	// Re-init the frame buffers because the data packet length may be different now
	int n = FRAME_BUF_LEN_FOR_NCH( channelListLength );
	init_frame_buf_min(n);
}

int GetChannelListLength()
{
	return channelListLength;
}

unsigned char* GetChannelList()
{
	return channelList;
}