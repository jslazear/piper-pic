#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>
#include <math.h>

#include "../common/common.h"
#include "../common/stty.h"
#include "../common/frame_buf.h"

#include "psyncadc.h"

// Local function prototypes
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_p( void );		// prints status
static char do_cmd_ph( void );		// prints file header info
static char do_cmd_ch( void );		// sets the channel mask
static char do_cmd_data( void );	// turns data packet spitting on/off
static char do_cmd_save( void );	// save board state in EEPROM
static char do_cmd_restore( void );	// restore board state from EEPROM
static char do_cmd_dper( void );	// Data period
static char do_cmd_fsrc( void );	// Frame source (int or ext)
static char do_cmd_noise( void );	// ADC noise test
static char do_cmd_filter( void );	// Filter settling test
static char do_cmd_test( void );	// Test command, general debugging
static char do_cmd_echo( void );	// changes the echo mode of the board
static char do_cmd_d( void );           // prints delta between channels in ADC units

// helper functions
static void print_channel_list( void );


/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */
void process_cmd( char *cmd )
{
	char *s;
	unsigned char r;
	
	// Parse the command string
	cmd_parse(cmd);

	// Parse the command string
	s = tok_get_str();	// first token is the command name

        if(!strcmp(s,""))
            return;

	if( !strcmp( s, "led" ))		
		r = do_cmd_led();		// set the LED
	else if( !strcmp( s, "p" ))		// Full print command
		r = do_cmd_p();
	else if( !strcmp( s, "ph" ))	// Print header command
		r = do_cmd_ph();
	else if( !strcmp( s, "ch" ))	// Sets the channel mask
		r = do_cmd_ch();
	else if( !strcmp( s, "save" ))	// Save board state in EEPROM
		r = do_cmd_save();
	else if( !strcmp( s, "restore" ))// Load board state from EEPROM
		r = do_cmd_restore();
	else if( !strcmp( s, "data" ))
		r = do_cmd_data();
	else if( !strcmp( s, "dper"))	// set the "data period" (ubc frame rate skip)
		r = do_cmd_dper();
	else if( !strcmp( s, "fsrc"))	// set the frame source (int or ext)
		r = do_cmd_fsrc();
	else if( !strcmp( s, "n" ))	// Debug command (should rename)
		r = do_cmd_noise();
	else if( !strcmp( s, "f" ))	// Debug command (should rename)
		r = do_cmd_filter();
	else if( !strcmp( s, "t" ))	// Debug command (should rename)
		r = do_cmd_test();
        else if( !strcmp( s, "echo" ))
		r = do_cmd_echo();	// Sets echo on or off
        else if( !strcmp( s, "d" ))
                r = do_cmd_d();             // Prints delta
	else 							
		r = STATUS_FAIL_CMD;	// User typed unknown command
	
	print_status( r );
}


/*-----------------------------------------------------------
 * Configuration Commands
 *-------------------------------------------------------------*/

/** DPER
 *	Sets the "data period", that is how often we spit out a data packet relative to UBC frame period
 *
 * Usage:
 *	dper 10 -> Spits out data packet every 10th frame count
 */
static char do_cmd_dper( void )
{
	// Ensure that token is available
	if (!tok_available() )
		return STATUS_FAIL_NARGS;

	// Get the period (1-9999)
	if( !tok_valid_uint( 4 ) )
		return STATUS_FAIL_INVALID;
	int dper = tok_get_int16();

	// Set the period	
	SetDPer( dper );

	return STATUS_OK;	
}	

/** CH
 * Sets the channel mask (which channels are reported over the wire)
 *
 * Usage:
 * ch 0 2 3 5 11 13  to just report back for the specified channels
 * ch all			// All channels
 * ch all -0 -17   // All channels except 0 and 17
 */
static char do_cmd_ch( void )
{
	unsigned long mask = 0x0000000;		// Assume no channels
	char *tok;
	int ch, len, i;

	while( tok_available() ) {
		// A number (positive or negative)
		if( tok_valid_num( 2, 1, 0 ) ) {	// True for any 2 digit number, allows a negative sign (even -0)
			ch = tok_get_int16();			// Reads the number in (with atoi)
			if( ch >= 0 )
				mask |= (0x0001L << ch);		// add the channel onto our list
			else
				mask &= ~(0x0001L << -ch);	// remove from our mask
		}
		else {
			tok = tok_get_str();			// Read the next token as a string

			// A range of channels 5:32  (note: no spaces)
			char *p = strchr( tok, ':' );

			if( p )  {						// if we found a ':', process the channel range
				*p = '\0';		// Terminate the string at the colon, so tok now looks like '5','\0','3','2','\0'

				// Parse the range arguments
				int n1 = atoi(tok);
				int n2 = atoi(p+1);		// p+1 points to '3' in our example
				if( n1 < 0 || n2 < 0 || n1 > NUM_ADC_CHANNELS || n2 >= NUM_ADC_CHANNELS || n1>n2 )
					return STATUS_FAIL_INVALID;
				
				// Set the bits
				for( i=n1; i<=n2; ++i )
					mask |= (0x0001L << i);
			}
			else if( !strcmp( tok, "all" ) )
				mask = 0xFFFFFFFF;
			else
				return STATUS_FAIL_INVALID;
		}				
	}

	// Debug
	/*
	sprintf( str, "mask: %lX\r\n", mask );
	TTYPuts( str ); */

	// Convert bit mask into an array of channel numbers
	unsigned char channel_list[NUM_ADC_CHANNELS];
	for( ch=0,len=0; ch<NUM_ADC_CHANNELS; ++ch )  {
		if( ((unsigned int)mask) & 0x1 )  {
			channel_list[len] = ch;
			len++;
		}
		mask >>= 1;
	}

	// Stop data acquisition and set the channel list	
	DisableAcquisition();
	SetChannelList( channel_list, len );
	EnableAcquisition();

	// Print out the channel list
	print_channel_list();

	return STATUS_OK;	
}

static void print_channel_list( void )
{
	int i;

	int len = GetChannelListLength();
	unsigned char *channel_list = GetChannelList();

	// Print out the channel list
	char str[16];			// Note, we're only printing one channel at a time, so don't need huge buffer
	for( i=0; i<len; ++i )  {
		sprintf( str, "%d ", channel_list[i] );
		TTYPuts( str );
	}
	TTYPuts("\r\n");
}

/** FSRC
 *	Sets frame source clock to internal or external
 *
 *	Usage:
 *	fsrc int	-> Internal frame source
 *	fsrc ext	-> External frame source
 */
static char do_cmd_fsrc( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	int status = 0;
	if( !strcmp( arg, "int" ) ) {
		DisableAcquisition();
		status = SetFrameSource( FRAME_SOURCE_INTERNAL );
		EnableAcquisition();
	}
	else if( !strcmp( arg, "ext" ) ) {
		DisableAcquisition();
		status = SetFrameSource( FRAME_SOURCE_EXTERNAL );
		EnableAcquisition();
	}
	else
		return STATUS_FAIL_INVALID;
	
	// Status will be 0 if we requested "ext" mode but there is no SyncBox connected
	if( !status )  {
		TTYPuts("Error could not set frame source\r\n");
		return STATUS_FAIL_COULD_NOT_COMPLETE;
	}

	return STATUS_OK;
}


/** Save
 * Save all board configuration settings in the PIC's EEPROM (FLASH).
 */
static char do_cmd_save( void )
{
	DisableAcquisition();

	SaveState();

	EnableAcquisition();

	return STATUS_OK;
}

/** Restore
 * Restore all the board configuration settings in the PIC's EEPROM (FLASH).
 * Notes:
 * This command is issued automatically when the board powers up.
 */
static char do_cmd_restore( void )
{
	DisableAcquisition();

	RestoreState();

	EnableAcquisition();

	return STATUS_OK;
}


/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/

/** P
 * Prints current ADC readings from all channels
 * Sample Output:

		 CH    ADC     MV      CH    ADC     MV
		 0      0 -10240      16      0 -10240
		 1      0 -10240      17      0 -10240
		 2      0 -10240      18      0 -10240
		 3      0 -10240      19      0 -10240
		 4      0 -10240      20      0 -10240
		 5      0 -10240      21      0 -10240
		 6      0 -10240      22      0 -10240
		 7      0 -10240      23      0 -10240
		 8      0 -10240      24      0 -10240
		 9      0 -10240      25      0 -10240
		10      0 -10240      26      0 -10240
		11      0 -10240      27      0 -10240
		12      0 -10240      28      0 -10240
		13      0 -10240      29      0 -10240
		14      0 -10240      30  45903   4100
		15      0 -10240      31  32770      0
		OK!

   Note that channel 30 is jumpered to the internal 4.096 V reference and ch 31 is grounded.
   The other channels are floating.
 */
static char do_cmd_p( void )
{
	int i;
	char str[100];	// max string len is about 51 chars, so 100 is very safe

	sprintf( str, "\nCH    ADC     MV      CH    ADC     MV\r\n" );
	TTYPuts(str);
	
	for( i=0; i<16; ++i )  {
		unsigned int adc1, adc2;
		int mv1, mv2;

		adc1 = GetAdcReading(i);
		adc2 = GetAdcReading(i + 16);

		mv1 = ADC_TO_MV( adc1 );
		mv2 = ADC_TO_MV( adc2 );

		sprintf( str, "%2d %6u %6d      %2d %6u %6d\r\n",  i, adc1, mv1, i+16, adc2, mv2 );

		TTYPuts(str);
	}

	return STATUS_OK;
}

/** PH
 * Print file header info 
 * This gives details on the frame rate, serial port bandwidth utilization, frame clock source, etc.
 */
static char do_cmd_ph( void )
{
	char str[100];	// max string len is about 51 chars, so 100 is very safe

	// Frame count, rate, etc...
	unsigned long frame_count = GetFrameCount();
	float frame_rate = GetFrameRate();
	unsigned long framePeriod = GetFramePeriod();
	char *fsrc = (GetFrameSource()==FRAME_SOURCE_INTERNAL) ?"internal" :"external";
	sprintf( str, "Frame: %lu  Rate: %.3f Hz  Period: %lu  Src: %s\r\n", frame_count, frame_rate, framePeriod, fsrc );
	TTYPuts(str);

	int dper = GetDPer();
	float packet_rate = frame_rate / dper;
	int nCh = GetChannelListLength();	// number of channels we're currently reading out
	int frame_buf_len = FRAME_BUF_LEN_FOR_NCH(nCh);	// frame buffer length in bytes
	float data_rate = (frame_buf_len-1) * packet_rate * 10.0 / 1000.0;	// kbps (note 10 for port), -1 since \0 isn't transmitted
	float baud_usage =  (data_rate / (BAUD_RATE/1000.0)) * 100.0;
	sprintf( str, "Data Period: %d  Rate: %.3f Hz  Port Bandwidth: %.1f kbps (%.1f\%c)\r\n", dper, packet_rate, data_rate, baud_usage, '%' );
	TTYPuts(str);

	// Print out the channel list
	TTYPuts("CH: ");
	print_channel_list();

	return STATUS_OK;
}


/** DATA
 *	Turns on/off data stream.
 *
 *	Usage:
 *	data on		-> Turns data stream on
 *	data off	-> Turns data stream off
 */
static char do_cmd_data( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( !strcmp( arg, "on" ) )
		SetDataSpit( 1 ); 	// turn data stream on
	else if( !strcmp( arg, "off" ) )
		SetDataSpit( 0 ); 	// turn data stream off
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/** LED
 *	Set LED light.
 *
 *	Usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led( void )
{
	char *arg;
	
	// Check that the next token is a valid number
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	arg = tok_get_str();
	if( !strcmp( arg, "on" ) )
		PIN_LED1 = 1;
	else if( !strcmp( arg, "off" ) )
		PIN_LED1 = 0;
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/** ECHO
 *	Sets echo on or off.
 *
 *	Usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo( void )
{
	char *arg;

	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( !strcmp( arg, "on" ) )
		cmd_set_echo( 1 );
	else if( !strcmp( arg, "off" ) )
		cmd_set_echo( 0 );
	else
		return STATUS_FAIL_INVALID;

	return STATUS_OK;

}

/*-----------------------------------------------------------
 * Diagnostic / Test Commands
 *-------------------------------------------------------------*/
/** D
 * Prints current DELTA readings from all channels
 */
static char do_cmd_d( void )
{
	int i;
        long long sum=0;
	long long sum_sq=0;
        long delta_sq;
        unsigned int adc0;
	char str[100];	// max string len is about 51 chars, so 100 is very safe

        adc0 = GetAdcReading(0);

	sprintf( str, "\nCH  ADC    DELTA\r\n" );
	TTYPuts(str);
        sprintf( str, " 0 %6u\r\n", adc0 );
        TTYPuts(str);


	for( i=1; i<32; ++i )  {
		unsigned int adc1, adc2;
                int delta;

                adc1 = GetAdcReading(i-1);
                adc2 = GetAdcReading(i);

                delta = adc2 - adc1;
                sum += delta;
                delta_sq = (long)delta*(long)delta;
                sum_sq += delta_sq;

		sprintf( str, "%2d %6u %6d\r\n",  i, adc2, delta );

		TTYPuts(str);
	}

	// Compute N^2 * Variance.  Note as long as sum and sum_sq are 64 bits
	// and N < 65536, this won't overflow
	long long nnvar = (i-1)*sum_sq - sum*sum;

	// Compute avg and std as floats
	float fN = (float)(i-1);
	float avg = (float)sum / fN;
	float std = sqrt( (float)(nnvar) / (fN*fN) );

	// Print out statistics
        TTYPuts( "\r\n" );
        sprintf( str, "fN  %i\r\n", (int)fN);
        TTYPuts( str );
	sprintf( str, "avg %f\r\n", avg);
	TTYPuts( str );
        sprintf( str, "dev %f bits\r\n", std);
        TTYPuts( str );

	return STATUS_OK;
}

/** N
 * ADC Noise test command
 */
static char do_cmd_noise( void )
{
	adc_noise_test( 1000 );
	return STATUS_OK;
}


/** F
 * Filter settling test command
 */
static char do_cmd_filter( void )
{
	filter_settle_test();
	return STATUS_OK;
}

/** T
 * Debug test command
 */
static char do_cmd_test( void )
{
	char str[64];

	int i = 0;

	while( 1 )  {
		sprintf( str, "Test: %d\r\n", i );
		TTYPuts(str);
		++i;

		PIN_LED1 = 0;		// Will be turned on by test under certain conditions
		adc_noise_test( 100 );
	 	__delay32( (long)(0.1 * CLOCKFREQ) );
	}

	return STATUS_OK;
}

/* This function is really defined in common_functions.c
 * But we can't include that file in this project because it references a bunch of other stuff
 * that we don't need / want for this board.  Should really move print_status to its own
 * file in the /common directory.
 *
 * Print a human readable status message depending on the status code.
 */
void print_status( char status )
{
	switch( status )  {
		case STATUS_OK:
			TTYPuts( "OK" );
			break;
		case STATUS_FAIL_CMD:
			TTYPuts( "FAIL: unknown command" );
			break;
		case STATUS_FAIL_NARGS:
			TTYPuts( "FAIL: incorrect number of arguments" );
			break;
		case STATUS_FAIL_INVALID:
			TTYPuts( "FAIL: invalid argument format" );
			break;
		case STATUS_FAIL_RANGE:
			TTYPuts( "FAIL: argument out of range" );
			break;
		case STATUS_FAIL_COULD_NOT_COMPLETE:
			TTYPuts( "FAIL: the requested operation could not be completed" );
			break;
		default:
			TTYPuts( "FAIL: unknown error" );
			break;
	}

	// All responses must end with ! to signal parser this is end of response
	TTYPuts( "!\r\n" );
}
