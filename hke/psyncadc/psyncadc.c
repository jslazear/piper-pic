#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>

#include "psyncadc.h"
#include "../common/ptty.h"
#include "../common/psync_common.h"
#include "../common/frame_buf.h"

#include <libpic30.h>

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL16);         /* External clock with 16xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF);                         /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN);           /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF);                    /* Code protect disabled */

/*  Global variables */

/*
 * Check to make sure that we have the correct program on the correct hardware
 */
/*
void TestPsyncADCVersion( void )
{
  #define FLASH_PIN(pin) \
            pin = 1; \
            __delay32( (long)(0.25 * CLOCKFREQ) ); \
            pin = 0; \
            __delay32( (long)(0.25 * CLOCKFREQ) )

  #ifdef PSYNCADC
     if( PORTDbits.RD1 == 1)  {
		// If we get here, we have version 1 software on a version 2 board
	    while( 1 ) {
            // Init TRIS to set red led as output (we're running version 1 code here)
			// Version 2 hardware has a Red LED, so blink RED LED forever
            FLASH_PIN(LATBbits.LATB3);  //
		}
	 }
  #endif
  #ifdef PSYNCADC2
     if( PORTDbits.RD1 == 0)  {
		// If we get here, we have version 2 software on a version 1 board
	    while(1) {
			// Blink PIC_LED forever
            FLASH_PIN(PIN_LED);
		}
	 }
  #endif
}
*/

/*
 * INTERRUPT USAGE
 *
 * Int1,    ~200 Hz, Priority 4, triggered by UBC Sync box.  Starts ADC aquisition cycle.
 * OC1,      10 kHZ, Priority 2, enabled after UBC Sync pulse, does a sweep of all 32 channels of ADC.
 *           For each aquisition cycle, this interrupt is triggered 32 times, then disables itself.
 * Timer 3,  4 kHz, Priority 1 (lowest), transmits chars over UART.
 *           Also triggers ADC aquisition cycle when in INTERNAL trigger mode (no sync box).
 */
void init_pic()
{
	// Set up values for GPIO pins

    // Outputs common to v1 and v2 hardware
	TRISBbits.TRISB11 = 0;	// MUX Address lines
	TRISBbits.TRISB12 = 0;	// MUX Address lines
	TRISBbits.TRISB13 = 0;	// MUX Address lines
	TRISBbits.TRISB14 = 0;	// MUX Address lines
	TRISBbits.TRISB15 = 0;	// MUX Address lines
   	TRISBbits.TRISB0 = 0;	// Test Point 2
	TRISBbits.TRISB1 = 0;	// Test Point 1

  #ifdef PSYNCADC1  // Version 1 hardware
	TRISCbits.TRISC1 = 0;	// Set LED as an output
	TRISBbits.TRISB10 = 0;	// FRAME test point output
	TRISBbits.TRISB5 = 0;	// Set pic_ready_bar line to an output
	TRISFbits.TRISF0 = 0;	// DAC SYNC
	TRISFbits.TRISF1 = 0;	// DAC LOAD

   	TRISDbits.TRISD4 = 0;	// OSCOPE1
	TRISGbits.TRISG15 = 0;	// OSCOPE2
  #endif
  #ifdef PSYNCADC2 // Version 2 hardware
	TRISBbits.TRISB5 = 0;	// Set LED1 as an output
	TRISBbits.TRISB4 = 0;	// Set LED2 as an output
	TRISBbits.TRISB3 = 0;	// Set LED3 as an output
	TRISBbits.TRISB8 = 0;	// Set frame test point as output
	TRISBbits.TRISB2 = 0;	// Set pic_ready_bar line to an output
	TRISDbits.TRISD10 = 0;	// Instrumentation Amplifier Gain A0
	TRISDbits.TRISD11 = 0;	// Instrumentation Amplifier Gain A1

    TRISDbits.TRISD7 = 0;	// Oscope 1
    TRISDbits.TRISD6 = 0;	// Oscope 2
    TRISDbits.TRISD5 = 0;	// Oscope 3
    TRISDbits.TRISD4 = 0;	// Oscope 4
  #endif

	// Set the direction for GPIO

	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;	

	// Oscope debug pins, set them to outputs
	TRISDbits.TRISD4 = 0;
	TRISGbits.TRISG15 = 0;
	PIN_OSCOPE1 = 0;
	PIN_OSCOPE2 = 0;
	PIN_TESTPOINT1 = 0;
	PIN_TESTPOINT2 = 0;

    // Set the instrumentation amplifier to a gain of 1
    PIN_AMP_GAIN_A0 = 0;
    PIN_AMP_GAIN_A1 = 0;

	// Short delay to wait for power supply voltages to settle
 	__delay32( (long)(0.01 * CLOCKFREQ) );

	// TestPsyncADCVersion();  // Attempt to confirm that correct version of program is selected for the hardware
	
    // Setup the UART - 115.2 kbs
	unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
	unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
	OpenUART2( U2MODEvalue, U2STAvalue, BRG_SETTING );
	ConfigIntUART2( UART_RX_INT_DIS & UART_TX_INT_DIS );	// Disable TX and RX interrups

	// Timer3 ticks at 4 kHz (period=5000).  Used to move characters from our software TX queue to the UART hardware
	OpenTimer3( T3_ON & T3_IDLE_CON & T3_GATE_OFF & T3_PS_1_1 & T3_SOURCE_INT, 5000 );
	ConfigIntTimer3(T3_INT_PRIOR_1 & T3_INT_ON);	// Enable interrupt at lowest priority

	// Setup timer 2 and set its period to 10 kHz (100 us, or 3.2 ms to sample all 32 channels)
	// Note timer is set to OFF, will be set to on in int_handler after each UBC frame count
	OpenTimer2( T2_OFF & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, 2000);

	// Open the output compare module to generate 5 usec pulses, with interrupts on FALLING edge
	// these pulses will trigger the convert pin of the ADC
	OpenOC1( OC_IDLE_CON & OC_TIMER2_SRC & OC_CONTINUE_PULSE, CONVERT_PULSE_STOP_TICK, 0 );

	// Finally, turn on OC2 interrupt, won't be triggered till timer2 is enabled (inside int handler from UBC frame int)
	ConfigIntOC1( OC_INT_ON & OC_INT_PRIOR_2 );    // Enable interrupt at 2nd lowest priority

	// Setup the SPI port1 (For ADC and DACs)
	OpenSPI1( FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON & 
				MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
				PRI_PRESCAL_4_1,
				SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR );
	ConfigIntSPI1( SPI_INT_DIS );	// NO SPI interrupts

	//------------------------------------------------------------------------------
	//The SPI module supports 4 different Serial Clock formats. The user can
	//select one of these 4 formats by configuring the Clock Polarity Select, or
	//CKP, and Clock Edge Select, or CKE, bits in the SPI Control Register.
	//See the dsPIC family reference for a description.
	//NOTE: the settings here are a bit off from our usual, the AD5724 DAC chip needed different settings
	//  -- it wants valid data on the falling edge of sclk.  Other DACs, like the DAC7715 (on the AO board) want valid
	// data on the rising SCLK edge
	//------------------------------------------------------------------------------
	// Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
	SPI1CONbits.CKE = 0;	// CKE=1 means SDO changes on RISING SCLK edges, because AD5754 DAC reads on the falling edge
	SPI1CONbits.CKP = 0;	// CKP=0 means SCLK is active high
	SPI1CONbits.SMP = 0;	// If the SMP bit is set, then the input sampling is done at the end of the bit output

	// Timer 4/5 is used as a 32 bit timer to measure the current sampling period (Frame rate from UBC)
	OpenTimer45(T4_ON & T4_GATE_OFF & T4_PS_1_8 & T4_32BIT_MODE_ON & T4_SOURCE_INT, 0xFFFFFFFF);


	INTCON1bits.NSTDIS = 0;		// This line enables interrupt nesting.  (The default)
								// Nesting allows us to achieve a very stable latency to service
								// the external interrupt when a frame count arrives from the SyncBox
								// But it means we have to think very hard.  See disc. at top of init_pic() function
								// Without nesting I measured a min latency of 1.35 us and a max of 4.7 us
								// This is due to the timer3 interrupt.
								// With nesting, I measure a min latency of 1.40 us and a max of 1.55 us

	// By default set us to external frame source, but if no SyncBox is detected, will set to internal
	if( test_psync() )
		SetFrameSource( FRAME_SOURCE_EXTERNAL );
	else
		SetFrameSource( FRAME_SOURCE_INTERNAL );

    // Set the clock mode LED (yellow)
    PIN_LED2 = PIN_CLK_MODE;    // EXT = low => LED OFF

	// Attempt to load settings from EEPROM memory, these will override the defaults if someone has done a "save" command
	RestoreState();

	// Start taking data
	EnableAcquisition();
}


int main( void )
{
	unsigned char defaultChannelList[] = {30,31};
  
	// Set our default channel list -- this will init the frame buffer
	// Important - do this *before* init_pic()  (before interrupts are enabled)
	SetChannelList( defaultChannelList, sizeof(defaultChannelList) );

	// TODO: look for signal from UBC Sync box, if not there, set to internal clock
	init_pic(); 	// Setup the PIC internal peripherals

	PIN_LED1 = 0;

	while(1) 
	{	
		char s[100];				// Note the CH command can have long argument list (up to 90 characters)

		cmd_gets(s, sizeof(s));		// Get a command from the user

		process_cmd(s);				// Deal with the command
	}	
}

void OnIdle( void )
{
}