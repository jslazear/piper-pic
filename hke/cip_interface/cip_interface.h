//---------> Some useful stuff

#define BAUD_RATE	9600		// Note 9600 baud rate to match the CIP itself
#define CLOCKFREQ 1843200L		//  4.000 MHz instruction cycle (synthesised from 4 MHz crystal)
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)) - 1.00 + 0.5 ))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Adds 0.5 to convert from float to integer correctly

//-----> OUTPUT PINS
#define PIN_LED1			LATBbits.LATB3	// Front pannel LED
#define PIN_LED2			LATBbits.LATB2	// Front pannel LED

#define PIN_OSCOPE1			LATGbits.LATG6	// Debug pins for looking at with scope
#define PIN_OSCOPE2			LATGbits.LATG7
#define PIN_OSCOPE3			LATGbits.LATG8
#define PIN_OSCOPE4			LATGbits.LATG9
#define PIN_OSCOPE5			LATBbits.LATB1
#define PIN_OSCOPE6			LATBbits.LATB0

//------> INPUT PINS
#define PIN_COM_MODE		PORTDbits.RD11
#define PIN_STROBE			PORTDbits.RD8	// CIP Strobe - note this is also INT1

#define CIP_DATA_LOW		((unsigned char)(PORTB>>8))	// CIP data least significant byte (B8-B16)
#define CIP_DATA_HIGH		((unsigned char)PORTD)	// CIP data most significant byte (D0-D7)



//---------> Function Prototypes
// cip.C
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler
