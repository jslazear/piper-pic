#include <p30F5011.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <libpic30.h>


#include "cip_interface.h"
#include "../common/stty.h"
#include "../common/tty_print.h"

// Device configuration register macros for building the hex file
_FOSC(CSW_FSCM_OFF & XT_PLL4);          // XT with 4xPLL oscillator, Failsafe clock off
_FWDT(WDT_OFF & WDTPSA_512 & WDTPSB_1); // Watchdog timer enabled, set to period of 1024 ms
										// Watchdog period = 2 ms * prescale_A * prescale_B
_FBORPOR(PBOR_OFF & MCLR_EN);           // Brown-out reset disabled, MCLR reset enabled
_FGS(CODE_PROT_OFF);                    // Code protect disabled

// Local functions
static unsigned int readCIP(void);
static void writeDataToPort( unsigned int data );


void init_pic()
{
	// Set the direction for GPIO
	TRISBbits.TRISB3 = 0;	// Set LED1 as an output
	TRISBbits.TRISB2 = 0;	// Set LED2 as an output

	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;	

	// Oscope debug pins, set them to outputs
	TRISGbits.TRISG6 = 0;	// Oscope1
	TRISGbits.TRISG7 = 0;	// Oscope2
	TRISGbits.TRISG8 = 0;	// Oscope3
	TRISGbits.TRISG9 = 0;	// Oscope4
	TRISBbits.TRISB1 = 0;	// Oscope5
	TRISBbits.TRISB0 = 0;	// Oscope6

	// Init the outputs
	PIN_LED1 = 0;
	PIN_LED2 = 0;

	// Short delay to wait for power supply voltages to settle
	PIN_LED1 = 1;		// Blink LED1 on power up
 	__delay32( (long)(0.10 * CLOCKFREQ) );
	PIN_LED1 = 0;

	// Setup the UART - 9600 bps
	unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
	unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
	OpenUART2( U2MODEvalue, U2STAvalue, BRG_SETTING );
	ConfigIntUART2( UART_RX_INT_DIS & UART_TX_INT_DIS );	// Disable TX and RX interrups

	// Enable the watchdog timer
	RCONbits.SWDTEN	= 1;		// Turn on the watchdog (period is configured at the top of this file
}


int main( void )
{
	init_pic(); 	// Setup the PIC internal peripherals
	cur_strobe = 0;
	pre_strobe = 0;

	while(1) 
	{
		if( !PIN_STROBE )
		{
			unsigned int data = readCIP();
			writeDataToPort( data );

			// Wait 100 ms for debouncing.  The CIP strobe is only 1 ms, but the maximum command
			// rate is roughly 3 / second, so 100 ms delay is fine.  Also, this allows it to work very
			// well with the push button CIP simulator.
			// Could do this delay more efficiently with an interrupt handler, or by checking a hardware timer if this
			// board had *anything* else to do.

			PIN_LED2 = 1;
			__delay32( (long)(0.010 * CLOCKFREQ) );
			PIN_LED2 = 0;
		}

		ClrWdt();			// Reset the watchdog timer
	}	
}

// Reads in the 16 bit data word from the CIP
static unsigned int readCIP(void)
{
	unsigned int data = (CIP_DATA_HIGH<<8) | CIP_DATA_LOW;
	return data;
}

// Writes a 16 bit data word as 4 asci hex characters to the port along with a \r\n
static void writeDataToPort( unsigned int data )
{
	TTYPrintHex16( data );
	TTYPutc('\r');
	TTYPutc('\n');
}
