#ifndef __BP_COMMON_INCLUDED
#define __BP_COMMON_INCLUDED

// Clock Frequency Constants (used for timing calculations below)
#define CLOCKFREQ 20000000L		// 20.000 MHz clock (synthesised from 5 MHz crystal, or externally supplied by UBC)
#define UBC_CLK_FREQ 25000000L          // 25.000 MHz clock inside sync box (note MCE has 50 MHz crystal)


// TIMING CONFIGURATIONS
// Project-specific timing constants
// Generated with the help of timings.py
//
// UBC Sync Box Cheat Sheet
// COM Port: 9600 baud, 8bits, no parity, 1 stop bit, no flow control.
// Set to "Append line feeds to incoming line ends"
// Commands: ? - list current parameters
//           rl 40 - set row_len=40
//           nr 25 - set num_rows=25
//           fr 40 - set free_run=40 (this seems to be number of frames to average)
//           this will result in a frame rate of 625 Hz, or 1.6 ms per frame
// Syncbox forgets all settings when it is powered down!!!
#ifdef PIPER
    // PIPER - Row_len=40, num_rows=25, fr=40, T_UBC=1.6 ms (625 Hz), T_HKE = 1.0 s
    #define CONFIG_NAME "PIPER"

    #define UBC_CLK_DIVISOR (40*25*40)          // Number to divide UBC clock by to get ubc frame rate

    #define UBC_FRAMES_PER_HKE_FRAME   625      // Number of complete UBC frames that fit in one HKE frame
                                                // Used to be called "M"
    // ADC Sample timing constants
    #define CYCLES_PER_TICK           5000      // Number of clock cycles between samples

    // Communications constants
    #define MASTER_TICKS  40			// PMaster's talk window allocation
    #define NUM_ADDRESSES 20			// Max number of cards (not including PMaster)
    #define TICKS_PER_ADDRESS_SLOT 198	        // Individual card's talk window allocation
    #define TALK_TICK_MARGIN		4	// Interrupt ticks before end of talk slot to stop transmitting
						// to allow hardware transmit buffer to clear out before we
						// release the RS-485 bus
#endif

#ifdef BETTII
    // BETTI - Row_len=64, num_rows=24, fr=40, T_UBC=2.4576 ms (406.90 Hz)
    #define CONFIG_NAME "BETTII"

    #define UBC_CLK_DIVISOR (64*24*40)          // Number to divide UBC clock by to get ubc frame rate

    #define UBC_FRAMES_PER_HKE_FRAME   512      // Number of complete UBC frames that fit in one HKE frame
                                                // Also called "M"

    // ADC Sample timing constants
    #define CYCLES_PER_TICK          6144       // Number of clock cycles between samples

    // Communications constants
    #define NUM_ADDRESSES 20			// Max number of cards (not including PMaster)

    // TODO: THINK ABOUT THESE NUMBERS!!!!  change timings.py to use baud rate when calculating
    #define MASTER_TICKS  96			// PMaster's talk window allocation
    #define TICKS_PER_ADDRESS_SLOT 200	        // Individual card's talk window allocation
    #define TALK_TICK_MARGIN		4	// Interrupt ticks before end of talk slot to stop transmitting
						// to allow hardware transmit buffer to clear out before we
						// release the RS-485 bus
#endif

#define TICKS_PER_FRAME (MASTER_TICKS + NUM_ADDRESSES*TICKS_PER_ADDRESS_SLOT)

// Compute UBC Frame Length in nanoseconds (note must use integer arithmetic for preprocessor)
#define T_UBC_FRAME ((1000*1000*1000/UBC_CLK_FREQ) * UBC_CLK_DIVISOR)

// Compute HKE Frame Length in nanoseconds (note must use integer arithmetic for preprocessor)
#define T_HKE_TICK_NS ((1000*1000*1000/CLOCKFREQ) * CYCLES_PER_TICK)
#define T_HKE_FRAME (T_HKE_TICK_NS * TICKS_PER_FRAME)


// Check if our master time equation is satisfied.  Note cross multiplied to avoid floating point math that preprocessor can't do
#if T_UBC_FRAME * UBC_FRAMES_PER_HKE_FRAME != T_HKE_FRAME
    #error "TICKS_PER_FRAME is not consistent with UBC_FRAMES_PER_HKE_FRAME"
#endif


// Project-independent constants
// CONSTANTS for all boards
#define BAUD_RATE	115200
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)- 1 + 0.5)))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Add 0.5 to convert to int from float

#define RESET_DELAY_SECONDS     0.050

#define CYCLES_PER_ADC_CONVERT_PULSE    200     // ADC convert pulse duration, in clock cycles

// Frame communication constants
// NOTE: the constants below assume a 4 kHz interrupt rate
// If any board has a different rate, must override these!!!!
#define MASTER_ADDRESS ((unsigned char)0xFF)	// Master address (can be anything > NUM_ADDRESSES)

// Function prototypes

// bp_common_functions.c
char check_frame_clock(void);
void sync_to_frame_clock(void);
void update_communication(char isNewFrame);
void config_timer2(void);
void config_uart2(void);
void bp_print_version_info(char *version_string );     // prints the standard info for the VER command
char *bp_parse_cmd_string(char *cmd_string);
int address_compare(char *next_tok, int card_address);
void print_address(void);

#endif