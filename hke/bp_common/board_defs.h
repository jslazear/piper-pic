/* One of these macros should be defined for each project
 * They are set in the project file (.mcp), not in any source file (.c) or header file (.h)
 * MPLAB -> Project -> Build Options -> Project -> MPLAB C30 -> Preprocessor Macros
 * Alternate: open *.mcp file and edit
 */

#ifdef TREAD
	#include "../tread/tread.h"
#endif

#ifdef DSPID
	#include "../dspid/dspid.h"
#endif

#ifdef ANALOG_OUT
	#include "../analog_out/analog_out.h"
#endif

#ifdef ANALOG_IN
	#include "../analog_in/analog_in.h"
#endif

#ifdef PMASTER
	#include "../pmaster/pmaster.h"
#endif

#ifdef PMOTOR
	#include "../pmotor/pmotor.h"
#endif

#ifdef PIXIEBIAS
	#include "../pixiebias/pixiebias.h"
        #include "../pixiebias/version.h"
#endif

#ifdef PIXIEREADOUT
	#include "../pixiereadout/pixiereadout.h"
#endif
