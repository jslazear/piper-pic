/********************************************************
This file includes functions useful to boards that are "parasites" on the backplane.

Currently, functions are ***COPIED*** from the bp_common_functions.c.

Should change this so that all boards that include bp_common_functions.c also include this
*********************************************************/
#include <p30F5011.h>
#include <uart.h>

// Include the appropriate .h file depending on the board / project we're working on (needed for PIN_FRAME_CLK)
#include "../bp_common/board_defs.h"    // Relative path seems to be needed for MPLABX to find it properly


/*
 * Inits the UART2 with the standard settings.  Copied from bp_common_functions.c
 * BRG = FCLK/(BaudRate*16) - 1
 * BRG = 0 gives 1.25 MBAUD, this is a non-standard rate, but works well with the new FTDI USB Fiber Serial Board
 */
void init_uart2(unsigned brg)
{
    unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
    unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
    OpenUART2(U2MODEvalue, U2STAvalue, brg);

    ConfigIntUART2(UART_RX_INT_DIS & UART_TX_INT_DIS); // Disable TX and RX interrups
}


/*
 * Wait for a rising edge on the external FRAME clock
 * Frame clock ticks at roughly 1 Hz, so at most we wait a couple of seconds in this routine
 *
 * This function is only called in Init_Pic() on board power up.
 *
 * This function is called in all board programs, e.g. TRead, DSPID, etc., so all the boards
 * are synched to the FRAME clock. This function is necessary because all the boards share
 * the same communications line and they each must know when it is their turn to talk.
 */
void sync_to_frame_clock(void)
{
	int prev = PIN_FRAME_CLK;
	while( 1 ) {
		// Blink the LED rapidly to indicate to user we're framing
		PIN_LED = 1;
		int now = PIN_FRAME_CLK;
		PIN_LED = 0;

		if( prev==0 && now == 1 )  {
			PIN_LED = 0;
			return;
		}
		prev = now;
 	}
}
