/*
 * This file is for functions that are specific to the HKE backplane boards.
 */
#include <string.h>
#include <timer.h>
#include <uart.h>
#include <outcompare.h>

#include "../common/tty_print.h"    // for print_address() function

// Include the appropriate .h file depending on the board / project we're working on (needed for PIN_FRAME_CLK)
// Note: board_defs.h will include <board>.h (board = tread.h, dspid.h...)
// which will in turn include a bunch of other .h files, including bp_common_functions.h
// This all seems kind of convoluted...
#include "../bp_common/board_defs.h"    // Relative path seems to be needed for MPLABX to find it properly


/** Configures Timer2 and OC2  (call once from init_pic)
 * Configures Timer2 period.
 * Configures OC2 to generate pules on timer2 reset.
 * Turns on OC2 interrupt.
 */
void config_timer2(void)
{
    // Setup timer 2 and set its period to
    unsigned period = CYCLES_PER_TICK - 1;  // Note, PIC timer counts from 0 to period, inclusive, so need to sub 1 to get desired sampling / tick rate
    OpenTimer2(T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, period);

    // Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
    OpenOC2(OC_IDLE_CON & OC_TIMER2_SRC & OC_CONTINUE_PULSE, CYCLES_PER_ADC_CONVERT_PULSE, 0);

    TMR2 = (3*CYCLES_PER_TICK/4);   // Clear the timer.  This value sets the phase of the OC2 interrupts with respect to the frame_clk
                              // By setting to (3/4)*cycles_per_tick we wait 1/4 period before our first OC2 interrupt.
                              // This gives some time before the first interrupt, and extra time to the last tick of the frame
                              // The last tick often has to write a bunch of data (esp. DSPID), so needs some extra time

    // Finally, turn on interrupts!
    ConfigIntOC2(OC_INT_ON & OC_INT_PRIOR_2);
}

/*
 * Configures UART2 with the standard settings for the HKE backplane boards (call once from init_pic)
 */
void config_uart2(void)
{
    unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
    unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
    OpenUART2(U2MODEvalue, U2STAvalue, BRG_SETTING);

    ConfigIntUART2(UART_RX_INT_DIS & UART_TX_INT_DIS); // Disable TX and RX interrupts
}

/*
 * Prints our card hex address like so
 * #3:\r\n
 */
void print_address(void)
{
	extern unsigned char CardAddress;

	// All responses must begin with #
	TTYPutc( '#' );
    TTYPrintHex8( CardAddress );            // from tty_print.c
	TTYPuts( ":\r\n" );
}

/** Gets the command (assuming cmd_parse) has been called.
 *  Returns the command name (the first token) *if the command is for this board*
 *  Returns NULL if the command is for some other board.
 */
char *bp_parse_cmd_string(char *cmd_string)
{
    extern unsigned char CardAddress;
    char *s;

    // Parse the command string
    cmd_parse(cmd_string);

  	// parse the CARD HEX ADDRESS
	// Check if command is for us, if not return and do nothing
    s = tok_get_str();      // Get the next token as a string
    if( !address_compare(s, CardAddress) )
        return NULL;            // Skip commands not addressed to this board
    tok_decrement_num_args(1);  // Don't count the address as an argument to our command (for parse_channel_list)

	// Print out our board address to identify who response is from
	print_address();

	s = tok_get_str();	// next token is the command name
    return s;
}

/*
 * Wait for a rising edge on the external FRAME clock
 * Frame clock ticks at roughly 1 Hz, so at most we wait a couple of seconds in this routine
 *
 * This function is only called in Init_Pic() on board power up.
 *
 * This function is called in all board programs, e.g. TRead, DSPID, etc., so all the boards
 * are synched to the FRAME clock. This function is necessary because all the boards share
 * the same communications line and they each must know when it is their turn to talk.
 */
void sync_to_frame_clock(void)
{
	int prev = PIN_FRAME_CLK;
	while( 1 ) {
		// Blink the LED rapidly to indicate to user we're framing
		PIN_LED = 1;
		int now = PIN_FRAME_CLK;
		PIN_LED = 0;

		if( prev==0 && now == 1 )  {
			PIN_LED = 0;
			return;
		}
		prev = now;
 	}
}


/* 
 * Reads the frame clock and looks for a rising edge.
 * Returns 1 if there has been a rising edge since the last call to this routine.
 */
char check_frame_clock()
{
    static char prev = 1;   // Init to 1 so that the first call to this routine won't give a false positive
                            // if we happen to call it first during the 50% of the time that frame_clk is high
    char now = PIN_FRAME_CLK; // Read the frame clock
    char isNewFrame = 0;    // Assume it's not a new frame

    if( prev == 0 && now == 1)
        isNewFrame = 1;
    prev = now;
    return isNewFrame;
}

/*
 * Update counters to keep track of when it is our turn to transmit over the serial port.
 * Send data out over com port if it is time.
 *
 * This routine needs to be called at every 4 kHZ timer interrupt for all 'backplane' boards.
 *
 * Returns 1 if we are on the first tick of a new frame.  Use this to reset all
 * 'state' variables if desired.
 */
void update_communication( char isNewFrame )
{
    extern unsigned char CardAddress; // Get the card address for this card (Global Variable!)

    static unsigned char curAddress = MASTER_ADDRESS;
    static int talk_tick = 0;         // The 4 kHz timer tick offset within the current address slot
                                      // increments from 0..197 for all regular cards (0..39 for PMaster)

    // Reset curAddress and tick on external frame clock tick
    // Do this first, to initialize to a known state before doing checks below
    if (isNewFrame) {
        curAddress = MASTER_ADDRESS; // Reset us back to the begining of the slot (FF)
        talk_tick = 0;
    }

    // Figure out how big the current slot is (The master card gets fewer ticks)
    // talk_tick will range from [0 to max_tick] inclusive
    int max_tick = TICKS_PER_ADDRESS_SLOT - 1;
    if (curAddress == MASTER_ADDRESS) // See if we're the master card
        max_tick = MASTER_TICKS - 1;

    // If it's our slot, do some talking
    if (curAddress == CardAddress) {
        // Initiate data packet transfer on 1st tick of our talk slot
        if (talk_tick == 0)
            PIN_485_ENABLE = 1; // Enable the RS-485 driver (so we can transmit over UART)

        // On most ticks in our slot, except for a few near the end, transmit characters out the uart
        // If talk_tick_margin is 0, we talk during all our ticks.  If 1, we don't talk during last tick.
        if (talk_tick <= max_tick - TALK_TICK_MARGIN)
            TTYUpdateTX(); // Send data to the hardware TX FIFO

        // On last tick in our talk slot
        if (talk_tick == max_tick)
            PIN_485_ENABLE = 0; // Disable the RS-485 driver (our slot is over)
    }

    // After talking, check if it is time to advance internal counters to next slot
    // See if it is time to go to the next address slot
    if (talk_tick >= max_tick) {
        talk_tick = 0;
     
        // Increment which card is talking.
        if (curAddress == MASTER_ADDRESS)
            curAddress = 0;
        else
            curAddress++;
    }
    else {
        // Update counter to keep track of which card should be talking
        talk_tick++;
    }
}

void bp_print_version_info( char *version_string )
{
    // Prints the string from version.h which includes the compile date and time
    TTYPuts(version_string);

    // Print the name of the configuration that we're compiled for
    TTYPuts("Configuration: ");
    TTYPuts(CONFIG_NAME);
    TTYPuts("\r\n");
}

// Accepts 1 or 2 hex digit address.  i.e. "A", "a", "0", "00", "1F"...
// Returns a number larger than 8 bits if the address is invalid
// Returns: 1 if the next token matches our card address
int address_compare(char *next_tok, int card_address)
{
    int n = strlen(next_tok);

    unsigned char msn = 0, lsn = 0;

    if (n == 1) { // Single character address (the usual)
        lsn = parse_hex_digit(next_tok[0]);
    }
    else if (n == 2) { // 2 character address
        msn = parse_hex_digit(next_tok[0]);
        lsn = parse_hex_digit(next_tok[1]);
    }
    else if (n == 3) {  // 3 characters
        if (!strcmp(next_tok, "all"))
            return 1;   // All matches all board addresses
        return 0;       // Invalid address does not match our address
    }
    else
        return 0;       // Invalid address does not match our address

    // Verify that both digits are valid (parse_hex_digit returns -1 for invalid)
    if (lsn <= 0xF && msn <= 0xF) {
        unsigned char addr = (msn << 4) + lsn; // Compute the address

        return addr == card_address;        // Returns 1 if addres match, 0 otherwise
    }
    else
        return 0; // Invalid address does not match our address
}
