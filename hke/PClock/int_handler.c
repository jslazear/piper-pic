#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>

#include "pclock.h"

// frame count global variable
static volatile unsigned long frame_count;



/*----------------------------- Frame Count Timer Interrupt-------------------------*/

static void spiWrite(unsigned data)
{
//	SPI2STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
	while( SPI2STATbits.SPITBF );	// Wait for TX buffer to be empty
	SPI2BUF = data;					// Write to the SPI port
}


/* Timer 2 Interrupt Handler.  Called at a rate of 1 kHz
 */
void __attribute__((__interrupt__, __auto_psv__)) _T2Interrupt( void )
{
	IFS0bits.T2IF = 0;		// Clear the interrupt flag
	PIN_OSCOPE1 = 1;

	frame_count++;

	unsigned data;

	// Word1 - Start of frame indication
	data = 0xFF00 | SYNCBOX_STATUS_BYTE;
	spiWrite(data);

	// Word2 - Most significant 16 bits of frame count
	data = (unsigned)(frame_count >> 16);
	spiWrite(data);

	// Word3 - Least significant 16 bits of frame count
	data = (unsigned)frame_count;
	spiWrite(data);

	// Word4 - 0xFFFF - just leaves SDO high
	spiWrite(0xFFFF);

	PIN_OSCOPE1 = 0;
}

