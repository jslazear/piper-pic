#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>

#include "pclock.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL8);          /* External 5 MHz clock with 8xPLL oscillator --> 10 MHz instruction cycle, Failsafe clock off */
_FWDT(WDT_OFF);                         /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN);           /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF);                    /* Code protect disabled */

void init_pic()
{
	// Set up values for GPIO pins
	PIN_LED = 1;		// Turn on LED1

	// Set the direction for GPIO
	TRISBbits.TRISB10 = 0;	// Set frame ack testpoint pin as an output
	TRISCbits.TRISC1 = 0;	// Set LED as an output
	TRISDbits.TRISD4 = 0;	// Oscope 1
	TRISGbits.TRISG15 = 0;	// Oscope 2

	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;	

	// Oscope debug pins, set them to outputs
	TRISDbits.TRISD4 = 0;
	TRISGbits.TRISG15 = 0;
	PIN_OSCOPE1 = 0;
	PIN_OSCOPE2 = 0;

	// Short delay to wait for power supply voltages to settle
 	__delay32( (long)(0.01 * CLOCKFREQ) );

    // Setup the UART - 56 kbs
	unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
	unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
	OpenUART2( U2MODEvalue, U2STAvalue, BRG_SETTING );
	ConfigIntUART2( UART_RX_INT_DIS & UART_TX_INT_DIS );	// Disable TX and RX interrups

	// Setup timer 2 and set its period to 200 Hz
	// timer freq = 10MHz / pre-scale / period
	OpenTimer2( T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, 50000U);
	EnableIntT2;

	// Setup the SPI port2
	OpenSPI2( FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON & 
				MASTER_ENABLE_OFF & SLAVE_ENABLE_OFF & FRAME_ENABLE_OFF &
				PRI_PRESCAL_1_1 & SEC_PRESCAL_1_1,
				SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR );
	ConfigIntSPI2( SPI_INT_DIS );	// NO SPI interrupts

	// Set the clock/data phasing on the SPI port (see Fig. 20-3 in family reference)
	SPI2CONbits.CKE = 0;	// Must set low for slave mode without using SS pin (Fam. Ref 20-10)
	SPI2CONbits.CKP = 1;	// Very important, CKP=1 sets clk/data phasing to UBC standard
	SPI2CONbits.SMP = 0;	// Must be 0 for slave mode

	PIN_LED = 0;		// Default to LED off for power saving
}


int main( void )
{
	init_pic(); 	// Setup the PIC internal peripherals

	while( 1 ) {
		// Do nothing here - int handler is generating clock pulses
		// Could respond to serial commands here, changing frequency, etc...
	}
}

// Not currently used
void OnIdle( void )
{
}
